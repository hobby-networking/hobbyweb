<?php
public function handle($invitation)
{
    \Mail::queue('invitations.emailBody', $invitation, function ($m) use ($invitation) {
            $m->from('info@hobbynetworking.com', 'HobbyNetworking');
            $m->to($invitation->email);
            $m->subject("You have been invited by ". $invitation->user->name);
        });
}
