<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Models\Hobby;
use App\Models\HobbyCategory;
use App\Models\HobbyAlias;
use App\Models\UserHobbies;
use App\Models\UserPost;
use Response;
use Session;
use Image;
use Input;
use Redirect;
use URL;
use App\Models\Settings;
use Analytics;

class DashboardController extends Controller
{ 
    private $post_limit = 6;
    private $chooseHobbyPageHobbyLimit = 9;

	public function chooseHobbyPage($nextInvite=false){
		$hobbies = Hobby::take($this->chooseHobbyPageHobbyLimit)->get();
        $user_hobbies = Auth::user()->hobbies('hobby_id')->get()->toArray();
		//dd($user_hobbies);
    	return view('frontend.dashboard.choosehobby')->with('hobbies',$hobbies)->with('user_hobbies',$user_hobbies)->with('nextInvite',$nextInvite);
    }

    public function checkHobbyAvailability(){

        $name = $_REQUEST['name'];
        $check_hobby = Hobby::where('name', 'LIKE', $name.'%')->first();
        if(count($check_hobby)>0){

            echo 'matched';
        }else{

            echo 'ok';

        }
    }
    public function addHobbyPage(){
		$hobbycat = HobbyCategory::all();
		$loggedinUser = Auth::user();
        $hobby_alias = HobbyAlias::all();
		return view('frontend.dashboard.addhobby')->with('hobbycat',$hobbycat)->with('loggedinUser',$loggedinUser)->with('hobby_alias',$hobby_alias);

    }

    public function getProfilePage($slug) {
        $user = null;
        $userPost = array();
        $settings = Settings::where('username','=',$slug)->first();
        if(!empty($settings)){
            $user = $settings->user;
            $title = "User Profile Visit: ".$user->name." [user_id: ".$user->id."]";
            Analytics::trackPage($slug, $title);
            $userPost = UserPost::where("user_id","=",$user->id)->where('type','=','user')->orderBy('id', 'DESC')->take($this->post_limit)->get();
            //dd($userPost);
        }else{
            return redirect()->back()->withErrors(['Sorry user not found']);
        }
       return view('frontend.dashboard.new_userprofile')->with('user',$user)->with('userPost',$userPost);
    }
    
     public function addHobbyAjaxPost(Request $request){
        //dd($_FILES);
        $hobby_name = $request->hobby_name;
        $hobby_cat = $request->hobby_cat;
        $hobby_slug = str_slug($hobby_name);

        if(empty(Hobby::where('name','=', $hobby_name)->first())){
            $related_topics = explode(',', $request->related_topics);
            //dd($related_topics);
            $image = $request->hobby_image;
            $hobby = new Hobby;
            
           $hobbyCategory = HobbyCategory::where('name','=', $request->hobby_cat)->first();
            
           if(empty($hobbyCategory)){
            $hobbyCategory =  new HobbyCategory;
            $hobbyCategory->name = $request->hobby_cat;
            $hobbyCategory->save();
           } 
            //dd($hobbyCategory);
            $hobby->name = $request->hobby_name;
            $hobby->cat_id = $hobbyCategory->id;
            $hobby->hobby_slug = $hobby_slug;
           
            if($request->hasFile('file')){
                $hobby->profile_pic = $request->file;
                //dd($request->file);
            }
            //$hobby->profile_pic = Session::get('hobby_image_name');
            Session::forget('hobby_image_name');
            if($hobby->save()) {
                Analytics::trackEvent("hobby_created", "hobby: ".$hobby->id."_created_by_user: ".Auth::user()->id, "New hobby ".$hobby->name." added by User: ".Auth::user()->id);
                foreach($related_topics as $hobbyalias) {
                    $hobbyAlias = new HobbyAlias;
                    $hobbyAlias->hobby_id = $hobby->id;
                    $hobbyAlias->alias_name = $hobbyalias;
                    $hobbyAlias->is_active = 'yes';
                    $hobbyAlias->save();
                }
                $data['success'] = true;
            }else{
                $data['success'] = false;
            }
            $userHobby = new UserHobbies;
            $userHobby->hobby_id = $hobby->id;
            $userHobby->user_id = Auth::user()->id;
            $userHobby->is_active = "yes";
            $userHobby->save();
        }else{
            $data['success'] = false;
            $data['msg'] = "Hobby Already Exist";
        }
        
        return Response::json($data);
    }
    public function postHobbyPhotograph(Request $request){

        if($request->file())
        {
            $file = $request->file();
            $image_name = Hobby::setProfilePic($file);
            Session::set('hobby_image_name', $image_name);
        }else{
            return Redirect::back()->withErrors(['Please Select a Image File to Upload!!']);
        }
    }

    public function postAddRemoveHobbyAjax(Request $request){
        $isSelected = false;
        if(!empty(UserHobbies::where('user_id','=',Auth::user()->id)->where('hobby_id','=',$request->hobby_id)->first())) {
            UserHobbies::where('user_id','=',Auth::user()->id)->where('hobby_id','=',$request->hobby_id)->first()->delete();
            Analytics::trackEvent("hobby_removed", "hobby: ".$request->hobby_id."_created_by_user: ".Auth::user()->id, "Hobby: ".$request->hobby_id." removed by User: ".Auth::user()->id);
        }else{
            $userHobby = new UserHobbies;
            $userHobby->hobby_id = $request->hobby_id;
            $userHobby->user_id = Auth::user()->id;
            $userHobby->is_active = "yes";
            $userHobby->save();
            $isSelected = true;
            Analytics::trackEvent("hobby_added", "hobby: ".$request->hobby_id."_created_by_user: ".Auth::user()->id, "Hobby: ".$request->hobby_id." added by User: ".Auth::user()->id);
        }

        return Response::json(['is_selected'=>$isSelected]);
    }

    public function postChooseHobbyAjaxPost(Request $request){

        
        // $hobby_ids = explode(",",$request->hobby_ids);
        
        // UserHobbies::where('user_id','=',Auth::user()->id)->delete();
        // $user_hobby = Auth::user()->hobbies;
        // //dd($user_hobby);
        // foreach ($hobby_ids as $key => $eachId) {
           
        //    $userHobby = new UserHobbies;
        //    $userHobby->hobby_id = $eachId;
        //    $userHobby->user_id = Auth::user()->id;
        //    $userHobby->is_active = "yes";
        //    $userHobby->save();

        // }

        echo 'success';exit;

    }
    public function searchHobbyName(Request $request) {
        $searchhobby = $request->hobby_search;

        $hobbylist = Hobby::where('name','LIKE','%'.$searchhobby.'%')->get();
        //dd($hobbylist);
        $user_hobbies_added = Auth::user()->hobbies('hobby_id')->get()->toArray();

        $result = array();
        $html = "";
   
        foreach ($hobbylist as $key=>$eachHobby) {
            $html .= "<div class='col-lg-3 col-md-3 col-sm-6 col-xs-12 pro_div' onclick='selectHobbyCat(this)'>";
                $html .="<div ";
                if(!in_array($eachHobby->id,$user_hobbies_added)){
                    $html .= "style='display:none;'";
                }
                $html .=    " class='chbox'>".
                            "<img src='".URL::asset('frontend/dashboard/assets/img/tick.png')."'>".
                        "</div>".
                        "<div class='pro_block'>";
                        if(in_array($eachHobby->id,$user_hobbies_added)){

                            $html .= "<input checked='checked' value='".$eachHobby->id."' type='checkbox' id='".$eachHobby->id."' name='hobby_".$key."' class='hide'>";

                        }else{

                            $html .= "<input value='' type='checkbox' id='".$eachHobby->id."' name='hobby_".$key."' class='hide'>";
                        }
                    $html .= "<img src='".$eachHobby->profile_pic."' class='select_img img-responsive'>".
                            "<h5>".$eachHobby->name."</h5>".
                            "<p>".$eachHobby->category->name."</p>".
                        "</div>".
                     "</div>";
        }
        $result['html']=$html;
        $result['success']=true;
        return response()->json($result);
    }


    public function scrollLimit(Request $request){
        $page_no = $request->page_id;
        $pageSize = $this->chooseHobbyPageHobbyLimit;
        $hobbylist = Hobby::skip($page_no * $pageSize)->take($pageSize)->get();
        $result = array();
        $user_hobbies = Auth::user()->hobbies('hobby_id')->get()->toArray();
        $html = "";
        foreach ($hobbylist as $key=>$eachHobby) {
            $html .= "<div class='col-lg-3 col-md-3 col-sm-6 col-xs-12 pro_div' onclick='selectHobby(this)'>";
                        if(!in_array($eachHobby->id,$user_hobbies)){
                            $html .= "<div class='chbox' style='display:none;'>";
                        }else{
                            $html .= "<div class='chbox'>";
                        }
                        $html .="<img src='".URL::asset('frontend/dashboard/assets/img/tick.png')."'>".
                        "</div>".
                        "<div class='pro_block'>";
                            if(in_array($eachHobby->id,$user_hobbies)){
                                $html .="<input value='' checked='checked' type='checkbox' id='".$eachHobby->id."' name='hobby_".$key."' class='hide'>";
                            }else{
                                $html .="<input value='' type='checkbox' id='".$eachHobby->id."' name='hobby_".$key."' class='hide'>";
                            }
                            
                        $html .="<img src='".$eachHobby->profile_pic."' class='select_img img-responsive'>".
                            "<h5>".$eachHobby->name."</h5>".
                            "<p>".$eachHobby->category->name."</p>".
                        "</div>".
                     "</div>";
        }
        $result['html']=$html;
        $result['success']=true;
        return response()->json($result);
    }
    
    public function postHobbyNameChecker(Request $request) {

        $hobby= new Hobby;  
        $hobby_name = $request->get('name');  
      

        $query = DB::table('hobbies')->select('name')
                 ->where('name','!=',$name)
                 ->get();
            if($query)
            {
                return"Username not available!";
            }
            return "Username available!";
       
            $product->save();


    }
   
    public function inviteFriends($withbtn=false){

        return view('frontend.dashboard.invite')->with('withbtn',$withbtn);
    }


 }   
