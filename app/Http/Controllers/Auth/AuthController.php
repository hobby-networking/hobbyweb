<?php

namespace App\Http\Controllers\Auth;

Use Auth;
use App\Models\User;
use App\Models\Settings;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\RegistersUsers;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\SignupRequest;
use Response;
use Session;
use Mail;
use URL;
use App\Models\UserVerification;
use App\Models\SocialAccount;
use Illuminate\Http\Request;
use Analytics;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $loginPath = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth, User $user)
    {
        $this->user = $user; 
        $this->auth = $auth;
        $this->middleware('guest', ['except' => array('getLogout','apiLogin')]);
    }

    public function getLogout() {
        $this->auth->logout();
        Analytics::unsetUserId();
        return redirect('/');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  LoginRequest  $request
     * @return Response
     */
    public function postLogin(LoginRequest $request) {
        if ($this->auth->attempt($request->only('email', 'password'))) {
            $request->flashExcept('password');
            $user = Auth::user();
            Analytics::setUserId(md5(Auth::user()->id));
            if(!empty($user->userSettings)){
                $settings = $user->userSettings;
                if(empty($settings->username)){
                    $settings->username = Settings::generateusername($user);
                }
                $settings->save();
            }else{
                $settings = new Settings;
                $settings->user_id = $user->id;
                $settings->username = Settings::generateusername($user);
                $settings->save();
            }
            return redirect($this->userRedirectUrl());
        }
 
        return redirect('/')->withErrors([
            'email' => 'The credentials you entered did not match our records. Try again.',
        ]);
    }

    public function postFblogin(Request $request) {
        $response = array();
        if (empty($request->email)) {
            $response['redirectTo'] = URL::to('/');
            $response['errors'] = 'Can\'t login as no email id linked with your facebook account, please click on bellow signup button to register.';
            $response['login_success'] = false;
        }else{
            if (SocialAccount::where('provider_user_id', '=', $request->id)->count() > 0) {
                $socialAccount = SocialAccount::where('provider_user_id', '=', $request->id)->first();
                $user = User::find($socialAccount->user_id);
                Auth::login($user);
            } else if (User::where('email', '=', $request->email)->count() > 0) {
                $user = User::where('email', '=', $request->email)->first();
                $newSocialAccount = new SocialAccount;
                $newSocialAccount->user_id = $user->id; $newSocialAccount->provider_user_id = $request->id; $newSocialAccount->provider = "facebook"; 
                $newSocialAccount->save();
                Auth::login($user);
            } else {
                $newPassword = substr($request->id, 10);
                $user = new User;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->password = bcrypt($newPassword);
                $user->save();
                $newSocialAccount = new SocialAccount;
                $newSocialAccount->user_id = $user->id; $newSocialAccount->provider_user_id = $request->id; $newSocialAccount->provider = "facebook"; 
                $newSocialAccount->save();

                Auth::login($user);
                $success_msg[] = "Welcome ".$user->name;
                Session::flash('success_msg', $success_msg);
                $userVerification = null;
                if (count(UserVerification::where('user_id','=',$user->id)->where('type','=','email')->get())>0) {
                    $userVerification = UserVerification::where('user_id','=',$user->id)->where('type','=','email')->first();
                    $userVerification->confirmation_code = str_random(30);
                    $userVerification->save();
                }else{
                    $userVerification = new UserVerification;
                    $userVerification->user_id = $user->id;
                    $userVerification->confirmation_code = str_random(30);
                    $userVerification->type = 'email';
                    $userVerification->save();
                }
                try{ //Try to fire welcome mail
                    Mail::send('emails.signup', array('confirmation_code'=>$userVerification->confirmation_code,'name'=>$user->name, 'email'=>$user->email, 'password'=>$newPassword), function($message) use($user) {
                        $message->to($user->email, $user->name)
                                ->subject('HobbyNetworking :: Your account has been created successfully!');
                    });
                }catch (Exception $e) {}
            }
            $user = Auth::user();
            //dd($user);
            if (!empty($user)) {
                Analytics::setUserId(md5(Auth::user()->id));
                if(!empty($user->userSettings)){
                    $settings = $user->userSettings;
                    if(empty($settings->username)){
                        $settings->username = Settings::generateusername($user);
                    }
                    $settings->save();
                }else{
                    $settings = new Settings;
                    $settings->user_id = $user->id;
                    $settings->username = Settings::generateusername($user);
                    $settings->save();
                }
                $response['redirectTo'] = URL::to('/').$this->userRedirectUrl();
                $response['errors'] = '';
                $response['login_success'] = true;
            }else{
                $response['redirectTo'] = URL::to('/');
                $response['errors'] = 'Having problem to get data from facebook. Please Try again.';
                $response['login_success'] = false;
            }
        }
        return Response::json($response, 200);
    }

    public function postSignup(SignupRequest $request) {
        if (User::where('email', '=', $request->email)->count() > 0) {
            return redirect('/')->withErrors([
                'email' => 'User With this email id already exist.',
            ])->withInput();
        }
        if (User::where('phone', '=', $request->phone)->count() > 0) {
            return redirect('/')->withErrors([
                'email' => 'User With this Phone number already exist.',
            ])->withInput();
        }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);

        if($user->save()){

            if(!empty($user->userSettings)){
                $settings = $user->userSettings;
                if(empty($settings->username)){
                    $settings->username = Settings::generateusername($user);
                }
                $settings->save();
            }else{
                $settings = new Settings;
                $settings->user_id = $user->id;
                $settings->username = Settings::generateusername($user);
                $settings->save();
            }

            Auth::login($user);
            Analytics::setUserId(md5(Auth::user()->id));
            $success_msg[] = "Welcome ".$user->name;
            Session::flash('success_msg', $success_msg);
            $userVerification = null;
            if (count(UserVerification::where('user_id','=',$user->id)->where('type','=','email')->get())>0) {
                $userVerification = UserVerification::where('user_id','=',$user->id)->where('type','=','email')->first();
                $userVerification->confirmation_code = str_random(30);
                $userVerification->save();
            }else{
                $userVerification = new UserVerification;
                $userVerification->user_id = $user->id;
                $userVerification->confirmation_code = str_random(30);
                $userVerification->type = 'email';
                $userVerification->save();
            }
            try{ //Try to fire welcome mail
                Mail::send('emails.signup', array('confirmation_code'=>$userVerification->confirmation_code,'name'=>$user->name, 'email'=>$user->email, 'password'=>$request->password), function($message) use($user) {
                    $message->to($user->email, $user->name)
                            ->subject('HobbyNetworking :: Your account has been created successfully!');
                });
            }catch (Exception $e) {}
            return redirect($this->userRedirectUrl());

        }else{
            return redirect('/')->withErrors([
                'Error in signup',
            ]);
        }
    }

    private function userRedirectUrl() {
        $result = ''; 
        if(Auth::check()){
            $user = Auth::user();
            $user_type = $user->type;
            switch ($user_type) {
                case 'admin':
                    $result  = route('admin_dashboard');
                    break;
                case 'user':
                    $result  = '/';
                    break;
                default:
                    $result  = '/';
                    break;
            }
        }else{
            $result  = '/';
        }
        return $result;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
