<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use Input;
use App\Http\Requests;
use App\Models\UserPost;
use App\Models\UserHobbies;
use App\Models\User;
use App\Models\Hobby;
use App\Models\Community;
use Analytics;

class PageController extends Controller
{
	private $post_limit = 6;
	public function getPostBoxPage(){

		return view('frontend.post_box');
	}
	
  public function getAboutUsPage() {
    return view('pages.about_us');
  }

  public function getTermsOfUsePage() {
    return view('pages.terms_of_use');
  }

  public function getPrivacyPolicyPage() {
    return view('pages.privacy_policy');
  }

  public function getFaqPage() {
    return view('pages.faq');
  }

  public function getContactUsPage() {
    return view('pages.contact_us');
  }
  public function getContactUsForm(){
      $data = \Input::all();
      $rules = array (
          'full_name' => 'required|min:5',
          'email' => 'required|email',
          'message' => 'required|min:5'
      );
      $validator = \Validator::make($data, $rules);
      if ($validator -> passes()){
        $mailDaya = array(
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'subject' => $data['subject'],
            'user_message' => $data['message'],
        );
         \Mail::send('emails.contact_us', $mailDaya, function($message) use ($data)
          {
              $message->from($data['email'] , $data['full_name']);              
              $message->to('help@hobbynetworking.com', 'Help')->subject('contact us form hobbynetworking');
          });
          return view('pages.contact_us')->with('message', 'Your message has been sent. Thank You!'); 
       }else{
          return Redirect::route('contact_us_page')->withErrors($validator)->withInput();
       }
  }

	public function getWelcomePage() {
    $page = 0;
    if(Input::has('page')){
      $page = Input::get('page');
    }
		if(Auth::check()){
			$user = Auth::user();
      $user = User::find($user->id);
      Analytics::trackEvent("feed_page_open", "feed_page_open_by_user: ".$user->id, "Open News Feed Page As Already Login User: ".$user->id);
			if($user->type == 'admin') {
				return Redirect::route('admin_dashboard');
			} else {
				if(count($user->hobbies) == 0){
					return Redirect::route('choosehobby_page',true);
				}
        if ($page!=0) {
          $html = '';
          foreach(UserPost::where("user_id","=",$user->id)->where('type','=','user')->orderBy('id', 'DESC')->skip($page*$this->post_limit)->take($this->post_limit)->get() as $eachUserPost) {
              $html .= \View::make( 'frontend.posts.each_post' )->with( 'eachPostUser', $eachUserPost );
          }
          $data['html'] = $html;
          return \Response::json($data);
        }
				$userPost = UserPost::where("user_id","=",$user->id)->where('type','=','user')->orderBy('id', 'DESC')->take($this->post_limit)->get();
				$listUser = $user->getConnectedUserByUser(null);
				$listCommunity = null;
        $limit = 3;
        $hobbies = $user->hobbiesmodel();
        $count = count($hobbies->get());
        $hobbyList['first_list'] = $hobbies->skip(0)->take($limit)->get();
        $hobbyList['second_list'] = $hobbies->skip($limit)->get();
				return view("frontend.index")->with("loggedinUser",$user)->with("userPost",$userPost)->with("listUser",$listUser)->with("listCommunity",$listCommunity)->with("show_post_box",true)->with("search_on",false)->with('selected_hoby','my_feed')->with('hobbyList',$hobbyList)->with('count',$count)->with('limit',$limit)->with('isMyHobby',false);
			}
		}else{
      $user = User::find(Input::get('user_id'));
      if ($page!=0) {
        $html = '';
        foreach(UserPost::where("user_id","=",$user->id)->where('type','=','user')->orderBy('id', 'DESC')->skip($page*$this->post_limit)->take($this->post_limit)->get() as $eachUserPost) {
            $html .= \View::make( 'frontend.posts.each_post' )->with( 'eachPostUser', $eachUserPost );
        }
        $data['html'] = $html;
        return \Response::json($data);
      }
      Analytics::trackEvent("landing_page_without_login", "landing_page_without_login", "Open Landing Page without Login.");
    }
    $welcomePageHobbies = Hobby::where('profile_pic','!=',null)->take(12)->get();
		return view('welcome_new')->with('welcomePageHobbies',$welcomePageHobbies);
	}

    public function getSignupPage() {
    	return view('signup');
    }

   public function getHobbyPageByName($name=null,$page = 0){
      $rawName = $name;
      $isMyHobby = false;
      if($name!=null){
        /*$hobbyNameArr = explode('_',$name);
        if(is_array($hobbyNameArr)){
          $name = implode(' ',$hobbyNameArr);
        }*/
        //$hobbyName = strtolower($name);
        $hobbyName = $name;
        //$hobbyNameArray = explode(' ', $hobbyName);
        //if(count($hobbyNameArray)>0)
          //$hobbyName = implode('_',$hobbyNameArray);
          //$hobbyDetails = Hobby::whereRaw('Lower(name) = ?', [trim($name)])->first();
          $hobbyDetails = Hobby::where('hobby_slug',$hobbyName)->first();
          if(!empty($hobbyDetails)){
             if ($page!=0) {
              $html = '';
              foreach($hobbyDetails->allUserPost($this->post_limit,$page*$this->post_limit) as $eachUserPost) {
                  $html .= \View::make( 'frontend.posts.each_post' )->with( 'eachPostUser', $eachUserPost );
              }
              $data['html'] = $html;
              return \Response::json($data);
              }
            if(Auth::check()){
              $user = Auth::user();
              $listUser = $user->getConnectedUserByUser($hobbyDetails);
              $limit = 3;
              $hobbies = $user->hobbiesmodel();
              //echo "<pre>";print_r($hobbies);die;
              $count = count($hobbies->get());
              $hobbyListRaw['first_list'] = $hobbies->skip(0)->take($limit)->get();
              $hobbyListRaw['second_list'] = $hobbies->skip($limit)->get();
              $hobbyList = $hobbyListRaw;
              $secondList = array();
              foreach($hobbyListRaw['second_list'] as $eachHobby){
                //array_push($secondList, $eachHobby->hobby->slug());
                array_push($secondList, $eachHobby->hobby->hobby_slug);
              }
              foreach($hobbyListRaw['first_list'] as $eachHobby){
                /*if($rawName == $eachHobby->hobby->slug()) {
                  $isMyHobby = true;
                }*/
                if($rawName == $eachHobby->hobby->hobby_slug) {
                  $isMyHobby = true;
                }
              }
              if(in_array($rawName, $secondList)){
                $isMyHobby = true;
                $hobbyList['first_list'] = $hobbyListRaw['first_list'];
                $firstElementOfFirstList = $hobbyList['first_list'][0];
                $hobbyList['first_list'][0] = UserHobbies::where('hobby_id','=',$hobbyDetails->id)->where('user_id','=',$user->id)->first();
                $hobbyList['second_list'] = collect();
                foreach($hobbyListRaw['second_list'] as $eachHobby){
                  if($eachHobby->id == $hobbyList['first_list'][0]->id){
                    $hobbyList['second_list']->push($firstElementOfFirstList);
                  }else{
                    $hobbyList['second_list']->push($eachHobby);
                  }
                }
              }
              //dd($hobbyDetails->allUserPost());
              Analytics::trackEvent($hobbyName."_feed_as_user", $hobbyName."_feed_as_user: ".$user->id, "Browsing ".$hobbyDetails->name." Feeds as Logged in User: ".$user->id.".");
              $userPost = $hobbyDetails->allUserPost($this->post_limit);
              return view("frontend.index")->with("loggedinUser",$user)->with("userPost",$userPost)->with("listUser",$listUser)->with("show_post_box",true)->with("search_on",true)->with('selected_hoby',$hobbyName)->with("hobby",$hobbyDetails)->with('hobbyList',$hobbyList)->with('count',$count)->with('limit',$limit)->with('isMyHobby',$isMyHobby);
            }else{
              $allPost = $hobbyDetails->allUserPost($this->post_limit);
                if ($page!=0) {
                  $html = '';
                  foreach($hobbyDetails->allUserPost($this->post_limit,$page*$this->post_limit) as $eachUserPost) {
                      $html .= \View::make( 'frontend.posts.each_post' )->with( 'eachPostUser', $eachUserPost );
                  }
                  $data['html'] = $html;
                  return \Response::json($data);
                }
              $listUser = array();
              Analytics::trackEvent($hobbyName."_feed_as_guest", $hobbyName."_feed_as_guest", "Browsing ".$hobbyDetails->name." Feeds as guest.");
     		  return view("frontend.index")->with("userPost",$allPost)->with("listUser",$listUser)->with("show_post_box",false)->with("search_on",true)->with('selected_hoby',$hobbyName)->with("hobby",$hobbyDetails)->with('isMyHobby',false);
            }
          }else{
            abort(404 , 'Not Found' , []);
          }
      }else{
        abort(404);
      }  
    }

   	public function getSearchPage($cat=null,$slug=null) {
   		//dd(Input::get('slug'));
      $pageTitle = "Search :: ";
   		if(!empty(Input::get('slug'))){
   			$slug = Input::get('slug');
   		}
   		if(!empty(Input::get('cat'))){
   			$cat = Input::get('cat');
   		}
   		$result = array();
   		switch($cat){
   			case "hobby":
          $pageTitle .= "Hobby ::";
   				$result = $this->hobbySearch($slug);
   				break;
   			case "community":
          $pageTitle .= "Community ::";
   				$result = $this->communitySearch($slug);
   				break;
   			case "friends":
          $pageTitle .= "Friends ::";
   				$result = $this->friendsSearch($slug);
   				break;
   			default:
          $pageTitle .= "Hobby / Community / Friends ::";
   				$result = $this->allSearch($slug);
   		}
   		return view('frontend.search')->with('result',$result)->with('cat',$cat)->with('slug',$slug)->with('pageTitle', $pageTitle);
   	}

   	private function hobbySearch($slug='') {
   		$result = array();
   		foreach(Hobby::where('name','LIKE','%'.$slug.'%')->where('is_active','=','yes')->get() as $eachHobby){
   			$result[] = array('element'=>$eachHobby, 'cat'=>'hobby');
   		}
   		return $result;
   	}

   	private function communitySearch($slug='') {
   		$result = array();
   		foreach(Community::where('name','LIKE','%'.$slug.'%')->where('status','=','yes')->get() as $eachCommunity){
        if($eachCommunity->type == "secret") {
          if (Auth::check() && ($eachCommunity->is_member=="yes" || $eachCommunity->created_by == Auth::user()->id)){  
            $result[] = array('element'=>$eachCommunity, 'cat'=>'community');
          }
        }else{
   			  $result[] = array('element'=>$eachCommunity, 'cat'=>'community');
        }
   		}
   		return $result;
   	}

   	private function friendsSearch($slug='') {
   		$result = array();
   		foreach(User::where('name','LIKE','%'.$slug.'%')->where('is_active','=','yes')->get() as $eachUser){
   			$result[] = array('element'=>$eachUser, 'cat'=>'friends');
   		}
   		return $result;
   	}

   	private function allSearch($slug='') {
   		$result = array();
      $result = array_merge($result,$this->hobbySearch($slug));
      $result = array_merge($result,$this->communitySearch($slug));
      $result = array_merge($result,$this->friendsSearch($slug));
      //dd($result);
   		return $result;
   	}

    //18.02.17
    public function getExplainOurVideoPage() {
      return view('pages.explain_our_video');
    }

     public function getHNVideoPage() {
      return view('pages.hn_video');
    }
}
