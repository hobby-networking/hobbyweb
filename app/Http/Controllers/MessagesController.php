<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use Redirect;
use Response;
use App\Helpers\Settings;

class MessagesController extends Controller
{
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {
        $currentUserId = Auth::user()->id;

        // All threads, ignore deleted/archived participants
        $threads = Thread::getAllLatest()->get();

        // All threads that user is participating in
        // $threads = Thread::forUser($currentUserId)->latest('updated_at')->get();

        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages($currentUserId)->latest('updated_at')->get();

        return view('messenger.index', compact('threads', 'currentUserId'));
    }

    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect('messages');
        }

        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();

        // don't show the current user in list
        $userId = Auth::user()->id;
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();

        $thread->markAsRead($userId);

        return view('messenger.show', compact('thread', 'users'));
    }

    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {
        $users = User::where('id', '!=', Auth::id())->get();

        return view('messenger.create', compact('users'));
    }

    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store()
    {
        $input = Input::all();
        $thread = Thread::create(
            [
                'subject' => $input['subject'],
            ]
        );
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'body'      => $input['message'],
            ]
        );
        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'last_read' => new Carbon,
            ]
        );
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipants($input['recipients']);
        }
        return redirect('messages');
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect('messages');
        }

        $thread->activateAllParticipants();

        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::id(),
                'body'      => Input::get('message'),
            ]
        );

        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipants(Input::get('recipients'));
        }

        return redirect('messages/' . $id);
    }
    private function messageCollection(Thread $thread, $senderId) {
        $messages = "";
        $thread->markAsRead(Auth::user()->id);
        foreach($thread->messages as $message){
            if($message->user->id == $senderId){
                $messages .= '<div class="row msg_container base_sent">'.
                '<div class="col-md-10 col-xs-10">'.
                    '<div class="messages msg_sent">'.
                        '<p>'.Settings::convert($message->body).'</p>'.
                        '<time datetime="'.Carbon::parse($message->created_at)->format('Y-m-dTH:i').'">'.$message->created_at->diffForHumans().'</time>'.
                    '</div>'.
                '</div>'.
                '<div class="col-md-2 col-xs-2 avatar">'.
                    '<img src="'.$message->user->profile_pic.'" class=" img-responsive " title="'.$message->user->name.'">'.
                '</div>'.
                '</div>';
            }else{
                $messages .= '<div class="row msg_container base_receive">'.
                '<div class="col-md-2 col-xs-2 avatar">'.
                    '<img src="'.$message->user->profile_pic.'" class=" img-responsive " title="'.$message->user->name.'">'.
                '</div>'.
                '<div class="col-md-10 col-xs-10">'.
                    '<div class="messages base_receive">'.
                        '<p>'.Settings::convert($message->body).'</p>'.
                        '<time datetime="'.Carbon::parse($message->created_at)->format('Y-m-dTH:i').'">'.$message->created_at->diffForHumans().'</time>'.
                    '</div>'.
                '</div>'.
                '</div>';
            }
        }
        return $messages;
    }
    private function detectThread($senderId, $receiverId) {
        $threadSubject = $senderId.'-'.$receiverId;
        $threadSubjectReverse = $receiverId.'-'.$senderId;
        $thread = null;
        if(empty(Thread::where('subject','=',$threadSubject)->first())) {
            if(empty(Thread::where('subject','=',$threadSubjectReverse)->first())) {
                $thread = Thread::create(
                    [
                        'subject' => $threadSubject,
                    ]
                );
                $thread->addParticipants([$senderId,$receiverId]);
            }else{
                $thread = Thread::where('subject','=',$threadSubjectReverse)->first();
            }
        }else{
            $thread = Thread::where('subject','=',$threadSubject)->first();
        }
        if(!$thread->hasParticipant($senderId)){
            $thread->addParticipant($senderId);
        }
        if(!$thread->hasParticipant($receiverId)){
            $thread->addParticipant($receiverId);
        }
        return $thread;
    }

    public function loadChatBox(Request $request) {
        $thread = $this->detectThread($request->senderId, $request->receiverId);
        $data['has_unread_msg'] = $thread->isUnread(Auth::user()->id);
        $name = explode(" ", User::find($request->receiverId)->name);
        $firstname = $name[0];
        $data['chatTitle'] = $firstname;
        $data['messages'] = "";
        if ($data['has_unread_msg'] || $request->call_type == "start_chat") {
            $data['messages'] = $this->messageCollection($thread,$request->senderId);
        }
        return \Response::json($data);
    }

    public function submitChatMessage(Request $request) {
        $thread = $this->detectThread($request->sender_id, $request->receiver_id);
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => $request->sender_id,
                'body'      => $request->message,
            ]
        );
        $data['messages'] = $this->messageCollection($thread,$request->sender_id);
        return \Response::json($data);
    }
}
