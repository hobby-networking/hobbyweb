<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\HobbyCategory;
use App\Models\User;
use App\Models\UserPost;
use App\Models\HideUserPost;
use App\Http\Requests;
use App\Models\UserPostImages;
use App\Models\UserPostVideos;
use App\Models\PostLike;
use App\Models\PostComments;
use Redirect;
use Carbon\Carbon;
use Response;
use Input;
use Analytics;

class PostController extends Controller
{
    public function hideThisPost($post_id) {
        if (!empty($userPost = UserPost::find($post_id))) {
            $user_id = Auth::user()->id;
            $hideUserPost = new HideUserPost;
            $hideUserPost->post_id = $post_id;
            $hideUserPost->user_id = $user_id;
            $hideUserPost->save();
        }
        return Redirect::back();
    }

    public function deleteThisPost($post_id) {
        if (!empty($userPost = UserPost::find($post_id))) {
            $user = Auth::user();
            if ($user->isAdmin()) {
                $userPost->delete();
            }
        }
        return Redirect::back();
    }

    public function addNewPost(Request $request){
        //dd($_POST['hobby_id']);
        //dd($_FILES);
        $currentuser = Auth::User();
        $userPost = new UserPost;
        $userPost->user_id = $currentuser->id;
        $userPost->content = $_POST['post_content'];
        $userPost->type = $request->post_type;
        $userPost->visibility = $request->post_mode;
        if($userPost->type == 'user'){
            $userPost->hobby_id = implode(',', $_POST['hobby_id']);
        }else{
            $userPost->hobby_id = null;
        }
        if($userPost->type == 'community'){
            $userPost->community_id = $request->community_id;
        }else{
            $userPost->community_id = null;
        }
        if($userPost->save()){
            Analytics::trackEvent("new_post", "post_".$userPost->id."_by_user_".Auth::user()->id."_hobbies:".$userPost->hobby_id, "New Post: ".$userPost->id." by User: ".Auth::user()->id." hobbies: ".$userPost->hobby_id);
            if($_POST['files']){
                $allFilesArray = explode('|', $_POST['files']);
                foreach ($allFilesArray as $key => $file) {
                    if($file!=null){
                        //dd($allFilesArray);
                       $eachFileArray = explode(',', $file);
                       // dd($eachFileArray);
                       if($eachFileArray[0]=='image'){
                            $userImages = new UserPostImages;
                            $userImages->user_id = $currentuser->id;
                            $userImages->post_id = $userPost->id;
                            $userImages->image = $eachFileArray[1];
                            $userImages->save();                          
                       }
                       else{
                            $userVideos = new UserPostVideos;
                            $userVideos->user_id = $currentuser->id;
                            $userVideos->post_id = $userPost->id;
                            $userVideos->video = $eachFileArray[1];
                            $userVideos->save();                        
                       }
                    }
                }
            }
        } 
        return Redirect::back();       
    }
    public function postFileAjax(Request $request){
        $data['success'] = false;
        #print_r($_FILES['file']); die();
        if(isset($_FILES['file'])){
            $file_name = UserPost::setPostFile($_FILES['file']);
            //dd($file_name);
            if($file_name['uploadedFileName']){
               $data['success'] = true; 
               $data['file_name'] = $file_name['uploadedFileName'];
               $data['filePath'] = $file_name['filePath'];
            }
            else{
                $data['file_name'] = $file_name['uploadedFileName'];
                $data['filePath'] = $file_name['filePath'];
            }
        }
        return \Response::json($data);
    }

    public function postFileVideoAjax($filename){
        $filename = time()."_".rand()."-".str_replace(' ','_',$filename);
        $inputHandler = fopen('php://input', "r");

        $fileHandler = fopen(public_path('uploades/video/'.$filename), "w+");
        while(true) {
            $buffer = fgets($inputHandler, 4096);
            if (strlen($buffer) == 0) {
                $data['success'] = true; 
                fclose($inputHandler);
                
                $data['file_name'] = $filename;
                $data['filePath'] = 'uploades/video/';
                \Settings::putFileInStorage($data['filePath'], $data['file_name'], $fileHandler);
                fclose($fileHandler);
                $data['filePath'] = \Settings::urlOfFileInStorage($data['filePath'].$data['file_name']);
                return \Response::json($data);
            }

            fwrite($fileHandler, $buffer);
        }
    }

    public function postUserAjaxPost(Request $request){
        
        $currentuser = Auth::User();
        //dd($_POST);
        $html = '';
        $hobbyCat = HobbyCategory::all();
    	$userPost = new UserPost;
    	$userPost->user_id = $currentuser->id;
        $userPost->content = $_POST['post_desc'];
    	$userPost->hobby_id = $_POST['catId'];
    	//$userPost->image = $request->image;
    	//$userPost->video = $request->video;

        if($userPost->save()){

            if(isset($_FILES['theFile']) && !empty($_FILES['theFile'])){

                //$img_name_array =array();
                $img_name_array = UserPost::setUplodedPic($_FILES['theFile']);
                foreach ($img_name_array as $keyImg => $valueImg) {
                    
                    $userImages = new UserPostImages;
                    $userImages->user_id = $currentuser->id;
                    $userImages->post_id = $userPost->id;
                    $userImages->image = $img_name_array[$keyImg];
                    $userImages->save();
                }
            }
            $user_images = UserPostImages::where("user_id","=",$currentuser->id)->where("post_id","=",$userPost->id)->get();
            //dd($user_images);
            //return view('frontend.index')->with('userPost',$userPost)->with('hobbyCat',$hobbyCat);
            $html .= '<div class="row" >';
                            
                            $html .= '<hr>';
                            $html .= '<div class="postedUser">';
                            $html .= '<p>';
                            $html .= '<img src="'.$currentuser->profile_pic.'" class="img-circle img-responsive">';
                            $html .= '<a href="#">'.$currentuser->name.'</a><br />'; 
                            $html .= '<span>'.date("M",strtotime(date($userPost->created_at))).'</span>'.date("j",strtotime(date($userPost->created_at)));
                            $html .= '</p>';
                            $html .= '<p>'.$userPost->content.'</p>';
                            $html .='</div>';
                            $html .='<ul class="activity">
                                <li><a href="#"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                            </ul>';
                            if(!empty($user_images) && count($user_images)>0){
                                foreach ($user_images as $eachKey => $eachPostImg) {
                                     
                                    $html .='<div id="container-post-image"><div class="item">';
                                    $html .='<img src="'.\URL::asset('frontend/UserUploads/').'/'.$eachPostImg->image.'" class="img-responsive PostimgHover">';
                                    $html .='</div></div>';
                                }
                               
                            }
                            
                            
            $html .= '</div>';
            $html .= '<script type="text/javascript">

                        $(function() {

                            var wall = new Freewall("#container-post-image");
                            wall.fitWidth();
                        })
                    </script>';



            $data['success'] = true;
            $data['html'] = $html;
        
        }else{
            $data['success'] = false;
        }
        return \Response::json($data);

    }

    public function createDir($path){      
        if (!file_exists($path)) {
            $old_mask = umask(0);
            mkdir($path, 0777, TRUE);
            umask($old_mask);
        }
    }
    public function createThumb($path1, $path2, $file_type, $new_w, $new_h, $squareSize = ''){
            /* read the source image */
            $source_image = FALSE;
            
            if (preg_match("/jpg|JPG|jpeg|JPEG/", $file_type)) {
                $source_image = imagecreatefromjpeg($path1);
            }
            elseif (preg_match("/png|PNG/", $file_type)) {
                
                if (!$source_image = @imagecreatefrompng($path1)) {
                    $source_image = imagecreatefromjpeg($path1);
                }
            }
            elseif (preg_match("/gif|GIF/", $file_type)) {
                $source_image = imagecreatefromgif($path1);
            }       
            if ($source_image == FALSE) {
                $source_image = imagecreatefromjpeg($path1);
            }

            $orig_w = imageSX($source_image);
            $orig_h = imageSY($source_image);
            
            if ($orig_w < $new_w && $orig_h < $new_h) {
                $desired_width = $orig_w;
                $desired_height = $orig_h;
            } else {
                $scale = min($new_w / $orig_w, $new_h / $orig_h);
                $desired_width = ceil($scale * $orig_w);
                $desired_height = ceil($scale * $orig_h);
            }
                    
            if ($squareSize != '') {
                $desired_width = $desired_height = $squareSize;
            }

            /* create a new, "virtual" image */
            $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
            // for PNG background white----------->
            $kek = imagecolorallocate($virtual_image, 255, 255, 255);
            imagefill($virtual_image, 0, 0, $kek);
            
            if ($squareSize == '') {
                /* copy source image at a resized size */
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $orig_w, $orig_h);
            } else {
                $wm = $orig_w / $squareSize;
                $hm = $orig_h / $squareSize;
                $h_height = $squareSize / 2;
                $w_height = $squareSize / 2;
                
                if ($orig_w > $orig_h) {
                    $adjusted_width = $orig_w / $hm;
                    $half_width = $adjusted_width / 2;
                    $int_width = $half_width - $w_height;
                    imagecopyresampled($virtual_image, $source_image, -$int_width, 0, 0, 0, $adjusted_width, $squareSize, $orig_w, $orig_h);
                }

                elseif (($orig_w <= $orig_h)) {
                    $adjusted_height = $orig_h / $wm;
                    $half_height = $adjusted_height / 2;
                    imagecopyresampled($virtual_image, $source_image, 0,0, 0, 0, $squareSize, $adjusted_height, $orig_w, $orig_h);
                } else {
                    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $squareSize, $squareSize, $orig_w, $orig_h);
                }
            }
            
            if (@imagejpeg($virtual_image, $path2, 90)) {
                imagedestroy($virtual_image);
                imagedestroy($source_image);
                return TRUE;
            } else {
                return FALSE;
            }
    }
    public function image_upload_demo_submit(){

        /*defined settings - start*/
        ini_set("memory_limit", "99M");
        ini_set('post_max_size', '100M');
        ini_set('max_execution_time', 600);
        define('IMAGE_SMALL_DIR', './uploades/small/');
        define('IMAGE_SMALL_SIZE', 20);
        define('IMAGE_MEDIUM_DIR', './uploades/medium/');
        define('IMAGE_MEDIUM_SIZE', 150);
        /*defined settings - end*/
        //print_r($_FILES['theFile']);exit;
        if(isset($_FILES['theFile'])){
            
            //$output['status']=FALSE;
            set_time_limit(0);
            $allowedImageType = array("image/gif",   "image/jpeg",   "image/pjpeg",   "image/png",   "image/x-png"  );
            
            $count = count($_FILES['theFile']['name']);

            for($increment=0;$increment < $count;$increment++){

                if ($_FILES['theFile']["error"][$increment] > 0) {
                    $output['error']= "Error in File";
                }
                elseif (!in_array($_FILES['theFile']["type"][$increment], $allowedImageType)) {
                    $output['error']= "You can only upload JPG, PNG and GIF file";
                }
                elseif (round($_FILES['theFile']["size"][$increment] / 1024) > 4096) {
                    $output['error']= "You can upload file size up to 4 MB";
                } else {
                    /*create directory with 777 permission if not exist - start*/
                    $this->createDir(IMAGE_SMALL_DIR);
                    $this->createDir(IMAGE_MEDIUM_DIR);
                    /*create directory with 777 permission if not exist - end*/
                    $path[0] = $_FILES['theFile']['tmp_name'][$increment];
                    $file = pathinfo($_FILES['theFile']['name'][$increment]);
                    $fileType = $file["extension"];
                    $desiredExt='jpg';
                    $fileNameNew = rand(333, 999) . time() . ".$desiredExt";
                    $path[1] = IMAGE_MEDIUM_DIR . $fileNameNew;
                    $path[2] = IMAGE_SMALL_DIR . $fileNameNew;
                    
                    if ($this->createThumb($path[0], $path[1], $fileType, IMAGE_MEDIUM_SIZE, IMAGE_MEDIUM_SIZE,IMAGE_MEDIUM_SIZE)) {
                        
                        if ($this->createThumb($path[1], $path[2],"$desiredExt", IMAGE_SMALL_SIZE, IMAGE_SMALL_SIZE,IMAGE_SMALL_SIZE)) {
                            
                            $output['count']=$increment;
                            $output[$increment]['status']=TRUE;
                            $output[$increment]['image_medium']= $path[1];
                            $output[$increment]['image_small']= $path[2];

                        }else{

                            $output[$increment]['status']=FALSE;

                        }
                    }
                }

            }
            if(isset($output['count'])){
                $output['count']=$output['count']+1;
            }
            //print_r($output);exit;
            echo json_encode($output);
        }
    }

    public function likeUserPost(Request $request) {
        $data['success'] = false;
        if(empty(PostLike::where('user_id','=',$request->user_id)->where('post_id','=',$request->post_id)->first())){
            $newPostLike = new PostLike;
            $newPostLike->user_id = $request->user_id;
            $newPostLike->post_id = $request->post_id;
            //dd(UserPost::find($newPostLike->post_id)->getUserByPost);
            if($newPostLike->save()){
                $userPost = UserPost::find($newPostLike->post_id);
                if ($userPost->user_id != $newPostLike->user_id) {
                    $userPost->getUserByPost->sendNotification(User::find($newPostLike->user_id),"post.like");
                }
                $data['success'] = true;
            }
        }
        $data['like_count'] = UserPost::find($request->post_id)->likeCount();
        return \Response::json($data);
    }

    public function unlikeUserPost(Request $request) {
        $data['success'] = false;
        if(!empty($postLike = PostLike::where('user_id','=',$request->user_id)->where('post_id','=',$request->post_id)->first())){
            if($postLike->delete()){
                $data['success'] = true;
            }
        }
        $data['like_count'] = UserPost::find($request->post_id)->likeCount();
        return \Response::json($data);
    }

    public function postAddComment(Request $request) {
        $postComment = new PostComments;
        $postComment->user_id = $request->user_id;
        $postComment->post_id = $request->post_id;
        $postComment->content = $request->comment;
        $data['success'] = false;
        $data['comments'] = '';
        if($postComment->save()){
            $data['success'] = true;
            $post = UserPost::find($request->post_id);
            //dd($post->comments_desc());
            foreach($post->comments_desc() as $eachComment){
                $data['comments'] .='<li class="comment">'.
                                        '<a class="pull-left" href="#">'.
                                            '<img class="avatar" src="'.$eachComment->user->profile_pic.'" alt="avatar">'.
                                        '</a>'.
                                        '<div class="comment-body">'.
                                            '<div class="comment-heading">'.
                                                '<h4 class="user">'.$eachComment->user->name.'</h4>'.
                                                '<h5 class="time">'.Carbon::createFromTimeStamp(strtotime($eachComment->created_at))->diffForHumans().'</h5>'.
                                            '</div>'.
                                            '<p>'.$eachComment->content.'</p>'.
                                        '</div>'.
                                    '</li>';
            }
        }
        return \Response::json($data);
    }
}
