<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\HobbyCategory;
use App\Models\Hobby;
use App\Models\HobbyAlias;
use App\Models\UserHobbies;
use App\Models\Community;
use App\Models\HobbyCatEditorsPic;
use App\Models\SeoPage;
use App\Http\Requests;
use Redirect;
use Session;
use Auth;
use Input;
use Image;
use Response;
use Mail;
use Validator;

class AdminController extends Controller
{
    public function getAdminDashboardPage() {
    	return view('backend.admin.dashboard');
    }

    public function postRemoveUserHobby(Request $request){
        $hobbyId = $request->hobby_id;
        $loggedinUserId = $request->loggedinUser;
        //dd($hobbyId);
        $userHobbies = UserHobbies::where('id','=',$hobbyId)->where('user_id','=',$loggedinUserId)->get();
        foreach ($userHobbies as $key => $eachUserHobbies) {
            //dd($eachUserHobbies);
            $eachUserHobbies->delete();
        }
        return Response::json(true);
    }

    public function getManageUsersPage() {
    	$users = User::all();
    	return view('backend.admin.manage_user')->with('users',$users);
    }
    public function getActiveUsers() {
        $users = User::where('is_active','yes')->orderBy('name','ASC')->get();
        return view('backend.admin.active_user')->with('users',$users);
    }
    public function getManageHobbiesPage() {
    	$hobbies = Hobby::all();
        $hobbyCats = HobbyCategory::pluck('name','id');
    	return view('backend.admin.manage_hobby_category')->with('hobbies',$hobbies)->with('edit',false)->with('hobbyCategories',$hobbyCats);
    }
    public function postEditorsPicByHobbyCatIdAdd(Request $request) {
        $editorsPic = new HobbyCatEditorsPic;
        $editorsPic->hobby_cat_id = $request->hobby_cat_id;
        $editorsPic->title = $request->title;
        $editorsPic->link = $request->link;
        $editorsPic->is_active = isset($request->is_active)?true:false;
        $editorsPic->is_in_my_feed = isset($request->is_in_my_feed)?true:false;
        if($request->hasFile('image')){
            $editorsPic->image = $request->file('image');
        }
        if($editorsPic->save()){
            $success_msg[] = $editorsPic->title." saved successfully";
            Session::flash('success_msg', $success_msg);
            return Redirect::back();
        }else{
            return Redirect::back()->withErrors(['Error in saving Hobbies Editors Pic']);
        }
    }
    public function postEditorsPicByHobbyCatIdEdit(Request $request) {
        $editorsPic = HobbyCatEditorsPic::find($request->editors_pic_id);
        $editorsPic->hobby_cat_id = $request->hobby_cat_id;
        $editorsPic->title = $request->title;
        $editorsPic->link = $request->link;
        $editorsPic->is_active = isset($request->is_active)?true:false;
        $editorsPic->is_in_my_feed = isset($request->is_in_my_feed)?true:false;
        if($request->hasFile('image')){
            $editorsPic->image = $request->file('image');
        }
        if($editorsPic->save()){
            $success_msg[] = $editorsPic->title." updated successfully";
            Session::flash('success_msg', $success_msg);
            return Redirect::back();
        }else{
            return Redirect::back()->withErrors(['Error in saving Hobbies Editors Pic']);
        }
    }
    public function getEditorsPicByHobbyCatIdEditPage($catId, $epId) {
        $hobbyCat = HobbyCategory::find($catId);
        $editorsPic = HobbyCatEditorsPic::find($epId);
        return view('backend.admin.manage_editorspic_by_hobbycat_id')->with('hobbyCat',$hobbyCat)->with('editorsPic',$editorsPic)->with('edit',true);
    }
    public function getEditorsPicByHobbyCatIdPage($catId) {
        $hobbyCat = HobbyCategory::find($catId);
        //dd($hobbyCat->editorsPic);
        return view('backend.admin.manage_editorspic_by_hobbycat_id')->with('hobbyCat',$hobbyCat)->with('edit',false);
    }
    public function getEditorsPicHobbyCatListPage($catId=null) {
        $hobbyCats = HobbyCategory::all();
        if(empty($catId)){
            return view('backend.admin.manage_editorspic_hobbycat_list')->with('hobbyCats',$hobbyCats)->with('edit',false);
        }else{
            $hobbyCat = HobbyCategory::find($catId);
            return view('backend.admin.manage_editorspic_hobbycat_list')->with('hobbyCats',$hobbyCats)->with('hobbyCat',$hobbyCat)->with('edit',true);
        }
    }
    public function getManageCommunitiesPage() {
        $communities = Community::all();
        return view('backend.admin.manage_communities')->with('communities',$communities)->with('edit',false);
    }
    public function getManageAliasPage() {
        $hobbyCats = HobbyCategory::all();
        return view('backend.admin.manage_alias')->with('hobbyCats',$hobbyCats)->with('edit',false);
    }
    public function getEditHobbyCategoryPage($id) {
    	$hobbyCat = HobbyCategory::find($id);
    	if(empty($hobbyCat)){
    		return Redirect::back()->withErrors(['Hobby Category id not found']);
    	}
    	$hobbies = Hobby::all();
        $hobbyCats = HobbyCategory::pluck('name','id');
    	return view('backend.admin.manage_hobby_category')->with('hobbies',$hobbies)->with('edit',true)->with('hobbyCat',$hobbyCat)->with('hobbyCategories',$hobbyCats);
    }
    public function getEditCommunityPage($id) {
        $community = Community::find($id);
        if(empty($community)){
            return Redirect::back()->withErrors(['Community not found']);
        }
        $communities = Community::all();
        return view('backend.admin.manage_communities')->with('communities',$communities)->with('edit',true)->with('community',$community);
    }
    public function postEditCommunity(Request $request) {
        $community = Community::find($request->community_id);
        if(empty($community)){
            return Redirect::back()->withErrors(['Community not found']);
        }
        $community->name = $request->name;
        if($request->hasFile('profile_pic')){
            $community->profile_pic = $request->file('profile_pic');
        }
        if($request->hasFile('cover_pic')){
            $community->cover_pic = $request->file('cover_pic');
        }
        $community->status = isset($request->status)?true:false;
        $community->save();
        return Redirect::back();
    }
    public function getDeleteCommunity($id) {
        $community = Community::find($id);
        if(empty($community)){
            return Redirect::back()->withErrors(['Community not found']);
        }
        if($community->delete()){
            $success_msg[] = "Community delete successfully";
            Session::flash('success_msg', $success_msg);
            return Redirect::back();
        }
    }
    public function getDeleteHobbyCategoryPage($id) {
    	$hobbyCat = HobbyCategory::find($id);
    	if(empty($hobbyCat)){
    		return Redirect::back()->withErrors(['Hobby Category id not found']);
    	}
    	if($hobbyCat->delete()){
    		$success_msg[] = "Hobby delete successfully";
            Session::flash('success_msg', $success_msg);
    		return Redirect::back();
    	}
    }
    public function getEditHobbylistPage($id) {
    	$hobby = Hobby::find($id);
    	if(empty($hobby)){
    		return Redirect::back()->withErrors(['Hobby Category id not found']);
    	}
    	$hobbyCat = $hobby->category;
    	$hobbies = $hobbyCat->hobbies;
    	return view('backend.admin.manage_hobbies')->with('hobby',$hobby)->with('hobbies',$hobbies)->with('hobbyCat',$hobbyCat)->with('edit',true);
    }

    public function getDeleteHobbylistPage($id) {
    	$hobby = Hobby::find($id);
    	if(empty($hobby)){
    		return Redirect::back()->withErrors(['Hobby Category id not found']);
    	}
    	
    	if($hobby->delete()){

    		$success_msg[] = "Hobby delete successfully";
            Session::flash('success_msg', $success_msg);
    		return Redirect::back();
    	}
    }
     
     public function getListHobbyAliasPage($id) {
        $hobby = Hobby::find($id);
        if(empty($hobby)){
            return Redirect::back()->withErrors(['Hobby id not found']);
        }
        $hobbyAliases = $hobby->alias;
        // $hobbies = $hobbyAlias->hobbies;
        return view('backend.admin.manage_alias')->with('hobbyAliases',$hobbyAliases)->with('hobby',$hobby)->with('edit',false);
    }

    public function getHobbyListPage($id) {
    	$hobbyCat = HobbyCategory::find($id);
    	if(empty($hobbyCat)){
    		return Redirect::back()->withErrors(['Hobby Category id not found']);
    	}
    	$hobbies = $hobbyCat->hobbies;
    	return view('backend.admin.manage_hobbies')->with('hobbies',$hobbies)->with('hobbyCat',$hobbyCat)->with('edit',false);
    }

    public function postAddHobby(Request $request) {
    	$hobby = new Hobby;
    	$hobby->name = $request->name;
    	$hobby->cat_id = $request->cat_id;
    	$hobby->desc = $request->desc;
    	$hobby->is_active = isset($request->is_active)?true:false;
    	if($hobby->save()){
    		$success_msg[] = $hobby->name." saved successfully";
            Session::flash('success_msg', $success_msg);
    		return Redirect::back();
    	}else{
    		return Redirect::back()->withErrors(['Error in saving Hobby Category']);
    	}
    }
    public function postEditHobby(Request $request) {
        $hobby = Hobby::find($request->hobby_id);
        $hobby->name = $request->name;
        $hobby->hobby_slug = str_slug($request->name);
        if($request->hasFile('profile_pic')){
            $hobby->profile_pic = $request->file('profile_pic');
        }
        $hobby->desc = $request->desc;
        $hobby->is_active = isset($request->is_active)?true:false;
        if($hobby->save()){
            $success_msg[] = $hobby->name." updated successfully";
            Session::flash('success_msg', $success_msg);
            return Redirect::route('list_hobbies',$hobby->category->id);
        }else{
            return Redirect::back()->withErrors(['Error in saving Hobby Category']);
        }
    }
    public function postAddHobbyCategory(Request $request) {
    	$hobbyCat = new HobbyCategory;
    	$hobbyCat->name = $request->name;
    	$hobbyCat->desc = $request->desc;
    	$hobbyCat->is_active = isset($request->is_active)?true:false;
    	if($hobbyCat->save()){
    		$success_msg[] = $hobbyCat->name." saved successfully";
            Session::flash('success_msg', $success_msg);
    		return Redirect::back();
    	}else{
    		return Redirect::back()->withErrors(['Error in saving Hobby Category']);
    	}
    }
    public function postEditHobbyCategory(Request $request) {
    	//dd($request);
    	$hobbyCat = HobbyCategory::find($request->hobby_cat_id);
    	$hobbyCat->name = $request->name;
    	$hobbyCat->desc = $request->desc;
    	$hobbyCat->is_active = isset($request->is_active)?true:false;
    	if($hobbyCat->save()){
    		$success_msg[] = $hobbyCat->name." updated successfully";
            Session::flash('success_msg', $success_msg);
    		return Redirect::back();
    	}else{
    		return Redirect::back()->withErrors(['Error in saving Hobby Category']);
    	}
    }

    public function postAddHobbyAlias(Request $request) {
        $hobbyAlias = new HobbyAlias;
        $hobbyAlias->alias_name = $request->alias_name;
        $hobbyAlias->hobby_id = $request->hobby_id;
        $hobbyAlias->is_active = isset($request->is_active)?true:false;
        if($hobbyAlias->save()){
            $success_msg[] = $hobbyAlias->name." saved successfully";
            Session::flash('success_msg', $success_msg);
            return Redirect::back();
        }else{
            return Redirect::back()->withErrors(['Error in saving Hobby Alias']);
        }
    }

    public function getEditHobbyAliasPage($id) {
        $hobbyAlias = HobbyAlias::find($id);
        if(empty($hobbyAlias)){
            return Redirect::back()->withErrors(['Hobby Alias id not found']);
        }
        $hobby = $hobbyAlias->hobby;
        $hobbyAliases = $hobby->alias;
        return view('backend.admin.manage_alias')->with('hobbyAliases',$hobbyAliases)->with('edit',true)->with('hobbyAlias',$hobbyAlias)->with('hobby',$hobby);
    }

      public function getDeleteHobbyAliasPage($id) {
        $hobbyAlias = HobbyAlias::find($id);
        if(empty($hobbyAlias)){
            return Redirect::back()->withErrors(['Hobby Alias id not found']);
        }

        if($hobbyAlias->delete()){
            //dd($hobbyAlias);
            $success_msg[] = "Hobby Alias delete successfully";
            Session::flash('success_msg', $success_msg);
            return Redirect::back();
        }
        
    }

    public function postEditHobbyAlias(Request $request) {
        $hobbyAlias = HobbyAlias::find($request->hobby_alias_id);
        $hobbyAlias->alias_name = $request->alias_name;
        $hobbyAlias->is_active = isset($request->is_active)?true:false;
        if($hobbyAlias->save()){
            $success_msg[] = $hobbyAlias->alias_name." updated successfully";
            Session::flash('success_msg', $success_msg);
            return Redirect::back();
        }else{
            return Redirect::back()->withErrors(['Error in saving Hobby Alias']);
        }
    }

    public function editUsersPage($id) {

        $user = User::find($id);
        if(empty($user)){
            return Redirect::back()->withErrors(['User not found']);
        }
        return view('backend.admin.edit_user')->with('user',$user)->with('edit',true)->with('sendmail',false);
    }

    public function postEditUsers(Request $request,$id){
       
        $user = User::find($id);
        if(empty($user)){
            return Redirect::back()->withErrors(['User not found']);
        }
        $user->name = $_POST["name"];
        $user->phone = $_POST["phone"];
        if($request->hasFile('profile_pic')){
            $user->profile_pic = $request->file('profile_pic');
        }
        $user->is_active = isset($_POST["is_active"])?true:false;
        $user->save();

        return Redirect::route("manage_users");
    }

    public function addUsersPage() {

        return view('backend.admin.edit_user')->with('edit',false)->with('sendmail',false);
    }
    public function postAddUsers(){

        $user = new User;
        $user->name = $_POST["name"];
        $user->phone = $_POST["phone"];
        $user->email = $_POST["email"];
        $user->type = 'user';
        $user->is_active = isset($_POST["is_active"])?true:false;
        $user->save();

        return Redirect::route("manage_users");
    }

    public function deleteUsers($id){

        $user = User::find($id);

        if(!empty($user)){

            $user->delete();
            return Redirect::route("manage_users");
        }else{
            return Redirect::back()->withErrors(['Error in deleting User']);
        }

    }

    public function getSendmailUsersPage($id){

        $user = User::find($id);
        if(empty($user)){
            return Redirect::back()->withErrors(['User not found']);
        }
        return view('backend.admin.edit_user')->with('user',$user)->with('sendmail',true)->with('edit',false);

    }    

    public function postSendmailUsersPage($id){
        //dd($_POST);
        $data = Input::all();
        $rules = array (
            'email' => 'required',
            'emailsubject' => 'required',
            'desc' => 'required'
        );
        if(!empty($data['email'])){
          $useremail = $data['email'];
        }else{
        $useremail = 'help@hobbynetworking.com';
        }
        $validator = Validator::make($data, $rules);
        if ($validator -> passes()){
        if(!empty($data['email'])){
            $maildata = array(
            'email' => $data['email'],
            'subject' => $data['emailsubject'],
            'maildetails' => $data['desc'],
            );
            
         Mail::send(['html' => 'emails.admin_send_useremail'], $maildata, function($message) use ($maildata)
          {
              $message->from('help@hobbynetworking.com' , 'Hobbynetworking');              
              $message->to($maildata['email'], 'Info')->subject($maildata['subject']);
          });
          //Session::flash('success_msg', 'Your message has been sent. Thank You!');
           return Redirect::route('send_mail_users', ['id' => $id])->with('message', 'Your message has been sent. Thank You!'); 
          }else{
            //dd($_POST);
              return Redirect::route('send_mail_users', ['id' => $id])->withErrors('Please select at least one user!');
          }
        }else{
          return Redirect::route('send_mail_users', ['id' => $id])->withErrors($validator)->withInput();
        }
    }

    public function PasswordChange(){
        return view('backend.admin.adminpasswordchng');
    }

    public function PasswordUpdate(Request $request){
        if(!empty($request->newpassword)){
       $data = array('password' =>bcrypt($request->newpassword));
       User::where('id',$request->input('userid'))->update($data);
       Session::flash('message', 'Successfully Updated password!');
      return Redirect::route("admin_dashboard");
      }else{
        return view('backend.admin.adminpasswordchng');
        return Redirect::back()->withErrors(['New Password Field is required']);  
      }
    }

    public function GroupuserEmailSend(){
        
      $data = Input::all();
      $rules = array (
          'emailsubject' => 'required',
          'desc' => 'required'
      );
      if(!empty($data['sendmailuser'])){
          $useremail = $data['sendmailuser'];
      }else{
        $useremail = array('help@hobbynetworking.com');
      }
      $validator = Validator::make($data, $rules);
      if ($validator -> passes()){
        if(!empty($data['sendmailuser']) || !empty($data['memailid'])){
            if(!empty($data['memailid'])){
                $emailarray = explode(',',$data['memailid']);
                   $maildata = array(
                    'email' => array_merge($emailarray,$useremail),
                    'subject' => $data['emailsubject'],
                    'maildetails' => $data['desc'],
                   );
                }else{
                  $maildata = array(
                    'email' => array_merge($data['sendmailuser'],array('help@hobbynetworking.com')),
                    'subject' => $data['emailsubject'],
                    'maildetails' => $data['desc'],
                  );
              }
        
         Mail::send(['html' => 'emails.admin_send_useremail'], $maildata, function($message) use ($maildata)
          {
              $message->from('help@hobbynetworking.com' , 'Hobbynetworking');              
              $message->bcc($maildata['email'], 'Info')->subject($maildata['subject']);
          });
          //Session::flash('success_msg', $success_msg);
           return Redirect::route('active_users')->with('message', 'Your message has been sent. Thank You!'); 
          }else{
            //dd($_POST);
              return Redirect::route('active_users')->withErrors('Please select at least one user!');
          }
       }else{
          return Redirect::route('active_users')->withErrors($validator)->withInput();
       }
    }

    public function showHobbiesByUser($id)
    {
        $hobbies = Hobby::take(9)->get();
        $user_hobbies = UserHobbies::where('user_id',$id)->lists('hobby_id')->toArray();
        //echo "<pre>";print_r($user_hobbies);die;
        return view('backend.admin.show_hobby_by_user')->with('hobbies',$hobbies)->with('user_hobbies',$user_hobbies)->with('user_id',$id);
    }

     public function updateUserHobby(Request $request){

        $user_id = $request->user_id;
        $hobby_ids = $request->hobby_ids;
        
        UserHobbies::where('user_id','=',$user_id)->delete();
        
        foreach ($hobby_ids as $key => $eachId) {
           
           $userHobby = new UserHobbies;
           $userHobby->hobby_id = $eachId;
           $userHobby->user_id = $user_id;
           $userHobby->is_active = "yes";
           $userHobby->save();

        }

        $success_msg[] = "Hobby updated successfully";
            Session::flash('success_msg', $success_msg);
        return Redirect::route('show_hobbies',$user_id);
       
    }
    public function PreviewEmail(){
        //echo "<pre>";print_r(Input::all());
        $data = Input::all();
        $maildata = array(
                    'subject' => $data['subject'],
                    'maildetails' => $data['validation_desc'],
                   );
        
        return view('emails.admin_send_useremail')->with('subject', $data['subject'])->with('maildetails', $data['validation_desc']);
        //echo $returnHTML;die;
            //return response()->json( array('success' => true, 'returnHTML'=>$returnHTML) );
    }

    public function getManageSeoPage(){

        return view('backend.admin.manage_seo');
    }
    public function postAddSeo(Request $request){
        //echo $request->page_name;
        if($request->page_name != ' ' ){
            $seo_details = SeoPage::where('page_name',$request->page_name)->first();
            if(count($seo_details) > 0 ){
                $data = array('meta_keywords' =>$request->meta_keywords,
                                'meta_title' => $request->meta_title,
                                'meta_desc' => $request->desc
                            );
                SeoPage::where('page_name',$request->page_name)->update($data);
                $success_msg[] = $request->page_name." Page Seo Deails Update successfully";
                Session::flash('success_msg', $success_msg);
                return Redirect::back();
            } else {
                $seo = new SeoPage;
                $seo->page_name = $request->page_name;
                $seo->meta_keywords = $request->meta_keywords;
                $seo->meta_title = $request->meta_title;
                $seo->meta_desc = $request->desc;
                if($seo->save()){
                    $success_msg[] = $seo->page_name." Page Seo Deails saved successfully";
                    Session::flash('success_msg', $success_msg);
                    return Redirect::back();
                }else{
                    return Redirect::back()->withErrors(['Error in saving Seo Page']);
                }
            }
        } else {
                return Redirect::back()->withErrors(['Error in saving Seo Page']);
        }
        
    }
    public function getSeoByPage(){
        $seo_details = SeoPage::where('page_name',$_REQUEST['page_name'])->first();
        return $seo_details;
    }

}
