<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserVerification;
use App\Http\Requests;
use Auth;
use Notifynder;
use Mail;
use Response;
use Analytics;

class UserController extends Controller
{
    private function getLoggedInUser() {
        return Auth::user();
    }
    public function updateName(Request $request) {
        $user = $this->getLoggedInUser();
        $user->name = $request->value;
        $user->save();
        return $user->name;
    }

    public function updateStatus(Request $request) {
        $user = $this->getLoggedInUser();
        $user_setting = $user->userSettings;
        $user_setting->user_status = $request->value;
        $user_setting->save();
        return $user_setting->user_status;
    }

    public function updateAboutMe(Request $request) {
        $user = $this->getLoggedInUser();
        $user_setting = $user->userSettings;
        $user_setting->user_about_me = $request->value;
        $user_setting->save();
        return $user_setting->user_about_me;
    }

    public function readNotification($id) {
        $notification = Notifynder::findNotificationById($id);
        //dd($notification);
        Notifynder::readOne($id);
        return redirect($notification->url);
    }

    public function blockUser($userId){
    	$user = User::find($userId);
    	$loggedInUser = $this->getLoggedInUser();
    	$loggedInUser->blockFriend($user);
    	return redirect()->back();
    }

    public function unblockUser($userId){
    	$user = User::find($userId);
    	$loggedInUser = $this->getLoggedInUser();
    	$loggedInUser->unblockFriend($user);
    	return redirect()->back();
    }

    public function friendUser($userId){
    	$user = User::find($userId);
    	//dd($user);
    	$loggedInUser = $this->getLoggedInUser();
        Analytics::trackEvent("new_friend_request", "from: ".$loggedInUser->id." to ".$user->id, "New Friend Request send from: ".$loggedInUser->id." to ".$user->id.". ");
    	$loggedInUser->befriend($user);
    	return redirect()->back();
    }

    public function unfriendUser($userId){
    	$user = User::find($userId);
    	$loggedInUser = $this->getLoggedInUser();
    	$loggedInUser->unfriend($user);
    	return redirect()->back();
    }

    public function acceptFriendRequest($userId){
        $user = User::find($userId);
        $loggedInUser = $this->getLoggedInUser();
        $loggedInUser->acceptFriendRequest($user);
        return redirect()->back();
    }

    public function denyFriendRequest($userId){
        $user = User::find($userId);
        $loggedInUser = $this->getLoggedInUser();
        $loggedInUser->denyFriendRequest($user);
        return redirect()->back();
    }

    public function allCommunityPage(){
        $user = $this->getLoggedInUser();
        return view('frontend.all_user_community')->with('communityList',$user->communityList())->with('loggedInUser',$user);
    }

    public function allFriendsPage(){
        $user = $this->getLoggedInUser();
        return view('frontend.all_user_friends')->with('friendsList',$user->getFriends())->with('loggedInUser',$user);
    }

    public function allHobbyPage(){
        $user = $this->getLoggedInUser();
        return view('frontend.all_user_hobby')->with('loggedInUser',$user);
    }

    public function userPhotoesPage(){
        $user = $this->getLoggedInUser();
        return view('frontend.user_gallery')->with('photoList',$user->gallery)->with('loggedInUser',$user);
    }

    public function verifyEmail(){
        if (Auth::user()->email_verified == 1){
           return \Redirect::route('user_setting',Auth::user()->id); 
        }
        $userVerification = null;
        if (count(UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','email')->get())>0) {
            $userVerification = UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','email')->first();
            $userVerification->confirmation_code = str_random(30);
            $userVerification->save();
        }else{
            $userVerification = new UserVerification;
            $userVerification->user_id = Auth::user()->id;
            $userVerification->confirmation_code = str_random(30);
            $userVerification->type = 'email';
            $userVerification->save();
        }
        //dd($userVerification->confirmation_code);
        Mail::send('emails.emailverify', array('confirmation_code'=>$userVerification->confirmation_code), function($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                    ->subject('HobbyNetworking :: Verify your email address');
        });

       return \Redirect::route('user_setting',Auth::user()->id); 
    }

    public function verifyEmailApi(ApiRequest $request){
        $inputJson = json_decode($request->getContent());
        $data['error_code'] = 200;
        $data['email_already_verified'] = false;
        $data['error_msg'] = "Please check your mailbox for verification mail.";
        if (Auth::user()->email_verified == 1){
            $data['email_already_verified'] = true;
          $data['error_msg'] = "Your Mail id is already verified. "; 
          return Response::json($data);
        }
        $userVerification = null;
        if (count(UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','email')->get())>0) {
            $userVerification = UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','email')->first();
            $userVerification->confirmation_code = str_random(30);
            $userVerification->save();
        }else{
            $userVerification = new UserVerification;
            $userVerification->user_id = Auth::user()->id;
            $userVerification->confirmation_code = str_random(30);
            $userVerification->type = 'email';
            $userVerification->save();
        }
        //dd($userVerification->confirmation_code);
        Mail::send('emails.emailverify', array('confirmation_code'=>$userVerification->confirmation_code), function($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                    ->subject('hobby :: Verify your email address');
        });
       
       return Response::json($data);
    }

    public function verifyPhoneApi(ApiRequest $request){
        $inputJson = json_decode($request->getContent());

        $data['error_msg'] = "";
        $user = Auth::user();
        $number = $user->phone_no;
        $regex = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i"; 
        if (!empty($inputJson->payload->phone_no) && $inputJson->payload->phone_no != Auth::user()->phone_no && preg_match( $regex, $inputJson->payload->phone_no )) {
            $user->phone_no = $inputJson->payload->phone_no;
            $user->save();
            $number = $user->phone_no;
        }
        
        if (Auth::user()->phone_no_verified == 1){
           $data['error_msg'] = "Your phone number is already verified. "; 
           return Response::json($data);
        }
        //dd($number);
        $app_id = 'cac6b91b2a2b4100931ad98'; 
        $access_token = 'a22dddda23b7c335b9304ce6323c9c9ca67f962b'; 
        $mobile = '+91'.$number; 
             
        $content =  '&app_id=cac6b91b2a2b4100931ad98'. 
                    '&access_token='.rawurlencode($access_token). 
                    '&mobile='.rawurlencode($mobile); 
         
        $result = json_decode($this->sendVerificationToPhone($mobile));
        if ($result->status == 'success') {
            $result->showOTPinput = true;
            $userVerification = null;
            if (count(UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','phone')->get())>0) {
                $userVerification = UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','phone')->first();
                $userVerification->confirmation_code = $result->keymatch."@hobby$".$result->otp_start;
                $userVerification->save();
            }else{
                $userVerification = new UserVerification;
                $userVerification->user_id = Auth::user()->id;
                $userVerification->confirmation_code = $result->keymatch."@hobby$".$result->otp_start;
                $userVerification->type = 'phone';
                $userVerification->save();
            }
        }
        
        return Response::json($result);
    }

    public function verifyPhoneOtpApi(ApiRequest $request){
        $inputJson = json_decode($request->getContent());
        
        if (count(UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','phone')->get())>0) {
            $userVerification = UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','phone')->first();
            $data = explode("@hobby$",$userVerification->confirmation_code);
            $keymatch = $data[0];
            $otp_start = $data[1];
            $otp = $otp_start.$inputJson->payload->last_5_digit;
            $result = json_decode($this->confirmPhoneOtp($keymatch, $otp));

            if ($result->status == 'success') {
                $userVerification->delete();
                $user = User::findOrFail($userVerification->user_id);
                $user->phone_no_verified = 1;
                if ($user->phone_no_verified == 1 && $user->verified_email) {
                    $user->is_active = 'yes';
                }
                
                $user->save();
            }
            return Response::json($result);
        }
    }

    public function confirmEmail($confirmation_code) { //dd($confirmation_code);
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }
        $userVerification = UserVerification::where('confirmation_code','=',$confirmation_code)->first();
        if (count($userVerification)<1)
        {
            return \Redirect::to('/')->withErrors(['Link expired']);
        }
        $user = User::findOrFail($userVerification->user_id);
        $user->email_verified = 1;
        $user->save();

        $userVerification->delete();
        return \Redirect::route('user_setting',Auth::user()->id)->with('message', 'You have successfully verified your email id.');
    }

    public function verifyPhone(Request $request){
        //dd($request->new_no);
        $user = Auth::user();
        if (Auth::user()->phone_no_verified == 1){
           return \Redirect::route('user_setting',$user->id); 
        }
        $app_id = 'cac6b91b2a2b4100931ad98'; 
        $access_token = 'a22dddda23b7c335b9304ce6323c9c9ca67f962b'; 
         
        $number = $user->phone;
        $regex = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i"; 
        if (!empty($request->new_no) && $request->new_no != Auth::user()->phone && preg_match( $regex, $request->new_no )) {
            if(count(User::where('phone','=',$request->new_no)->get())>0) {
                $data['error_msg'] = "Phone no ".$request->new_no." is already used by another user. "; 
                return Response::json($data); die();
            }
            $user->phone = $request->new_no;
            try { 
                $user->save();
            } catch (Illuminate\Database\QueryException $e){ 
                $error_code = $e->errorInfo[1];
                if($error_code == 1062){
                    $data['error_msg'] = "Phone no ".$request->new_no." is already used by another user. "; 
                    return Response::json($data);
                    die();
                }
                
            }
            $number = $user->phone;
        }     
        $mobile = '+91'.$number;
        $content =  '&app_id=cac6b91b2a2b4100931ad98'. 
                    '&access_token='.rawurlencode($access_token). 
                    '&mobile='.rawurlencode($mobile); 
         
        $result = json_decode($this->sendVerificationToPhone($mobile));
        if ($result->status == 'success') {
            $html = '<label for="phone_no" class="label">Just got a missed call from</label><span class="input five-columns"><input type="text" class="otp_start input-unstyled three-columns" name="otp_start" id="otp_start" value="'.$result->otp_start.'" readonly=""><input type="text" class="input-unstyled eight-columns" name="otp_last_5" id="otp_last_5" placeholder="Enter last 5 digit"><a href="javascript:verifyPhone2()" title="Confirm Otp" class="button compact icon-tick">verify</a></span>';
            
            $result->html = $html;
            $userVerification = null;
            if (count(UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','phone')->get())>0) {
                $userVerification = UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','phone')->first();
                $userVerification->confirmation_code = $result->keymatch."@hobby$".$result->otp_start;
                $userVerification->save();
            }else{
                $userVerification = new UserVerification;
                $userVerification->user_id = Auth::user()->id;
                $userVerification->confirmation_code = $result->keymatch."@hobby$".$result->otp_start;
                $userVerification->type = 'phone';
                $userVerification->save();
            }
        }
        
        return Response::json($result);
    }

    private function sendVerificationToPhone($mobile) { 
        $cookie= "koekje.txt";
        $url = "https://www.cognalys.com/api/v1/otp/?app_id=cac6b91b2a2b4100931ad98&access_token=a22dddda23b7c335b9304ce6323c9c9ca67f962b&mobile=".$mobile;
         $ch = curl_init();

          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/'.$cookie);
          curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/'.$cookie);

          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $output = curl_exec($ch); //dd($output);
          if (curl_errno($ch)) die(curl_error($ch));

          curl_close($ch); 
        return $output;     
    }
    public function verifyPhoneOtp(Request $request){
        if (count(UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','phone')->get())>0) {
            $userVerification = UserVerification::where('user_id','=',Auth::user()->id)->where('type','=','phone')->first();
            $data = explode("@hobby$",$userVerification->confirmation_code);
            $keymatch = $data[0];
            $otp_start = $data[1];
            $otp = $otp_start.$request->otp_last_5;
            $result = json_decode($this->confirmPhoneOtp($keymatch, $otp));

            if ($result->status == 'success') {
                $userVerification->delete();
                $user = User::findOrFail($userVerification->user_id);
                $user->phone_no_verified = 1;
                $user->save();
            }
            return Response::json($result);
        }
    }
    private function confirmPhoneOtp($keymatch, $otp) { 
        $cookie= "koekje.txt";
        $url = "https://www.cognalys.com/api/v1/otp/confirm/?app_id=cac6b91b2a2b4100931ad98&access_token=a22dddda23b7c335b9304ce6323c9c9ca67f962b&keymatch=".$keymatch."&otp=".$otp."&mobile=+91".Auth::user()->phone_no;
         $ch = curl_init();

          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/'.$cookie);
          curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/'.$cookie);

          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $output = curl_exec($ch); //dd($output);
          if (curl_errno($ch)) die(curl_error($ch));

          curl_close($ch); //dd($output);
        return $output;     
    }
}
