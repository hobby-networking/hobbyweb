<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Models\User;
use App\Models\Settings;

class SettingsController extends Controller
{
    //
    public function getUserSettingPage(){

    	$user = Auth::user();
    	return view('frontend.dashboard.user_setting')->with('user',$user);

    }

    public function saveUserSetting(Request $request){
    	//dd($request);
    	if(count(User::where('id','=',$request->user_id)->first())>0){
    		$user = User::where('id','=',$request->user_id)->first();
            $user->name = $request->name;
            if($user->phone != $request->phone){
                $user->phone_no_verified = 0;
            }
            $user->phone = $request->phone;
    		
    		if(!empty($request->file('profile_image'))){
    			$user->profile_pic = $request->file('profile_image');
    		}
            if(!empty($request->file('cover_img'))){
                $user->cover_pic = $request->file('cover_img');
            }
    		$user->save();
    		$user_setting = $user->userSettings;
    		
    		$user_setting->user_status = $request->profile_status;
    		$user_setting->user_about_me = $request->about_me;
    		$user_setting->username = Settings::generateusername($user);
    		$user_setting->save();
    		return \Redirect::route('profile_page',$user_setting->username);

    	}else{

    		return \Redirect::back();
    	}

    }

    public function saveUserProfilePicture(Request $request){
    	dd($request);
    }


}
