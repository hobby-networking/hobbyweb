<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Models\User;
use App\Models\Community;
use App\Models\CommunityFollowers;
use App\Models\CommunityMembers;
use App\Models\CommunityInvitation;
use App\Models\Hobby;
use Redirect;
use Analytics;


class CommunityController extends Controller
{   
	public function acceptCommunityRequest($requestId) {
		$communityMember = CommunityMembers::find($requestId);
		$communityMember->is_accepted = 'yes';
		$communityMember->save();
		return redirect()->back();
	}

	public function denyCommunityRequest($requestId) {
		$communityMember = CommunityMembers::find($requestId);
		$communityFollower = CommunityFollowers::where('community_id','=',$communityMember->community_id)->where('user_id','=',$communityMember->user_id)->first();
		$communityFollower->delete();
		$communityMember->delete();
		return redirect()->back();
	}

	public function addCommunityPage($name = null) {
	 	$user = Auth::user();
	 	$arrCom = Community::where('slug','=',$name)->first();
	 	if($arrCom && ($arrCom->status == 'yes' || $arrCom->created_by == $user->id)) {
	 		  //dd($user);
	 		  if(!$user && $arrCom->type != "secret"){
                return view('frontend.dashboard.community')->with('arrCom',$arrCom);
	 		  }elseif(!empty($user)){ 
	 		  	if($arrCom->type == "secret" && ($arrCom->is_member=="yes" || $arrCom->created_by == $user->id)){//dd($arrCom->created_by == $user->id);
	               return view('frontend.dashboard.community')->with('arrCom',$arrCom);
	            }elseif($arrCom->type != "secret"){
	            	return view('frontend.dashboard.community')->with('arrCom',$arrCom);
	        	}else{
	              	return $redirect = Redirect::route('welcome_page');
	            }
	 		  }else{
	 		  	return $redirect = Redirect::route('welcome_page'); 
	 		  }
	 	}
	 	else{
          return $redirect = Redirect::route('welcome_page'); 
	 	}
    }
    
    public function inactiveCommunity($community_slug = null){
     	$loged_in_user_id = Auth::user()->id;
    	$communityDetails = Community::where('slug','=',$community_slug)->where('created_by','=',$loged_in_user_id)->first();
    	if($communityDetails){
          $communityDetails->status ='no';
          $communityDetails->save();
    	}
    	return Redirect::back(); 
    }
    public function activeCommunity($community_slug = null){
     	$loged_in_user_id = Auth::user()->id;
    	$communityDetails = Community::where('slug','=',$community_slug)->where('created_by','=',$loged_in_user_id)->first();
    	if($communityDetails){
          $communityDetails->status ='yes';
          $communityDetails->save();
    	}
    	return Redirect::back(); 
    }    
    public function followCommunity($community_slug = null){
     	$loged_in_user_id = Auth::user()->id;
    	$communityDetails = Community::where('slug','=',$community_slug)->first();
		$findCommunityFollower = CommunityFollowers::where('user_id','=',$loged_in_user_id)->where('community_id','=',$communityDetails->id)->first();
        if(!$findCommunityFollower){
			$communityFollowers = new CommunityFollowers;
			$communityFollowers->community_id = $communityDetails->id;
			$communityFollowers->user_id = $loged_in_user_id;
			$communityFollowers->is_active = 'yes';
			$communityFollowers->save();
        }
        return Redirect::back();    	    	
    }
    public  function unFollowComunity($community_slug = null){
     	$loged_in_user_id = Auth::user()->id;
    	$communityDetails = Community::where('slug','=',$community_slug)->first();
    	//dd($communityDetails);
    	if (!empty(CommunityFollowers::where('user_id','=',$loged_in_user_id)->where('community_id','=',$communityDetails->id)->first())) {
    		$findCommunityFollower = CommunityFollowers::where('user_id','=',$loged_in_user_id)->where('community_id','=',$communityDetails->id)->first();
			$findCommunityFollower->delete();
    	}
		
		if($communityDetails->is_member=='yes' || $communityDetails->is_requested_member=='yes'){
		   $communityMembers = CommunityMembers::where('user_id','=',$loged_in_user_id)->where('community_id','=',$communityDetails->id)->first();
		   $communityMembers->delete();
		}
		return Redirect::back();    	   	
    }
    public function joinAsMember($community_slug = null){
    	$loged_in_user_id = Auth::user()->id;
    	$communityDetails = Community::where('slug','=',$community_slug)->first();
       if($community_slug && $communityDetails->type == 'public'){
		$comunityMember = new CommunityMembers;
		$comunityMember->community_id = $communityDetails->id;
		$comunityMember->user_id = $loged_in_user_id;
		$comunityMember->is_accepted = 'yes';
		$comunityMember->save();
        
		$findCommunityFollower = CommunityFollowers::where('user_id','=',$loged_in_user_id)->where('community_id','=',$communityDetails->id)->first();
        if(!$findCommunityFollower){
			$communityFollowers = new CommunityFollowers;
			$communityFollowers->community_id = $communityDetails->id;
			$communityFollowers->user_id = $loged_in_user_id;
			$communityFollowers->is_active = 'yes';
			$communityFollowers->save();
        }
       }else{
		$comunityMember = new CommunityMembers;
		$comunityMember->community_id = $communityDetails->id;
		$comunityMember->user_id = $loged_in_user_id;
		$comunityMember->is_accepted = 'no';
		$comunityMember->save();

		$findCommunityFollower = CommunityFollowers::where('user_id','=',$loged_in_user_id)->where('community_id','=',$communityDetails->id)->first();
        if(!$findCommunityFollower){
			$communityFollowers = new CommunityFollowers;
			$communityFollowers->community_id = $communityDetails->id;
			$communityFollowers->user_id = $loged_in_user_id;
			$communityFollowers->is_active = 'no';
			$communityFollowers->save();
        }
        
       }
       return Redirect::back();
    }
    public function getCreateCommunityPage(){
    	//dd(1);
		$loggedin_user_id = \Auth::user()->id;
		$allMembers = User::where('id','!=',$loggedin_user_id)->where('is_active','=','yes')->where('type','=','user')->get();
		//dd($allMembers);
		$allmembersArr = array();
		    foreach ($allMembers as $key=>$eachMembers){
		        //array_push($allmembersArr, $eachMembers->name);
		        $allmembersArr[$key]['id'] = $eachMembers->id;
		        $allmembersArr[$key]['label'] = $eachMembers->name;
		    }

		$allHobbies = \Auth::user()->hobbies;
		
		$allHobbiesArr = array();
		    foreach ($allHobbies as $key=>$eachHobby){

		    	$hobbyDetails = $eachHobby->hobby;
		        $allHobbiesArr[$key]['id'] = $hobbyDetails->id;
		        $allHobbiesArr[$key]['label'] = $hobbyDetails->name;
		    }
		//dd($allHobbiesArr);
		return view('frontend.dashboard.community_create')->with('allMembers',$allmembersArr)->with('allHobbies',$allHobbiesArr);
	}
	public function getEditCommunityPage($id){
    	$commuity = Community::find($id);
		$loggedin_user_id = \Auth::user()->id;
		$allMembers = User::where('id','!=',$loggedin_user_id)->where('is_active','=','yes')->where('type','=','user')->get();
		//dd($allMembers);
		$allmembersArr = array();
	    foreach ($allMembers as $key=>$eachMembers){
	        $allmembersArr[$key]['id'] = $eachMembers->id;
	        $allmembersArr[$key]['label'] = $eachMembers->name;
	    }
		$allHobbies = \Auth::user()->hobbies;
		$allHobbiesArr = array();
		    foreach ($allHobbies as $key=>$eachHobby){
		    	$hobbyDetails = $eachHobby->hobby;
		        $allHobbiesArr[$key]['id'] = $hobbyDetails->id;
		        $allHobbiesArr[$key]['label'] = $hobbyDetails->name;
		    }
		return view('frontend.dashboard.community_edit')->with('allMembers',$allmembersArr)->with('allHobbies',$allHobbiesArr)->with('commuity',$commuity);
	}
	public function saveCommunityCreate(Request $request){
		$community = new Community;
		$community->name = $request->name;
		$community->slug = str_slug($request->name,'_');
		$community->related_peoples = trim($request->member_ids);
		$community->related_hobbies = trim($request->related_hobby_ids);
		$community->type = $request->radios;
		$community->status = 'yes';
		$invitedByUser = \Auth::user();

		$community->created_by = $invitedByUser->id;
		if(isset($request->file()['profile_pic']) && !empty($request->file())){
    		$community->profile_pic = $request->file()['profile_pic'];
    	}
    	if(isset($request->file()['cover_pic']) && !empty($request->file())){
    		$community->cover_pic = $request->file()['cover_pic'];
    	}
		$community->save();
		Analytics::trackEvent("community_created", "community: ".$community->id."_created_by_user: ".$invitedByUser->id, "New community ".$community->name." added by User: ".$invitedByUser->id);
		//Community Members : Debasis Chakraborty
        if(trim($request->member_ids)){
        	foreach (explode(',', trim($request->member_ids)) as $key => $member_id) {
        		$comunityMember = new CommunityMembers;
        		$comunityMember->community_id = $community->id;
        		$comunityMember->user_id = $member_id;
        		$comunityMember->is_accepted = 'yes';
        		$comunityMember->save();
        		$inviteToUser = User::find($member_id);
        		$extra['invitedBy'] = $invitedByUser->name;
				$extra['communityName'] = $community->name;
        		$inviteToUser->sendNotification($invitedByUser,"community.invite",$extra,route('community_page',$community->slug()));
        	}
        }
		$community_name = str_slug($community->name,'_');
		return \Redirect::to("/community/".$community_name);
	}

	public function editCommunityCreate(Request $request){
		//dd($request);
		$community = Community::find($request->community_id);
		$community->name = $request->name;
		$community->type = $request->radios;
		$community->description = $request->description;

		if(isset($request->file()['profile_pic']) && !empty($request->file())){
    		$community->profile_pic = $request->file()['profile_pic'];
    	}
    	if(isset($request->file()['cover_pic']) && !empty($request->file())){
    		$community->cover_pic = $request->file()['cover_pic'];
    	}
		$community->save();
		return \Redirect::to("/community/".$community->slug);
	}

	public function inviteCommunity(Request $request) {
		$data['success'] = false;
		$communityInvitation = new CommunityInvitation;
		$communityInvitation->invited_to = $request->inviteTo;
		$communityInvitation->community_id = $request->communityId;
		$communityInvitation->invited_by = $request->invitedBy;
		if($communityInvitation->save()){
			$inviteToUser = User::find($communityInvitation->invited_to);
			$invitedByUser = User::find($communityInvitation->invited_by);
			$extra['invitedBy'] = $invitedByUser->name;
			$extra['communityName'] = $communityInvitation->community->name;
			$inviteToUser->sendNotification($invitedByUser,"community.invite",$extra,route('community_page',$communityInvitation->community->slug()));
			$data['success'] = true;
		}
		return \Response::json($data);
	}

	public function acceptCommunityInvitationRequest($communityid){
		$user = Auth::user();
		$community = Community::find($communityid);
		$comunityMember = new CommunityMembers;
		$comunityMember->community_id = $communityid;
		$comunityMember->user_id = $user->id;
		$comunityMember->is_accepted = 'yes';
		$comunityMember->save();
		foreach (CommunityInvitation::where('invited_to','=',$user->id)->where('community_id','=',$communityid)->get() as  $eachInvitatrion) {
			$eachInvitatrion->delete();
		}
		return \Redirect::to("/community/".$community->slug);
	}

	public function rejectCommunityInvitationRequest($communityid){
		$user = Auth::user();
		$community = Community::find($communityid);
		foreach (CommunityInvitation::where('invited_to','=',$user->id)->where('community_id','=',$communityid)->get() as  $eachInvitatrion) {
			$eachInvitatrion->delete();
		}
		return \Redirect::to("/community/".$community->slug);
	}
}
