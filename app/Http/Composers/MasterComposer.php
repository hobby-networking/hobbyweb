<?php
namespace App\Http\Composers;
use Illuminate\Contracts\View\View;
use App\Models\User;
use Auth;

class MasterComposer {

    public function compose(View $view)
    {
        $friendRequests = array();
    	$loggedInUser = null;
    	if (Auth::check()) {
    		$user = Auth::user();
	    	$allFriendRequests = $user->getFriendRequests();
	    	foreach($allFriendRequests as $eachFriendRequest){
	    		array_push($friendRequests, User::find($eachFriendRequest->sender_id));
	    	}
            $loggedInUser = $user;
    	}
        $view->with('friendRequests', $friendRequests)->with('currentLoggedInUser',$loggedInUser);
    }

}