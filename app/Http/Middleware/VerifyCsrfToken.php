<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'messages/load/chatbox',
        'user/profile/update/name',
        'user/profile/update/status',
        'user/profile/update/about_me',
        'searchHobbyName',
        'postuser',
        'postuservideo/*',
        'add_new_post',
        'cropupload',
        'crop',
        'auth/fblogin'
    ];
}
