<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Route::middleware('optimizeImages')->group(function () {


Route::get('/', [
	'as'=>'welcome_page',
	'uses'=>'PageController@getWelcomePage'
]);
Route::get('monitor.html', function () {
    return view('backend.admin.monitor');
});
Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

Route::post('cropupload', 'CropController@postUpload');
Route::post('crop', 'CropController@postCrop');

Route::get('/about_us', [
	'as'=>'about_us_page',
	'uses'=>'PageController@getAboutUsPage'
]);

Route::get('/terms_of_use', [
	'as'=>'terms_of_use_page',
	'uses'=>'PageController@getTermsOfUsePage'
]);

Route::get('/privacy_policy', [
	'as'=>'privacy_policy_page',
	'uses'=>'PageController@getPrivacyPolicyPage'
]);

Route::get('/faq', [
	'as'=>'faq_page',
	'uses'=>'PageController@getFaqPage'
]);

Route::get('/contact_us', [
	'as'=>'contact_us_page',
	'uses'=>'PageController@getContactUsPage'
]);

Route::post('/contact_us_submit', [
	'as'=>'contact_us_page_post',
	'uses'=>'PageController@getContactUsForm'
]);

Route::get('/share', function() {
    return view('frontend.social_share_page');
});

Route::get('/post-box-page', [
	'as'=>'post_box_page',
	'uses'=>'PageController@getPostBoxPage'
]);
Route::get('/signup', [
	'as'=>'signup_page',
	'uses'=>'PageController@getSignupPage'
]);

Route::get('/login', function(){
	return \Redirect::route('welcome_page');
});


Route::group(['prefix' => 'auth'], function () {
	Route::get('/logout', [
		'as' => 'auth.logout', 
		'uses' => 'Auth\AuthController@getLogout'
	]);

	Route::post('/login', [
		'as' => 'auth.login', 
		'uses' => 'Auth\AuthController@postLogin'
	]);

	Route::post('/fblogin', [
		'as' => 'auth.fblogin', 
		'uses' => 'Auth\AuthController@postFblogin'
	]);

	Route::post('/signup', [
		'as' => 'auth.signup', 
		'uses' => 'Auth\AuthController@postSignup'
	]);
});

Route::group(['prefix' => 'password'], function () {
	Route::post('/email', [
		'as' => 'password.email', 
		'uses' => 'Auth\PasswordController@postEmail'
	]);
});

Route::get('/my-hobby/{name}/{page?}', [
	'as'=>'manage_hobby_by_name',
	'uses'=>'PageController@getHobbyPageByName'
]);

Route::get('/explainer-videos', [
	'as'=>'explain_our_video',
	'uses'=>'PageController@getExplainOurVideoPage'
]);

Route::get('/hn-videos', [
	'as'=>'hn_video',
	'uses'=>'PageController@getHNVideoPage'
]);

/*
search categories = hobby, community, friends
*/
Route::get('/search/{cat?}/{slug?}', [
	'as'=>'search_slug',
	'uses'=>'PageController@getSearchPage'
]);

Route::get('/community/{slug}',[
	'as' =>'community_page',
	'uses' =>'CommunityController@addCommunityPage'
]);

Route::post('/postuser', [
	'as'=>'postuser_ajax',
	'uses'=>'PostController@postFileAjax'
]);
Route::post('/postuservideo/{filename?}', [
	'as'=>'postuser_ajax_video',
	'uses'=>'PostController@postFileVideoAjax'
]);
Route::group(['middleware' => 'auth'], function() {
	
	Route::group(['prefix' => 'user'], function () {
		Route::group(['prefix' => '/profile/update'], function () {
		    Route::post('/name', ['as' => 'update.profile.name', 'uses' => 'UserController@updateName']);
		    Route::post('/status', ['as' => 'update.profile.status', 'uses' => 'UserController@updateStatus']);
		    Route::post('/about_me', ['as' => 'update.profile.about_me', 'uses' => 'UserController@updateAboutMe']);
		});
		Route::group(['prefix' => '/all'], function () {
			Route::get('/community',['as' =>'all.user.community','uses' =>'UserController@allCommunityPage']);
			Route::get('/hobby',['as' =>'all.user.hobby','uses' =>'UserController@allHobbyPage']);
			Route::get('/friends',['as' =>'all.user.friends','uses' =>'UserController@allFriendsPage']);
			Route::get('/photos',['as' =>'all.user.photos','uses' =>'UserController@userPhotoesPage']);
		});
		Route::group(['prefix' => 'profile'], function() {
			Route::get('/verify_email', [
			    'as' => 'verify_email',
			    'middleware' => 'auth',
			    'uses' => 'UserController@verifyEmail',
			]);

			Route::get('verify/email/{confirmationCode}', [
			    'as' => 'confirm_email',
			    'uses' => 'UserController@confirmEmail'
			]);

			Route::post('/verify_phone', [
			    'as' => 'verify_phone',
			    'middleware' => 'auth',
			    'uses' => 'UserController@verifyPhone',
			]);
			Route::post('/verify_phone_otp', [
			    'as' => 'verify_phone_otp',
			    'middleware' => 'auth',
			    'uses' => 'UserController@verifyPhoneOtp',
			]);
		});
	});

	Route::get('/read/notification/{id}', [
		'as'=>'read_notification',
		'uses'=>'UserController@readNotification'
	]);
	Route::post('/user_post_like', [
		'as'=>'user_post_like',
		'uses'=>'PostController@likeUserPost'
	]);

	Route::post('/user_post_unlike', [
		'as'=>'user_post_unlike',
		'uses'=>'PostController@unlikeUserPost'
	]);

	Route::post('/remove_user_hobby', [
		'as'=>'remove_user_hobby',
		'uses'=>'AdminController@postRemoveUserHobby'
	]);

	Route::get('/create-community', [
		'as'=>'community_create',
		'uses'=>'CommunityController@getCreateCommunityPage'
	]);
	Route::get('/edit-community/{id}', [
		'as'=>'community_edit',
		'uses'=>'CommunityController@getEditCommunityPage'
	]);
	Route::get('/hide/post/{post_id}',[
		'as' =>'hide_this_post',
		'uses' =>'PostController@hideThisPost'
	]);

	Route::get('/delete/post/{post_id}',[
		'as' =>'delete_this_post',
		'uses' =>'PostController@deleteThisPost'
	]);

	Route::get('/choosehobby/{nextInvite?}',[
		'as' =>'choosehobby_page',
		'uses' =>'DashboardController@chooseHobbyPage'
	]);

	Route::get('/addhobby',[
		'as' =>'addhobby_page',
		'uses' =>'DashboardController@addHobbyPage'
	]);
	
	Route::post('/addhobby',[
		'as' =>'addhobby_ajax',
		'uses' =>'DashboardController@addHobbyAjaxPost'
	]);

	// Route::get('/community/{slug}',[
	// 	'as' =>'community_page',
	// 	'uses' =>'CommunityController@addCommunityPage'
	// ]);
	Route::get('/join_community/{slug}',[
		'as' =>'join_community_page',
		'uses' =>'CommunityController@joinAsMember'
	]);
	Route::get('/unfollow_community/{slug}',[
		'as' =>'unfollow_community_page',
		'uses' =>'CommunityController@unFollowComunity'
	]);	
	Route::get('/follow_community/{slug}',[
		'as' =>'follow_community_page',
		'uses' =>'CommunityController@followCommunity'
	]);	
	Route::get('/active_community/{slug}',[
		'as' =>'active_community_page',
		'uses' =>'CommunityController@activeCommunity'
	]);	
	Route::get('/inactive_community/{slug}',[
		'as' =>'inactive_community_page',
		'uses' =>'CommunityController@inactiveCommunity'
	]);	
				
	Route::get('/setting/{id}',[
		'as' =>'user_setting',
		'uses' =>'SettingsController@getUserSettingPage'
	]);

	Route::post('/file-upload', [
		'as'=>'hobby_photograph',
		'uses'=>'DashboardController@postHobbyPhotograph'
	]);

	Route::post('/hobbynamechecker', [
		'as'=>'hobbyname_cheker',
		'uses'=>'DashboardController@postHobbyNameChecker'
	]);
	/* BULTU  INVITATION */
	Route::get('/invites-via-social-media/{withbtn?}', [
		'as'=>'invitation',
		'uses'=>'DashboardController@inviteFriends'
	]);
	/* BULTU  INVITATION */
	Route::get('/hobby-availability', [
		'as'=>'hobby-availability',
		'uses'=>'DashboardController@checkHobbyAvailability'
	]);

	Route::post('/choosehobby_ajax', [
		'as'=>'choosehobby_ajax',
		'uses'=>'DashboardController@postChooseHobbyAjaxPost'
	]);
	Route::post('/add_remove_hobby', [
		'as'=>'add_remove_hobby',
		'uses'=>'DashboardController@postAddRemoveHobbyAjax'
	]);
    
    Route::post('/searchHobbyName', [
		'as'=>'searchobby',
		'uses'=>'DashboardController@searchHobbyName'
	]);

	Route::post('/scrollinglimit', [
		'as'=>'limitscroll',
		'uses'=>'DashboardController@scrollLimit'
	]);
    Route::post('/add_new_post', [
		'as'=>'add_new_post',
		'uses'=>'PostController@addNewPost'
	]);

	Route::post('/image_upload_demo_submit', [
		'as'=>'image_upload_demo_submit_ajax',
		'uses'=>'PostController@image_upload_demo_submit'
	]);

	Route::post('/save-user-setting', [
		'as'=>'save-user-setting',
		'uses'=>'SettingsController@saveUserSetting'
	]);
	Route::post('/save-user-profile-pic', [
		'as'=>'save-user-profile-pic',
		'uses'=>'SettingsController@saveUserProfilePicture'
	]);
	Route::post('/invite-community', [
		'as'=>'invite-community-member',
		'uses'=>'CommunityController@inviteCommunity'
	]);
	Route::get('/accept/invite-community/{communityid}', [
		'as'=>'accept_community_invitation',
		'uses'=>'CommunityController@acceptCommunityInvitationRequest'
	]);
	Route::get('/reject/invite-community/{communityid}', [
		'as'=>'reject_community_invitation',
		'uses'=>'CommunityController@rejectCommunityInvitationRequest'
	]);
	Route::post('/save-community', [
		'as'=>'save-community-post',
		'uses'=>'CommunityController@saveCommunityCreate'
	]);
	Route::post('/edit-community', [
		'as'=>'edit-community-post',
		'uses'=>'CommunityController@editCommunityCreate'
	]);
	Route::group(['prefix' => 'messages'], function () {
	    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
	    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
	    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
	    Route::post('/load/chatbox', ['as' => 'messages.load.chatbox', 'uses' => 'MessagesController@loadChatBox']);
	    Route::post('/submit', ['as' => 'messages.submit.chat', 'uses' => 'MessagesController@submitChatMessage']);
	    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
	    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
	});
	Route::group(['prefix' => 'user'], function() {
		Route::get('/block/{userId}', [
			'as'=>'block_user',
			'uses'=>'UserController@blockUser'
		]);
		Route::get('/unblock/{userId}', [
			'as'=>'unblock_user',
			'uses'=>'UserController@unblockUser'
		]);
		Route::get('/friend/{userId}', [
			'as'=>'friend_user',
			'uses'=>'UserController@friendUser'
		]);
		Route::get('/unfriend/{userId}', [
			'as'=>'unfriend_user',
			'uses'=>'UserController@unfriendUser'
		]);
		Route::get('/accept/friend/{userId}', [
			'as'=>'accept_friend_request',
			'uses'=>'UserController@acceptFriendRequest'
		]);
		Route::get('/deny/friend/{userId}', [
			'as'=>'deny_friend_request',
			'uses'=>'UserController@denyFriendRequest'
		]);
		Route::get('/accept/community/{requestId}', [
			'as'=>'accept_community_request',
			'uses'=>'CommunityController@acceptCommunityRequest'
		]);
		Route::get('/deny/community/{requestId}', [
			'as'=>'deny_community_request',
			'uses'=>'CommunityController@denyCommunityRequest'
		]);
		Route::post('/comment/add', [
			'as'=>'add_post_comment',
			'uses'=>'PostController@postAddComment'
		]);
	});
	
	Route::group(['prefix' => 'admin','middleware'=>'admin'], function() {

		Route::get('/dashboard', [
			'as'=>'admin_dashboard',
			'uses'=>'AdminController@getAdminDashboardPage'
		]);

		Route::group(['prefix' => 'manage'], function() {

			Route::get('/users', [
				'as'=>'manage_users',
				'uses'=>'AdminController@getManageUsersPage'
			]);
			Route::get('/users/send-group-email', [
				'as'=>'active_users',
				'uses'=>'AdminController@getActiveUsers'
			]);
			Route::post('/users/send-groupemail', [
				'as'=>'active_users_emailsend',
				'uses'=>'AdminController@GroupuserEmailSend'
			]);
			Route::get('/users/preview-email', [
				'as'=>'preview_email',
				'uses'=>'AdminController@PreviewEmail'
			]);
			Route::get('/users/add', [
				'as'=>'add_user',
				'uses'=>'AdminController@addUsersPage'
			]);
			Route::post('/users/add', [
				'as'=>'add_user_post',
				'uses'=>'AdminController@postAddUsers'
			]);
			Route::get('/users/edit/{id}', [
				'as'=>'edit_users',
				'uses'=>'AdminController@editUsersPage'
			]);
			Route::post('/users/edit/{id}', [
				'as'=>'edit_user_post',
				'uses'=>'AdminController@postEditUsers'
			]);
			Route::get('/users/delete/{id}', [
				'as'=>'delete_users',
				'uses'=>'AdminController@deleteUsers'
			]);
			Route::get('/users/send-mail/{id}', [
				'as'=>'send_mail_users',
				'uses'=>'AdminController@getSendmailUsersPage'
			]);
			Route::post('/users/send-mail/{id}', [
				'as'=>'send_mail_users_post',
				'uses'=>'AdminController@postSendmailUsersPage'
			]);
			Route::get('/users/show-bobbies/{id}', [
				'as'=>'show_hobbies',
				'uses'=>'AdminController@showHobbiesByUser'
			]);
			Route::group(['prefix' => 'editors_pic'], function(){
				Route::get('/category/list/{catId?}', [
					'as'=>'manage_editorspic_hobbycat_list',
					'uses'=>'AdminController@getEditorsPicHobbyCatListPage'
				]);
				Route::get('/list/{catId}', [
					'as'=>'manage_editorspic_by_hobbycat',
					'uses'=>'AdminController@getEditorsPicByHobbyCatIdPage'
				]);
				Route::get('/edit/{catId}/{epId}', [
					'as'=>'manage_editorspic_by_hobbycat_edit_page',
					'uses'=>'AdminController@getEditorsPicByHobbyCatIdEditPage'
				]);
				Route::post('/add', [
					'as'=>'manage_editorspic_by_hobbycat_add',
					'uses'=>'AdminController@postEditorsPicByHobbyCatIdAdd'
				]);
				Route::post('/edit', [
					'as'=>'manage_editorspic_by_hobbycat_edit',
					'uses'=>'AdminController@postEditorsPicByHobbyCatIdEdit'
				]);
			});
			Route::get('/communities', [
				'as'=>'manage_communities',
				'uses'=>'AdminController@getManageCommunitiesPage'
			]);
			Route::get('/community/edit/{id}', [
				'as'=>'edit_community',
				'uses'=>'AdminController@getEditCommunityPage'
			]);
			Route::post('/community/edit', [
				'as'=>'post_edit_community',
				'uses'=>'AdminController@postEditCommunity'
			]);
			Route::get('/community/delete/{id}', [
				'as'=>'delete_community',
				'uses'=>'AdminController@getDeleteCommunity'
			]);
			Route::get('/hobbies', [
				'as'=>'manage_hobbies',
				'uses'=>'AdminController@getManageHobbiesPage'
			]);
			Route::get('/hobby/alias/list/{id}', [
				'as'=>'list_hobbies_alias',
				'uses'=>'AdminController@getListHobbyAliasPage'
			]);
			Route::get('/hobby/edit/{id}', [
				'as'=>'edit_hobbies',
				'uses'=>'AdminController@getEditHobbyCategoryPage'
			]);
			Route::get('/hobby/delete/{id}', [
				'as'=>'delete_hobbies',
				'uses'=>'AdminController@getDeleteHobbyCategoryPage'
			]);

			Route::get('/hobby/list/{id}', [
				'as'=>'list_hobbies',
				'uses'=>'AdminController@getHobbyListPage'
			]);

			Route::get('/hobby/list/edit/{id}', [
				'as'=>'edit_list_hobbies',
				'uses'=>'AdminController@getEditHobbylistPage'
			]);
			Route::get('/hobby/list/delete/{id}', [
				'as'=>'delete_list_hobbies',
				'uses'=>'AdminController@getDeleteHobbylistPage'
			]);

			Route::post('/hobby/cat/add', [
				'as'=>'hobby_cat_add',
				'uses'=>'AdminController@postAddHobbyCategory'
			]);
			Route::post('/hobby/cat/edit', [
				'as'=>'hobby_cat_edit',
				'uses'=>'AdminController@postEditHobbyCategory'
			]);
			Route::post('/userhobby/update', [
				'as'=>'userhobby_update',
				'uses'=>'AdminController@updateUserHobby'
			]);
			Route::post('/hobby/add', [
				'as'=>'hobby_add',
				'uses'=>'AdminController@postAddHobby'
			]);
			Route::post('/hobby/edit', [
				'as'=>'hobby_edit',
				'uses'=>'AdminController@postEditHobby'
			]);

			Route::group(['prefix' => '/hobby/alias'], function() {
				Route::post('/add', [
					'as'=>'hobby_alias_add',
					'uses'=>'AdminController@postAddHobbyAlias'
				]);
				Route::get('/edit/{id}', [
					'as'=>'edit_hobbies_alias',
					'uses'=>'AdminController@getEditHobbyAliasPage'
				]);
				Route::get('/delete/{id}', [
					'as'=>'delete_hobbies_alias',
					'uses'=>'AdminController@getDeleteHobbyAliasPage'
				]);
				Route::post('/edit', [
					'as'=>'hobby_alias_edit',
					'uses'=>'AdminController@postEditHobbyAlias'
				]);

			});
			Route::get('/seo', [
				'as'=>'manage_seo',
				'uses'=>'AdminController@getManageSeoPage'
			]);

			Route::post('/seo/add', [
				'as'=>'seo_add',
				'uses'=>'AdminController@postAddSeo'
			]);

			Route::get('/seo/details', [
				'as'=>'seo_details',
				'uses'=>'AdminController@getSeoByPage'
			]);
			
		});
	});

});
Route::get('/{slug}',[
	'as' =>'profile_page',
	'uses' =>'DashboardController@getProfilePage'
]);