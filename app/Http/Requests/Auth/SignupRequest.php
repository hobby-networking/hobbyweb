<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'accept_term' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            //'g-recaptcha-response' => 'required|captcha',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'accept_term.required' => 'You need to accept the Terms and Conditions.',
            'name.required'  => 'Please enter your name',
            'email.required'  => 'Please enter your email',
            'email.email'  => 'Please enter valid email',
            'password.required'  => 'Please enter password',
            'g-recaptcha-response.required' => 'Please Enter Captcha',
        ];
    }
}
