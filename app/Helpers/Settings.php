<?php
/**
* Author: Sudipta Dhara
*/
namespace App\Helpers;
use App\Models\User;
use App\Models\SeoPage;

class Settings
{
	
	function __construct() {
		# code...
	}
	/*
	* Use: \Settings::base64ToImage()
	*/
	public static function base64ToImage($base64_string='', $output_file='') {
		if (!empty($base64_string) && !empty($output_file)) {
			$tmpFilePath = $output_file . '/uploads/invoice_image/';
		    \Storage::put($output_file, base64_decode($base64_string));
	    	return asset($output_file);
		}
	    return null;
	}

	public static function convert($text) {
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        if(preg_match($reg_exUrl, $text, $url)) {
            return preg_replace($reg_exUrl, "<a href=".$url[0]." target='_blank' class='post_links' data-urlive-container='.posted_url_preview'>".$url[0]."</a> <div class='posted_url_preview'></div>", $text);
        }else{
            return $text;
        }
    }

    public static function getSeodata($last_value='') {
    	$seo_details = SeoPage::where('page_name',$last_value)->first();
    	return $seo_details;
    }

    public static function putFileInStorage($filePath, $fileName, $file) {
    	return \Storage::put($filePath . $fileName, $file);
    }

	public static function urlOfFileInStorage($file_name_with_path) {
		if (\Storage::exists($file_name_with_path)) {
			return \Storage::url($file_name_with_path);
		}else if (\File::exists(public_path($file_name_with_path))) {
			return \URL::to($file_name_with_path);
		}else{
			return \URL::to('frontend/UserUploads/noimage.png');
		}
    	
    }
}