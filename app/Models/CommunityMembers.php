<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class CommunityMembers extends Model
{
	public function user() {
        return $this->hasOne('App\Models\User','id','user_id');
    }
    public function community() {
        return $this->hasOne('App\Models\Community','id','community_id');
    }
}
