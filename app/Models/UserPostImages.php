<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use URL;
use File;
use ImageOptimizer;

class UserPostImages extends Model
{
	protected $table = "userposts_images";
   
    public function getImageAttribute(){ 
        
        //$folderName = \Auth::user()->id;
        $profile_pic = $this->attributes['image'];
        $tmpFilePath = 'frontend/UserUploads/';
        $profilePicPath = public_path($tmpFilePath.$profile_pic);
        $physicalPath = $profilePicPath;
        if (File::exists($profilePicPath)) { 
            $profilePicPath = URL::to($tmpFilePath).'/'.$profile_pic;
            if (!\Storage::exists($tmpFilePath.$profile_pic)) {
                    $fileHandler = fopen($physicalPath, "r");
                    \Settings::putFileInStorage($tmpFilePath, $profile_pic, $fileHandler);
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
                }else{
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
                }
        }else{
            $profilePicPath = URL::to($tmpFilePath).'/noimage.png';
        }
        return $profilePicPath;
    }

    public function setProfileImageAttribute($value){

        if ($value!=null) {
            
            $file = $value;
            $exif = exif_read_data($file, "FILE,COMPUTED,ANY_TAG,IFD0,THUMBNAIL,COMMENT,EXIF", true);
            // $exif = exif_read_data($file);
            $image_name = str_replace(' ','_',str_replace('+','',str_replace('-','',$file->getClientOriginalName())));
            $image_name = str_replace(' ','_',time()."_".rand()."-".str_replace(' ','_',$image_name));
            $file->move('frontend/dashboard/assets/img/UserUploads/original/', $image_name);  
            $pathToImage = sprintf('frontend/dashboard/assets/img/UserUploads/%s', $image_name);
            $pathToOriginalImage = sprintf('frontend/dashboard/assets/img/UserUploads/original/%s', $image_name);
            ImageOptimizer::optimize($pathToOriginalImage, $pathToImage);
            $image = Image::make($pathToOriginalImage);
            if(!empty($exif['Orientation'])) {
                switch($exif['Orientation']) {
                    case 8:
                        $image->rotate(90);
                        break;
                    case 3:
                        $image->rotate(180);
                        break;
                    case 6:
                        $image->rotate(-90);
                        break;
                }
            }
            $image->save(); 
            \Settings::putFileInStorage($pathToImage, $image_name, $file);
            $this->attributes['image'] = $image_name;
        }
    }

    
}
