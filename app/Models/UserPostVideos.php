<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use URL;
use File;

class UserPostVideos extends Model
{
	protected $table = "userpost_video";
   
    public function getVideoAttribute(){ 
        
        //$folderName = \Auth::user()->id;
        $profile_pic = $this->attributes['video'];
        $tmpFilePath = 'uploades/video/';
        $profilePicPath = public_path($tmpFilePath.$profile_pic);
        //dd($profilePicPath);
        $physicalPath = $profilePicPath;
        if (File::exists($profilePicPath)) { 
            $profilePicPath = URL::to($tmpFilePath).'/'.$profile_pic;
            if (!\Storage::exists($tmpFilePath.$profile_pic)) {
                $fileHandler = fopen($physicalPath, "r");
                \Settings::putFileInStorage($tmpFilePath, $profile_pic, $fileHandler);
                $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
            }else{
                $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
            }
        }else{
            $profilePicPath = URL::to($tmpFilePath).'/noimage.png';
            $profilePicPath = null;
        }
        //dd($profilePicPath);
        return $profilePicPath;
    }

    
}
