<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserFollowing extends Model
{
    public function following() {
  	
  	return $this->belongsToMany('App\Models\UserFollowers', 'user_id', 'following_id');
	}
}
