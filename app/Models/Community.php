<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;
use URL;
use Image;
use Auth;
use App\Models\CommunityFollowers;
use App\Models\CommunityMembers;
use App\Models\CommunityInvitation;
use App\Models\Hobby;
use App\Models\UserHobbies;
use Carbon\Carbon;

class Community extends Model
{
    //
    protected $table = 'community';
    public function community() {
    	return $this->belongsTo('App\Models\User','user_id');
    }
    public function getIsCreatorAttribute(){
        if(Auth::user()){
          $loged_in_user_id = Auth::user()->id;
        }
        else{
          return 'no';
        }
      
      $community_creator = $this->attributes['created_by'];
      if($loged_in_user_id == $community_creator){
         return 'yes';
      }
      else{
         return 'no';
      }
    }
    public function getIsMemberAttribute(){
        if(Auth::user()){
          $loged_in_user_id = Auth::user()->id;
        }
        else{
          return 'no';
        }        
        $loged_in_user_id = Auth::user()->id;
        $findMember = CommunityMembers::where('community_id','=',$this->attributes['id'])->where('user_id','=',$loged_in_user_id)->where('is_accepted','=','yes')->first();
        if($findMember){
          return 'yes';
        }
        else{
          return 'no';
        }
    }
    public function getIsRequestedMemberAttribute(){
        if(Auth::user()){
          $loged_in_user_id = Auth::user()->id;
        }
        else{
          return 'no';
        }        
        $loged_in_user_id = Auth::user()->id;
        $findMember = CommunityMembers::where('community_id','=',$this->attributes['id'])->where('user_id','=',$loged_in_user_id)->where('is_accepted','=','no')->first();
        if($findMember){
          return 'yes';
        }
        else{
          return 'no';
        }
    }
    public function getIsFollowerAttribute(){
        if(Auth::user()){
          $loged_in_user_id = Auth::user()->id;
        }
        else{
          return 'no';
        }        
        $loged_in_user_id = Auth::user()->id;
        $findFollwer = CommunityFollowers::where('community_id','=',$this->attributes['id'])->where('user_id','=',$loged_in_user_id)->first();
        if($findFollwer){
          return 'yes';
        }
        else{
          return 'no';
        }
    }    
    public function getTotalMemberCountAttribute(){
        return $totalMemberCount = CommunityMembers::where('community_id','=',$this->attributes['id'])->where('is_accepted','=','yes')->count();
    }
    public function getTotalFollowerCountAttribute(){
        return $totalFollowerCount = CommunityFollowers::where('community_id','=',$this->attributes['id'])->where('is_active','=','yes')->count();
    }            
    public function getProfilePicAttribute() {
        $profile_pic = $this->attributes['profile_pic'];
        $tmpFilePath = 'frontend/dashboard/assets/img/community/';
        $profilePicPath = public_path($tmpFilePath).$profile_pic;
        $physicalPath = $profilePicPath;
        if(!empty($profile_pic)){
            if (File::exists($profilePicPath)) { 
                $profilePicPath = URL::to($tmpFilePath).'/'.$profile_pic;
                if (!\Storage::exists($tmpFilePath.$profile_pic)) {
                    $fileHandler = fopen($physicalPath, "r");
                    \Settings::putFileInStorage($tmpFilePath, $profile_pic, $fileHandler);
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
                }else{
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
                }
            }else{
                $profilePicPath = URL::to('backend/admin/assets/img/').'/user.png';
            }
        }else{
            $profilePicPath = URL::to('backend/admin/assets/img/').'/user.png';
        }
        return $profilePicPath;
    }

    public function setProfilePicAttribute($value) {

        if ($value!=null) {
        	$file = $value;
            $exif = exif_read_data($file);
            $image_name = str_replace(' ','_',str_replace('+','',str_replace('-','',$file->getClientOriginalName())));
            $image_name = str_replace(' ','_',time()."_".rand()."-".str_replace(' ','_',$image_name));
            $tmpFilePath = 'frontend/dashboard/assets/img/community/';
            $file->move($tmpFilePath, $image_name);  
            $image = \Image::make(sprintf('%s', $tmpFilePath.$image_name));
            if(!empty($exif['Orientation'])) {
                switch($exif['Orientation']) {
                    case 8:
                        $image->rotate(90);
                        break;
                    case 3:
                        $image->rotate(180);
                        break;
                    case 6:
                        $image->rotate(-90);
                        break;
                }
            }
            $image->save(); 
            $filePath = public_path($tmpFilePath.$image_name);
            \ImageOptimizer::optimize($filePath);
            \Settings::putFileInStorage($tmpFilePath, $image_name, $file);
            $this->attributes['profile_pic'] = $image_name;
        }
    }

    public function getCoverPicAttribute() { 
        
        $profile_pic = $this->attributes['cover_pic'];
        $tmpFilePath = 'frontend/dashboard/assets/img/community/';
        $profilePicPath = public_path($tmpFilePath).$profile_pic;
        $physicalPath = $profilePicPath;
        if(!empty($profile_pic)){
            if (File::exists($profilePicPath)) { 
                $profilePicPath = URL::to($tmpFilePath).'/'.$profile_pic;
                if (!\Storage::exists($tmpFilePath.$profile_pic)) {
                    $fileHandler = fopen($physicalPath, "r");
                    \Settings::putFileInStorage($tmpFilePath, $profile_pic, $fileHandler);
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
                }else{
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
                }
            }else{
                $profilePicPath = URL::to('backend/admin/assets/img/').'/user.png';
            }
        }else{
            $profilePicPath = URL::to('backend/admin/assets/img/').'/user.png';
        }
        
        return $profilePicPath;
    }

    public function setCoverPicAttribute($value) {

        if ($value!=null) {
           
            $file = $value;
            $exif = exif_read_data($file);
            $image_name = str_replace(' ','_',str_replace('+','',str_replace('-','',$file->getClientOriginalName())));
            $image_name = str_replace(' ','_',time()."_".rand()."-".str_replace(' ','_',$image_name));
            $tmpFilePath = 'frontend/dashboard/assets/img/community/';
            $file->move($tmpFilePath, str_replace(' ','_',$image_name));  
            $image = \Image::make(sprintf('%s', $tmpFilePath.str_replace(' ','_',$image_name)));
            if(!empty($exif['Orientation'])) {
                switch($exif['Orientation']) {
                    case 8:
                        $image->rotate(90);
                        break;
                    case 3:
                        $image->rotate(180);
                        break;
                    case 6:
                        $image->rotate(-90);
                        break;
                }
            }
            $image->save(); 
            $filePath = public_path($tmpFilePath.$image_name);
            \ImageOptimizer::optimize($filePath);
            \Settings::putFileInStorage($tmpFilePath, $image_name, $file);
            $this->attributes['cover_pic'] = str_replace(' ','_',$image_name);
        }
    }

    public function relatedHobbies() {
        $myArray = explode(',', $this->attributes['related_hobbies']); 
        $hobbies = null;
        foreach($myArray as $eachValue) {
            $hobbies[] = Hobby::find((int)$eachValue);
        }
        return $hobbies;
    }

    public function posts() {
        $posts = null;
        //foreach($this->relatedHobbies() as $eachHobby) {
        foreach(UserPost::where('type','=','community')->where('community_id','=',$this->attributes['id'])->orderBy('created_at','desc')->get() as $eachPost) {
            if($eachPost->visibility == 'public') {
                $posts[] = array('post'=>$eachPost, 'date'=>Carbon::parse($eachPost->updated_at)->format('Y-m-d h:i:s'));
            }else{
                if($eachPost->isAllowerdToView()){
                    $posts[] = array('post'=>$eachPost, 'date'=>Carbon::parse($eachPost->updated_at)->format('Y-m-d h:i:s'));
                }
            }
            
        }
        if(!empty($posts)){
           $this->array_sort_by_column($posts, 'date'); 
        }
        
        $finalPosts = array();
        if(!empty($posts)){
            foreach($posts as $eachPost) {
                $finalPosts[] = $eachPost['post'];
            }
        }
        return $finalPosts;
    }

    public function array_sort_by_column(&$array, $column, $direction = SORT_DESC) {
        $reference_array = array();
        foreach($array as $key => $row) {
            $reference_array[$key] = $row[$column];
        }
        array_multisort($reference_array, $direction, $array);
    }

    public function members() {
        return $this->hasMany('App\Models\CommunityMembers');
    }
    public function creator() {
        return User::find($this->attributes['created_by']);
    }

    public function slug() {
        return str_slug($this->attributes['name'],'_');
    }

    public function invitedMembersBy($user_id) {
        return CommunityInvitation::where('invited_by','=',$user_id)->where('community_id','=',$this->attributes['id'])->get();
    }
}
