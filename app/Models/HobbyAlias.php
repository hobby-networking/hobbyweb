<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HobbyAlias extends Model
{
    public function hobby() {
      return $this->belongsTo('App\Models\Hobby','hobby_id');

    }
}
