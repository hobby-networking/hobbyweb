<?php

namespace App\Models;
use URL;
use File;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use App\Models\Settings;
use App\Models\Community;
use App\Models\CommunityMembers;
use App\Models\CommunityInvitation;
use Hootlex\Friendships\Traits\Friendable;
use App\Models\UserHobbies;
use Cmgmyr\Messenger\Traits\Messagable;
use Fenos\Notifynder\Notifable;
use Junaidnasir\Larainvite\InviteTrait;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class User extends Authenticatable
{
    use Friendable;
    use Messagable;
    use Notifable;
    use InviteTrait;
    use AuthenticableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hobbyWisefriends($hobby=null){
        $friends = $this->getFriends();
        // dd($friends);
        $finalfriends = array();
        if (!empty($hobby)) {
            foreach ($friends as $eachFriend) {
                //$eachFriend = app('App\Models\User')->where('id','=',$eachFriendd->recipient_id)->first();
                foreach($eachFriend->hobbies as $eachUserHobby){
                    if($eachUserHobby->hobby_id == $hobby->id) {
                        $finalfriends[] = $eachFriend;
                        break;
                    }
                }
            }
        }else{
            $finalfriends = $friends;
        }
        return $finalfriends;
    }

    public function hasFriendRequestTo($recipient)
    {
        return \Hootlex\Friendships\Models\Friendship::whereRecipient($recipient)->whereSender($this)->whereStatus(\Hootlex\Friendships\Status::PENDING)->exists();
    }

    public function getUsernameAttribute(){ 

        if(!empty($this->userSettings)){
            return $this->userSettings->username;
        }else{
            $settings = new Settings;
            $settings->user_id = $this->attributes['id'];
            $settings->username = Settings::generateusername($this);
            // dd($this->userSettings);
            if(empty($this->userSettings)){
                $settings->save();
            }
            return $settings->username;
        }
        return null;
    }

    public function getProfilePicAttribute(){ 
        
        $profile_pic = $this->attributes['profile_pic'];
        $tmpFilePath = 'frontend/dashboard/assets/img/users/';
        $profilePicPath = public_path($tmpFilePath).$profile_pic;
        $physicalPath = $profilePicPath;
        if(!empty($profile_pic)){
            if (File::exists($profilePicPath)) { 
                $profilePicPath = URL::to($tmpFilePath).'/'.$profile_pic;
                if (!\Storage::exists($tmpFilePath.$profile_pic)) {
                    $fileHandler = fopen($physicalPath, "r");
                    \Settings::putFileInStorage($tmpFilePath, $profile_pic, $fileHandler);
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
                }else{
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
                }
            }else{
                $profilePicPath = URL::to('backend/admin/assets/img/').'/user.png';
            }
        }else{
            
            $profilePicPath = URL::to('backend/admin/assets/img/').'/user.png';
        }
        
        return $profilePicPath;
    }

    public function setProfilePicAttribute($value){

        if ($value!=null) {
           
            $file = $value;
            $exif = exif_read_data($file);
            $image_name = str_replace(' ','_',str_replace('+','',str_replace('-','',$file->getClientOriginalName())));
            $image_name = str_replace(' ','_',time()."_".rand()."-".str_replace(' ','_',$image_name));
            $tmpFilePath = 'frontend/dashboard/assets/img/users/';
            $file->move($tmpFilePath, $image_name);  
            $image = \Image::make(sprintf('%s', $tmpFilePath.$image_name));
            if(!empty($exif['Orientation'])) {
                switch($exif['Orientation']) {
                    case 8:
                        $image->rotate(90);
                        break;
                    case 3:
                        $image->rotate(180);
                        break;
                    case 6:
                        $image->rotate(-90);
                        break;
                }
            }
            $image->save();
            $filePath = public_path($tmpFilePath.$image_name);
            \ImageOptimizer::optimize($filePath); 
            \Settings::putFileInStorage($tmpFilePath, $image_name, $file);
            $this->attributes['profile_pic'] = $image_name;
        }
    }
    public function setProfilePicRawAttribute($value){
        $this->attributes['profile_pic'] = $value;
    }
    public function getCoverPicAttribute(){ 
        
        $cover_pic = $this->attributes['cover_pic'];
        $tmpFilePath = 'frontend/dashboard/assets/img/users/';
        $profilePicPath = public_path($tmpFilePath).$cover_pic;
        $physicalPath = $profilePicPath;
        if(!empty($cover_pic)){
            if (File::exists($profilePicPath)) { 
                $profilePicPath = URL::to($tmpFilePath).'/'.$cover_pic;
                if (!\Storage::exists($tmpFilePath.$cover_pic)) {
                    $fileHandler = fopen($physicalPath, "r");
                    \Settings::putFileInStorage($tmpFilePath, $cover_pic, $fileHandler);
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$cover_pic);
                }else{
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$cover_pic);
                }
            }else{
                $profilePicPath = URL::to('frontend/dashboard/assets/img/').'/pp.jpg';
            }
        }else{
            
            $profilePicPath = URL::to('frontend/dashboard/assets/img/').'/pp.jpg';
        }
        
        return $profilePicPath;
    }

    public function setCoverPicAttribute($value){
        if ($value!=null) {
            $file = $value;
            $exif = exif_read_data($file);
            
            $image_name = str_replace(' ','_',str_replace('+','',str_replace('-','',$file->getClientOriginalName())));
            $image_name = str_replace(' ','_',time()."_".rand()."-".str_replace(' ','_',$image_name));
            $tmpFilePath = 'frontend/dashboard/assets/img/users/';
            $file->move($tmpFilePath, $image_name);  
            $image = \Image::make(sprintf('%s', $tmpFilePath.$image_name));
            if(!empty($exif['Orientation'])) {
                switch($exif['Orientation']) {
                    case 8:
                        $image->rotate(90);
                        break;
                    case 3:
                        $image->rotate(180);
                        break;
                    case 6:
                        $image->rotate(-90);
                        break;
                }
            }
            $image->save(); 
            $filePath = public_path($tmpFilePath.$image_name);
            \ImageOptimizer::optimize($filePath);
            \Settings::putFileInStorage($tmpFilePath, $image_name, $file);
            $this->attributes['cover_pic'] = $image_name;
        }
    }
    public function isAdmin() {
        return ($this->attributes['type'] == "admin");
    }

    public function isMember() {
        //dd(1); 
        return ($this->attributes['type'] == "user");
    }

    public function userSettings() { //dd(1); 
        return $this->hasOne('App\Models\Settings');
    }

    public function hobbies() {
        return $this->hasMany('App\Models\UserHobbies');
    }

    public function hobbiesmodel() {
        $userHobbies = UserHobbies::where('user_id','=',$this->attributes['id']);
        return $userHobbies;
    }
    
    public function communities() {
        return $this->hasMany('App\Models\Community','created_by');
    }
    public function joinedCommunities() {
        return $this->hasMany('App\Models\CommunityMembers','user_id');
    }

    public function communityList($hobby=null) {
        $communityList = array();
        $ownCreaded = $this->communities;
        $joined = $this->joinedCommunities;
        foreach($ownCreaded as $eachCommunity){
            if(empty($hobby)){
                $communityList[] = $eachCommunity;
            }else{
                $related_hobbies = explode(',', $eachCommunity->related_hobbies);
                if(in_array($hobby->id, $related_hobbies)){
                    $communityList[] = $eachCommunity;
                }
            }
            
        }
        foreach($joined as $eachCommunity){
            if(empty($hobby)){
                $communityList[] = $eachCommunity->community;
            }else{
                $related_hobbies = explode(',', $eachCommunity->community->related_hobbies);
                if(in_array($hobby->id, $related_hobbies)){
                    $communityList[] = $eachCommunity->community;
                }
            }
        }
        return $communityList;
    }

    public function getPostsAttribute() {
        $posts = UserPost::where('user_id','=',$this->attributes['id'])->where('type','=','user')->get();
        return $posts;
    }

    public function gallery() {
        return $this->hasMany('App\Models\UserPostImages','user_id');
    }

    public function getUserById($id = null){

        return $this->find($id)->first();

    }

    public static function getConnectedUser(){

        $user = Auth::user();
        $userHobbies = $user->hobbies;

        $listArrCurrUser = array();
        $listArrAllUser = array();
        $listUser = array();

            if(!empty($userHobbies)){
                foreach ($userHobbies as $key => $eachHobby) {
                    $listArr[$key] = $eachHobby->hobby_id;
                }
            }

            $allUser = User::where('type','=',"user")->where('is_active','=',"yes")->where('id','!=',$user->id)->get();

            foreach ($allUser as $keyUser => $eachUser) {
                
                $eachUserHobby = $eachUser->hobbies;
                
                if(!empty($eachUserHobby)) {

                    foreach ($eachUserHobby as $i => $allUserHobby) {
                        
                        $listArrAllUser[$i] = $allUserHobby->hobby_id;
                    }
                }
                $matchFound = array_intersect($listArr, $listArrAllUser);

                if(is_array($matchFound) && count($matchFound)>0){
                    $listUser[$keyUser] = $eachUser->id;
                }

            }

        return $listUser;
    }
    public function getConnectedUserByUser($hobby=null){
        //dd(1);
        $user = $this;
        $userHobbies = $user->hobbies;
        $friends = array();
        foreach($user->getFriends() as $eachFriend){
            $friends[] = $eachFriend->recipient_id;
        }
        $listArrCurrUser = array();
        $listArrAllUser = array();
        $listUser = array();

        if(!empty($userHobbies)){
            if(empty($hobby)){
                foreach ($userHobbies as $key => $eachHobby) {
                    $listArr[$key] = $eachHobby->hobby_id;
                }
            }else{
                $listArr[0]=$hobby->id;
            }
        }

        $allUser = User::where('type','=',"user")->where('is_active','=',"yes")->where('id','!=',$user->id)->get();
        foreach ($allUser as $keyUser => $eachUser) {
            $eachUserHobby = $eachUser->hobbies;
            if(!empty($eachUserHobby)) {
                foreach ($eachUserHobby as $i => $allUserHobby) {
                    $listArrAllUser[$i] = $allUserHobby->hobby_id;
                }
            }
            $matchFound = array_intersect($listArr, $listArrAllUser);
            if(is_array($matchFound) && count($matchFound)>0 && !in_array($eachUser->id, $friends)){
                $listUser[$keyUser] = $eachUser->id;
            }
        }
        return $listUser;
    }
    public function sendNotification($fromUser,$category,$extra=array(),$url = null) {
        if(empty($url))
        {
            $url = \URL::to('/');
        }
        if ($category == 'post.like') {
            $extra['img'] = $fromUser->profile_pic;
            $extra['user_name'] = $fromUser->name;
        }
        \Notifynder::category($category)
            ->from($fromUser->id)
            ->to($this->attributes['id'])
            ->url($url)
            ->extra($extra)
            ->send();
    }

    public function communityJoinRequest() {
        $joinRequest = array();
        foreach (Community::where('created_by','=',$this->attributes['id'])->where('status','=','yes')->get() as $eachCommunity) {
            foreach (CommunityMembers::where('community_id','=',$eachCommunity->id)->where('is_accepted','=','no')->get() as $eachJoinRequest) {
                $joinRequest[] = $eachJoinRequest;
            }
        }
        return $joinRequest;
    }

    public function getPeopleWithSimilarHobby(){
        $user = $this;
        $userHobbies = $user->hobbies;

        $listArrCurrUser = array();
        $listArrAllUser = array();
        $listUser = array();
        $friends = array();
        foreach($user->getFriends() as $eachFriend){
            $friends[] = $eachFriend->recipient_id;
        }
        //dd($friends);
        if(!empty($userHobbies)){
            foreach ($userHobbies as $key => $eachHobby) {
                $listArr[$key] = $eachHobby->hobby_id;
            }
        }

        $allUser = User::where('type','=',"user")->where('is_active','=',"yes")->where('id','!=',$user->id)->get();
        foreach ($allUser as $keyUser => $eachUser) {
            $eachUserHobby = $eachUser->hobbies;
            if(!empty($eachUserHobby)) {
                foreach ($eachUserHobby as $i => $allUserHobby) {
                    $listArrAllUser[$i] = $allUserHobby->hobby_id;
                }
            }
            $matchFound = array_intersect($listArr, $listArrAllUser);
            if(is_array($matchFound) && count($matchFound)>0 && !in_array($eachUser->id, $friends)){
                $listUser[$keyUser] = $eachUser->id;
            }
        }
        shuffle($listUser);
        return $listUser;
    }

    public function suggestedCommunities() {
        $userHobbies = $this->hobbies;
        $communityList = array();
        $communityId = array();
        $userCommunities = array();
        foreach ($this->communityList() as $eachUserCommunity) {
            $userCommunities[] = $eachUserCommunity->id;
        }
        //dd($userCommunities);
        if(!empty($userHobbies)){
            foreach ($userHobbies as $key => $eachHobby) {
                foreach($eachHobby->hobby->suggestedCommunities() as $eachCommunity) {
                    if(!in_array($eachCommunity->id, $communityId) && !in_array($eachCommunity->id, $userCommunities)){
                        $communityId[] = $eachCommunity->id;
                        $communityList[] = $eachCommunity;
                    }
                }
            }
        }
        shuffle($communityList);
        return $communityList;
    }

    public function isInvitedToJoinCommunity($community) {
        if(count(CommunityInvitation::where('community_id','=',$community->id)->where('invited_to','=',Auth::user()->id)->get())>0){
            return true;
        }
        return false;
    }
}
