<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HobbyCategory extends Model
{
    public function hobbies() {
    	return $this->hasMany('App\Models\Hobby','cat_id');
    }

    public function editorsPic() {
    	return $this->hasMany('App\Models\HobbyCatEditorsPic','hobby_cat_id');
    }
}
