<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = "settings";

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public static function generateusername($user) {
    	if(!empty($user)){ //dd($user->userSettings);
    		if(empty($user->userSettings)){
    			$username = str_slug($user->name,'_');
    			if(!empty($settings = Settings::where('username','=',$username)->first())) {
    				$username = $username.'_'.$user->id;
    			}
    			return $username;
    		}
    		return $user->userSettings->username;
    	}
    	return null;
    }
}
