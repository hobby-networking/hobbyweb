<?php

namespace App\Models;
use File;
use Image;
use URL;
use Intervention\Image\ImageManager;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserPost;
use App\Models\Community;
use Auth;

class Hobby extends Model
{
    public function category() {
    	return $this->belongsTo('App\Models\HobbyCategory','cat_id');
    }

    public function alias() {
        return $this->hasMany('App\Models\HobbyAlias','hobby_id');
    }

    public function suggestedCommunities() {
        $communityList = array();
        $suggestedCommunities = Community::whereRaw('find_in_set(?, related_hobbies)', [$this->attributes['id']])->get();
        $userCommunities = array();
        foreach (Auth::user()->communityList() as $eachUserCommunity) {
            $userCommunities[] = $eachUserCommunity->id;
        }
        foreach ($suggestedCommunities as $eachSuggestedCommunity) {
            if (!in_array($eachSuggestedCommunity->id, $userCommunities)) {
                $communityList[] = $eachSuggestedCommunity;
            }
        }
        return $communityList;
    }

    public function allUserHobby() {
        return UserPost::where("user_id","=",$user->id)->where('type','=','user')->orderBy('created_at','desc');
    }
    public function allUserPost($take=0,$skip=0) {
        if($take == 0){
            return UserPost::whereRaw('find_in_set(?, hobby_id)', [$this->attributes['id']])->where('type','=','user')->orderBy('created_at','desc')->get();
        }else{
            return UserPost::whereRaw('find_in_set(?, hobby_id)', [$this->attributes['id']])->where('type','=','user')->orderBy('created_at','desc')->take($take)->skip($skip)->get();
        }
    }
    public function userPostHobby() {
        return UserPost::where('hobby_id','=',$this->attributes['id'])->where('user_id','=',\Auth::user()->id)->orderBy('created_at','desc')->get();
    }
    public function allHobbyCat(){
        return HobbyCategory::where('is_active','=','yes')->get();
    }
    public function getProfilePicAttribute(){ 
        $profile_pic = $this->attributes['profile_pic'];
        $tmpFilePath = 'frontend/dashboard/assets/img/hobby/';
        $profilePicPath = public_path($tmpFilePath.$profile_pic);
        $physicalPath = $profilePicPath;
        //dd($profilePicPath);
        if (File::exists($profilePicPath) && !empty($profile_pic)) { 
            $profilePicPath = URL::to($tmpFilePath).'/'.$profile_pic;
            if (!\Storage::exists($tmpFilePath.$profile_pic)) {
                $fileHandler = fopen($physicalPath, "r");
                \Settings::putFileInStorage($tmpFilePath, $profile_pic, $fileHandler);
                $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
            }else{
                $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
            }
        }else{
            $profilePicPath = URL::to($tmpFilePath.'../').'/no_img.jpg';
        }
        //$manager = new ImageManager(array('driver' => 'imagick'));
        //$profilePicPath = $manager->make($profilePicPath)->resize(240, 240);
        //$profilePicPath = $manager->make($profilePicPath)->resize(240);
        //dd($profilePicPath);
        return $profilePicPath;
    }

    public static function setProfilePic($file){
        if ($file!=null) {
         $file = $file['file'];
            $exif = exif_read_data($file);
            $image_name = str_replace(' ','_',str_replace('+','',str_replace('-','',$file->getClientOriginalName())));
            $image_name = str_replace(' ','_',time()."_".rand()."-".str_replace(' ','_',$image_name));
            $tmpFilePath = 'frontend/dashboard/assets/img/hobby/';
            $file->move($tmpFilePath, $image_name);  
            $image = Image::make(sprintf('%s', $tmpFilePath.$image_name));
            if(!empty($exif['Orientation'])) {
                switch($exif['Orientation']) {
                    case 8:
                        $image->rotate(90);
                        break;
                    case 3:
                        $image->rotate(180);
                        break;
                    case 6:
                        $image->rotate(-90);
                        break;
                }
            }
            $image->save(); 
            $filePath = public_path($tmpFilePath.$image_name);
            \ImageOptimizer::optimize($filePath);
            \Settings::putFileInStorage($tmpFilePath, $image_name, $file);
            return $image_name;
        }
    }
    public function setProfilePicAttribute($value){
        if ($value!=null) {
            $iid = "";
            if(isset($this->attributes['id'])){
                $iid = $this->attributes['id'];
            }else{
                $iid="new";
            }
          $file = $value;
          $exif = exif_read_data($file);
            $image_name = str_replace(' ','_',str_replace('+','',str_replace('-','',$file->getClientOriginalName())));
            $image_name = str_replace(' ','_',time()."_".rand()."-".str_replace(' ','_',$image_name));
            $tmpFilePath = 'frontend/dashboard/assets/img/hobby/';
            $file->move($tmpFilePath, $image_name);  
            $image = Image::make(sprintf('%s', $tmpFilePath.$image_name));
            if(!empty($exif['Orientation'])) {
                switch($exif['Orientation']) {
                    case 8:
                        $image->rotate(90);
                        break;
                    case 3:
                        $image->rotate(180);
                        break;
                    case 6:
                        $image->rotate(-90);
                        break;
                }
            }
            $filePath = public_path($tmpFilePath.$image_name);
            \ImageOptimizer::optimize($filePath);
            $image->save(); 
            \Settings::putFileInStorage($tmpFilePath, $image_name, $file);
            $this->attributes['profile_pic'] = $image_name;
        }
    }

    public function posts() {
        return $this->hasMany('App\Models\UserPost','hobby_id');
    }

    public function slug() {
        $hobbyName = strtolower($this->attributes['name']);
        $hobbyNameArr = explode(' ', $hobbyName);

        if(count($hobbyName)>0)
            $hobbyName = implode('_',$hobbyNameArr);
        return $hobbyName;
    }

}