<?php

namespace App\Models;
use App\Models\Hobby;
use Illuminate\Database\Eloquent\Model;

class UserHobbies extends Model
{
    public function hobby() {
    	return $this->hasOne('App\Models\Hobby','id','hobby_id');
    }
    public function user() {
    	return $this->hasOne('App\Models\User','id','user_id');
    }
    public function getConnectedUserByHobby(){

        $user = $this->user;
        $userHobbies = $user->hobbies;

        $listArrCurrUser = array();
        $listArrAllUser = array();
        $listUser = array();

            if(!empty($userHobbies)){
            	$listArr[0] = $this->attributes['hobby_id'];
                // foreach ($userHobbies as $key => $eachHobby) {
                //     $listArr[$key] = $eachHobby->hobby_id;
                // }
            }

            $allUser = User::where('type','=',"user")->where('is_active','=',"yes")->where('id','!=',$user->id)->get();
            foreach ($allUser as $keyUser => $eachUser) {
                $eachUserHobby = $eachUser->hobbies;
                if(!empty($eachUserHobby)) {
                    foreach ($eachUserHobby as $i => $allUserHobby) {
                        $listArrAllUser[$i] = $allUserHobby->hobby_id;
                    }
                }
                $matchFound = array_intersect($listArr, $listArrAllUser);
                if(is_array($matchFound) && count($matchFound)>0){
                    $listUser[$keyUser] = $eachUser->id;
                }
            }
        return $listUser;
    }
}
