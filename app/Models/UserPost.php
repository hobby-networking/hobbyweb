<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\UserPostImages;
use App\Models\PostLike;
use App\Models\HideUserPost;
use URL;
use File;
use Auth;
use App\Helpers\Settings as SettingsHelper;
use ImageOptimizer;

class UserPost extends Model
{
	protected $table = "userposts";
   
    public function isAllowerdToView() { 
        $user = Auth::user();
        $allowed = false;
        if (!Auth::check() && $this->attributes['visibility'] == "public") {
            return true;
        }
        if (!empty($user) && empty(HideUserPost::where('user_id','=',$user->id)->where('post_id','=',$this->attributes['id'])->first()) ) {
            if ($this->attributes['type'] == "community") {
                //dd($user->communities);
                foreach($user->joinedCommunities as $eachCommunity) {
                    if ($this->attributes['community_id'] == $eachCommunity->community_id) {
                       $allowed = true;
                    }
                }
                foreach($user->communities as $eachCommunity) {
                    if ($this->attributes['community_id'] == $eachCommunity->id) {
                       $allowed = true;
                    }
                }
            }
            if ($this->attributes['type'] == "user") {
                foreach($user->hobbies as $eachHobby) {
                    //dd($eachHobby);
                    if (in_array($eachHobby->hobby_id, explode(',',$this->attributes['hobby_id']))) {
                        $allowed = true;
                    }
                }
            }
            if($user->isAdmin()){
                $allowed = true;
            }
        }
        return $allowed;
    }

    public function community() { 
        return $this->hasOne('App\Models\Community','community_id');
    }

    public function getUplodedPicAttribute(){ 
        
        //$folderName = \Auth::user()->id;
        $profile_pic = $this->attributes['image'];
        $tmpFilePath = 'frontend/UserUploads/';
        $profilePicPath = public_path($tmpFilePath.$profile_pic);
        $physicalPath = $profilePicPath;
        if (File::exists($profilePicPath)) { 
            $profilePicPath = URL::to($tmpFilePath).'/'.$profile_pic;
            if (!\Storage::exists($tmpFilePath.$profile_pic)) {
                    $fileHandler = fopen($physicalPath, "r");
                    \Settings::putFileInStorage($tmpFilePath, $profile_pic, $fileHandler);
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
                }else{
                    $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$profile_pic);
                }
        }else{
            $profilePicPath = URL::to($tmpFilePath).'/user.png';
        }
        return $profilePicPath;
    }
    public static function setPostFile($file){
     $result = [];
     $uploadedFileName = '';
     $filePath ='';
     $tmpFilePath = 'frontend/UserUploads/';
        if ($file['name']!=null) {
            $image_name = time()."_".rand()."-".basename(str_replace(' ','_',str_replace('+','',str_replace('-','',$file['name']))));
            $filePath = public_path($tmpFilePath.$image_name);
            if(move_uploaded_file($file['tmp_name'], $filePath)){ 
                $uploadedFileName = $image_name;
                $exif = exif_read_data($filePath);
                $image = \Image::make($filePath);
                if(!empty($exif['Orientation'])) {
                    switch($exif['Orientation']) {
                        case 8:
                            $image->rotate(90);
                            break;
                        case 3:
                            $image->rotate(180);
                            break;
                        case 6:
                            $image->rotate(-90);
                            break;
                    }
                }
                $image->save();
                ImageOptimizer::optimize($filePath);
		 $fileHandler = fopen($filePath, "r");
                \Settings::putFileInStorage($tmpFilePath, $image_name, $fileHandler);
            }else{
                $uploadedFileName = null;
            }
        }
        $result['uploadedFileName'] = $uploadedFileName;
        $result['filePath'] = \Settings::urlOfFileInStorage($tmpFilePath.$image_name);
       return  $result;    
    }
    public static function setUplodedPic($value){

    	$img_name_array = array();
        $tmpFilePath = 'frontend/UserUploads/';
        
        for($i=0;$i<count($value['name']);$i++){
            $value['name'] = str_replace(' ','_',str_replace('+','',str_replace('-','',$value['name'])));
	        if ($value['name'][$i]!=null) {
	            
	            //$file = $value[$i];
	            $image_name = time()."_".rand()."-".basename($value['name'][$i]);
                $profilePicPath = public_path($tmpFilePath.$image_name);
                if(move_uploaded_file($value['tmp_name'][$i], $profilePicPath)){
    	            //$file->move('frontend/UserUploads/', $image_name);  
    	            //$image = Image::make(sprintf('frontend/UserUploads/%s', $image_name));
    	            //$image->save(); 
    	            $img_name_array[$i] = $image_name;
                    \Storage::putFileInStorage($tmpFilePath, $image_name, $value['tmp_name'][$i]);
                }else{

                    $img_name_array[$i] = null;
                }
	        }
	    }

	    return $img_name_array;
    }

    public function post_images() {
        return $this->hasMany('App\Models\UserPostImages');
    }

    public function userPostImages($user_id=null,$limit=null){

        return UserPostImages::where("post_id","=",$this->attributes['id'])->where("user_id","=",$user_id)->take($limit)->get();

    }

    public function getUserByPost(){

        return $this->belongsTo('App\Models\User','user_id');
    }

    public function post_videos() {
        return $this->hasMany('App\Models\UserPostVideos');
    }

    public function comments() {
        return $this->hasMany('App\Models\PostComments','post_id');
    }

    public function comments_desc() {
        return \App\Models\PostComments::where('post_id',$this->attributes['id'])->orderBy('created_at', 'DESC')->get();
    }

    public function userPostVideos($user_id=null){

        return UserPostVideos::where("post_id","=",$this->attributes['id'])->where("user_id","=",$user_id)->get();

    } 
    public function isLikedBy($user_id) {
       if(!empty(PostLike::where('user_id','=',$user_id)->where('post_id','=',$this->attributes['id'])->first())){
            return true;
       }
       return false;
    }   

    public function likeCount() {
        return count(PostLike::where('post_id','=',$this->attributes['id'])->get());
    }

    public function getContentAttribute() {
        return SettingsHelper::convert($this->attributes['content']);
    }
}
