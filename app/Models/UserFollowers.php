<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserFollowers extends Model
{
    public function followers() {
  
  	return $this->belongsToMany('App\Models\UserFollowing', 'follower_id', 'user_id');
	}
}
