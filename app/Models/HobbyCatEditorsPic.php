<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;
use Image;
use URL;

class HobbyCatEditorsPic extends Model
{
    public function setImageAttribute($value){
        if ($value!=null) {
            $iid = "";
            if(isset($this->attributes['id'])){
                $iid = $this->attributes['id'];
            }else{
                $iid="new";
            }
            $file = $value;
            $image_name = str_replace(' ','_',str_replace('+','',str_replace('-','',$file->getClientOriginalName())));
            $image_name = str_replace(' ','_',time()."_".rand()."-".str_replace(' ','_',$image_name));
            $tmpFilePath = 'frontend/dashboard/assets/img/';
            $file->move($tmpFilePath, $image_name);  
            $image = Image::make(sprintf('%s', $tmpFilePath.$image_name));
            $image->save(); 
            $filePath = public_path($tmpFilePath.$image_name);
            \ImageOptimizer::optimize($filePath);
            \Settings::putFileInStorage($tmpFilePath, $image_name, $file);
            $this->attributes['image'] = $image_name;
        }
    }
    public function getImageAttribute(){ 
        $image = $this->attributes['image'];
        $tmpFilePath = 'frontend/dashboard/assets/img/';
        $profilePicPath = public_path($tmpFilePath.$image);
        $physicalPath = $profilePicPath;
        if (File::exists($profilePicPath) && !empty($image)) { 
            $profilePicPath = URL::to($tmpFilePath).'/'.$image;
            if (!\Storage::exists($tmpFilePath.$image)) {
                $fileHandler = fopen($physicalPath, "r");
                \Settings::putFileInStorage($tmpFilePath, $image, $fileHandler);
                $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$image);
            }else{
                $profilePicPath = \Settings::urlOfFileInStorage($tmpFilePath.$image);
            }
        }else{
            $profilePicPath = URL::to($tmpFilePath.'../').'/no_img.jpg';
        }
        //$manager = new ImageManager(array('driver' => 'imagick'));
        //$profilePicPath = $manager->make($profilePicPath)->resize(240, 240);
        //$profilePicPath = $manager->make($profilePicPath)->resize(240);
        //dd($profilePicPath);
        return $profilePicPath;
    }
}
