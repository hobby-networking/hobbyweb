<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommunityInvitation extends Model
{
    public function community() {
    	return $this->belongsTo('App\Models\Community','community_id');
    }
}
