<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Settings as SettingsHelper;

class PostComments extends Model
{
    public function user() {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function getContentAttribute() {
        return SettingsHelper::convert($this->attributes['content']);
    }
}
