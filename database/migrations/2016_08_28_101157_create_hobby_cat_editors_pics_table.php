<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHobbyCatEditorsPicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hobby_cat_editors_pics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hobby_cat_id')->unsigned();
            $table->string('title');
            $table->string('link');
            $table->string('image');
            $table->enum('is_active',['yes','no'])->default('no');
            $table->enum('is_in_my_feed',['yes','no'])->default('no');
            $table->timestamps();

            $table->foreign('hobby_cat_id')
                  ->references('id')->on('hobby_categories')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hobby_cat_editors_pics');
    }
}
