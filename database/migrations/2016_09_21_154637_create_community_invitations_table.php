<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_invitations', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('invited_to')->unsigned();
            $table->integer('community_id')->unsigned();
            $table->bigInteger('invited_by')->unsigned();
            $table->timestamps();

            $table->foreign('invited_to')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');

            $table->foreign('community_id')
                  ->references('id')->on('community')
                  ->onDelete('cascade');  

            $table->foreign('invited_by')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('community_invitations');
    }
}
