<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('community_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->enum('is_admin',['yes','no'])->default('no');
            $table->enum('is_accepted',['yes','no'])->default('no');

            $table->foreign('community_id')
                  ->references('id')->on('community')
                  ->onDelete('cascade');  
                  
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');  

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('community_members');
    }
}
