<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHobbiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hobbies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('profile_pic')->nullable();
            $table->string('cover_pic')->nullable();
            $table->string('desc');
            $table->integer('cat_id')->unsigned();
            $table->enum('is_active',['yes','no'])->default('yes');
            $table->timestamps();
            
            $table->foreign('cat_id')
                  ->references('id')->on('hobby_categories')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hobbies');
    }
}
