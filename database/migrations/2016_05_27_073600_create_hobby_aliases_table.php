<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHobbyAliasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hobby_aliases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hobby_id')->unsigned();
            $table->timestamps();
            $table->string('alias_name');
            $table->enum('is_active',['yes','no'])->default('yes');
            $table->foreign('hobby_id')
                  ->references('id')->on('hobbies')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hobby_aliases');
    }
}
