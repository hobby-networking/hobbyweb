<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHobbiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_hobbies', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('hobby_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('cat_id')->unsigned();
            $table->enum('is_active',['yes','no'])->default('yes');
            $table->timestamps();

            $table->foreign('hobby_id')
                  ->references('id')->on('hobbies')
                  ->onDelete('cascade');
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_hobbies');
    }
}
