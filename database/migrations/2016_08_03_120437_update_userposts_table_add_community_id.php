<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserpostsTableAddCommunityId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userposts', function (Blueprint $table) {
            $table->string('hobby_id')->nullable()->change();
            $table->integer('community_id')->nullable()->after('hobby_id');
            $table->enum('type',['user','community'])->default('user')->after('video');
            $table->enum('visibility',['public','private'])->default('public')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('userposts', function (Blueprint $table) {
            $table->dropColumn('community_id');
            $table->dropColumn('type');
            $table->dropColumn('visibility');
        });
    }
}
