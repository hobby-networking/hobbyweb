<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserpostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userposts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            //$table->integer('cat_id')->unsigned();
            $table->string('hobby_id');
            $table->text('content');
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            /*$table->foreign('cat_id')
                  ->references('id')->on('hobby_categories')
                  ->onDelete('cascade');*/
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('userposts');
    }
}
