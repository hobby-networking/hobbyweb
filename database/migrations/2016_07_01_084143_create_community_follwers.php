<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityFollwers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_follwers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('community_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->enum('is_active',['yes','no'])->default('yes');
            $table->timestamps();

            $table->foreign('community_id')
                  ->references('id')->on('community')
                  ->onDelete('cascade');  
                  
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');                            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('community_follwers');
    }
}
