<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Community extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name',100)->unique();
            $table->string('profile_pic');
            $table->string('cover_pic');
            $table->integer('created_by')->unsigned();
            $table->string('related_hobbies');
            $table->string('related_peoples');
            $table->enum('type',array('public','secret','closed'));
            $table->enum('status',array('yes','no'));
            $table->timestamps();

            $table->foreign('created_by')
                  ->references('id')->on('users')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('community');
    }
}
