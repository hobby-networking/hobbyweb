<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <title>Hobby Networking</title>
    <meta name="google-site-verification" content="QTDPXMCFs8bofWg0DC9HxHnbcvwju7tgb_PCSYfsC5s" />
    <!-- Bootstrap -->
    <link href="{{ Cdn::asset('frontend/welcome_new/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ Cdn::asset('frontend/welcome_new/css/lity.css') }}" rel="stylesheet">
    <link href="{{ Cdn::asset('frontend/welcome_new/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ Cdn::asset('frontend/welcome/assets/css/style.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link href="{{ Cdn::asset('frontend/welcome/assets/css/uikit.docs.min.css') }}" rel="stylesheet">
    <script src="{{ Cdn::asset('frontend/welcome/assets/js/modernizr.js') }}"></script> <!-- Modernizr -->
    <script src="//cdn.jsdelivr.net/blazy/latest/blazy.min.js" type="text/javascript"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      .success.alert-success {
          border-color: transparent;
          background-color: transparent;
      }
    </style>
    {!! Analytics::render() !!}
    @php 
      $current_url = Request::getPathInfo();
      $urlArr = explode("/",$current_url);
      $last_value = end($urlArr);
      if($last_value==''){
        $last_value = 'home';
      }
      $seo_details = Settings::getSeodata($last_value);
    @endphp
    <meta property="og:title" content="{{@$seo_details->meta_title}}"/>
    <meta property="og:keywords" content="{{@$seo_details->meta_keywords}}"/>
    <!-- <meta property="og:image" content="https://scontent.fmaa1-1.fna.fbcdn.net/t31.0-8/12471394_547065292117403_5793609828223003075_o.jpg""/> -->
    <meta property="og:description" content="{{@$seo_details->meta_desc}}"/>
  </head>
  <body>
    <div class="wrapper">
      <header class="header">
        <div class="container">
          <div class="row">
            <div class="logo col-md-3 col-sm-3 col-xs-12">
              <a href="#"><img src="{{ Cdn::asset('frontend/welcome_new/img/logo.png') }}"></a>
            </div>
          </div>
        </div>
      </header>
      <section class="slider">
        <div class="container-fluid">
          <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              

              <!-- Wrapper for slides -->
              <div class="carousel-inner" >
                <div class="item active" role="listbox">
                    <video id="myVideo" class="b-lazy" width="100%" autoplay onended="run();">
                      <source data-src="{{ Cdn::asset('frontend/welcome_new/video/Electric_Guitar_Fretboard.mp4') }}">
                      <source data-src="{{ Cdn::asset('frontend/welcome_new/video/Electric_Guitar_Fretboard.webm') }}" type="video/webm">
                      <source id="ss" data-src="{{ Cdn::asset('frontend/welcome_new/video/Electric_Guitar_Fretboard.ogv') }}"  type="video/ogg">
                      <!--<source src="video/Slow_motion.m4v" type="video/mp4">
                      <source src="video/The Quest for Your Life.m4v" type="video/mp4">-->
                    </video>  
                {{-- <video id="myVideo" controls preload="auto" width="640" height="480" data-setup='{"example_option":true}'>
                    <source id="ss" src="{{ Cdn::asset('frontend/welcome_new/video/Surfer1.flv') }}" type="rtmp/flv">
                </video> --}}
                </div>
                <div class="carousel-caption">
                    <a href="https://youtu.be/KtdIzmNZL54" class="bnr-img" target="_blank" data-lity><img src="{{ Cdn::asset('frontend/welcome_new/img/play-btn.png') }}" class=""></a>
                    <h1>A Single Platform for Multiple Hobbies</h1>
                    {!!Form::open(['method'=>'post','url'=>'/auth/login', 'id'=>'form-login-home','class'=>'form-inline','role'=>"form"])!!}
                      <div class="form-group">
                        {!! Form::text('email',Input::old('email'),['name'=>'email', 'id'=>'email','class'=>'form-control', 'required'=>'required', 'placeholder'=>'Email','autocomplete'=>'off']) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::password('password',['name'=>'password', 'id'=>'pass','class'=>'form-control', 'placeholder'=>'Password', 'required'=>'required', 'autocomplete'=>'off']) !!}
                      </div>
                      {!!Form::button('Login', array(
                            'type' => 'submit',
                            'class'=> 'sign_up btn btn-default bnr-login',
                            'onclick'=>'return true',
                            'id'=>'login'
                        ));!!}
                  {!!Form::close()!!}
                  <br>
                  {{-- <a href="{{ URL::to('/redirect') }}"><button type="submit" class="sign_up fblogin btn btn-default bnr-login" id="fblogin">Login With Facebook</button></a> --}}

                  <fb:login-button size="large"
                    scope="public_profile,email"
                    onlogin="checkLoginState();" show-faces="false" use-continue-as="true" button-type="continue_with">
                  </fb:login-button>
                    <p class="error">
                    @if (Session::has('success_msg'))
                        @foreach (Session::get('success_msg') as $success)
                            {{ $success }}
                        @endforeach
                    @endif

                    @if($errors->any())
                        @foreach ($errors->all() as $error) 
                            {{$error}}
                        @endforeach
                    @endif
                    </p>
                    @if (session('status'))
                      <div class="success alert-success">
                             {{ session('status') }}
                      </div>
                    @endif
                    <p>Forgot your password? <a href="javascript:void(0)" class="cd-form-forgot_password">Click Here</a></p>
                    <h4>Are you not a member yet ? Join today. Its Free !</h4>
                    <a href="javascript:void(0)" class="cd-signup sign_up"><button type="button" class="btn btn-primary btn-lg slider-sign">SIGN UP</button></a>
                </div>
              </div>

              
            </div>
          </div>
        </div>
        <div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
            <div class="cd-user-modal-container"> <!-- this is the container wrapper -->
                <!-- <ul class="cd-switcher">
                    <li><a href="javascript:void(0)">Sign in</a></li>
                    <li><a href="javascript:void(0)">New account</a></li>
                </ul> -->

                <div id="cd-login"> <!-- log in form -->
                    {!!Form::open(['method'=>'post','url'=>'/auth/login', 'id'=>'form-login','class'=>'cd-form','role'=>"form"])!!}
                        <p class="fieldset">
                            <label class="image-replace cd-email" for="signin-email">E-mail</label>
                            {!! Form::email('email',Input::old('email'),['name'=>'email', 'id'=>'signin-email','class'=>'full-width has-padding has-border', 'required'=>'required', 'placeholder'=>'E-mail','autocomplete'=>'off']) !!}
                            <span class="cd-error-message">Error message here!</span>
                        </p>

                        <p class="fieldset">
                            <label class="image-replace cd-password" for="signin-password">Password</label>
                            {!! Form::password('password',['name'=>'password', 'id'=>'signin-password','class'=>'full-width has-padding has-border', 'placeholder'=>'Password', 'required'=>'required', 'autocomplete'=>'off']) !!}
                            <a href="javascript:void(0)" class="hide-password">Hide</a>
                            <span class="cd-error-message">Error message here!</span>
                        </p>

                        <p class="fieldset">
                            <input type="checkbox" id="remember-me" checked>
                            <label for="remember-me">Remember me</label>
                        </p>

                        <p class="fieldset">
                            <input class="full-width" type="submit" value="Login">
                        </p>
                    {!!Form::close()!!}
                    
                    <p class="cd-form-bottom-message"><a href="javascript:void(0)" class="cd-form-forgot_password">Forgot your password?</a></p>
                    <!-- <a href="javascript:void(0)" class="cd-close-form">Close</a> -->
                </div> <!-- cd-login -->

                <div id="cd-signup"> <!-- sign up form -->
                    {!!Form::open(['method'=>'post','url'=>'/auth/signup', 'id'=>'form-signup','class'=>'cd-form','role'=>"form"])!!}
                        <p class="fieldset">
                            <label class="image-replace cd-username" for="signup-username">Username</label>
                            {!! Form::text('name',old('name'),['name'=>'name', 'id'=>'signup-username','class'=>'full-width has-padding has-border', 'required'=>'required', 'placeholder'=>'Name','autocomplete'=>'off', 'pattern'=>"[a-zA-Z ]+",  'title'=>"Only enter letters"]) !!}
                            <span class="cd-error-message">Error message here!</span>
                        </p>

                        <p class="fieldset">
                            <label class="image-replace cd-email" for="signup-email">E-mail</label>
                            {!! Form::email('email',null,['name'=>'email', 'id'=>'signup-email','class'=>'full-width has-padding has-border', 'required'=>'required', 'placeholder'=>'E-mail','autocomplete'=>'off']) !!}
                            <span class="cd-error-message">Error message here!</span>
                        </p>

                        <p class="fieldset">
                            <label class="image-replace cd-mobile" for="signup-phone">Phone No.</label>
                            {!! Form::input('number','phone',null,['name'=>'phone', 'id'=>'signup-phone','class'=>'full-width has-padding has-border', 'required'=>'required', 'placeholder'=>'Phone No','autocomplete'=>'off', 'max'=>'9999999999', 'min'=>'7000000000']) !!}
                            <small class="label_msg">You can verify your phone number later!</small>
                            <span class="cd-error-message">Error message here!</span>
                        </p>

                        <p class="fieldset">
                            <label class="image-replace cd-password" for="signup-password">Password</label>
                            {!! Form::password('password',['name'=>'password', 'id'=>'signup-password','class'=>'full-width has-padding has-border', 'placeholder'=>'Password', 'required'=>'required', 'autocomplete'=>'off', 'minlength'=>6]) !!}
                            <a href="javascript:void(0)" class="hide-password">Hide</a>
                            <span class="cd-error-message">Error message here!</span>
                        </p>
                         
                        <?php /* {!! app('captcha')->display(); !!} */ ?>

                        <div class="g-recaptcha" data-sitekey="6Lf1qxIUAAAAAHhjg4_2Fnf0hfhJEFgxwjUfFDVS"></div>

                        <p class="fieldset">
                            <input type="checkbox" id="accept-terms" value="yes" name="accept_term">
                            <label for="accept-terms">I agree to the <a href="{{ route('terms_of_use_page') }}" target="_blank">T &amp; C and Privacy Policy</a></label>
                        </p>

                        <p class="fieldset">
                            <input class="full-width has-padding" type="submit" value="Create account">
                        </p>
                    {!!Form::close()!!}

                    <!-- <a href="javascript:void(0)" class="cd-close-form">Close</a> -->
                </div> <!-- cd-signup -->

                <div id="cd-reset-password"> <!-- reset password form -->
                    <p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

                    {!!Form::open(['method'=>'post','url'=>'/password/email', 'id'=>'form-reset-password','class'=>'cd-form','role'=>"form"])!!}
                        <p class="fieldset">
                            <label class="image-replace cd-email" for="reset-email">E-mail</label>
                            <input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail" name="email">
                            <span class="cd-error-message">Error message here!</span>
                        </p>

                        <p class="fieldset">
                            <input class="full-width has-padding" type="submit" value="Reset password">
                        </p>
                    {!!Form::close()!!}

                    <p class="cd-form-bottom-message"><a href="javascript:void(0)">Back to log-in</a></p>
                </div> <!-- cd-reset-password -->
                <a href="javascript:void(0)" class="cd-close-form">Close</a>
            </div> <!-- cd-user-modal-container -->
        </div> <!-- cd-user-modal -->
      </section>
      <section class="passion">
        <div class="container">
          <div class="row">
            <h2>Networking Just Got Passionate</h2>
            <h3>We are on a mission to Connect all similar Passionate people Globally so that you can<br>
              enjoy doing what you really Love to do</h3>
            <h4>See how Hobby Networking is Reinventing Passion</h4>
            <div class="col-md-3 col-sm-6 col-xs-12 passion-itm">
              <div class="passion-top">
                <img src="{{ Cdn::asset('frontend/welcome_new/img/passion-1.png') }}">
              </div>
              <div class="passion-btm">
                <h5>Connect</h5>
                <p>Connect with the World people who pursue the same interests and hobbies as yours.</p>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 passion-itm">
              <div class="passion-top">
                <img src="{{ Cdn::asset('frontend/welcome_new/img/passion-2.png') }}">
              </div>
              <div class="passion-btm">
                <h5>Share</h5>
                <p>Share your hobbies in different Hobby Groups & Pages. Create your own Hobby Group & Pages.</p>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 passion-itm">
              <div class="passion-top">
                <img src="{{ Cdn::asset('frontend/welcome_new/img/passion-3.png') }}">
              </div>
              <div class="passion-btm">
                <h5>LEARN</h5>
                <p>Learn more about your hobbies from others and help others to learn from your experience.</p>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 passion-itm">
              <div class="passion-top">
                <img src="{{ Cdn::asset('frontend/welcome_new/img/passion-4.png') }}">
              </div>
              <div class="passion-btm">
                <h5>Invite</h5>
                <p>Invite your friends to be a part of this global platform on Hobbies and help them do something creative.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="network"> 
        <div class="container">
          <div class="row">
            <div class="col-md-7 col-xs-12 network-left">
              <h2>What is Hobby Networking</h2>
              <!--<p><strong>A platform created by the Hobbyists, for the Hobbyists. What ever is your Hobby, this platform gives every individual on Earth an opportunity</strong></p>-->
              <p>A platform created by the Hobbyists, for the Hobbyists. What ever is your Hobby, this platform gives every individual on Earth an opportunity to showcase what you have always loved to do, connect with other individuals from all corners of the world driven by the same passion, help you learn from others.</p>
              <p>It is a medium for expressing the Hobby Networking members wide-ranging and eclectic interests, tastes and travels.</p>
              <p>We look forward to create a happy world where all of us can pursue what we have always craved for.</p>
              <a href="javascript:void(0)"><button type="button" class="btn btn-primary btn-lg network-btn">WHAT IS YOUR HOBBY?</button></a>
            </div>
          </div>
        </div>
      </section>
      <section class="get-net">
        <div class="container">
            <div class="row">
                <h2>Give in to Your Awesomeness. <br>Get Your Hobbynetworking</h2>
                <a href="javascript:void(0)"><button type="button" class="btn btn-primary btn-lg join-click cd-signup">Click To join</button></a>
            </div>
        </div>
      </section>
      <section class="home-galary">
        <div class="container">
          <div class="row">
            <h2>Explore Your Hobbies</h2>
            @foreach($welcomePageHobbies as $eachHobby)
            <div class="col-md-3 col-sm-6 col-xs-12 glry-itm">            
              <div class="thumbnail">
                  <div class="caption">
                      <img class="b-lazy" 
                        {{-- src="{{ Cdn::asset('frontend/dashboard/assets/img/no_img.jpg') }}" --}}
                        data-src="{{ $eachHobby->profile_pic }}"
                      />
                      <p>{{ $eachHobby->name }}</p>
                      <a href="{{route('manage_hobby_by_name',$eachHobby->hobby_slug)}}" class="label label-default more" rel="tooltip" title="View more about {{ $eachHobby->name }}">View More</a></p>
                  </div>
                  <img class="b-lazy" src="{{ Cdn::asset('frontend/dashboard/assets/img/no_img.jpg') }}" data-src="{{ $eachHobby->profile_pic }}" alt="{{ $eachHobby->name }}" class="bigHobbyImg" width="292px" height="219px">
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </section>
      <footer class="footer">
        @include('pages.layout.footer')
      </footer>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ Cdn::asset('frontend/welcome_new/js/jquery.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ Cdn::asset('frontend/welcome_new/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ Cdn::asset('frontend/welcome/assets/js/uikit.min.js') }}"></script>
    <script src="{{ Cdn::asset('frontend/welcome_new/js/lity.js') }}"></script>
    <script src="{{ Cdn::asset('frontend/welcome/assets/js/main.js') }}"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
          $("[rel='tooltip']").tooltip();    
          var bLazy = new Blazy({
          
          });
          $('.thumbnail').hover(
              function(){
                  $(this).find('.caption').slideDown(250); //.fadeIn(250)
              },
              function(){
                  $(this).find('.caption').slideUp(250); //.fadeOut(205)
              }
          ); 
      });
    </script>
    <script src="{{ Cdn::asset('frontend/welcome_new/js/jquery.scrollify.min.js') }}"></script>
    <script src="{{ Cdn::asset('frontend/welcome_new/js/jquery.scrollify.js') }}"></script>
    <script src="{{ Cdn::asset('frontend/dashboard/assets/js/bootstrap.youtubepopup.min.js') }}"></script>
    <script>
        video_count =1;
        videoPlayer = document.getElementById("ss");        
        video=document.getElementById("myVideo");

        function run(){
            video_count++;
            if (video_count == 1) video_count = 1;
            videoPlayer.setAttribute("src","video"+video_count+".m4v");       
            video.play();

       }
    </script>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '515326071876163',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();   
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
  
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }
  function statusChangeCallback(response) {
    console.log(response);
    FB.api('/me', { locale: 'en_US', fields: 'name, email' },
      function(response) {
        var url="{{ URL::to('/') }}/auth/fblogin";
        $.post(url, response, function(result){
          if (result.login_success) {
            window.location = result.redirectTo;
          }else{
            $('.error').html(result.errors);
          }
        });
      }
    );
  }
</script>
  </body>
</html>