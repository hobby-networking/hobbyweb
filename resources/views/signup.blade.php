<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Hobby Networking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
<link rel="shortcut icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
<!--font Css-->
<link href="{{ Cdn::asset('frontend/welcome/assets/css/font-awesome.css" rel="stylesheet" type="text/css') }}">

<!--Image Overlay Css -->
<link rel="stylesheet" href="{{ Cdn::asset('frontend/welcome/assets/css/shortcodes.css') }}" />

<!--animation effect Css -->
<link href="{{ Cdn::asset('frontend/welcome/assets/css/uikit.docs.min.css') }}" rel="stylesheet">

<!--bootstrap Css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{ Cdn::asset('frontend/welcome/assets/css/bootstrap.css') }}">

<!--mother Css -->
<link href="{{ Cdn::asset('frontend/welcome/assets/css/hobby-networking.css') }}" rel="stylesheet" type="text/css">

<!--responsive css -->
<link href="{{ Cdn::asset('frontend/welcome/assets/css/responsive.css') }}" rel="stylesheet" type="text/css">
{!! Analytics::render() !!}
</head>

<body>
<div id="page">
<section id="sign_up_page">
    	<div class="container">
        	<div class="logo_sec"><a href="{{ route('welcome_page') }}"><img src="{{ Cdn::asset('frontend/welcome/assets/images/hobby-networking-logo.png') }}" alt="Hobby Sign Up Page" title="Hobby Sign Up Page"></a></div>
            <div class="sign_up_body">
            	<div class="row">
                	<div class="sign_up_field">
                        	<div class="hobby_global">
                            	<h1>Join Hobby Networking - It's Free</h1>
                                <form class="contact_page" method="post" action="">
                					<div class="register clearfix">
                                    	<input type="text" placeholder="Full Name" value="" />
                                        <input type="text" placeholder="User Name" value="" />
                                        <input type="text" placeholder="Password" value="" />
                                        <input type="text" placeholder="Confirm Password" value="" />
                                        <input type="text" placeholder="Email Address" value="" />
                                         <input type="text" placeholder="Conform Address" value="" />
                                          <input type="text" placeholder="Date of Birth" value="" />
                                          <div class="hobby_global clearfix">
                                          		<div class="terms_left">
                                                	<p>Terms of Use</p>
                                                    </div>
                                          		<div class="terms_left">
                                                	<ul>
                                                    	<li><input name="" type="radio" value="Yes"></li>
                                                    	<li><input name="" type="radio" value=""></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                          <input type="submit" value="Submit" />      
                                    </div>
								</form>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
	</section>
  
<!--footer Section-->  
<footer>
    	<div class="container">
        	<div class="footer_top">
            	<a href="#"><img src="{{ Cdn::asset('frontend/welcome/assets/images/footer-hobby-networking-logo.png') }}" alt="Hobby Networking Logo" title="Hobby Networking Logo"></a></div>
            <div class="footer_bttn">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Group</a></li>
                    <li><a href="#">Search Friends</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms of Use</a></li>
                    <li><a href="#">Contact Us</a></li>
                    </ul>
                <p>© 2015 Hobby Networking All rights reserved</p>    
                </div>    
            </div>
        </footer>
</div>

<!--bootstrap Js-->
<script type="text/javascript" src="{{ Cdn::asset('frontend/welcome/assets/js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/welcome/assets/js/bootstrap.js') }}"></script>
<!--animation Js-->
<script type="text/javascript" src="{{ Cdn::asset('frontend/welcome/assets/js/uikit.min.js') }}"></script>

 
</body>
</html>