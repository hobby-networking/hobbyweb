<!DOCTYPE html>
<html>
<head>
    <title>Hobbynetworking Contact Us</title>
</head>
<body>
You received a message from hobbynetworking.com:

<p>
Name: {{ $full_name }}
</p>

<p>
{{ $email }}
</p>

<p>
{{ $subject }}
</p>

<p>
{{ $user_message }}
</p>
</body>
</html>