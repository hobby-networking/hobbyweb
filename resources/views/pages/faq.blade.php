<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <title>Hobby Networking | Faq</title>

    <!-- Bootstrap -->
    <link href="{{ Cdn::asset('frontend/welcome_new/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ Cdn::asset('frontend/welcome_new/css/style.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  {!! Analytics::render() !!}
  </head>
  <body>
    <div class="wrapper">
      <header class="header">
        <div class="container">
          <div class="row">
            <div class="logo col-md-3 col-sm-3 col-xs-12">
              <a href="{{ route('welcome_page') }}"><img src="{{ Cdn::asset('frontend/welcome_new/img/logo.png') }}"></a>
            </div>
          </div>
        </div>
      </header>
      <section class="slider1">
        <div class="container-fluid">
          <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              

              <!-- Wrapper for slides -->
              <div class="carousel-inner" >
                <div class="item active" role="listbox">
                  <img src="{{ Cdn::asset('frontend/welcome_new/img/about-banner.jpg') }}">
                </div>
                
              </div>

              
            </div>
          </div>
        </div>
      </section>
      <section class="passion about-us-page-section-2">
        <div class="faq_container container tnc">
            <div class="row">
              <h2>Faq.</h2>
              <img src="{{ Cdn::asset('frontend/welcome_new/img/about-banner-2.jpg') }}">
              <p>
                <h5>Login & Register:-</h5>
                <ol type="1">
                  <li>How to Create an Account on HobbyNetworking?<br>
                    Please follow the 3 steps as mentioned below:
                    <ul>
                      <li>Go to www.hobbynetworking.com.</li>
                      <li>If you see the signup form, fill out your name, email address or phone number, password, birthday and gender. </li>
                      <li>Click Sign Up.</li>
                      <li>You can also sign up using your Facebook login id.</li>
                    </ul>
                  </li>
                  <li>
                    How do I login into HobbyNetworking?
                    <ul>
                      <li>Username: You can login with your username “email address”.</li>
                      <li>Password: Enter your password.</li>
                      <li>Click Log in.</li>
                    </ul>
                  </li>
                  <li>
                    How do I log out from HobbyNetworking?
                    <ul>
                      <li>Click at the top of the hobbynetworking page.</li>
                      <li>Select logout</li>
                    </ul>
                  </li>
                </ol>
              </p>
              <p>
                <h5>Password:-</h5>
                <ol type="1">
                  <li>How to change/reset my account password?
                    <ul>
                      <li>Click forgot password.</li>
                      <li>Type the email address and follow the set of instructions.</li>
                    </ul>
                  </li>
                  <li>
                    How can I make my password strong?
                    <ul>
                      <li>When you try to create a new password, you should make sure that It is at least 6 characters long. Please try to use a complex combination of numbers, letters and punctuation marks etc.</li>
                      <li>If you see a message asking you that the password you entered isn't strong enough, please  try to mix  together uppercase and lowercase letters or making the password longer and intricate.</li>
                      <li>The password you create should be easy for you to remember but hard for someone else to figure out. For extra security, your hobbynetworking password should be different than the passwords you use to log into other accounts, like your email or bank account etc.</li>
                    </ul>
                  </li>
                </ol>
              </p>
              <p>
                <h5>Profile:-</h5>
                <ol type="1">
                  <li>How can I change my profile picture?
                    <ul>
                      <li>Go to my “Profile “</li>
                      <li>Click Edit Profile</li>
                      <li>Click on the icon “Change Image”</li>
                      <li>Upload a new image </li>
                    </ul>
                  </li>
                  <li>
                    How do I change my account information in HobbyNetworking?
                    <ul>
                      <li>Go to my “Profile “</li>
                      <li>Click Edit Profile</li>
                      <li>Once you click edit, pages with all your details are featured.</li>
                      <li>Change the details as you wanted, and then save. </li>
                    </ul>
                  </li>
                  <li>
                    How is the procedure to change notification settings?
                    <ul>
                      <li>Go to my “Profile “</li>
                      <li>Click Edit Profile</li>
                      <li>Click on notifications</li>
                      <li>Edit as per your requirements</li>
                      <li>Then “Save”.</li>
                    </ul>
                  </li>
                </ol>
              </p>
            </div>
        </div>
      </section>
          
      
      <footer class="footer">
        @include('pages.layout.footer')
      </footer>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ Cdn::asset('frontend/welcome_new/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
          $("[rel='tooltip']").tooltip();    
       
          $('.thumbnail').hover(
              function(){
                  $(this).find('.caption').slideDown(250); //.fadeIn(250)
              },
              function(){
                  $(this).find('.caption').slideUp(250); //.fadeOut(205)
              }
          ); 
      });
    </script>
    <script src="{{ Cdn::asset('frontend/welcome_new/js/jquery.scrollify.min.js') }}"></script>
    <script src="{{ Cdn::asset('frontend/welcome_new/js/jquery.scrollify.js') }}"></script>
    <script>
        video_count =1;
        videoPlayer = document.getElementById("ss");        
        video=document.getElementById("myVideo");

        function run(){
            video_count++;
            if (video_count == 1) video_count = 1;
            videoPlayer.setAttribute("src","video"+video_count+".m4v");       
            video.play();

       }

    </script>

  </body>
</html>