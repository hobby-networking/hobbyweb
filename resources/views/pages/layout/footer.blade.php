<div class="container">
          <div class="row">
            <div class="footer-list-1 col-md-6 col-xs-12">
              <a href="{{ route('welcome_page') }}"><img src="{{ Cdn::asset('frontend/welcome_new/img/logo-ftr.png') }}"></a>
              <p><b>The story behind hobbynetworking</b><br>
                Many of us have lost their once big time hobbies, interest etc as all of us are busy with our regular day to day life.
Hence the  thought about interests and how to share the same encouraged us in this tech age to create a platform of linking the world people with each other. Maybe it sounds simple but some of us can recall the days of collecting stamps or doing trick photography or just bird watching... <br><b>well</b>  we are near about the same people and the differentiating factors would be our culture and traditions based on our backgrounds. <br>So Why not touch base ?
              </p>
            </div>
            <div class="footer-list-2 col-md-2 col-xs-12">
              <h3>Know More</h3>
              <ul>
                <li><a href="{{route('search_slug','community')}}">Search Community</a></li>
                <li><a href="{{route('search_slug','friends')}}">Search Friends</a></li>
                <li><a href="{{route('search_slug','hobby')}}">Search Hobbies</a></li>
              </ul>
              <ul>
                <li><a href="mailto:help@hobbynetworking.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>help@hobbynetworking.com</a></li>
              </ul>
            </div>
            <div class="footer-list-3 col-md-4 col-xs-12">
              <h3>&nbsp;</h3>
              <ul>
                <li><a href="{{ route('about_us_page') }}">About Us</a></li>
                <li><a href="{{ route('faq_page') }}">Faq</a></li>
                <li><a href="{{ route('privacy_policy_page') }}">Privacy Policy</a></li>
                <li><a href="{{ route('terms_of_use_page') }}">Terms of use</a></li>
                <li><a href="{{ route('contact_us_page') }}">Contact Us</a></li>
                <li><a href="{{ route('explain_our_video') }}">Explainer Videos</a></li>
                <li><a href="{{ route('hn_video') }}">HN Videos</a></li>
                <li><a href="/blog/">Blog</a></li>
              </ul>
            </div>
            <div class="col-md-12 col-xs-12 copy">
              <div class="container">
                <div class="row">
                  <p>Copyright &copy; <?php echo date("Y"); ?> hobbynetworking.com. All Rights Reserved</p>
                </div>
              </div>
            </div>
          </div>
        </div>
<script type="text/javascript">
    window._pt_lt = new Date().getTime();
    window._pt_sp_2 = [];
    _pt_sp_2.push('setAccount,5d1671a9');
    var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    (function() {
        var atag = document.createElement('script'); atag.type = 'text/javascript'; atag.async = true;
        atag.src = _protocol + 'cjs.ptengine.com/pta_en.js';
        var stag = document.createElement('script'); stag.type = 'text/javascript'; stag.async = true;
        stag.src = _protocol + 'cjs.ptengine.com/pts.js';
        var s = document.getElementsByTagName('script')[0]; 
        s.parentNode.insertBefore(atag, s); s.parentNode.insertBefore(stag, s);
    })();
</script>