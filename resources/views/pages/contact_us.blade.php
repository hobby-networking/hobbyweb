<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <title>Hobby Networking | Contact us</title>

    <!-- Bootstrap -->
    <link href="{{ Cdn::asset('frontend/welcome_new/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ Cdn::asset('frontend/welcome_new/css/style.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  {!! Analytics::render() !!}
  </head>
  <body>
    <div class="wrapper">
      <header class="header">
        <div class="container">
          <div class="row">
            <div class="logo col-md-3 col-sm-3 col-xs-12">
              <a href="{{ route('welcome_page') }}"><img src="{{ Cdn::asset('frontend/welcome_new/img/logo.png') }}"></a>
            </div>
          </div>
        </div>
      </header>
      <section class="slider1">
        <div class="container-fluid">
          <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              

              <!-- Wrapper for slides -->
              <div class="carousel-inner" >
                <div class="item active" role="listbox">
                  <img src="{{ Cdn::asset('frontend/welcome_new/img/contact-banner.jpg') }}">
                </div>
                
              </div>

              
            </div>
          </div>
        </div>
      </section>
      <section class="passion about-us-page-section-1">
        <div class="container">
          <div class="row">
            <h2>Contact Us</h2>
          </div>
        </div>
      </section>

      <section class="passion contact-section">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 contact-left">
            <h5><a href="mailto:help@hobbynetworking.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>help@hobbynetworking.com</a></h5>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 contact-right">
                {{ Form:: open(array('action' => 'PageController@getContactUsForm')) }} 
                  <div class="form-group">
                    <label for="fullname">Full Name</label>
                    <input name="full_name" type="text" class="form-control" id="fullname" placeholder="" value="{{ old('full_name') }}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input name="email" type="email" class="form-control" id="exampleInputEmail1" placeholder="" value="{{ old('email') }}">
                  </div>
                  <div class="form-group">
                    <label for="subject">Subject</label>
                    <input name="subject" type="text" class="form-control" id="subject" placeholder="" value="{{ old('subject') }}">
                  </div>
                  <div class="form-group">
                    <label for="msg">Message</label>
                    {{ Form:: textarea ('message', '', array('placeholder' => 'Message', 'class' => 'form-control', 'id' => 'msg', 'rows' => '4' )) }}
                  </div>
                  {{ Form::submit('Submit', array('class' => 'btn btn-default contact-btn')) }}
                {{ Form:: close() }}
                  @if(isset($message))
                    {{$message}}
                  @endif
                <ul class="errors">
                  @if($errors->any())
                        @foreach ($errors->all() as $error) 
                            <li>{{$error}}</li>
                        @endforeach
                    @endif
                </ul>
            </div>

          </div>
        </div>
      </section>
      
      <footer class="footer">
        @include('pages.layout.footer')
      </footer>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ Cdn::asset('frontend/welcome_new/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
          $("[rel='tooltip']").tooltip();    
       
          $('.thumbnail').hover(
              function(){
                  $(this).find('.caption').slideDown(250); //.fadeIn(250)
              },
              function(){
                  $(this).find('.caption').slideUp(250); //.fadeOut(205)
              }
          ); 
      });
    </script>
    <script src="{{ Cdn::asset('frontend/welcome_new/js/jquery.scrollify.min.js') }}"></script>
    <script src="{{ Cdn::asset('frontend/welcome_new/js/jquery.scrollify.js') }}"></script>
    <script>
        video_count =1;
        videoPlayer = document.getElementById("ss");        
        video=document.getElementById("myVideo");

        function run(){
            video_count++;
            if (video_count == 1) video_count = 1;
            videoPlayer.setAttribute("src","video"+video_count+".m4v");       
            video.play();

       }

    </script>

  </body>
</html>