<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <title>Hobby Networking | About Us</title>

    <!-- Bootstrap -->
    <link href="{{ Cdn::asset('frontend/welcome_new/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ Cdn::asset('frontend/welcome_new/css/style.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
     @php 
      $current_url = Request::getPathInfo();
      $urlArr = explode("/",$current_url);
      $last_value = end($urlArr);
      if($last_value==''){
        $last_value = 'home';
      }
      $seo_details = Settings::getSeodata($last_value);
    @endphp
    <meta property="og:title" content="{{@$seo_details->meta_title}}"/>
    <meta property="og:keywords" content="{{@$seo_details->meta_keywords}}"/>
    <!-- <meta property="og:image" content="https://scontent.fmaa1-1.fna.fbcdn.net/t31.0-8/12471394_547065292117403_5793609828223003075_o.jpg""/> -->
    <meta property="og:description" content="{{@$seo_details->meta_desc}}"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  {!! Analytics::render() !!}
  </head>
  <body>
    <div class="wrapper">
      <header class="header">
        <div class="container">
          <div class="row">
            <div class="logo col-md-3 col-sm-3 col-xs-12">
              <a href="{{ route('welcome_page') }}#"><img src="{{ Cdn::asset('frontend/welcome_new/img/logo.png') }}"></a>
            </div>
          </div>
        </div>
      </header>
      <section class="slider1">
        <div class="container-fluid">
          <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              

              <!-- Wrapper for slides -->
              <div class="carousel-inner" >
                <div class="item active" role="listbox">
                  <img src="{{ Cdn::asset('frontend/welcome_new/img/about-banner.jpg') }}">
                </div>
                
              </div>

              
            </div>
          </div>
        </div>
      </section>
      <section class="passion about-us-page-section-1">
        <div class="container tnc">
          <div class="row">
            <h2>About Us</h2>
            <p>We are a technology based Start-up company who has launched a platform exclusively for hobbyists. We believe that all Humans are driven by Passion. Passions - they have always loved to do, passions they followed since childhood, but now it’s buried deep down because of work, studies and other mundane life responsibilities. 
Collecting stamps, amateur theatre groups, travelling around unknown destinations, playing a musical instrument, creating master pieces made out of clay, wood, bids, etc. , collecting cards of superheroes, cartoon characters , etc and the list goes on and on . These are just some of the passion listed here. Every individual is unique in their thoughts and creations.
Our passion for the things we loved most helped us to create a platform that will give every individual on Earth to showcase what they have always loved to do, connect with other individuals from all corners of the world driven by the same passion, help them learn from others, even Barter & Exchange and of course earn money out of their passion. 
We look forward creating a happy world where people can pursue what they have always craved for.
We are the hobby people, we are called hobbynetworking.com</p>
          </div>
        </div>
      </section>
      <section class="passion about-us-page-section-2">
        <div class="container disc">
            <div class="row">
              <h2>Know more Who We Are</h2>
              <img src="{{ Cdn::asset('frontend/welcome_new/img/about-banner-2.jpg') }}">
              <p>We are you. We at Hobby Networking come from the same normal working backgrounds of finance, technology, travel etc...</p>
              <p><strong>Our Mission</strong><br>
              Many of us have lost their once big time hobbies, interest etc ,as all of us busy with our regular day to day life.</p>

              <p>Hence the  thought about interests and how to share the same encouraged us in this tech age to create a platform of linking the world people with each other. Maybe it sounds simple but some of us can recall the days of collecting stamps , or doing trick photography or just bird watching... well  we are near about the same people and the differentiating factors would be our culture and traditions based on our backgrounds. So Why not touch base ?
              </p>

              <p><strong>What is a Hobby</strong></p>
              <p>An interest, a off- beat skill , an artistic view of the world around us , a good read , a thrill seeking adventure ,enthralling the taste buds, a socio initiative... All these are what makes you a more interesting person and gives you something fascinating to talk about with others. It can also help you to avoid feeling bored and often provides the opportunity to meet new people with similar interests.
              </p>
            </div>
        </div>
      </section>
          
      
      <footer class="footer">
        @include('pages.layout.footer')
      </footer>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ Cdn::asset('frontend/welcome_new/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
          $("[rel='tooltip']").tooltip();    
       
          $('.thumbnail').hover(
              function(){
                  $(this).find('.caption').slideDown(250); //.fadeIn(250)
              },
              function(){
                  $(this).find('.caption').slideUp(250); //.fadeOut(205)
              }
          ); 
      });
    </script>
    <script src="js/jquery.scrollify.min.js"></script>
    <script src="js/jquery.scrollify.js"></script>
    <script>
        video_count =1;
        videoPlayer = document.getElementById("ss");        
        video=document.getElementById("myVideo");

        function run(){
            video_count++;
            if (video_count == 1) video_count = 1;
            videoPlayer.setAttribute("src","video"+video_count+".m4v");       
            video.play();

       }

    </script>

  </body>
</html>