<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <title>Hobby Networking | Terms Of Use</title>

    <!-- Bootstrap -->
    <link href="{{ Cdn::asset('frontend/welcome_new/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ Cdn::asset('frontend/welcome_new/css/style.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  {!! Analytics::render() !!}
  </head>
  <body>
    <div class="wrapper">
      <header class="header">
        <div class="container">
          <div class="row">
            <div class="logo col-md-3 col-sm-3 col-xs-12">
              <a href="{{ route('welcome_page') }}"><img src="{{ Cdn::asset('frontend/welcome_new/img/logo.png') }}"></a>
            </div>
          </div>
        </div>
      </header>
      <section class="slider1">
        <div class="container-fluid">
          <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              

              <!-- Wrapper for slides -->
              <div class="carousel-inner" >
                <div class="item active" role="listbox">
                  <img src="{{ Cdn::asset('frontend/welcome_new/img/about-banner.jpg') }}">
                </div>
                
              </div>

              
            </div>
          </div>
        </div>
      </section>
      <section class="passion about-us-page-section-1">
        <div class="container">
          <div class="row">
            <h2>INTRODUCTION</h2>
            <p>Hobby networking helps to share your hobbies with others by which each other may gain knowledge about the various type of hobbies. This will provide a platform for the enhancement of knowledge about different things which they were totally unaware of. Any information about your hobby will be made public since it is a website which will be having open access.</p>
          </div>
        </div>
      </section>
      <section class="passion about-us-page-section-2">
        <div class="container tnc">
            <div class="row">
              <h2>TERMS AND CONDITIONS FOR USING THE WEBSITE</h2>
              <img src="{{ Cdn::asset('frontend/welcome_new/img/about-banner-2.jpg') }}">
              <ol type="1">
                <li>Any user(s) / visitor(s) / member(s) and/or any person whoever uses this website shall in no way whatsoever it may be:
                  <ol type="a">
                    <li>destroy, deletes or alters any information provided in the website either by the promoter of the website or by any other users of the website;</li>
                    <li>diminishes the value of the platform/website or affects it injuriously through any means for any purpose;</li>
                    <li>steal / conceals / destroy / tampers / hacks any source code of the website for personal gain and/or for financial gain and/or for any other purposes;</li>
                    <li>use any grossly offensive messages or any other sort of message to cause inconvenience or annoyance or to mislead the user of the website;</li>
                    <li>intentionally or with due knowledge or without knowledge violates the privacy of the website and also the privacy of another user(s) / visitor(s) / member(s) or any person whoever uses this website;</li>
                    <li>capture or publish or distribute or circulate any images of a private area of a person in the website/platform with or without the concerned person’s consent;</li>
                    <li>publish or transmit any material that contains sexually explicit acts of any nature in the website;</li>
                    <li>circulate any obscene material  of any sort in the website;</li>
                    <li>with / without any intention / knowledge transfer or transmit or exchange or communicate any sort of matters which threatens the unity, integrity or sovereignty;</li>
                    <li>upload / publish / communicate / transfer / transmit / exchange any sort of matters or pictures or messages or videos or songs and/or anything else to strike terror or hurts / injures the national security both internally and externally and also the friendly relation with foreign countries;</li>
                    <li>transfer / exchange / publish / transmit / communicate any sort of matters or pictures or messages or videos or songs and/or anything else which will disrupt the public order and/or decency or morality in the country or foreign countries;</li>
                    <li>share / display / transfer / exchange / publish / communicate any sort of matters or views or  any pictures or messages or videos or songs and/or anything else that may/will insult others or provoke to commit an offence in the user(s) / member(s) / outsider(s) / visitor(s);</li>
                    <li>exchange / displays / share / publish any material of children’s sexual acts or any images or text or child pornographic material or records;</li>
                    <li>facilitates any type of child abuse / exploitations on the website except which is actually published for public good because it serves the interest of science, literary or art or other object of general concern;</li>
                    <li>use / upload by any means any type of virus, spam, phishing, worms, corrupted data, spoofing, cookies, Trojans, web bugs and/or any other viruses;</li>
                    <li>use any abusive language and/or illegal words while chatting with the other users;</li>
                    <li>try to seek any type of personal information and/or account information and/or password provided in the website by any other user;</li>
                    <li>bully or harass or intimidate any other person;</li>
                    <li>associated with multi level marketing, gambling, pyramid structure scheme, any illegal activities, unlawful business, cyber terrorism, etc;</li>
                    <li>post / upload / host any content or do any sort of activities which may infringe the intellectual property rights of others;</li>
                    <li>shall not violate the laws of the land for time being in force;</li>
                    <li>send any sort of personal email / message at any point of time.</li>
                  </ol>
                </li>
                <li>
                  Any person who wants to register/create an account with this platform / website / service provider shall abide by the following steps:
                  <ol type="a">
                    <li>any person who is registering an account with the website shall of the minimum age of thirteen (13) years;</li>
                    <li>all the material facts relating to full name, a valid and existing email-identity of own, phone number, address of residence, educational background, etc which is useful for the website and also lawful purpose;</li>
                    <li>no one shall fraudulently or dishonestly uses any electronic signature, password or other unique identification characteristics of any other person apart from his/her own;</li>
                    <li>no one shall register their account by “Bot” or by means of any other automated process or method;</li>
                    <li>any person who wants to register or create an account with the website shall create his / her own personal log-in identity and password which is unique and not already in use by others. The security of the password to log into the registered account with the website is the sole responsibility of the registered user;</li>
                    <li>no one shall furnish any type of false information and/or create more than one account for the same person by using different identity / name and/or share the same account by one or more person;</li>
                    <li>the users can neither transfer their existing account nor can convert it rather they can only upgrade their account to avail the better/ premium facilities made available by the website;</li>
                    <li>creating fake profile is an offence, and if any third party user creates a fake profile and transfer / transmit / communicate any sort of hate speech, defamatory, offensive, insulting, annoying, politically or religiously sensitive and against public morality or decency which creates public disharmony or post unlawful pictures then no responsibility for the above-mentioned unlawful work will crop on the website and the platform / website will not be liable for such work of the third party users;</li>
                    <li>account of the registered user(s) should be updated at regular interval for avoiding any type of inconvenience.</li>
                  </ol>
                </li>
                <li>The user(s) / member(s) of this website must be eighteen (18) years of age or above and shall certify that they are fully capable and competent to use the site and shall abide by and comply with all the terms and conditions and privacy policy mentioned in the website.</li>
                <li>Children from the age of thirteen (13) years till they attain the age of eighteen (18) years will be strictly treated as minor(s) as per Indian Law and they can visit and view / use the certain specified content of this website only for their educational purposes and/or for the enhancement of their knowledge which will be helpful directly and/or indirectly for their education.</li>
                <li>Our website does not allow or encourage children who are less than eighteen (18) years of age to view / use the website by creating fake profile and/or using fake date of birth or any other information.</li>
                <li>Children below the age of eighteen (18) years cannot enter the Barter and Exchange market since minors are strictly prohibited from using it.</li>
                <li>The promoter of the website may at any point of time do the following activities without having any kind of obligations / liabilities towards the users of the website:
                  <ol type="a">
                    <li>bring about changes in the present services provided by the website;</li>
                    <li>introduce services which can be availed only after paying due consideration amount for the same;</li>
                    <li>stop any services at any point of time by giving three (3) days prior notice to user(s) who is currently availing those services;</li>
                    <li>terminate the account of any user if the user is found to have committed any unlawful work(s) which is prohibited in the eye of law of the land and is also strictly prohibited under the terms and conditions for availing the services of the website;</li>
                    <li>immediately delete any conversation / views shared / material contents which is unlawful and illegal in the eyes of law without any prior notice to the user engaged in such unlawful work;</li>
                    <li>immediately destroy the content if it is found to have violated the law and/or infringed other’s lawful right;</li>
                  </ol>
                </li>
                <li>Website is not liable for any third party information, data, or communication link made available in the website. It is only liable for the information or communication posted or transmitted or provided by the promoter of the website.</li>
                <li>Any views sharing, messages, uploaded pictures will be made public since this platform is an open platform for sharing of views.</li>
                <li>Any person using this site must be well aware of the fact that their personal activities shall in no way create any problem of any sort for this platform/website and also the public at large.</li>
                <li>Any person who links to a website shall not endorse its content and the website shall not be responsible for any sort linking, deep-linking, framing, data mining, meta-tagging.</li>
                <li>Website is not liable for any copyright, trademark, patent infringement which is actually done by any third party user. No user(s) shall use / infringe / circulate any sort of things which is protected under the respective Intellectual Property Laws. No one shall knowingly or unknowingly upload such materials which harms the right of others. If any claim relating to the infringement of intellectual property arises from any third party whose right actually got hurt / infringed then in that case, the profile from which the infringement is done will be removed with immediate effect after due verification of the same.</li>
                <li>The login I.D and password will be kept confidential in case you connect to other website by using the account of this website.</li>
                <li>If any user(s) / member(s) want to deactivate his / her / their existing account maintained with the website then a request from their end should be sent to the customer care of the website and the same request will be taken care of </li>
                <li>within a minimum of three (3) days time period and maximum of seven (7) days time period your account will be deactivated.</li>
                <li>If any user(s) / member(s) wants to delete his / her / their existing account then in that case request should be sent to the customer care of the website and the same request will be taken care of within a minimum of two (2) months time period and maximum of one hundred and twenty (120) days time period your account will be permanently deleted.</li>
              </ol>
            </div>
        </div>
      </section>
      <section class="passion about-us-page-section-3">
        <div class="container tnc">
          <div class="row">
            <h2>PRIVACY POLICY</h2>
            <ol type="a">
              <li>The user name, birthday, gender, profile picture, e-mail id, or some other personal details / professional information of the user shall be publicly visible unless and until it is expressly mentioned in our website’s privacy setting policy and/or a choice / option is given to select the privacy mode to a user(s) / member(s) / visitor(s) or whosoever uses this website.</li>
              <li>Our website / platform does not share any sort of personal details and/or any other information of the user(s) / member(s) / visitor(s) and/or whosoever uses this website to any third party when the privacy mode has been selected by the user(s) / member(s) / visitor(s) and/or whosoever uses the website in their respective profile(s).</li>
              <li>This hobby networking website keep their user(s) / member(s) / visitor(s) or whosoever uses this website , sensitive information, password and/or other details secure and does not share any details with any third party without the prior consent of the user(s) / member(s) / visitor(s) or whosoever uses this website.</li>
              <li>Any user(s) / member(s) / visitor(s) or whosoever using this website, knowingly or unknowingly shares his/her password / personal details / confidential information with any other person(s) / user(s) / member(s) / third party(s) then the website shall not be liable and/or responsible in case the shared password / personal details / confidential information is intentionally / unintentionally / knowingly / unknowingly misused by that / those person(s) / user(s) / member(s) / third party(s).</li>
              <li>If the profile of any user(s) / member(s) / visitor(s) and/or whosoever uses this website/platform gets hacked or compromised by any one, and/or suddenly lost any information / details, post(s), comment(s), picture(s), video(s), or any other content(s) due to the unlawful activities of any third party then the website shall not be liable and/or responsible for such act of the third party(s).</li>
              <li>If any user(s) / member(s) / visitor(s) or whosoever uses this website face any sort of problem relating to the use of this website, they can contact us by clicking on the “Contact Us” icon provided in the website.</li>
              <li>In any matter(s) relating to password problem or username problem or login problem request can be made by the user(s) / member(s) to the website to reset or change or recover their password or username or login details by following the necessary step provided by the platform i.e. by clicking on the “forgot password” icon or “forgot username” icon provided in the website.</li>
              <li>This website always encourages and motivates their user(s) / member(s) / visitor(s) or whosoever uses this website to share their interest, hobbies nationally / internationally with the other member(s) / user(s) of this website without violating the laws  of the respective Lands / States and/or without indulging themselves in any illegal act and/or in any criminal and/or other activities prohibited by the laws of the respective Lands / States for the time being in force and/or shall not violate any terms and conditions of this website which shall cause harm to any person living or dead, or may cause any sort of injury / disturbance / annoyance / humiliation / ignorance / harassing / hurting sentiments / emotions / security / etc to anybody or to the world at large.</li>
              <li>This website is not liable for any sort of activities of the user(s) / member(s) / visitor(s) or whosoever uses this website. If any complaint is filed by any user(s) / member(s) / visitor(s) or whosoever uses this website against any post, comment, videos, images, profile or any content shared or uploaded or written by any other user(s) / member(s) / visitor(s) or whosoever uses this website all the necessary steps will be taken to provide help to the users as soon as possible both technically and legally.</li>
              <li>This website shall only disclose the personal data or information or any other details of the user(s) / member(s) / visitor(s) or whosoever uses this website to any third party only by an order of the law of the Land for the time being in force.</li>
              <li>Hobbynetworking.com does not encourage any illegal or unlawful activities while using the services of this website inside or outside the website by its user(s) / member(s) / visitor(s) or whosoever uses this website. Any offender(s), criminal(s), hackers, sex offenders etc, who are offender(s) and criminal(s) in the eyes of laws are not invited or offered to join this website and if it is found that any.</li>
              <li>Hobbynetworking.com requires the personal / professional / educational or any sort of information, data, details of their user(s) / member(s) / visitor(s) or whosoever uses this website for lawful purposes and for the proper functioning / activity of the website. The user(s) / member(s) / visitor(s) or whosoever uses this website can update, customize, or change any information posted by them in their respective profile(s), or delete any information written / posted / customized / updated / shared earlier on their own wish.</li>
              <li>The website shall not be responsible for the authenticity of the personal information or professional information or sensitive personal data or information or anything supplied by the provider of such information to our website or any other person acting on behalf of that person.</li>
              <li>All the user(s) / member(s) of this website having their respective profile must keep their account settings private and confidential. On the other hand, the user(s) / member(s) may make the privacy settings visible only to people whom they know but that too at their own risk so that any future unwanted problem can be avoided.</li>
              <li>All the user(s) / member(s) of this website must keep a strong password for their accounts.</li>
              <li>All the user(s) / member(s) / visitor(s) or whosoever uses this website must log out / sign out properly from their respective accounts after the completion of their session.</li>
              <li>All the user(s) / member(s) / visitor(s) or whosoever uses this website must comply with all the terms and conditions and policies provided and mentioned in the hobbynetworking.com website and when they click “I agree” we take it for granted that every user(s) / member(s) / visitor(s) or whosoever uses this website has read all the terms and policies and conditions of the website and agreed to be bound and abided by it.</li>
              <li>Hobbynetworking.com allow their user(s) / member(s) / visitor(s) or any person using this website to invite friends to join the website, or find friends by using the friend finder tool provided in the website. Anyone using this website can search for any profile by using the details one has publicly provided in the website, (for e.g. - name or User Id).</li>
              <li>The user(s) / member(s) / visitor(s) or whosoever uses this website can log in or access or visit this website through their valid Facebook, Gmail, LinkedIn, Twitter email id.</li>
              <li>Our website allows their user(s) / member(s) to share / upload post(s) or other content(s) from their Facebook / LinkedIn / Twitter account to their hobbynetworking.com profile page. But that / those shared / uploaded post(s) or other content(s) should not infringe any of the above mentioned terms and conditions and privacy policy of the website.</li>
              <li>Our website does not endorse, advertise, or in any way initiate the transmission or transfer of any content(s) used in the website by the user(s) / member(s) and also does not select the recipient(s) to whom the content(s) will be delivered / shared / viewed / downloaded / uploaded / etc and does not change any information or content(s) in any message(s).</li>
          </div>
        </div>
      </section>    
      <section class="passion about-us-page-section-4">
        <div class="container tnc">
          <div class="row">
            <h2>DISPUTE RESOLUTION & GOVERNING LAW & JURISDICTION</h2>
            <ol type="a">
              <li>All the terms and conditions of this hobby networking website and every legal proceeding shall be governed by and interpreted in accordance with the laws of India, and the courts of Kolkata any shall have exclusive jurisdiction in the matters relating to any dispute or anything else, which may arise in regards to the use of this website by any user(s) / member(s) / visitor(s) or whosoever uses this website.</li>
              <li>The relationship between the hobby networking website and any user(s) / member(s) / visitor(s) or whosoever uses this website shall be governed by and interpreted in accordance with the laws of India.</li>
              <li>All the user(s) / member(s) / visitor(s) or whosoever uses this website whether resident or non- resident of India, shall be prosecuted and shall be governed by the laws of India, for any offence or contravention committed within India or outside India by any person, if the act of offence or any illegal activities constituting the offences involves a computer, computer system or computer network located in India.</li>
            </ol>
          </div>
        </div>
      </section>
      <section class="passion about-us-page-section-5">
        <div class="container disc">
          <div class="row">
            <h2>DISCLAIMER</h2>
            <p>THIS WEBSITE DOES NOT TAKE THE RESPONSIBILITY OF AUTHENTICATION OF THE POSTS, IDENTITY, VIDEOS, PICTURES FILES, COMMENTS AND/OR ANY SORT OF CONTENTS UPLOADED AND/OR SHARED BY THE USER(S) / MEMBER(S) OF THE WEBSITE. OUR WEBSITE DOES NOT CHANGE AND/OR ALTER ANY POSTS / PHOTOS / VIDEOS / MESSAGES / COMMENTS AND/OR ANY SORT OF CONTENTS UPLOADED AND/OR SHARED BY THE USER(S) / MEMBER(S)  i.e. IT KEEPS EVERYTHING SHARED AND/OR UPLOADED ON “AS IS AND WHERE IS BASIS”. OUR WEBSITE IS NOT LIABLE FOR ANY COPYRIGHT, TRADEMARK, PATENT INFRINGEMENT WHICH IS ACTUALLY DONE BY ANY THIRD PARTY USER(S) / MEMBER(S) / VISITOR(S) OF THIS WEBSITE.THE SERVICE OF THE WEBSITE MAY GET INTERRUPTED DUE TO VARIOUS PROBLEMS WHICH MAY ARISE FROM TIME TO TIME. SO OUR WEBSITE DOES NOT PROVIDE ANY SORT OF GUARANTEE TO THE USER(S) / MEMBER(S) THAT THEY WILL GET UNINTERRUPTED SERVICE FROM OUR END.</p>
          </div>
        </div>
      </section>
      <footer class="footer">
        @include('pages.layout.footer')
      </footer>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ Cdn::asset('frontend/welcome_new/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
          $("[rel='tooltip']").tooltip();    
       
          $('.thumbnail').hover(
              function(){
                  $(this).find('.caption').slideDown(250); //.fadeIn(250)
              },
              function(){
                  $(this).find('.caption').slideUp(250); //.fadeOut(205)
              }
          ); 
      });
    </script>
    <script src="{{ Cdn::asset('frontend/welcome_new/js/jquery.scrollify.min.js') }}"></script>
    <script src="{{ Cdn::asset('frontend/welcome_new/js/jquery.scrollify.js') }}"></script>
    <script>
        video_count =1;
        videoPlayer = document.getElementById("ss");        
        video=document.getElementById("myVideo");

        function run(){
            video_count++;
            if (video_count == 1) video_count = 1;
            videoPlayer.setAttribute("src","video"+video_count+".m4v");       
            video.play();

       }

    </script>

  </body>
</html>