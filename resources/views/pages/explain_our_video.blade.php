@extends('frontend.dashboard.template')

@section('extra_css')


@section('content')
<div class="vid-container">
      <div class="row mar-topbot">
          <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=sp4muOffUJQ" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/signup.jpg') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=sp4muOffUJQ" class="youtube">HN - Explainer Video I - Sign Up</a></h4>
            </div>
            
            <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=PfISJI66KeM" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/login_hobby.jpg') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=PfISJI66KeM" class="youtube">HN - Explainer Video II - Login</a></h4>
            </div>
            <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=SZl7AI73xVE" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/home.jpg') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=SZl7AI73xVE" class="youtube">HN - Explainer Video III - Choose A Hobby</a></h4>
            </div>
            <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=Nn1A1HNVDhw" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/post_log.jpg') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=Nn1A1HNVDhw" class="youtube">HN - Explainer Video IV- Add A Hobby</a></h4>
            </div>
        </div>
        
        <div class="res-mob row mar-topbot">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=KZukN-IDIIo" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/dashboard.jpg') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=KZukN-IDIIo" class="youtube">HN - Explainer Video V - Dashboard</a></h4>
            </div>
            
            <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=0ZoGST1Wmtk" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/side_panel.jpg') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=0ZoGST1Wmtk" class="youtube">HN - Explainer Video VI - Side Panel</a></h4>
            </div>
            <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=oiHVA1WBViY" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/fantastic_you.jpg') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=oiHVA1WBViY" class="youtube">HN - Explainer Video VII - Profile Creation</a></h4>
            </div>
           <!--  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-10">
                <a href="https://www.youtube.com/watch?v=sp4muOffUJQ" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/signup.jpg') }}" alt=""></a>
                <h4><a href="https://www.youtube.com/watch?v=sp4muOffUJQ" class="youtube">Video title goes here</a></h4>
            </div> -->
        </div>
        
    </div>

@stop

<style type="text/css">
 @media only screen and (max-width: 768px) { 
  .YouTubeModal {
  text-align: center;
  padding: 0!important;
  }

  .YouTubeModal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog{
    width:95%!important;
    height:75%!important; 
    max-width: 650px!important;
    display: inline-block;
  text-align: left;
  vertical-align: middle;
    /*top: 30%!important;*/
  }
  .modal-dialog iframe{ 
   width: 97%!important;
   height: 70%!important;
  }
  



}
</style>
@section('extra_js')

<script type="text/javascript">
  $(function () {
    $(".youtube").YouTubeModal({autoplay:0});
  });
//   (function ($) {
//     "use strict";
//     function centerModal() {
//         $(this).css('display', 'block');
//         var $dialog  = $(this).find(".modal-dialog"),
//         offset       = ($(window).height() - $dialog.height()) / 2,
//         bottomMargin = parseInt($dialog.css('marginBottom'), 10);

//         // Make sure you don't hide the top part of the modal w/ a negative margin if it's longer than the screen height, and keep the margin equal to the bottom margin of the modal
//         if(offset < bottomMargin) offset = bottomMargin;
//         $dialog.css("margin-top", offset);
//     }

//     $(document).on('show.bs.modal', '.modal', centerModal);
//     $(window).on("resize", function () {
//         $('.modal:visible').each(centerModal);
//     });
// });
</script>
@stop