@extends('frontend.dashboard.template')

@section('extra_css')


@section('content')
<div class="vid-container">
      <div class="row mar-topbot">
          <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=5Iy28YVnsns" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/hn1.png') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=5Iy28YVnsns" class="youtube">HN - Your way of Life</a></h4>
            </div>
            
            <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=KtdIzmNZL54" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/hn2.png') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=KtdIzmNZL54" class="youtube">HN - Video</a></h4>
            </div>
            <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=mZIBmQ9x9dc" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/hn3.png') }}" alt=""></a>
                <h4 class="res-mob vid-title"><a href="https://www.youtube.com/watch?v=mZIBmQ9x9dc" class="youtube">HN - Video</a></h4>
            </div>
            <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=1TZZdkhxSws" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/hn4.png') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=1TZZdkhxSws" class="youtube">HN - Animated Movie</a></h4>
            </div>
        </div>
        
        <div class="row mar-topbot">
          <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=az_pkFMCRFk" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/we_are.png') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=az_pkFMCRFk" class="youtube">Hobby Networking Videos Cool Stuff</a></h4>
            </div>
            
            <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=K4ZA8aHuqI4" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/length.png') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=K4ZA8aHuqI4" class="youtube">How Hobby Networking Works</a></h4>
            </div>
            <div class="res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=3Yfg_wmVrrI" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/connect.png') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=3Yfg_wmVrrI" class="youtube">Hobby Networking Movie</a></h4>
            </div>
            <div class=" res-mob col-lg-3 col-md-3 col-sm-3 col-xs-10">
                <a href="https://www.youtube.com/watch?v=3wTO7UfZUvo" class="youtube"><img src="{{ Cdn::asset('frontend/welcome_new/img/what_your_hobby.png') }}" alt=""></a>
                <h4 class="vid-title"><a href="https://www.youtube.com/watch?v=3wTO7UfZUvo" class="youtube">Hobby Networking Video</a></h4>
            </div>
        </div>
        
    </div>


@stop


@section('extra_js')
<style type="text/css">
 @media only screen and (max-width: 768px) { 
  .YouTubeModal {
  text-align: center;
  padding: 0!important;
  }

  .YouTubeModal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog{
    width:95%!important;
    height:75%!important; 
    max-width: 650px!important;
    display: inline-block;
  text-align: left;
  vertical-align: middle;
    /*top: 30%!important;*/
  }
  .modal-dialog iframe{ 
   width: 97%!important;
   height: 70%!important;
  }
  



}
</style>
<script type="text/javascript">
  $(function () {
    $(".youtube").YouTubeModal({autoplay:0});
  });
</script>
@stop