<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
    <title>Hobby Networking | Privacy Policy</title>

    <!-- Bootstrap -->
    <link href="{{ Cdn::asset('frontend/welcome_new/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ Cdn::asset('frontend/welcome_new/css/style.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  {!! Analytics::render() !!}
  </head>
  <body>
    <div class="wrapper">
      <header class="header">
        <div class="container">
          <div class="row">
            <div class="logo col-md-3 col-sm-3 col-xs-12">
              <a href="{{ route('welcome_page') }}"><img src="{{ Cdn::asset('frontend/welcome_new/img/logo.png') }}"></a>
            </div>
          </div>
        </div>
      </header>
      <section class="slider1">
        <div class="container-fluid">
          <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              

              <!-- Wrapper for slides -->
              <div class="carousel-inner" >
                <div class="item active" role="listbox">
                  <img src="{{ Cdn::asset('frontend/welcome_new/img/about-banner.jpg') }}">
                </div>
                
              </div>

              
            </div>
          </div>
        </div>
      </section>
      
      <section class="passion about-us-page-section-3">
        <div class="privacy container tnc">
          <div class="row">
            <h2>PRIVACY POLICY</h2>
            <img src="{{ Cdn::asset('frontend/welcome_new/img/about-banner-2.jpg') }}">
            <ol type="a">
              <li>The user name, birthday, gender, profile picture, e-mail id, or some other personal details / professional information of the user shall be publicly visible unless and until it is expressly mentioned in our website’s privacy setting policy and/or a choice / option is given to select the privacy mode to a user(s) / member(s) / visitor(s) or whosoever uses this website.</li>
              <li>Our website / platform does not share any sort of personal details and/or any other information of the user(s) / member(s) / visitor(s) and/or whosoever uses this website to any third party when the privacy mode has been selected by the user(s) / member(s) / visitor(s) and/or whosoever uses the website in their respective profile(s).</li>
              <li>This hobby networking website keep their user(s) / member(s) / visitor(s) or whosoever uses this website , sensitive information, password and/or other details secure and does not share any details with any third party without the prior consent of the user(s) / member(s) / visitor(s) or whosoever uses this website.</li>
              <li>Any user(s) / member(s) / visitor(s) or whosoever using this website, knowingly or unknowingly shares his/her password / personal details / confidential information with any other person(s) / user(s) / member(s) / third party(s) then the website shall not be liable and/or responsible in case the shared password / personal details / confidential information is intentionally / unintentionally / knowingly / unknowingly misused by that / those person(s) / user(s) / member(s) / third party(s).</li>
              <li>If the profile of any user(s) / member(s) / visitor(s) and/or whosoever uses this website/platform gets hacked or compromised by any one, and/or suddenly lost any information / details, post(s), comment(s), picture(s), video(s), or any other content(s) due to the unlawful activities of any third party then the website shall not be liable and/or responsible for such act of the third party(s).</li>
              <li>If any user(s) / member(s) / visitor(s) or whosoever uses this website face any sort of problem relating to the use of this website, they can contact us by clicking on the “Contact Us” icon provided in the website.</li>
              <li>In any matter(s) relating to password problem or username problem or login problem request can be made by the user(s) / member(s) to the website to reset or change or recover their password or username or login details by following the necessary step provided by the platform i.e. by clicking on the “forgot password” icon or “forgot username” icon provided in the website.</li>
              <li>This website always encourages and motivates their user(s) / member(s) / visitor(s) or whosoever uses this website to share their interest, hobbies nationally / internationally with the other member(s) / user(s) of this website without violating the laws  of the respective Lands / States and/or without indulging themselves in any illegal act and/or in any criminal and/or other activities prohibited by the laws of the respective Lands / States for the time being in force and/or shall not violate any terms and conditions of this website which shall cause harm to any person living or dead, or may cause any sort of injury / disturbance / annoyance / humiliation / ignorance / harassing / hurting sentiments / emotions / security / etc to anybody or to the world at large.</li>
              <li>This website is not liable for any sort of activities of the user(s) / member(s) / visitor(s) or whosoever uses this website. If any complaint is filed by any user(s) / member(s) / visitor(s) or whosoever uses this website against any post, comment, videos, images, profile or any content shared or uploaded or written by any other user(s) / member(s) / visitor(s) or whosoever uses this website all the necessary steps will be taken to provide help to the users as soon as possible both technically and legally.</li>
              <li>This website shall only disclose the personal data or information or any other details of the user(s) / member(s) / visitor(s) or whosoever uses this website to any third party only by an order of the law of the Land for the time being in force.</li>
              <li>Hobbynetworking.com does not encourage any illegal or unlawful activities while using the services of this website inside or outside the website by its user(s) / member(s) / visitor(s) or whosoever uses this website. Any offender(s), criminal(s), hackers, sex offenders etc, who are offender(s) and criminal(s) in the eyes of laws are not invited or offered to join this website and if it is found that any.</li>
              <li>Hobbynetworking.com requires the personal / professional / educational or any sort of information, data, details of their user(s) / member(s) / visitor(s) or whosoever uses this website for lawful purposes and for the proper functioning / activity of the website. The user(s) / member(s) / visitor(s) or whosoever uses this website can update, customize, or change any information posted by them in their respective profile(s), or delete any information written / posted / customized / updated / shared earlier on their own wish.</li>
              <li>The website shall not be responsible for the authenticity of the personal information or professional information or sensitive personal data or information or anything supplied by the provider of such information to our website or any other person acting on behalf of that person.</li>
              <li>All the user(s) / member(s) of this website having their respective profile must keep their account settings private and confidential. On the other hand, the user(s) / member(s) may make the privacy settings visible only to people whom they know but that too at their own risk so that any future unwanted problem can be avoided.</li>
              <li>All the user(s) / member(s) of this website must keep a strong password for their accounts.</li>
              <li>All the user(s) / member(s) / visitor(s) or whosoever uses this website must log out / sign out properly from their respective accounts after the completion of their session.</li>
              <li>All the user(s) / member(s) / visitor(s) or whosoever uses this website must comply with all the terms and conditions and policies provided and mentioned in the hobbynetworking.com website and when they click “I agree” we take it for granted that every user(s) / member(s) / visitor(s) or whosoever uses this website has read all the terms and policies and conditions of the website and agreed to be bound and abided by it.</li>
              <li>Hobbynetworking.com allow their user(s) / member(s) / visitor(s) or any person using this website to invite friends to join the website, or find friends by using the friend finder tool provided in the website. Anyone using this website can search for any profile by using the details one has publicly provided in the website, (for e.g. - name or User Id).</li>
              <li>The user(s) / member(s) / visitor(s) or whosoever uses this website can log in or access or visit this website through their valid Facebook, Gmail, LinkedIn, Twitter email id.</li>
              <li>Our website allows their user(s) / member(s) to share / upload post(s) or other content(s) from their Facebook / LinkedIn / Twitter account to their hobbynetworking.com profile page. But that / those shared / uploaded post(s) or other content(s) should not infringe any of the above mentioned terms and conditions and privacy policy of the website.</li>
              <li>Our website does not endorse, advertise, or in any way initiate the transmission or transfer of any content(s) used in the website by the user(s) / member(s) and also does not select the recipient(s) to whom the content(s) will be delivered / shared / viewed / downloaded / uploaded / etc and does not change any information or content(s) in any message(s).</li>
          </div>
        </div>
      </section>
       
      <section class="passion about-us-page-section-4">
        <div class="container tnc">
          <div class="row">
            <h2>DISPUTE RESOLUTION & GOVERNING LAW & JURISDICTION</h2>
            <ol type="a">
                <li>All the terms and conditions of this hobby networking website and every legal proceeding shall be governed by and interpreted in accordance with the laws of India, and the courts of Kolkata any shall have exclusive jurisdiction in the matters relating to any dispute or anything else, which may arise in regards to the use of this website by any user(s) / member(s) / visitor(s) or whosoever uses this website.</li>
                <li>The relationship between the hobby networking website and any user(s) / member(s) / visitor(s) or whosoever uses this website shall be governed by and interpreted in accordance with the laws of India.</li>
                <li>All the user(s) / member(s) / visitor(s) or whosoever uses this website whether resident or non- resident of India, shall be prosecuted and shall be governed by the laws of India, for any offence or contravention committed within India or outside India by any person, if the act of offence or any illegal activities constituting the offences involves a computer, computer system or computer network located in India.</li>
            </ol>
          </div>
        </div>
      </section> 
      <section class="passion about-us-page-section-5">
        <div class="container disc">
          <div class="row">
            <h2>DISCLAIMER</h2>
            <p>THIS WEBSITE DOES NOT TAKE THE RESPONSIBILITY OF AUTHENTICATION OF THE POSTS, IDENTITY, VIDEOS, PICTURES FILES, COMMENTS AND/OR ANY SORT OF CONTENTS UPLOADED AND/OR SHARED BY THE USER(S) / MEMBER(S) OF THE WEBSITE. OUR WEBSITE DOES NOT CHANGE AND/OR ALTER ANY POSTS / PHOTOS / VIDEOS / MESSAGES / COMMENTS AND/OR ANY SORT OF CONTENTS UPLOADED AND/OR SHARED BY THE USER(S) / MEMBER(S)  i.e. IT KEEPS EVERYTHING SHARED AND/OR UPLOADED ON “AS IS AND WHERE IS BASIS”. OUR WEBSITE IS NOT LIABLE FOR ANY COPYRIGHT, TRADEMARK, PATENT INFRINGEMENT WHICH IS ACTUALLY DONE BY ANY THIRD PARTY USER(S) / MEMBER(S) / VISITOR(S) OF THIS WEBSITE.THE SERVICE OF THE WEBSITE MAY GET INTERRUPTED DUE TO VARIOUS PROBLEMS WHICH MAY ARISE FROM TIME TO TIME. SO OUR WEBSITE DOES NOT PROVIDE ANY SORT OF GUARANTEE TO THE USER(S) / MEMBER(S) THAT THEY WILL GET UNINTERRUPTED SERVICE FROM OUR END.</p>
          </div>
        </div>
      </section>  
      
      <footer class="footer">
        @include('pages.layout.footer')
      </footer>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ Cdn::asset('frontend/welcome_new/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
          $("[rel='tooltip']").tooltip();    
       
          $('.thumbnail').hover(
              function(){
                  $(this).find('.caption').slideDown(250); //.fadeIn(250)
              },
              function(){
                  $(this).find('.caption').slideUp(250); //.fadeOut(205)
              }
          ); 
      });
    </script>
    <script src="js/jquery.scrollify.min.js"></script>
    <script src="js/jquery.scrollify.js"></script>
    <script>
        video_count =1;
        videoPlayer = document.getElementById("ss");        
        video=document.getElementById("myVideo");

        function run(){
            video_count++;
            if (video_count == 1) video_count = 1;
            videoPlayer.setAttribute("src","video"+video_count+".m4v");       
            video.play();

       }

    </script>

  </body>
</html>