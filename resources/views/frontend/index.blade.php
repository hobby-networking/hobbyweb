@extends('frontend.dashboard.dashboard_template')

@section('title', ($selected_hoby != "my_feed")? $hobby->name." Feed :: " :"My Feed :: ")

@section('extra_css')
<link rel="stylesheet" href="{{ Cdn::asset('frontend/dashboard/assets/css/material.indigo-pink.min.css') }}">
<link rel="stylesheet" href="{{ Cdn::asset('frontend/dashboard/assets/css/mdl-jquery-modal-dialog.css') }}">
<link rel="stylesheet" href="{{ Cdn::asset('frontend/dashboard/assets/css/mediaelementplayer.css') }}">
<style type="text/css">
</style>
@yield('editors_pic_css')
@stop
@section('content')
<?php 
$type = 'user';
$community_id = null;
?>
<div class="container xyz">
  <div class="row">
  	<div class="col-lg-12 mdlBlock"> 
      @if(Auth::check() && $show_post_box && !$loggedinUser->isAdmin())
          @include('frontend.post_btn',['type'=>$type,'community_id'=>$community_id])
      @endif
        <?php  $colSize = (!Auth::check() || $loggedinUser->isAdmin())?12:8; ?>
    	<div class="col-lg-{{$colSize}} col-md-{{$colSize}} col-sm-12 col-xs-12" id="checkDiv">
          @if(Auth::check() && !$loggedinUser->isAdmin())
            @if($show_post_box)
            <div id="loadNewPostBox">
                <div class="_4-u2 _1qby ">
                     <form  method="post" name="user_post_2" action="{{route('add_new_post')}}" class="post_form_2">            
                        @include('frontend.post_box',['type'=>$type,'community_id'=>$community_id])
                        <!-- modal -->
                        <div id="myModal2" class="modal fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                         <h3 id="myModalLabel3">Select Hobby where you would like to Post</h3>
                                    </div>
                                    <div class="modal-body">
                                            <select id="choice" name="hobby_id[]" multiple="multiple" required="required">
                                            @if(Auth::check())
                                              @foreach(Auth::user()->hobbies as $eachHobby)
                                              <option value="{{ $eachHobby->hobby->id }}">{{ $eachHobby->hobby->name }}</option>
                                              @endforeach
                                            @endif
                                            </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <button class="btn-success btn" id="SubForm2">Confirm and Submit Your Post</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end modal -->  
                    </form>       
                </div>              
            </div>          
            @endif	
            <!-- Start Selected Hobby -->
            @if(Auth::check())
              <ul class="userOption">
                  <li <?php if($selected_hoby == "my_feed"){echo "class='active_hobby'";} ?>><a href="{{ url('/') }}"> MY FEED</a></li>
                      @foreach($hobbyList['first_list'] as $userHobbies)
                          <?php $hobbyName = $userHobbies->hobby->hobby_slug; ?>
                          <li <?php if($selected_hoby == $hobbyName){echo "class='active_hobby'";} ?>><a href="{{route('manage_hobby_by_name',$hobbyName)}}"> {{ $userHobbies->hobby->name }}</a><span class="removeHobby" id="hobby_{{$userHobbies->id}}"><i class="fa fa-times-circle-o" aria-hidden="true"></i></span></li>
                      @endforeach
                      @if($count > $limit)
                      <li>
                          <div class="faq-c">
                              <div class="faq-q">
                                  <span class="faq-t">+</span>MORE
                              </div>
                              <div class="faq-a">
                                  <p class="hyp"></p>
                                  <p><a href="{{ route('choosehobby_page') }}" class="add_update_hobby">+ Add/Update Hobby</a></p>
                                  @foreach($hobbyList['second_list'] as $userHobbies)
                                      <?php 
                                          $hobbyName = $userHobbies->hobby->hobby_slug;
                                       ?>
                                      <p><a href="{{route('manage_hobby_by_name',$hobbyName)}}">{{ $userHobbies->hobby->name }}</a></p>
                                  @endforeach
                              </div>
                          </div>
                      </li>
                      @else
                      <li><a href="{{ route('choosehobby_page') }}">+ Add/Update Hobby</a></li>
                      @endif
              </ul>
            @endif
            <!-- End Selected Hobby -->
            <!-- Start Editors Pic -->
            @if(Auth::user())
            <div class="editors_pic_div">
              @if(!empty($hobby))
                @include('frontend.widget.editors_pic', array("hobby" => $hobby))
              @else
                @include('frontend.widget.editors_pic', array("hobby" => 'all'))
              @endif
            </div>
            @endif
            <!-- End Editors Pic -->
          @endif
            <div class="col-lg-12">
              @if(count($userPost)>0)
                <div id="show_ajax_post">
                  <?php $x = 1; ?>
                    @foreach($userPost as $eachPostUser)
                    @if($eachPostUser->isAllowerdToView())
                      @if($x == 2 || count($userPost)==1)
                        @if(!empty($hobby))
                          @include('frontend.widget.news_timeline', array("hobby" => $hobby))
                        @else
                          @include('frontend.widget.news_timeline', array("hobby" => 'all'))
                        @endif
                      @endif
                        @include('frontend.posts.each_post', array("eachPostUser" => $eachPostUser))
                      <?php $x++; ?> 
                    @endif
                    @endforeach
                </div>
              @else
                @if(!empty($hobby))
                  @include('frontend.widget.news_timeline', array("hobby" => $hobby))
                @else
                  @include('frontend.widget.news_timeline', array("hobby" => 'all'))
                @endif
              @endif
            </div>
            <div class="clearfix"></div>
      </div>
  	@if(Auth::check() && !$loggedinUser->isAdmin())
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
      	@include('frontend.sidebar.postpage_sidebar_right')
      </div>
    @endif
      <div class="clearfix"></div>  		
  	</div>
  </div>
</div>
@stop
@section('extra_js')
<script src="https://storage.googleapis.com/code.getmdl.io/1.0.5/material.min.js"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/mdl-jquery-modal-dialog.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/mediaelement-and-player.min.js') }}"></script>
@if(Auth::check())
<!-- Submit Post comment Start -->
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/post.js') }}"></script>
<script type="text/javascript">
function addCommentPost(post_id, comment) {
    postCommentAdd(post_id,comment,{{ Auth::user()->id }},"{{ csrf_token() }}","{{ route('add_post_comment') }}");
}
</script>
<!-- Submit Post comment End -->
<script type="text/javascript">
$(document).ready(function() {
    $('video,audio').mediaelementplayer({
      defaultVideoWidth: 480,
      defaultVideoHeight: 240,
      enableAutosize: true,
      videoHeight: -1,
    });
    var no=1;
    var currentPage = 1;
    $(window).scroll(function () {
        if(no==1)
        {
            if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
                no=2;
                $.ajax({
                    type: "GET",
                    @if($selected_hoby == "my_feed")
                    url: "{{ route('welcome_page') }}/?page="+currentPage,
                    @else
                    url: "{{ route('manage_hobby_by_name',$hobby->hobby_slug) }}/"+currentPage,
                    @endif
                    cache: true,
                    success: function(data){
                      $('#show_ajax_post').append(data.html);
                      currentPage++;
                      no=1;
                    }
                });
            }
        }
    });
    $('.faq-q').click(function(e){
        $('.faq-a').slideToggle();
    });
    $('.friend_image').mouseover(function() {
        $(this).next('.friends_options').stop(true, true).show(400);
    }).mouseout(function() {
      if(!$('.friends_options:hover').length > 0){
        $(this).next('.friends_options').stop(true, true).hide(400);
      }
    });
    $('.friends_options').mouseout(function() {
      if(!$('.friends_options:hover').length > 0){
      $(this).stop(true, true).hide(400);
    }
    });
    $('.removeHobby').click(function(eee){
        var hobbyId = $(this).attr('id').replace('hobby_','');
        var loggedinUserId = {{ $loggedinUser->id }};
        showDialog({
          title: 'Remove Hobby',
          text: 'Do you want to remove your hobby?',
          negative: {
              title: 'Nope'
          },
          positive: {
              title: 'Yay',
              onClick: function (e) {
                  $.ajax({
                    url: "{{ route('remove_user_hobby') }}", 
                    type: "POST",
                    data: {
                        'hobby_id':hobbyId,
                        'loggedinUser':loggedinUserId,
                        '_token':"{{ csrf_token() }}"
                    },
                    success: function(result){
                        location.reload();
                    }
                });
              }
          }
        });
        //console.log(loggedinUserId);
    });
    @if ($loggedinUser->isAdmin())
    /* Delete Post For Admin Start */
    $('.deleteHobby').click(function(eee){
        var url = $(this).data('href');
        showDialog({
          title: 'Delete This Post',
          text: 'Do you want to permanently remove this post?',
          negative: {
              title: 'Nope'
          },
          positive: {
              title: 'Yay',
              onClick: function (e) {
                window.location = url;
              }
          }
        });
        //console.log(loggedinUserId);
    });
    /* Delete Post For Admin End */
    @endif
    $('.userOption li').hover(
    function(){  //this is fired when the mouse hovers over
        $(this).find('.removeHobby').show();
    },
    function(){  //this is fired when the mouse hovers out
         $(this).find('.removeHobby').hide();
    });
});
var flag = true;
function like(post_id, user_id) {
  if(flag){
    flag = false;
    $.ajax({
        url: "{{ route('user_post_like') }}", 
        type: "POST",
        data: {
            'post_id':post_id,
            'user_id':user_id,
            '_token':"{{ csrf_token() }}"
        },
        success: function(result){
            $('#postLike_'+post_id).find('.count').html(result.like_count);
            if(result.success){
              $('#postLike_'+post_id).addClass('activeLike');
              $('#postLike_'+post_id).switchClass('bulb','bulb-on');
              $('#postLike_'+post_id).attr('href',"javascript:unlike("+post_id+","+user_id+")");
            }else{
              $('#postLike_'+post_id).removeClass('activeLike');
              $('#postLike_'+post_id).switchClass('bulb-on','bulb');
              $('#postLike_'+post_id).attr('href',"javascript:like("+post_id+","+user_id+")");
            }
            flag = true;
            //location.reload();
        }
    });
  }
}
function unlike(post_id, user_id) {
  if(flag){
    flag = false;
    $.ajax({
        url: "{{ route('user_post_unlike') }}", 
        type: "POST",
        data: {
            'post_id':post_id,
            'user_id':user_id,
            '_token':"{{ csrf_token() }}"
        },
        success: function(result){
            $('#postLike_'+post_id).find('.count').html(result.like_count);
            if(result.success){
              $('#postLike_'+post_id).removeClass('activeLike');
              $('#postLike_'+post_id).switchClass('bulb-on','bulb');
              $('#postLike_'+post_id).attr('href',"javascript:like("+post_id+","+user_id+")");
            }else{
              $('#postLike_'+post_id).addClass('activeLike');
              $('#postLike_'+post_id).switchClass('bulb','bulb-on');
              $('#postLike_'+post_id).attr('href',"javascript:unlike("+post_id+","+user_id+")");
            }
            flag = true;
        }
    });
  }
}
</script>
<script type="text/javascript">
    $(document).ready(function(e){
        
        //var text = document.html('NOTIFY WITH LINK < a href="http://google.com">EXAMPLE< /a >');
       var n = noty({
        text: 'NOTIFY WITH LINK EXAMPLE',
        type: 'success',
        dismissQueue: true,
        layout: 'bottomCenter',
        closeWith: ['click'],
        theme: 'relax',
        maxVisible: 10,
        animation: {
        open: {height: 'toggle'},
        close: {height: 'toggle'},
        easing: 'swing',
        speed: 1500 // opening & closing animation speed
    }

    });
       $(".noty_bar").css("background-color","#dc4622");
        $(".noty_text").css("color","#FFFFFF");
        //$('.noty_text').html('You can check out our Explainer Videos for this section. <a href="https://www.youtube.com/watch?v=KZukN-IDIIo" target="_blank" style="color: #000000; text-decoration: underline;">Dashboard</a>');
        $('.noty_text').html('You can check out our Explainer Videos for this section. <a href="javascript:void(0)" rel="KZukN-IDIIo" class="youtube" style="color: #000000; text-decoration: underline;" data-lity>Dashboard</a>');
    // setTimeout(function () {
    //     $('#noty_bottomRight_layout_container').find('li').trigger('click');
    // }, 5000);
    setTimeout(function(){ $('#noty_bottomCenter_layout_container').find('li').fadeOut() }, 5000);
    });
</script>

<script type="text/javascript">
  $(function () {
    $(".youtube").YouTubeModal({autoplay:0});
  });
</script>

@endif
@yield('extra_js1')
@yield('extra_js2')
@yield('editors_pic_js')
@yield('news_timeline_js')
@stop