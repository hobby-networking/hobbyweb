<?php $user = $eachPostUser->getUserByPost;
$loggedInUser1 = null;
$loginFlag = false;
if(Auth::check()){
    $loggedInUser1 = Auth::user();
    $loginFlag = true;
    //dd($loggedInUser1->isAdmin());
}
?>
<div class="row tmline">
    @if($loginFlag && ($user->id != $loggedInUser1->id) && !$loggedInUser1->isAdmin())
        <div class="hide_this_post pull-right"><a href="{{route('hide_this_post',$eachPostUser->id)}}" title="Hide this post"><i class="fa fa-eye-slash" aria-hidden="true"></i></a></div>
    @elseif($loginFlag && $loggedInUser1->isAdmin())
        <div class="delete_this_post pull-right"><a data-href="{{route('delete_this_post',$eachPostUser->id)}}" title="Delete this post" class="deleteHobby"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>
    @endif
    <div class="postedUser">
    	<p>
            <img src="{{ $user->profile_pic }}" class="img-circle img-responsive">
            <a href="{{ route('profile_page',$user->username) }}">{{ $user->name }}</a><br /> 
            <span>{{date("M",strtotime(date($eachPostUser->created_at)))}}</span> 
            {{date("j",strtotime(date($eachPostUser->created_at)))}}
        </p>
        <p class="status">{!! $eachPostUser->content !!}</p>                            
    </div>
    @if(!empty($eachPostUser->userPostImages($user->id)) && count($eachPostUser->userPostImages($user->id))>0)
        <div class="product-gallery post_gallery_{{ (count($eachPostUser->userPostImages($user->id)) >= 4)?"4":count($eachPostUser->userPostImages($user->id)) }}">
            <?php $imageLimit = 1; ?>
            @foreach($eachPostUser->userPostImages($user->id) as $eachPostImage)
                <?php $image_name = $eachPostImage->image; ?>
                @if($imageLimit <5)
                <div class="gallery-img">
                    <a href="{{ $image_name }}" data-lightbox="postImageGallery{{$eachPostUser->id}}">
                        <img src='{{$image_name }}' alt="{{$image_name }}" />
                    </a>
                    <div data-desc="">
                    </div>
                </div>
                @else
                <a href="{{ $image_name }}" data-lightbox="postImageGallery{{$eachPostUser->id}}">
                    @if(count($eachPostUser->userPostImages($user->id)) == $imageLimit)
                        <div class="more_post_images">{{ $imageLimit-4 }} more</div>
                    @endif
                </a>
                @endif
                <?php $imageLimit++; ?>
            @endforeach
        </div>                                                             
    @endif                       
    @if(!empty($eachPostUser->userPostVideos($user->id)) && count($eachPostUser->userPostVideos($user->id))>0)  
        @foreach($eachPostUser->userPostVideos($user->id) as $eachPostVideo)
        <?php $video_name = $eachPostVideo->video; ?>                                  
            {{-- <video id="myVideo_demo" width="72%" height="100%" controls="controls"> 
                <source src="{{$video_name}}" type="video/mp4"> 
            </video> --}}
            @if(!empty($video_name))
                <video src="{{$video_name}}" height="240"></video>
            @endif
        @endforeach
    @endif 
    <!--amrita-->
    @if(Auth::check() && !$loggedInUser1->isAdmin())
    <div class="col-sm-12">
        <div class="panel panel-white post panel-shadow">
            <div class="post-description">
                <div class="stats">
                <?php $bulb = "bulb"; ?>
                  @if($eachPostUser->isLikedBy($loggedInUser1->id))
                    <?php $bulb = "bulb-on"; ?>
                    <a href="javascript:unlike({{$eachPostUser->id}},{{$loggedInUser1->id}})" class="btn btn-default stat-item activeLike {{$bulb}}" id="postLike_{{$eachPostUser->id}}">
                  @else
                    <a href="javascript:like({{$eachPostUser->id}},{{$loggedInUser1->id}})" class="btn btn-default stat-item {{$bulb}}" id="postLike_{{$eachPostUser->id}}">
                  @endif
                        {{-- <i class="fa fa-lightbulb-o"></i><span class="count">{{$eachPostUser->likeCount()}}</span> --}}
                        <span class="count">{{$eachPostUser->likeCount()}}</span>
                    </a>
                </div>
            </div>
            <div class="post-footer">
                <div class="input-group">
                    <input class="form-control add_comment add_comment_{{ $eachPostUser->id }}" data-post_id="{{ $eachPostUser->id }}" placeholder="Add a comment" type="text">
                    <span class="input-group-addon">
                        <a href="javascript:void(0)" class="add_comment_submit add_comment_submit_{{ $eachPostUser->id }}" data-post_id="{{ $eachPostUser->id }}"><i class="fa fa-edit"></i></a>  
                    </span>
                </div>
                <ul class="comments-list comments_list_{{ $eachPostUser->id }}">
                @foreach($eachPostUser->comments_desc() as $eachComment)
                    <li class="comment">
                        <a class="pull-left" href="#">
                            <img class="avatar" src="{{ $eachComment->user->profile_pic }}" alt="avatar">
                        </a>
                        <div class="comment-body">
                            <div class="comment-heading">
                                <h4 class="user">{{ $eachComment->user->name }}</h4>
                                <h5 class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($eachComment->created_at))->diffForHumans() }}</h5>
                            </div>
                            <p>{!! $eachComment->content !!}</p>
                        </div>
                    </li>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif
    <!--babu-->
</div>