@extends('frontend.dashboard.template')

@section('title', $pageTitle)

@section('extra_css')
<style type="text/css">
.pro_div{
    min-height: 300px;
}
</style>
@stop

@section('content')

<div class="container-fluid xyz">
    <div class="row2">
        <div class="col-lg-8 mdlBlock">
            <h1>Search</h1>
            <div class="col-lg-12">
                <div id="custom-search-input">
                    <div class="input-group col-md-12" style="margin-bottom: 30px;">
                    {!! Form::open(['method'=>'get', 'url'=>route("search_slug"), 'id'=>'form-search','class'=>'form-horizontal','onsubmit'=>"", 'files'=> false]) !!}
                        @if(!empty($cat))
                        <select name="cat" class="selectSearchType">
                            <option value="hobby" <?php if($cat=="hobby"){echo "selected";} ?>>Hobby</option>
                            <option value="community" <?php if($cat=="community"){echo "selected";} ?>>Community</option>
                            <option value="friends" <?php if($cat=="friends"){echo "selected";} ?>>Friends</option>
                        </select>
                        @endif

                        <div class="all-srchdp">
                            <input type="text" id ="hobby_search" name="slug" class="form-control input-lg" placeholder="Search Here..." value="{{$slug}}" />
                            <span class="input-group-btn searchBtn">
                                <button class="btn btn-info btn-lg" type="submit" id="search_hobby">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                    {!! Form::close() !!}
                    </div>

                </div>
            </div>

            <div class="col-lg-12">
                <div class="row" id="search_grid">
                @if(count($result)>0)
                 @foreach($result as $eachResult)
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pro_div dp-prk">
                        @if($eachResult['cat']=="hobby")
                        <?php 
                            $hobbyName = strtolower($eachResult['element']->hobby_slug);
                            /*$hobbyNameArr = explode(' ', $hobbyName);

                            if(count($hobbyName)>0)
                                $hobbyName = implode('_',$hobbyNameArr);*/

                            //dd($selected_hoby);
                         ?>
                        <a href="{{ route('manage_hobby_by_name',$hobbyName) }}">
                        @endif
                        @if($eachResult['cat']=="community")
                        <?php
                            $comSlug = str_slug($eachResult['element']->name,'_');
                        ?>
                        <a href="{{ route('community_page',$comSlug) }}">
                        @endif
                        @if($eachResult['cat']=="friends")
                        <a href="{{ route('profile_page',$eachResult['element']->username) }}">
                        @endif
                        <div class="pro_block">
                            <img src="{{ $eachResult['element']->profile_pic }}" class="select_img img-responsive select_hobby_image" alt="{{$eachResult['element']->name}}" title="{{$eachResult['element']->name}}">
                            <h5>{{ substr($eachResult['element']->name,0,10) }}<?php if(strlen($eachResult['element']->name) > 10){echo '...';}?></h5>
                        </div>
                        </a>
                    </div>
                @endforeach
                @else
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pro_div middle center">
                    No Result Found
                    </div>
                @endif
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('extra_js')
<script type="text/javascript">
    $(document).ready(function(e){
        $('.xyz').click(function(e){
            var div_class = $(e.toElement).attr('class');
            if(div_class=="row2"){
               history.go(-1); 
            }
        });
    });
</script>
@stop