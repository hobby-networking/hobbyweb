<link rel="stylesheet" href="{{ Cdn::asset('frontend/dashboard/assets/css/chat.css') }}">
<div class="container chat_container">
    <div class="row chat-window col-xs-5 col-md-3 hide" id="chat_window_0" style="right: 100px;">
        <div class="col-xs-12 col-md-12">
        	<div class="panel panel-default">
                <div class="panel-heading top-bar">
                    <div class="col-md-8 col-xs-8">
                        <h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> <span class="chat_title"></span></h3>
                    </div>
                    <div class="col-md-4 col-xs-4 chat_box_buttons" style="text-align: right;">
                        {{-- <a href="javascript:void(0)"><span id="minim_chat_window" class="glyphicon glyphicon-minus icon_minim"></span></a> --}}
                        <a href="javascript:void(0)"><span class="glyphicon glyphicon-remove icon_close" data-id="chat_window_1"></span></a>
                    </div>
                </div>
                <div class="panel-body msg_container_base">
                    
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-sm chat_input" placeholder="Write your message here..." name="msg" data-receiver_id=""/>
                    </div>
                </div>
    		</div>
        </div>
    </div>
</div>
<div class="friendListHiddenDiv"> 
<span class="close" style="display:none;"><a href="javascript:openChatListBox()"><i class="fa fa-angle-double-right"></i></a></span>
<span class="open"><a href="javascript:openChatListBox()"><!-- <i class="fa fa-angle-double-left"></i> --><i class="fa fa-comment-o"></i></a></span>
<p>Chat With</p>
    <div class="frndlist">
    @foreach(Auth::user()->getFriends() as $eachFriend)
        <a href="javascript:startChat({{Auth::user()->id}}, {{$eachFriend->id}})"><img src="{{ $eachFriend->profile_pic }}" class="img-responsive img-circle imgBoder" alt="{{ $eachFriend->name }}" title="{{ $eachFriend->name }}"></a>
    @endforeach
    </div>
</div>
@section('chat_script')
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/chat.js') }}"></script>
<script type="text/javascript">
var form;
    function postIt(message,receiver_id) {
        //console.log("MSG: "+message+" Rec Id:"+receiver_id);
        var url = "{{route('messages.submit.chat')}}"; // the script where you handle the form input.
        $.ajax({
           type: "POST",
           url: url,
           data: {
                'sender_id':{{ Auth::user()->id }},
                'receiver_id':receiver_id,
                'message': message,
                '_token':"{{ csrf_token() }}"
            }, 
           success: function(data) {
               $(".chat-window[data-receiver='"+receiver_id+"']").find('.msg_container_base').html(data.messages);
               $(".chat-window[data-receiver='"+receiver_id+"']").find('.chat_input').val('');
               var $t = $(".chat-window[data-receiver='"+receiver_id+"']").find('.msg_container_base');
                $t.animate({"scrollTop": $t[0].scrollHeight}, "slow");
           }
        });
        return false;
    }
    $(document).ready(function(a) {
        $('body').on('keydown','.chat_input',function(e) {
            if(e.keyCode == 13){
                postIt($(this).val(), $(this).attr('data-receiver_id'));
            }
        });
    });
    function openChatListBox(){
        var divElement = $('.friendListHiddenDiv');
        if(divElement.css('right') == "-"+divElement.css('width')){
            divElement.animate({'right': '0px'}, 'slow');
            divElement.find('.close').show();
            divElement.find('.open').hide();
        }else{
            divElement.animate({'right': "-"+divElement.css('width')}, 'slow');
            divElement.find('.close').hide();
            divElement.find('.open').show();
        }
    }
</script>
@stop