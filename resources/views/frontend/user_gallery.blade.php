@extends('frontend.dashboard.dashboard_template')
@section('extra_css')
<style type="text/css">
#userPhotoColumn {
	column-width: 320px;
	column-gap: 15px;
  width: 90%;
	max-width: 1100px;
	margin: 50px auto;
}

div#userPhotoColumn figure {
	background: #fefefe;
	border: 2px solid #fcfcfc;
	box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
	margin: 0 2px 15px;
	padding: 15px;
	padding-bottom: 10px;
	transition: opacity .4s ease-in-out;
  display: inline-block;
  column-break-inside: avoid;
}

div#userPhotoColumn figure img {
	width: 100%; height: auto;
	margin-bottom: 5px;
}

div#userPhotoColumn figure figcaption {
  font-size: .9rem;
	color: #444;
  line-height: 1.5;
}

div#userPhotoColumn small { 
  font-size: 1rem;
  float: right; 
  text-transform: uppercase;
  color: #aaa;
} 

div#userPhotoColumn small a { 
  color: #666; 
  text-decoration: none; 
  transition: .4s color;
}

div#userPhotoColumn:hover figure:not(:hover) {
	opacity: 0.4;
}
.all_user_community .nav-tabs { border-bottom: 2px solid #DDD; text-align: center;}
.all_user_community .nav-tabs>li {display: inline-block;float: none;margin: 0 auto;}
.all_user_community .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
.all_user_community .nav-tabs > li > a { border: none; color: #666; }
.all_user_community .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
.all_user_community .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
.all_user_community .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
@media screen and (max-width: 750px) { 
  #userPhotoColumn { column-gap: 0px; }
  #userPhotoColumn figure { width: 100%; }
}
</style>
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="all_user_community">
			<ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#my_community" aria-controls="my_community" role="tab" data-toggle="tab">My Photos</a></li>
            </ul>
            </div>
			<div id="userPhotoColumn">
			@foreach($photoList as $eachPhoto)
			  	<figure>
			  		<a href="{{ $eachPhoto->image }}" data-lightbox="userPhotosGallery"><img src="{{ $eachPhoto->image }}"></a>
				</figure>
			@endforeach	
			</div>
        </div>
	</div>
</div>
@stop

@section('extra_js')

@stop