<div class="floatingContainer">
  <div class="subActionButton person" onclick="openChatListBox();">
      <p class="floatingText"><span class="floatingTextBG">Chat</span></p>
  </div>
  <div class="actionButton" onclick="addPostbox();" data-toggle="modal" data-target=".bd-example-modal-lg"><p class="floatingText">
    <span class="floatingTextBG">Post</span></p>
  </div>
</div>
  
<form  method="post" name="user_post" action="{{route('add_new_post')}}" class="post_form">   
<div id="popupModel" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md popup_post_box">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" onclick="removeHtmlPostBox()" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="">×</span>
        </button>
      </div>
     <div class="col-lg-12" id="addPostBox">
        <div class="_4-u2 _1qby ">   
              @include('frontend.post_box',['type'=>$type,'community_id'=>$community_id]) 
        </div>
      <div class="clearfix"></div>
     </div>
      <div class="clearfix"></div>
    </div>   
  </div>
  <div class="clearfix"></div>
</div>
<div id="myModal" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                 <h3 id="myModalLabel3">Select Hobby</h3>

            </div>
            <div class="modal-body">
                    <select id="choice" name="hobby_id[]" multiple="multiple" required="required">
                    @foreach(Auth::user()->hobbies as $eachHobby)
                      <option value="{{$eachHobby->hobby->id}}">{{$eachHobby->hobby->name}}</option>
                    @endforeach
                    </select>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn-success btn" id="SubForm">Confirm and Submit Your Post</button>
            </div>
        </div>
    </div>
</div>

</form>  
<script type="text/javascript">
  
  function addPostbox(){

    //$('#addPostBox').load('{{ route("post_box_page") }}');
    //$("#addPostBox").append($("#loadNewPostBox").clone()).html();
    
    if($("#loadNewPostBox").length){
      //$('#loadNewPostBox').load('{{ route("post_box_page") }}');
      //$('#loadNewPostBox').empty();
    }
  }
  function removeHtmlPostBox(){

    //$("#loadNewPostBox").append($("#addPostBox").clone()).html();
    //$('#addPostBox').empty();
    
    //$('#loadNewPostBox').load('{{ route("post_box_page") }}');
  }
</script>