@extends('frontend.dashboard.dashboard_template')
@section('extra_css')
<style type="text/css">
.all_user_community .nav-tabs { border-bottom: 2px solid #DDD; text-align: center;}
.all_user_community .nav-tabs>li {display: inline-block;float: none;margin: 0 auto;}
.all_user_community .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
.all_user_community .nav-tabs > li > a { border: none; color: #666; }
.all_user_community .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
.all_user_community .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
.all_user_community .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.all_user_community .tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
.all_user_community .tab-pane { padding: 15px 0; }
.all_user_community .tab-content{padding:20px}
.all_user_community .each_comminity_member_img { width: 31px; border: 1px solid; border-radius: 50%; float: left;}
.all_user_community .each_community { position: relative; width: 150px; height: 200px; overflow: hidden;}
.all_user_community .each_community .community_name { position: absolute; top: 0px;}
.all_user_community .each_community .community_members_count {
    bottom: 30px;
    position: absolute;
}
.all_user_community .each_community .community_members {
    position: absolute;
    bottom: 0px;
}
</style>
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
            <div class="all_user_community">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#my_community" aria-controls="my_community" role="tab" data-toggle="tab">My Communities</a></li>
                    <li role="presentation"><a href="#recommended" aria-controls="recommended" role="tab" data-toggle="tab">Recommended</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="my_community">
                    	<div class="row">
							<div class="col-md-12">
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<a href="{{ route('community_create') }}">
									<div class="each_community">
										<p class="create_community">Create your own community</p>
									</div>
									</a>
								</div>
								<?php $userCommunities = array(); ?>
								@foreach($communityList as $eachCommunity)
								<?php $userCommunities [] = $eachCommunity->id; ?>
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<a href="{{ route('community_page',$eachCommunity->slug()) }}">
									<div class="each_community">
										<img src="{{ $eachCommunity->profile_pic }}" class="each_comminity_img" />
										<p class="community_name">{{ $eachCommunity->name }}</p>
										<p class="community_members_count">{{ $eachCommunity->members->count() }} Members</p>
										<div class="community_members">
											@foreach($eachCommunity->members->take(5) as $eachCommunityMember)
												<a href="{{ route('profile_page',$eachCommunityMember->user->username) }}">
													<img src="{{ $eachCommunityMember->user->profile_pic }}" class="each_comminity_member_img" alt="{{ $eachCommunityMember->user->name }}" title="{{ $eachCommunityMember->user->name }}" />
												</a>
											@endforeach
										</div>
									</div>
									</a>
								</div>
								@endforeach
							</div>
						</div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="recommended">
                    @foreach($loggedInUser->hobbies as $eachHobby)
                    	@if(count($eachHobby->hobby->suggestedCommunities())>0)
                    	<div class="row">
                    		{{ $eachHobby->hobby->name }}
							<div class="col-md-12">
								@foreach($eachHobby->hobby->suggestedCommunities() as $eachCommunity)
								@if(!in_array($eachCommunity->id, $userCommunities))
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<a href="{{ route('community_page',$eachCommunity->slug()) }}">
									<div class="each_community">
										<img src="{{ $eachCommunity->profile_pic }}" class="each_comminity_img" />
										<p class="community_name">{{ $eachCommunity->name }}</p>
										<p class="community_members_count">{{ $eachCommunity->members->count() }} Members</p>
										<div class="community_members">
											@foreach($eachCommunity->members->take(4) as $eachCommunityMember)
											<a href="{{ route('profile_page',$eachCommunityMember->user->username) }}">
												<img src="{{ $eachCommunityMember->user->profile_pic }}" class="each_comminity_member_img" alt="{{ $eachCommunityMember->user->name }}" title="{{ $eachCommunityMember->user->name }}" />
											</a>
											@endforeach
										</div>
									</div>
									</a>
								</div>
								@endif
								@endforeach
							</div>
						</div>
						@endif
					@endforeach
                    </div>

                </div>
			</div>
        </div>
	</div>
</div>
@stop

@section('extra_js')

@stop