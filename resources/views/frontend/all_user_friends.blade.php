@extends('frontend.dashboard.dashboard_template')
@section('extra_css')
<style type="text/css">
.all_user_friends .nav-tabs { border-bottom: 2px solid #DDD; text-align: center;}
.all_user_friends .nav-tabs>li {display: inline-block;float: none;margin: 0 auto;}
.all_user_friends .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
.all_user_friends .nav-tabs > li > a { border: none; color: #666; }
.all_user_friends .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
.all_user_friends .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
.all_user_friends .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.all_user_friends .tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
.all_user_friends .tab-pane { padding: 15px 0; }
.all_user_friends .tab-content{padding:20px}
.all_user_friends .each_user_hobby_img { width: 31px; border: 1px solid; border-radius: 50%; float: left;}
.all_user_friends .each_friend { position: relative; width: 150px; height: 200px; overflow: hidden;}
.all_user_friends .each_friend .friend_name { position: absolute; top: 0px;}
.all_user_friends .each_friend .user_hobby_count {
    bottom: 30px;
    position: absolute;
}
.all_user_friends .each_friend .user_hobbies {
    position: absolute;
    bottom: 0px;
}
</style>
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
            <div class="all_user_friends">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#my_friends" aria-controls="my_friends" role="tab" data-toggle="tab">My Friends</a></li>
                    <li role="presentation"><a href="#recommended" aria-controls="recommended" role="tab" data-toggle="tab">Recommended</a></li>
                </ul>

                <!-- Tab panes -->
                <?php $myFriends = array(); ?>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="my_friends">
                    	<div class="row">
							<div class="col-md-12">
								@foreach($friendsList as $eachFriend)
								<?php array_push($myFriends, $eachFriend->id); ?>
								<div class="eachfrnd col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<a href="{{ route('profile_page',$eachFriend->username) }}">
									<div class="each_friend">
										<img src="{{ $eachFriend->profile_pic }}" class="each_comminity_img" />
										<p class="friend_name">{{ $eachFriend->name }}</p>
										<p class="user_hobby_count">{{ $eachFriend->hobbies->count() }} Hobbies</p>
										<div class="user_hobbies">
											@foreach($eachFriend->hobbies->take(5) as $eachHobby)
												<a href="{{ route('manage_hobby_by_name',$eachHobby->hobby->slug()) }}">
												<img src="{{ $eachHobby->hobby->profile_pic }}" class="each_user_hobby_img" alt="{{ $eachHobby->hobby->name }}" title="{{ $eachHobby->hobby->name }}" />
												</a>
											@endforeach
										</div>
									</div>
									</a>
								</div>
								@endforeach
							</div>
						</div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="recommended">
                    @foreach($loggedInUser->hobbies as $eachHobby)
                    <?php $ifHeaderPrinted = false; ?>
                    	<div class="row">
                    		<?php $friendsList = $loggedInUser->hobbyWisefriends($eachHobby->hobby); ?>
                    		@if(count($friendsList) > 0)
							<div class="col-md-12">
								@foreach($loggedInUser->hobbyWisefriends($eachHobby->hobby) as $eachFriend)
								@if(!in_array($eachFriend->id, $myFriends))
									@if(!$ifHeaderPrinted)
										{{ $eachHobby->hobby->name }}
										<?php $ifHeaderPrinted = true; ?>
									@endif
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<a href="{{ route('profile_page',$eachFriend->username) }}">
									<div class="each_friend">
										<img src="{{ $eachFriend->profile_pic }}" class="each_comminity_img" />
										<p class="friend_name">{{ $eachFriend->name }}</p>
										<p class="user_hobby_count">{{ $eachFriend->hobbies->count() }} Hobbies</p>
										<div class="user_hobbies">
											@foreach($eachFriend->hobbies->take(4) as $eachHobby)
												<a href="{{ route('manage_hobby_by_name',$eachHobby->hobby->slug()) }}">
												<img src="{{ $eachHobby->hobby->profile_pic }}" class="each_user_hobby_img" alt="{{ $eachHobby->hobby->name }}" title="{{ $eachHobby->hobby->name }}" />
												</a>
											@endforeach
										</div>
									</div>
									</a>
								</div>
								@endif
								@endforeach
							</div>
							@endif
						</div>
					@endforeach
                    </div>

                </div>
			</div>
        </div>
	</div>
</div>
@stop

@section('extra_js')

@stop