@extends('frontend.dashboard.template')

@section('extra_css')
<!-- <link rel="stylesheet" type="text/css" href="{{ Cdn::asset('frontend/dashboard/assets/css/custom.css') }}"> -->
<link rel="stylesheet" type="text/css" href="{{ Cdn::asset('frontend/admin/assets/css/form-icheck.css') }}">
<style type="text/css">
    
    .area_height{

        height:130px !important;
    }
</style>
@stop

@section('content')

<div class="container-fluid xyz">
    <div class="row">

        <div class="col-lg-8 mdlBlock">

            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5 style="text-align: center;">Update Your Profile <br/><small>(update profile for more expirience..)</small></h5>
                    <div class="ibox-tools">
                      
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6 b-r">
                        <!-- <h3 class="m-t-none m-b">Update Your Profile</h3>
                            <p>update profile for more expirience.</p> -->
                            <form method="post" role="form" action="#">
                                <input type="hidden" value="{{csrf_token()}}" name="_token"></input>
                                    <div class="form-group"><label>Name</label> 
                                    <input value="{{$user->name}}" type="text" placeholder="Enter Your Name" class="form-control">
                                </div>

                                <div class="form-group"><label>Status</label> 
                                    <input value="{{$user->userSettings->status}}" type="text" placeholder="Put a tag line of your profile" class="form-control">
                                </div>

                                <div class="form-group"><label>Change Password</label>
                                    <input type="password" placeholder="Please Enter a password" class="form-control">
                                </div>

                                <div class="form-group"><label>About Me</label> 
                                    <textarea placeholder="Write a short description about yourself.." class="form-control area_height">{{$user->userSettings->user_about_me}}</textarea> </div>
                                <div>
                                    <button class="btn btn-success btn-primary pull-right m-t-n-xs" type="submit"><strong>Save</strong></button>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-6"><h4>Change Profile Picture</h4>
                            
                            
                        </div>
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>

        </div>

            
           

           
    </div>


</div>
@stop

@section('extra_js')
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.caret.min.js') }}"></script>

@stop