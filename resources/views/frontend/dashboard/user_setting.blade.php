@extends('frontend.dashboard.template')

@section('extra_css')
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/dropstyle.css') }}" rel="stylesheet">
<style type="text/css">
    
    .submit{

        margin-left: 115px;
    }
</style>
@stop

@section('content')
<div class="container-fluid">

    
    <div class="row">
        <div class="center_div col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form class="frm" action="{{route('save-user-setting')}}" method="post" enctype="multipart/form-data">
                <div class="user_settings_form col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <?php $user_phone_no = !empty($user->phone)?$user->phone:null; ?>
                    <p> Update Profile </p>
                    <div class="cm"> Update your profile and share it with the world... </div>
                    <input value="{{$user->name}}" name="name" autocomplete="off" placeholder="Name" type="text" class="meterial_input" required pattern="[a-zA-Z\s]+" title="Only enter letters"/>
                    <input value="{{$user->id}}" name="user_id" autocomplete="off" type="hidden" required/>

                    <!-- <input value="{{$user->password}}" name="password" autocomplete="off" placeholder="Password" type="password"> -->
                    <input value="{{$user->email}}" name="email" autocomplete="off" placeholder="E-mail" type="email" class="meterial_input" readonly="readonly" />
                    @if($user->email_verified == "1")
                      <span><i class="fi-check"></i> Email is verified.</span>
                    @else
                      <span><i class="fi-x"></i> Email not verified. <a href="{{ route('verify_email') }}">Verify</a></span>
                    @endif
                    <input value="{{$user_phone_no}}" name="phone" autocomplete="off" placeholder="Phone Number" type="number" class="meterial_input" id="phone_no" max="9999999999" />
                    @if($user->phone_no_verified == "1")
                        <span><i class="fi-check"></i> Phone number is verified.</span>
                    @else
                        <span><i class="fi-x"></i> Phone number is not verified. <a href="javascript:verifyPhone()">Verify</a></span>
                    @endif
                      <p id="phone_otp_p" class="button-height inline-label"></p>
                    <input value="{{$user->userSettings->user_status}}" name="profile_status" autocomplete="off" placeholder="Profile Status" class="meterial_input" type="text"/>

                    {{-- <input value="{{$user->userSettings->user_about_me}}" name="about_me" autocomplete="off" placeholder="Write something about you" class="meterial_input" type="text"/> --}}

                    <textarea name="about_me" placeholder="write something about you" autocomplete="off" rows="7" class="meterial_input">{{$user->userSettings->user_about_me}}</textarea>

                    <hr>

                    <div style="margin-bottom: 20%;">
                    </div>
                    
                    
                </div>
                <div class="dropbox col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <p> Profile Picture </p>
                        <div id="drop" class="drop" style="background-image:url('{{ $user->profile_pic }}')">
                            <a>Browse</a>
                            <input type="file" id="profile_img" name="profile_image" />
                        </div>
                    <p>Cover Photo</p>
                        <div id="drop1" class="drop" style="background-image:url('{{ $user->cover_pic }}')">
                            <a>Browse</a>
                            <input type="file" id="cover_img" name="cover_img" />
                        </div>
                </div>
                <div class="user_settings_submit"><button class="submit">Submit</button></div>
            </form>
        </div>
    </div>
</div>

@stop

@section('extra_js')
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.knob.js') }}"></script>
<!-- jQuery File Upload Dependencies -->
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.ui.widget.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.fileupload.js') }}"></script>
<!-- Our main JS file -->
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/dropscript.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(e) {
        $("#profile_img").change(function() { 
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))
            { 
                var reader = new FileReader();
                reader.onload = imageIsLoadedProfile;
                reader.readAsDataURL(this.files[0]);
            }
        });
        $("#cover_img").change(function() { 
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))
            { 
                var reader = new FileReader();
                reader.onload = imageIsLoadedCover;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    function imageIsLoadedProfile(e) {
        $('#drop').css('background-image', 'url(' + e.target.result + ')');
    }
    function imageIsLoadedCover(e) {
        $('#drop1').css('background-image', 'url(' + e.target.result + ')');
    }
</script>
<script>
//verify Phone Number
function verifyPhone(){
  var new_no = $('#phone_no').val();
  if (new_no.length!=10) {
    alert('Phone number is not valid');
    return false;
  };
  var url = '{!! route('verify_phone') !!}';
  $.ajax(url, {
        type: 'POST',
        data: {
          _token: '{{ csrf_token() }}',
          new_no: new_no
        },
        success: function(data)
        {
          if (data.status == 'success')
          { 
              document.getElementById('phone_otp_p').innerHTML = data.html;
          }else{
              alert(data.error_msg);
          }
        },
        error: function()
        {
          alert('Error while contacting server, please try again');
        }
  });
}
function verifyPhone2(){
  var url = '{!! route('verify_phone_otp') !!}';
  var otp_last_5 = document.getElementById('otp_last_5').value;
  $.ajax(url, {
        type: 'POST',
        data: {
          _token: '{{ csrf_token() }}',
          otp_last_5: otp_last_5
        },
        success: function(data)
        {
          if (data.status == 'success')
          { 
              alert("Phone number verified successfully");
                setTimeout(function() {
                document.location.href = '{!! route('user_setting',$user->id) !!}'
              }, 2000);
          }else{
            var obj = data.errors;
            var arr = Object.keys(obj).map(function (key) {return obj[key]});
            for (var i = 0; i < arr.length; i++) {
              alert(arr[i]);
            }
        }
        },
        error: function()
        {
          alert('Error while contacting server, please try again');
        }
  });
}
</script>
@stop