<div class="footer">
    <ul class="footList">
        <li><a href="{{ route('welcome_page') }}">Home</a></li>
        <li><a href="{{ route('about_us_page') }}">AboutUs</a></li>
        <li><a href="{{route('search_slug','community')}}">Search Community</a></li>
        <li><a href="{{route('search_slug','friends')}}">Search Friends</a></li>
        <li><a href="{{ route('explain_our_video') }}">Explainer Videos</a></li>
        <li><a href="{{ route('hn_video') }}">HN Videos</a></li>
        <li><a href="{{ route('faq_page') }}">Faq</a></li>
        <li><a href="{{ route('privacy_policy_page') }}">Privacy Policy</a></li>
        <li><a href="{{ route('terms_of_use_page') }}">Terms of use</a></li>
        <li><a href="{{ route('contact_us_page') }}">Contact Us</a></li>
    </ul>
    <p>&copy; <?php echo date("Y"); ?> <a href="#">Hobby Networking</a> all rights reserved</p>
    <div class="clearfix"></div>
</div>
@if(Auth::check())
  @include('frontend.chat.chat_box')
@else
<div id="login_modal" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h3 id="myModalLabel3">Login</h3>
            </div>
            <div class="modal-body">
              
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
</div>
@endif
<div class="modal fade" id="tutorial_link" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div> -->
        <div class="modal-body">
          <p>Welcome to Hobbynetworking {{ ucwords(@Auth::user()->name) }}! This is Your Platform for expressing your Passion. 
You can check  all our Explainer Videos <a id="link" href="{{ route('explain_our_video') }}" target="_blank" style="color: #ef542e;">here</a>.
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" id="tutorial_link_close" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery-1.9.1.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery-ui.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/Am2_SimpleSlider.js') }}" type="text/javascript"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/lightbox.js') }}" type="text/javascript"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/idangerous.swiper.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.viewportchecker.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/global.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.cookie.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.urlive.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.noty.packaged.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/bootstrap.youtubepopup.min.js') }}"></script>
@if(!Auth::check())
<script src="{{ Cdn::asset('frontend/welcome/assets/js/modernizr.js') }}"></script>
<script src="{{ Cdn::asset('frontend/welcome/assets/js/main.js') }}"></script>
@endif
<script type="text/javascript">
   //$('.gallery-img').Am2_SimpleSlider();
   lightbox.option({
    'resizeDuration': 200,
    'wrapAround': true
  })
</script>
<script type="text/javascript"> 

    $(document).ready(function () 
    { 
      reopenChat();
      @if (!Auth::check())
      $('#open_login_modal').click(function(argument) {
        $('#login_modal').show();
      });
      @endif
      $('.floatingContainer').hover(function(){ console.log(1);
        $('.subActionButton').addClass('display');
      }, function(){
        $('.subActionButton').removeClass('display');
        $('.actionButton').removeClass('open');
      });
      $('.subActionButton').hover(function(){
        $(this).find('.floatingText').addClass('show');
      }, function(){
        $(this).find('.floatingText').removeClass('show');
      });

      $('.actionButton').hover(function(){
        $(this).addClass('open');
        $(this).find('.floatingText').addClass('show');
        $('.subActionButton').addClass('display');
      }, function(){
        $(this).find('.floatingText').removeClass('show');
      });
      // Get viewport height, gridTop and gridBottom
      var windowHeight = $(window).height(),
      gridTop = windowHeight * .3,
      gridBottom = windowHeight * .6; 
       
      // $(window).on('scroll', function() {
      //   var thisTop = $('#myVideo_demo').offset().top - $(window).scrollTop(); // Get the `top` of this `li`
      //   //alert(thisTop);
      //   var $video = $('#myVideo_demo');
      //  // $video[0].play();
      //   // Check if this element is in the interested viewport
      //   if (thisTop >= gridTop && (thisTop + $(this).height()) <= gridBottom) {
      //     //alert('ok'); 
      //     $video[0].videocontrols( 
      //     { 
      //         markers: [40, 84, 158, 194, 236, 272, 317, 344, 397, 447, 490, 580], 
      //         preview: 
      //         { 
      //             sprites: ['big_bunny_108p_preview.jpg'], 
      //             step: 10, 
      //             width: 200 
      //         }, 
      //         theme: 
      //         { 
      //             progressbar: 'blue', 
      //             range: 'pink', 
      //             volume: 'pink' 
      //         } 
      //     });
      //   } else {
      //     //$(this).css('background', 'gray');
      //   }
      // });    
      //$( ".post_links" ).urlive({container: $(this).find('.posted_url_preview')});
      $('.post_links').urlive({container:'.posted_url_preview', callbacks: {onSuccess: function(data) {
          //console.log(data);
          $(this).find('.posted_url_preview').html('11'); 
      },}});


      //$('.post_content').urlive({container: '.urlive-container'});
      $('.post_content').on('input propertychange', function () {
         $(this).urlive({container: '.urlive-container'});
      }).trigger('input');
 
    }); 
  var checkChatBatch={};
  var canCheckChat={};
    function startChat(senderId, receiverId) {
        if ($.cookie("openchat") != null) {
          var data = JSON.parse($.cookie("openchat"));
          var canPush = true;
          $.each(data.items, function (index, eachData) {
              if(receiverId==eachData.receiverId){
                canPush = false;
              }
          });
          if(canPush){
            data.items.push(
                {'senderId':senderId,'receiverId':receiverId}
            );
          }
          $.removeCookie('openchat');
          $.cookie("openchat",JSON.stringify(data));
        }else{
          $.removeCookie('openchat');
          $.cookie("openchat", JSON.stringify({'items':[{'senderId':senderId,'receiverId':receiverId}]}));
        }
        //console.log("Sender Id:"+senderId+" Receiver Id:"+receiverId);
        openChatBox("Loading...","",receiverId);
        $.ajax({
          url: "{{ route('messages.load.chatbox') }}", 
          type: "POST",
          data: {
              'senderId':senderId,
              'receiverId':receiverId,
              'call_type': "start_chat",
              '_token':"{{ csrf_token() }}"
          },
          success: function(result){
            openChatBox(result.chatTitle,result.messages,receiverId);
            checkChatBatch[receiverId] = setInterval(function() {checkChat(senderId, receiverId); }, 1000);
            canCheckChat[receiverId] = true;
          }
        });
    }
    
    function checkChat(senderId, receiverId) {
      if($('.chat-window[data-receiver="'+receiverId+'"]').length<1){
        clearInterval(checkChatBatch[receiverId]);
      }
      if(canCheckChat[receiverId]) {
        canCheckChat[receiverId] = false;
        $.ajax({
          url: "{{ route('messages.load.chatbox') }}", 
          type: "POST",
          data: {
              'senderId':senderId,
              'receiverId':receiverId,
              'call_type': "check_chat",
              '_token':"{{ csrf_token() }}"
          },
          success: function(result){
            canCheckChat[receiverId] = true;
            //console.log(result);
            if (result.has_unread_msg) {
              if($('.chat-window[data-receiver="'+receiverId+'"]').length>0){
                openChatBox(result.chatTitle,result.messages,receiverId);
              }
            }
          }
        });
      }
    }
    function reopenChat() {
      if ($.cookie("openchat") != null) {
        var data = JSON.parse($.cookie("openchat"));
        $.each(data.items, function (index, eachData) {
            startChat(eachData.senderId, eachData.receiverId);
        })
      }
    }
</script>
<script>
$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
     $("#menu-toggle-2").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled-2");
        $('#menu ul').hide();
        if($("#wrapper").hasClass("")){
        var n = noty({
          text: 'NOTIFY WITH LINK EXAMPLE',
          type: 'success',
          dismissQueue: true,
          layout: 'bottomCenter',
          closeWith: ['click'],
          theme: 'relax',
          maxVisible: 2,
          animation: {
            open: {height: 'toggle'},
            close: {height: 'toggle'},
            easing: 'swing',
            speed: 1500 // opening & closing animation speed
      }

    });
        $(".noty_bar").css("background-color","#dc4622");
        $(".noty_text").css("color","#FFFFFF");
        //$('.noty_text').html('You can check out our Explainer Videos for this section. <a href="https://www.youtube.com/watch?v=0ZoGST1Wmtk" target="_blank" style="color: #000000; text-decoration: underline;">SIDE PANEL</a>');
        $('.noty_text').html('You can check out our Explainer Videos for this section. <a <a href="javascript:void(0)" rel="0ZoGST1Wmtk" class="youtube1" style="color: #000000; text-decoration: underline;">SIDE PANEL</a>');

    // setTimeout(function () {
    //     $('#noty_bottomRight_layout_container').find('li').trigger('click');
    // }, 5000);
    setTimeout(function(){ $('#noty_bottomCenter_layout_container').find('li').fadeOut() }, 5000);

      
        }

        $(function () {
          $(".youtube1").YouTubeModal({autoplay:0});
        });
    });
     function initMenu() {
      $('#menu ul').hide();
      $('#menu ul').children('.current').parent().show();
      //$('#menu ul:first').show();
      $('#menu li a').click(
        function() {
          var checkElement = $(this).next();
          if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            return false;
            }
          if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#menu ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
            return false;
            }
          }
        );
      }
    $(document).ready(function() {initMenu();});
</script>
<script type="text/javascript">
    window._pt_lt = new Date().getTime();
    window._pt_sp_2 = [];
    _pt_sp_2.push('setAccount,5d1671a9');
    var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    (function() {
        var atag = document.createElement('script'); atag.type = 'text/javascript'; atag.async = true;
        atag.src = _protocol + 'cjs.ptengine.com/pta_en.js';
        var stag = document.createElement('script'); stag.type = 'text/javascript'; stag.async = true;
        stag.src = _protocol + 'cjs.ptengine.com/pts.js';
        var s = document.getElementsByTagName('script')[0]; 
        s.parentNode.insertBefore(atag, s); s.parentNode.insertBefore(stag, s);
    })();
</script>
<style type="text/css">
 @media only screen and (max-width: 768px) { 
  .YouTubeModal {
  text-align: center;
  padding: 0!important;
  }

  .YouTubeModal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog{
    width:95%!important;
    height:75%!important; 
    max-width: 650px!important;
    display: inline-block;
  text-align: left;
  vertical-align: middle;
    /*top: 30%!important;*/
  }
  .modal-dialog iframe{ 
   width: 97%!important;
   height: 70%!important;
  }
  
.noty_text .youtube {

  color: #000000; text-decoration: underline;
}


}
.noty_text .youtube {

  color: #000000 !important; text-decoration: underline;
}

</style>
@if (Auth::check())
<script type="text/javascript">
  $(document).ready(function(){
      var cookieValue = $.cookie("tutorial");
      if (typeof  cookieValue === 'undefined'){
        //no cookie
          $('#tutorial_link').modal('show');
          $("#tutorial_link_close").click(function () {
            $.cookie("tutorial", 1, { expires : 1, path: '/' }); 
          });

          $("#link").click(function () {
            $('#tutorial_link').modal('hide');
            $.cookie("tutorial", 1, { expires : 1, path: '/' }); 
          });
      }       
  })
</script>
@endif
@yield('extra_js')