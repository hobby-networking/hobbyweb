<header>
<style type="text/css">
    .noto-content.read{
        background-color: #edeff2;
    }
</style>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        @if(Auth::check() && !Auth::user()->isAdmin())
        <ul class="nav navbar-nav">
            <li class="active" >
            <button class="navbar-toggle collapse in" data-toggle="collapse" id="menu-toggle-2"> 
            <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            </li>
        </ul>
        @endif
        @if(Auth::check() && Auth::user()->isAdmin())
            <ul class="nav navbar-nav">
                <li class="active" >
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="navbar-toggle collapse in"> 
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        @endif
        <div class="container-fluid custom-container">
            <div class="row no_row row-header">
              <div class="brand-be"> <a href="{{ url('/') }}">
                <img src="{{ Cdn::asset('frontend/welcome/assets/images/footer-hobby-networking-logo.png') }}" alt="Hobby" class="be_logo"></a> </div>
                <div class="login-header-block">
                @if(Auth::check())
                    @if(!Auth::user()->isAdmin())
                    <div class="login_block">
                        <?php
                        $notifications = $currentLoggedInUser->getNotificationsNotRead($limit = null, $paginate = null, $order = 'desc');
                        //dd($currentLoggedInUser->communityJoinRequest());
                        ?>                                                                   
                        <a class="notofications-popup" href="#">
                            <i class="fa fa-bell"></i>
                            @if(count($notifications)+count($friendRequests)+count($currentLoggedInUser->communityJoinRequest()) > 0)
                            <span class="noto-count">{{ count($notifications)+count($friendRequests)+count($currentLoggedInUser->communityJoinRequest()) }}</span>
                            @endif
                        </a>
                        <div class="noto-popup notofications-block">
                            <div class="noto-label">Notification</div>
                            <div class="noto-body" style="max-height: 517px;">
                            @if(is_array($friendRequests))
                            @foreach($friendRequests as $eachFriendRequest)
                                <div class="noto-entry">
                                    <div class="noto-content clearfix">
                                        <div class="noto-img">  
                                            <a href="{{ route('profile_page',$eachFriendRequest->username) }}">
                                                <img src="{{ $eachFriendRequest->profile_pic}}" alt="" class="be-ava-comment">
                                            </a>
                                        </div>
                                        <div class="noto-text">
                                            <div class="noto-text-top">
                                                <span class="noto-name"><a href="{{ route('profile_page',$eachFriendRequest->username) }}">{{ $eachFriendRequest->name}}</a></span>
                                                <span class="noto-date"><i class="fa fa-user"></i> Friend Request</span>
                                            </div>
                                            <a href="{{ route('accept_friend_request',$eachFriendRequest->id) }}" class="noto-message">Accept</a>
                                            <a href="{{ route('deny_friend_request',$eachFriendRequest->id) }}" class="noto-message">Reject</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach 
                            @endif
                            @if(is_array($currentLoggedInUser->communityJoinRequest()))
                            @foreach($currentLoggedInUser->communityJoinRequest() as $eachCommunityJoinRequest)
                                <div class="noto-entry">
                                    <div class="noto-content clearfix">
                                        <div class="noto-img">  
                                            <a href="{{ route('profile_page',$eachCommunityJoinRequest->user->username) }}">
                                                <img src="{{ $eachCommunityJoinRequest->user->profile_pic}}" alt="" class="be-ava-comment">
                                            </a>
                                        </div>
                                        <div class="noto-text">
                                            <div class="noto-text-top">
                                                <span class="noto-name"><a href="{{ route('profile_page',$eachCommunityJoinRequest->user->username) }}">{{ $eachCommunityJoinRequest->user->name}}</a> Requested to join <a href="{{ route('community_page',$eachCommunityJoinRequest->community->slug) }}">{{ $eachCommunityJoinRequest->community->name}}</a></span>
                                                <span class="noto-date"><i class="fa fa-user"></i> Community Request</span>
                                            </div>
                                            <a href="{{ route('accept_community_request',$eachCommunityJoinRequest->id) }}" class="noto-message">Accept</a>
                                            <a href="{{ route('deny_community_request',$eachCommunityJoinRequest->id) }}" class="noto-message">Reject</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach   
                            @endif
                            @foreach($currentLoggedInUser->getNotifications($limit = null, $paginate = null, $order = 'desc') as $eachNotification)
                            {{-- if($eachNotification->read) --}}
                                <div class="noto-entry">
                                    <div class="noto-content clearfix <?php if($eachNotification->read){ echo "read";}?>">
                                    @if(!empty($eachNotification->extra['img']))
                                        <div class="noto-img">  
                                            <a href="{{ route('read_notification',$eachNotification->id) }}">
                                                <img src="{{ $eachNotification->extra['img'] }}" alt="" class="be-ava-comment">
                                            </a>
                                        </div>
                                        <div class="noto-text">
                                            <div class="noto-text-top">
                                                <span class="noto-name"><a href="{{ route('read_notification',$eachNotification->id) }}">{{ $eachNotification->notify_body }}</a></span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="noto-text-notify">
                                            <div class="noto-text-top">
                                                <span class="noto-name"><a href="{{ route('read_notification',$eachNotification->id) }}">{{ $eachNotification->notify_body }}</a></span>
                                            </div>
                                        </div>
                                    @endif
                                    </div>
                                </div>
                            @endforeach                                         
                            </div>                          
                        </div>
                        <?php
                            $user = Auth::user();
                            $msgThreads = $user->threads;
                            //dd($msgThreads);
                            $unreadMsgCount = 0;
                            //dd($msgThreads[2]->userUnreadMessagesCount($user->id));
                            foreach ($msgThreads as $eachMsgThreads) { 
                                //dd($eachMsgThreads->userUnreadMessagesCount($user->id));
                                $unreadMsgCount += intval($eachMsgThreads->userUnreadMessagesCount($user->id));
                            }
                            //dd($unreadMsgCount);
                        ?>
                        <a class="messages-popup" href="#">
                            <i class="fa fa-envelope"></i>
                            @if($unreadMsgCount > 0)
                            <span class="noto-count">{{ $unreadMsgCount }}</span>
                            @endif
                        </a>
                        <div class="noto-popup messages-block">
                            <div class="noto-label">Messages</div>
                            <div class="noto-body" style="max-height: 517px;">
                                @foreach ($msgThreads as $eachMsgThreads)
                                <?php $msg = $eachMsgThreads->getLatestMessageAttribute(); ?>
                                @if(!empty($msg) && !empty($msg->recipients[0]))
                                <?php
                                    $recipient = $msg->user;
                                    if (Auth::user()->id == $recipient->id) {
                                        $recipient = App\Models\User::find($msg->recipients[0]->user_id);
                                    }
                                ?>
                                <div class="noto-entry style-2">
                                    <div class="noto-content clearfix">
                                        <div class="noto-img">  
                                            <a href="javascript:startChat({{Auth::user()->id}},{{$recipient->id}})">
                                                <img src="{{ $recipient->profile_pic }}" alt="" class="be-ava-comment">
                                            </a>
                                        </div>
                                        <div class="noto-text">
                                            <div class="noto-text-top">
                                                <span class="noto-name"><a href="javascript:startChat({{Auth::user()->id}},{{$recipient->id}})">{{ $recipient->name }}</a></span>
                                                <span class="noto-date"><i class="fa fa-clock-o"></i> {{ $msg->created_at->diffForHumans() }}</span>
                                            </div>
                                            <div class="noto-message">{{ $msg->body }}</div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach                                          
                            </div>                          
                        </div>
                        <div class="be-drop-down login-user-down">
                            <img class="login-user" src="{{ Auth::user()->profile_pic }}" alt="{{ Auth::user()->name }}" width="40px" height="40px">
                            <span class="be-dropdown-content">Hi, <span>{{ substr(Auth::user()->name,0,16) }}</span></span>
                            <ul class="drop-down-list">
                                <li><a href="{{route('profile_page',Auth::user()->username)}}">Profile</a></li>
                                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                            </ul>
                        </div>
                        <a href="{{ route('invitation') }}" class="invite_icon"><i class="fa fa-user"></i>Invite</a>
                    </div>  
                    @endif
                @else
                <a class="login-popup-modal" href="{{ url('/') }}">
                    <i class="fa fa-user"> Login</i>
                </a>
                @endif
                </div>
                <div class="header-menu-block col-lg-4">
                    <button class="cmn-toggle-switch cmn-toggle-switch__htx"><span></span></button>
                    <form action="{{ route('search_slug') }}" class="input-search header-menu">
                      <div class="form-group fg_icon">
                        <input class="form-input" type="text" placeholder="Search Hobbynetworking..." name="slug">
                        <i class="fa fa-search"></i></div>
                      <input type="submit" value="">
                    </form>
                </div>
            </div>
        </div>
    </div><!-- bs-example-navbar-collapse-1 -->
    <div class="navbar-header fixed-brand">
        @if(Auth::check() && !Auth::user()->isAdmin())
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
          <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        @endif
        <div class="brand-be"> <a href="{{ url('/') }}">
        <img src="{{ Cdn::asset('frontend/welcome/assets/images/footer-hobby-networking-logo.png') }}" alt="Hobby" class="be_logo"></a> </div>
    </div><!-- navbar-header-->
  
</header>