<div id="sidebar-wrapper">
    <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
        @if(Auth::check())
        <?php $user = Auth::user(); 
           $currentRouteName = \Route::getCurrentRoute()->getName();

        ?>
        <li> <a href="{{ route('welcome_page') }}">My Feed</a> </li>
        <li <?php if($currentRouteName == "all.user.hobby") {?> class="active"<?php } ?>> <a href="{{ route('all.user.hobby') }}">Hobbies</a> </li>
        <li <?php if($currentRouteName == "all.user.community") {?> class="active"<?php } ?>> <a href="{{ route('all.user.community') }}">Communities</a> </li>
        <li <?php if($currentRouteName == "all.user.photos") {?> class="active"<?php } ?>> <a href="{{ route('all.user.photos') }}">Photos</a> </li>
        <li <?php if($currentRouteName == "all.user.friends") {?> class="active"<?php } ?>> <a href="{{ route('all.user.friends') }}">Friends</a> </li>
        <li> <a href="{{ route('welcome_page') }}">Posts</a> </li>
        <li class="show_in_mobile"><a href="{{route('profile_page',Auth::user()->username)}}">My Profile</a></li>
        <li <?php if($currentRouteName == "user_setting") {?> class="active"<?php } ?>> <a href="{{ route('user_setting',$user->id) }}">Edit Profile</a> </li>
        <li class="show_in_mobile"><a href="{{ url('/auth/logout') }}">Logout</a></li>
        @else
        <li>
            <a href="{{ url('/') }}">Login</a>
        </li>
        @endif
    </ul>
</div>