<nav class="navbar navbar-default no-margin">
<!-- Brand and toggle get grouped for better mobile display -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li class="active" >
            <button class="navbar-toggle collapse in" data-toggle="collapse" id="menu-toggle-2"> 
            <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            </li>
        </ul>
    </div><!-- bs-example-navbar-collapse-1 -->
    <div class="navbar-header fixed-brand">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
          <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <a class="navbar-brand" href="{{ url('/') }}">hobbynetworking</a>
    </div><!-- navbar-header-->
    @if(Auth::check())
       <?php /*dd($currentLoggedInUser);*/ ?>
    <div class="right_section right">
        <!-- Friends Request Section Start -->
        <div class="friendrequest">
            <button class="friendRequestButton" data-toggle="collapse" data-target="#friendRequestList"><i class="fa fa-users" aria-hidden="true"></i><span class="count">{{ (count($friendRequests)>0)?count($friendRequests):"" }}</span></button>
            <div id="friendRequestList" class="collapse">
                @if(count($friendRequests)>0)
                @foreach($friendRequests as $eachFriendRequest)
                    <div class="eachFriendRequestRow">
                        <a href="{{ route('profile_page',$eachFriendRequest->username) }}">
                            <img src="{{ $eachFriendRequest->profile_pic}}" class="img-responsive img-circle imgBoder">
                            <p class="friendRequestName">{{ $eachFriendRequest->name}}</p>
                        </a>
                        <div class="friendRequestActions">
                            <a href="{{ route('accept_friend_request',$eachFriendRequest->id) }}"><i class="fa fa-i fa-check-square-o " aria-hidden="true"></i></a>
                            <a href="{{ route('deny_friend_request',$eachFriendRequest->id) }}"><i class="fa fa-i fa-minus-square-o" aria-hidden="true"></i></a>
                        </div>
                    </div>
                @endforeach
                @else
                    <p>No Friend Request !!!</p>
                @endif
            </div>
        </div>
        <!-- Friends Request Section End -->

        <!-- Notification Section Start -->
        <?php
        $notifications = $currentLoggedInUser->getNotificationsNotRead($limit = null, $paginate = null, $order = 'desc');
        ?>
        <div class="notificationBox">
            <button class="notificationButton no_style" data-toggle="collapse" data-target="#notificationList"><i class="fa fa-bell-o" aria-hidden="true"></i><span class="count">{{ (count($notifications)>0)?count($notifications):"" }}</span></button>
            <div id="notificationList" class="collapse">
                @if(count($notifications)>0)
                @foreach($notifications as $eachNotification)
                    <div class="eachNotificationRow">
                        <a href="{{ route('read_notification',$eachNotification->id) }}">
                            <p class="notificationBody">{{ $eachNotification->notify_body }}</p>
                        </a>
                    </div>
                @endforeach
                @else
                    <p>No Unread Notification !!!</p>
                @endif
            </div>
        </div>
        <!-- Notification Section End -->
    </div>
    @endif
</nav>