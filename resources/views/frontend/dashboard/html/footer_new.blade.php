<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="footer-list-1 col-md-6 col-xs-12">
        <a href="#"><img src="img/logo-ftr.png"></a>
        <p>Lorem ipsum dolor sit amet, adipisicing elit consectetur, sed do eiusmod tempor incididunt ut dolore magna aliqua labore et. Ut enim ad minim veniam dolore magna aliqua. Ut enim ad minim veniam.</p>

        <ul>
          <li><a href="tel:+11234567890"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>+1 (123) 456-7890</a></li>
          <li><a href="mailto: help@hobbynetworking.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>help@hobbynetworking.com</a></li>
        </ul>
      </div>
      <div class="footer-list-2 col-md-2 col-xs-12">
        <h3>Want To Help?</h3>
        <ul>
          <li><a href="#">Group</a></li>
          <li><a href="#">Search Friends</a></li>
          <li><a href="#">Search Hobbies</a></li>
        </ul>
      </div>
      <div class="footer-list-3 col-md-4 col-xs-12">
        <h3>Know More?</h3>
        <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">Events</a></li>
          <li><a href="#">Service</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Shop</a></li>
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Gallery</a></li>
          <li><a href="#">Testimonials</a></li>
        </ul>
      </div>
      <div class="col-md-12 col-xs-12 copy">
        <div class="container">
          <div class="row">
            <p>Copyright &copy; <?php echo date("Y"); ?> hobbynetworking.com. All Rights Reserved</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<script type="text/javascript">
    window._pt_lt = new Date().getTime();
    window._pt_sp_2 = [];
    _pt_sp_2.push('setAccount,5d1671a9');
    var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    (function() {
        var atag = document.createElement('script'); atag.type = 'text/javascript'; atag.async = true;
        atag.src = _protocol + 'cjs.ptengine.com/pta_en.js';
        var stag = document.createElement('script'); stag.type = 'text/javascript'; stag.async = true;
        stag.src = _protocol + 'cjs.ptengine.com/pts.js';
        var s = document.getElementsByTagName('script')[0]; 
        s.parentNode.insertBefore(atag, s); s.parentNode.insertBefore(stag, s);
    })();
</script>