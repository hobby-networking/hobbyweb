<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>@yield('title', 'Welcome :: ') HobbyNetworking.com</title>
<link rel="icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
<link rel="shortcut icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
<meta name="author" content="HobbyNetworking" />
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300,900' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="{{ Cdn::asset('frontend/dashboard/assets/css/font-awesome.min.css') }}">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/main.css') }}" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="{{ Cdn::asset('frontend/dashboard/assets/jquery.videocontrols.css') }}">
<!-- Toastr style -->
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/mui.min.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/mui.min.js') }}"></script>
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/SimpleSlider.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/lightbox.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/stylesheet.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/loader.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/idangerous.swiper.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/jquery.urlive.css') }}" rel="stylesheet" />
@if(\Request::route()->getName() == "welcome_page")
<meta property="og:title" content="Welcome to Hobby Networking."/>
<meta property="og:image" content="https://scontent.fmaa1-1.fna.fbcdn.net/t31.0-8/12471394_547065292117403_5793609828223003075_o.jpg""/>
<meta property="og:description" content="Welcome to Hobby Networking. Click here to signup."/>
@endif
@yield('extra_css')
{!! Analytics::render() !!}
@php 
	$current_url = Request::getPathInfo();
	$urlArr = explode("/",$current_url);
	$last_value = end($urlArr);
	if($last_value==''){
		$last_value = 'home';
	}
	$seo_details = Settings::getSeodata($last_value);
@endphp
<meta property="og:title" content="{{@$seo_details->meta_title}}"/>
<meta property="og:keywords" content="{{@$seo_details->meta_keywords}}"/>
<!-- <meta property="og:image" content="https://scontent.fmaa1-1.fna.fbcdn.net/t31.0-8/12471394_547065292117403_5793609828223003075_o.jpg""/> -->
<meta property="og:description" content="{{@$seo_details->meta_desc}}"/>
@yield('extra_css')
<link rel="manifest" href="{{ URL::asset('manifest.json') }}">

<script type="text/javascript">
(function(i,s,o,g,r,a,m,n){
i['moengage_object']=r;t={}; q = function(f){return function(){(i['moengage_q']=i['moengage_q']||[]).push({f:f,a:arguments});};};
f = ['track_event','add_user_attribute','add_first_name','add_last_name','add_email','add_mobile',
'add_user_name','add_gender','add_birthday','destroy_session','add_unique_user_id','moe_events','call_web_push','track'];
for(k in f){t[f[k]]=q(f[k]);}
a=s.createElement(o);m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);
i['moe']=i['moe'] || function(){n=arguments[0];return t;}; a.onload=function(){if(n){i[r] = moe(n);}};
})(window,document,'script','https://cdn.moengage.com/webpush/moe_webSdk.min.latest.js','Moengage'); 

Moengage = moe({
app_id:"YRUW3JGAG6OBSG0AH2GH50FK",
debug_logs: 0
}); 
</script>
