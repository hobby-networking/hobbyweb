@extends('frontend.dashboard.userprofile_template')
<?php
$loggedinUser = Auth::user();
?>
@section('extra_css')
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/custom_style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/custom_animate.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/flexslider.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
* { margin:auto; }
#page-content-wrapper{ overflow-x: visible;}
.tab-content {
  margin-top: 8px;
}
</style>
@stop

@section('content')
@if(!empty($user->cover_pic))
<style type="text/css">
  .parallax.bg1{
      background-image: url({{ $user->cover_pic }});
  }
</style>
@endif
<div class="user_profile">
    <div class="row bg1 parallax">
    	<div class="container-fluid xyz col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<div class="headerTopInner"> 
                <img src="{{ $user->profile_pic }}" alt="{{ $user->name }}" class="headerTopInnerImg">
                @if(Auth::check() && $user->id == $loggedinUser->id)
                <h1><span class="editable" data-url="{{ route('update.profile.name') }}" data-attribute="['_token','{{ csrf_token() }}']">{{ $user->name }}</span></h1>
                <p class="single_line_desc"><span class="editable" data-url="{{ route('update.profile.status') }}">{{ $user->userSettings->user_status}}</span></p>
                @else
                <h1>{{ $user->name }}</h1>
                <p class="single_line_desc">{{ $user->userSettings->user_status}}</p>
                @endif
                
                {{-- <ul class="socialIcon">
                   <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li> 
                </ul> --}}
                @if(Auth::check() && ($user->id != $loggedinUser->id))
                <div class="friend_rqust_btn right">
                  @if($loggedinUser->isFriendWith($user))
                    <a class="mui-btn mui-btn--flat mui-btn--primary" href="{{ route('unfriend_user',$user->id) }}">Unfriend</a>
                  @else
                    @if($loggedinUser->hasFriendRequestFrom($user))
                      <a class="mui-btn mui-btn--flat mui-btn--primary" href="{{ route('accept_friend_request',$user->id) }}">Accept Request</a>
                    @else
                      @if(!$loggedinUser->hasFriendRequestTo($user))
                        <a class="mui-btn mui-btn--flat mui-btn--primary" href="{{ route('friend_user',$user->id) }}">Send friend Request</a>
                      @else
                        <a class="mui-btn mui-btn--flat mui-btn--primary">Friend Rrquest Send</a>
                      @endif
                    @endif
                  @endif
                  @if($loggedinUser->hasBlocked($user))
                    <a class="mui-btn mui-btn--flat mui-btn--primary" href="{{ route('unblock_user',$user->id) }}">Unblock</a>
                  @else
                    <a class="mui-btn mui-btn--flat mui-btn--primary" href="{{ route('block_user',$user->id) }}">Block</a>
                      <a href="javascript:startChat({{$loggedinUser->id}},{{$user->id}})" class="mui-btn mui-btn--flat mui-btn--primary">Chat</a>
                  @endif
                </div>
                @endif  
            </div>
        </div>
    </div>
   @if(!empty($user->userSettings->user_about_me))
    <div class="aboutPart">
        <div class="container">
            <div class=" col-lg-12">
                <h2>ABOUT ME</h2>
                @if(Auth::check() && $user->id == $loggedinUser->id)
                <p><span class="editable" data-type="textarea" data-url="{{ route('update.profile.about_me') }}">{{ $user->userSettings->user_about_me}}</span></p>
                @else
                <p> {{ $user->userSettings->user_about_me}}</p>
                @endif
            </div>
        </div>
        <div style="clear:both" ></div>
    </div>
    @endif
    <!-- User Activity Section -->
    <div class="container">
        <div class="col-lg-12 userActivity">
            <ul class="nav nav-tabs" id="myTab">
              <li class="active"><a data-target="#hobbies" data-toggle="tab">Hobbies</a></li>
              <li><a data-target="#profile" data-toggle="tab">Community</a></li>
              <li><a data-target="#messages" data-toggle="tab">Friends</a></li>
              <li><a data-target="#latest_post" data-toggle="tab">Latest Post</a></li>
              <li><a data-target="#gallery" data-toggle="tab">Gallery</a></li>
            </ul>

            <div class="tab-content">
              <!-- Start Hobbies Section -->
              <div class="tab-pane active" id="hobbies">
                @foreach($user->hobbies as $eachHobby)
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 userActvtyBlock">
                      <a href="{{route('manage_hobby_by_name',$eachHobby->hobby->slug())}}">
                      <img src="{{ $eachHobby->hobby->profile_pic }}" class="img-circle">
                      <p>{{ $eachHobby->hobby->name }}</p>
                      </a>
                  </div>
                @endforeach
              </div>
              <!-- End Hobbies Section -->

              <!-- Start Profile Section -->
              <div class="tab-pane" id="profile">
                @if(count($user->communities)>0)
                  @foreach($user->communities as $eachCommunity)
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 userActvtyBlock">
                      <a href="{{ route('community_page',$eachCommunity->slug()) }}">
                      <img src="{{ $eachCommunity->profile_pic }}" class="img-circle">
                      <p>{{ $eachCommunity->name }}</p>
                      </a>
                  </div>
                  @endforeach
                @else
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 userActvtyBlock">
                The User didnot join any community yet!
                </div>
                @endif
              </div>
              <!-- End Profile Section -->

              <!-- Start Friends Section -->
              <div class="tab-pane" id="messages">
                @if(count($user->getFriends())>0)
                  @foreach($user->getFriends() as $eachFriend)
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 userActvtyBlock">
                      <a href="{{ route('profile_page',$eachFriend->username) }}">
                      <img src="{{ $eachFriend->profile_pic }}" class="img-circle">
                      <p>{{ $eachFriend->name }}</p>
                      </a>
                  </div>
                  @endforeach
                @else
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 userActvtyBlock">
                  The User didnot join any community yet!
                  </div>
                @endif
              </div>
              <!-- End Friends Section -->

              <!-- Start Latest Post Section -->
              <div class="tab-pane" id="latest_post">
                <div class="col-lg-12">
                  <div id="show_ajax_post">
                      @foreach($userPost as $eachPostUser)
                        @if($eachPostUser->isAllowerdToView())
                            @include('frontend.posts.each_post', array("eachPostUser" => $eachPostUser))
                        @endif  
                      @endforeach
                  </div>
                </div>
              </div>
              <!-- End Latest Post Section -->

              <!-- Start Gallery Section -->
              <div class="tab-pane" id="gallery">
                <div id="sections-wrapper" class="animated">
                    <div class="lanimated" id="gallery">
                        <div class="container">
                            <div class="gallery">
                                @foreach($user->gallery as $eachGallery)
                                  <a tabindex="1"><img src="{{ $eachGallery->image }}"></a>
                                @endforeach
                                <span class="close"></span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
              </div>
              <!-- End Gallery Section -->
            </div>
        </div>
    </div>
    <!--End of User Activity Section -->
</div>
@stop

@section('extra_js')
 
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/toggler.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.flexslider.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jinplace-1.2.1.min.js') }}"></script>
@if(Auth::check())
<!-- Submit Post comment and like Start -->
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/post.js') }}"></script>
<script type="text/javascript">
function addCommentPost(post_id, comment) {
    postCommentAdd(post_id,comment,{{ Auth::user()->id }},"{{ csrf_token() }}","{{ route('add_post_comment') }}");
}
function like(post_id, user_id) {
  $.ajax({
      url: "{{ route('user_post_like') }}", 
      type: "POST",
      data: {
          'post_id':post_id,
          'user_id':user_id,
          '_token':"{{ csrf_token() }}"
      },
      success: function(result){
          $('#postLike_'+post_id).find('.count').html(result.like_count);
          if(result.success){
            $('#postLike_'+post_id).addClass('activeLike');
            $('#postLike_'+post_id).attr('href',"javascript:unlike("+post_id+","+user_id+")");
          }else{
            $('#postLike_'+post_id).removeClass('activeLike');
            $('#postLike_'+post_id).attr('href',"javascript:like("+post_id+","+user_id+")");
          }
          //location.reload();
      }
  });
}
function unlike(post_id, user_id) {
  $.ajax({
      url: "{{ route('user_post_unlike') }}", 
      type: "POST",
      data: {
          'post_id':post_id,
          'user_id':user_id,
          '_token':"{{ csrf_token() }}"
      },
      success: function(result){
          $('#postLike_'+post_id).find('.count').html(result.like_count);
          if(result.success){
            $('#postLike_'+post_id).removeClass('activeLike');
            $('#postLike_'+post_id).attr('href',"javascript:like("+post_id+","+user_id+")");
          }else{
            $('#postLike_'+post_id).addClass('activeLike');
            $('#postLike_'+post_id).attr('href',"javascript:unlike("+post_id+","+user_id+")");
          }
      }
  });
}
</script>
<!-- Submit Post comment and like End -->

@endif
<script type="text/javascript">
$(document).ready(function() {
   $('.editable').jinplace();
      
});
</script>

  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var $window = $(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      $(function() {
        //SyntaxHighlighter.all();
      });

      $window.load(function() {
        $('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 210,
          itemMargin: 5,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            $('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      $window.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>

  <script type="text/javascript">
      jQuery(function () {
        //jQuery('#myTab a:last').tab('show');
        var no=1;
        var currentPage = 1;
        $(window).scroll(function () {
            if(no==1) {
              if ($('#latest_post').hasClass('active') && $(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
                no=2;
                $.ajax({
                    type: "GET",
                    url: "{{ route('welcome_page') }}/?page="+currentPage+"&user_id={{ $user->id }}",
                    cache: true,
                    success: function(data){
                      $('#show_ajax_post').append(data.html);
                      currentPage++;
                      no=1;
                    }
                });
              }
            }
        });
      });
  </script>
@stop