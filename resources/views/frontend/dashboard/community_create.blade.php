@extends('frontend.dashboard.template')

@section('extra_css')
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/dropstyle.css') }}" rel="stylesheet">

<link href="{{ Cdn::asset('frontend/dashboard/assets/css/jquery.tag-editor.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')

<div class="container-fluid" >
    <div class="row">
    	<div class="center_div col-lg-12 col-md-12 col-sm-12 col-xs-12">
	    	
				<form class="frm" method="post" action="{{route('save-community-post')}}" enctype="multipart/form-data">
					<div class="crete_community_form col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<input type="hidden" value="{{csrf_token()}}" name="_token"/>
						<p> Create Community </p>
						<div class="cm"> Create a Community for you and some of your friends, like your movie night buddies, sports team, or related hobbies people. </div>
						<input placeholder="Community Name" id="name" name="name" type="text" class="meterial_input" required />
						<input placeholder="Members" id="members" name="members" type="text" class="meterial_input" />
						<input type="hidden" id="member_ids" value="" name="member_ids" />
						<input placeholder="Related Hobbies" id="related_hobbies" type="text" class="meterial_input" />
						<input type="hidden" id="related_hobby_ids" value="" name="related_hobby_ids"/>
						<input style="display: none;" type="file" name="profile_pic" id="profile_pic" />
						<input style="display: none;" type="file" name="cover_pic" id="cover_pic" />
						<hr/>
						<p> Privacy </p>

						<div class="card chose_community_type">
						  <label class="radio">
						    <input id="radio1" type="radio" value="public" name="radios" checked>
						    <span class="outer"><span class="inner"></span></span>Public </label>
						  <label class="radio">
						    <input id="radio2" type="radio" name="radios" value="closed">
						    <span class="outer"><span class="inner"></span></span>Closed</label>
						  <label class="radio">
						    <input id="radio3" type="radio" name="radios"  value="secret">
						    <span class="outer"><span class="inner"></span></span>Secret</label>			   
						</div>

						
					</div>
					<div class="dropbox col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<p> Display Picture </p>
							<div id="drop_profile_pic" class="drop">
								<a>Browse</a>
							</div>
						<p>Cover Photo</p>
							<div id="drop_cover_pic" class="drop">
								<a>Browse</a>
							</div>
					</div>
					<div class="new_community_submit"><button class="submit">Submit</button></div>
				</form>
			
		</div>
	</div>
</div>
@stop

@section('extra_js')
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.knob.js') }}"></script>
<!-- jQuery File Upload Dependencies -->
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.ui.widget.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.fileupload.js') }}"></script>
<!-- Our main JS file -->
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/dropscript.js') }}"></script>

<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.caret.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.tag-editor.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#drop_profile_pic").click(function(){
			$("#profile_pic").click();
		});
		$("#drop_cover_pic").click(function(){
			$("#cover_pic").click();
		});
		var source_data = <?php echo json_encode($allMembers); ?>;
		var source_data_hobby = <?php echo json_encode($allHobbies); ?>;

		var arr_source_data = [];
		var arr_source_data_hobby = [];

		//console.log(typeof(arr_source_data));
		
		$('#members').tagEditor({

		    autocomplete: {
		        delay: 0, // show suggestions immediately
		        position: { collision: 'flip' }, // automatic menu position up/down
		        source: source_data,
		        select: function(event,ui) {

					  if ($.inArray(ui.item.id,arr_source_data) === -1){
					    	arr_source_data.push(ui.item.id);
					  }
	                
            	},
		    },
		    onChange: function(field, editor, tags) {
		        
		        var value = $.map(arr_source_data,function(a){return $.inArray(a, tags) < 0 ? null : a;});
		        //console.log(value);
		        $('#member_ids').val(arr_source_data);
		        //alert( $('#member_ids').val());
		        // if(value.length<1){
		        // 	$('#members').tagEditor('removeTag', tags);
		        // }
		        
		    },
		    beforeTagDelete: function(field, editor, tags, val) {
		    	
		       $.map(source_data, function(value, key) {

				    if (value.label == val)
				    {
				        arr_source_data.splice(arr_source_data.indexOf(value.id),1);
				    }
				});

		        $('#member_ids').val(arr_source_data);
		        
		    },

		    forceLowercase: false,
		    placeholder: 'Type Members Name ...'

		});

		////////////////////////////////Hobby Tags///////////////////////////////////////

		$('#related_hobbies').tagEditor({

		    autocomplete: {
		        delay: 0, // show suggestions immediately
		        position: { collision: 'flip' }, // automatic menu position up/down
		        source: source_data_hobby,
		        select: function(event,ui) {

					  if ($.inArray(ui.item.id,arr_source_data_hobby) === -1){
					    	arr_source_data_hobby.push(ui.item.id);
					  }
	                
            	},
		    },
		    onChange: function(field, editor, tags) {
		        
		        var value = $.map(arr_source_data_hobby,function(a){return $.inArray(a, tags) < 0 ? null : a;});
		        //console.log(value);
		        $('#related_hobby_ids').val(arr_source_data_hobby);
		        

		    },
		    beforeTagDelete: function(field, editor, tags, val) {
		    	
		       $.map(source_data_hobby, function(value, key) {

				    if (value.label == val)
				    {
				        arr_source_data_hobby.splice(arr_source_data_hobby.indexOf(value.id),1);
				    }
				});

		        $('#related_hobby_ids').val(arr_source_data_hobby);
		        
		    },

		    forceLowercase: false,
		    placeholder: 'Related Hobbies ...'
		});

	});

</script>
<script type="text/javascript">
    $(document).ready(function(e) {
        $("#profile_pic").change(function() { 
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))
            { 
                var reader = new FileReader();
                reader.onload = imageIsLoadedProfile;
                reader.readAsDataURL(this.files[0]);
            }
        });
        $("#cover_pic").change(function() { 
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))
            { 
                var reader = new FileReader();
                reader.onload = imageIsLoadedCover;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    function imageIsLoadedProfile(e) {
        $('#drop_profile_pic').css('background-image', 'url(' + e.target.result + ')');
    }
    function imageIsLoadedCover(e) {
        $('#drop_cover_pic').css('background-image', 'url(' + e.target.result + ')');
    }
</script>
@stop