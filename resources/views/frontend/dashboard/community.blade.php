@extends('frontend.dashboard.community_template')

@section('extra_css')
<style type="text/css">
.userCover2 {
    background: url('{{htmlspecialchars_decode($arrCom->cover_pic)}}') no-repeat top center;
/*    background-size: cover;
    min-height: 565px;*/
}
</style>
<link rel="stylesheet" href="{{ Cdn::asset('frontend/dashboard/assets/css/mediaelementplayer.css') }}">
@stop

@section('content')
<?php 
$type = 'community';
$community_id = $arrCom->id;
$loggedInUser1 = null;
$loginFlag = false;
if(Auth::check()){
    $loggedInUser1 = Auth::user();
    $loginFlag = true;
    //dd($loggedInUser1->isAdmin());
}
?>
<div class="col-lg-12 proPicBlock userCover2 community_cover">
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 community_header">
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 proPic">
            <img src="{{ $arrCom->profile_pic }}" class="img-circle img-responsive">
        </div>
        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
          <div class="row nameRow">
          <h1>{{ $arrCom->name }}</h1>
          @if(Auth::check() && $arrCom->created_by == Auth::user()->id)
          <a href="{{ route('community_edit',$arrCom->id) }}">Edit Community</a>
          @endif
          
            <ul class="shortDetail">
                
               {{-- <li><i class="fa fa-eye iconcolr" aria-hidden="true"></i>{{$arrCom->total_follower_count}} following</li> --}}
                <li><i class="fa fa-users iconcolr" aria-hidden="true"></i>{{$arrCom->total_member_count}} Members</li>

                @if(Auth::check() && $loggedInUser1->isInvitedToJoinCommunity($arrCom))
                    <li><a href="{{route('accept_community_invitation',$arrCom->id)}}">Accept Join Request</a></li>
                    <li><a href="{{route('reject_community_invitation',$arrCom->id)}}">Reject Join Request</a></li>
                @else
                    @if($arrCom->type == 'closed' && $arrCom->is_creator == 'no' && $arrCom->is_member=='no' && $arrCom->is_follower=='no')

                     {{-- <li><a href="{{route('follow_community_page',$arrCom->slug)}}">Follow</a></li> --}}
                     <li><a href="{{route('join_community_page',$arrCom->slug)}}">Request to Join</a></li>

                    @elseif($arrCom->type == 'public' && $arrCom->is_creator == 'no' && $arrCom->is_member=='no' && $arrCom->is_follower=='no')

                     {{-- <li><a href="{{route('follow_community_page',$arrCom->slug)}}">Follow</a></li> --}}
                     <li><a href="{{route('join_community_page',$arrCom->slug)}}">Join</a></li>

                    @elseif($arrCom->type == 'closed' && $arrCom->is_creator == 'no' && $arrCom->is_member=='no' && $arrCom->is_follower=='yes')
                        @if($arrCom->is_requested_member == "yes")
                        <li><a href="{{route('unfollow_community_page',$arrCom->slug)}}">Cancel Request</a></li>
                        @else
                        {{-- <li><a href="{{route('unfollow_community_page',$arrCom->slug)}}">Unfollow</a></li> --}}
                        <li><a href="{{route('join_community_page',$arrCom->slug)}}">Request to Join</a></li>
                        @endif
                    @elseif($arrCom->type == 'public' && $arrCom->is_creator == 'no' && $arrCom->is_member=='no' && $arrCom->is_follower=='yes')

                     {{-- <li><a href="{{route('unfollow_community_page',$arrCom->slug)}}">Unfollow</a></li> --}}
                     <li><a href="{{route('join_community_page',$arrCom->slug)}}">Join</a></li>

                    @elseif($arrCom->is_creator == 'no' && $arrCom->is_member=='yes')

                      <li><a href="{{route('unfollow_community_page',$arrCom->slug)}}">Leave</a></li>

                    @else
                        @if($arrCom->status == 'yes')
                         <li><a href="{{route('inactive_community_page',$arrCom->slug)}}">Inactive Community</a></li>
                        @else
                         <li><a href="{{route('active_community_page',$arrCom->slug)}}">Active Community</a></li>
                         @endif
                    @endif
                @endif
                @if($arrCom->is_creator == 'yes' || $arrCom->is_member=='yes')
                    <li><a href="javascript:openInviteModal()">Invite To Join</a></li>
                @endif
            </ul>
            <h4>{{ $arrCom->description }}</h4>
          </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="community container xyz coverPage">
    <div class="row">
        <div class="col-lg-12 mdlBlock">
            @if(Auth::check())
            @include('frontend.post_btn',['type'=>$type,'community_id'=>$community_id])
            @endif
            @if(count($arrCom->relatedHobbies())>0)
            <div class="col-lg-12">
            @if(count($arrCom->relatedHobbies())==1 && empty($arrCom->relatedHobbies()[0]))
            @else
            <h4>Suggested Hobbies</h4>
            @endif
                <ul class="hobbies">
                    @foreach($arrCom->relatedHobbies() as $eachHobby)
                    @if(!empty($eachHobby))
                    <li>
                        <?php 
                            $hobbyName = strtolower($eachHobby->name);
                            $hobbyNameArr = explode(' ', $hobbyName);

                            if(count($hobbyName)>0)
                                $hobbyName = implode('_',$hobbyNameArr);
                         ?>
                        <a href="{{route('manage_hobby_by_name',$hobbyName)}}">
                        <img src="{{ $eachHobby->profile_pic }}" class="img-circle img-responsive imgBoderGreen">
                        <p>{{ $eachHobby->name }}</p>
                        </a>
                    </li>
                    @endif
                    @endforeach
                </ul>
                <div class="clearfix"></div>
            </div>
            @endif
            <div class="col-lg-12">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"> 
                    @if($arrCom->is_member=='yes' || $arrCom->is_creator=='yes')
                    <div id="loadNewPostBox">
                        <div class="_4-u2 _1qby ">
                             <form  method="post" enctype="multipart/form-data" name="user_post_2" action="{{route('add_new_post')}}" class="post_form_2">            
                                @include('frontend.post_box',['type'=>$type,'community_id'=>$community_id])
                                <!-- modal -->
                                <div id="myModal2" class="modal fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                 <h3 id="myModalLabel3">Select Hobby</h3>

                                            </div>
                                            <div class="modal-body">
                                                    <select id="choice" name="hobby_id[]" multiple="multiple">
                                                    @if(Auth::check())
                                                      @foreach(Auth::user()->hobbies as $eachHobby)
                                                      <option value="{{ $eachHobby->hobby->id }}">{{ $eachHobby->hobby->name }}</option>
                                                      @endforeach
                                                    @endif
                                                    </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                <button class="btn-success btn" id="SubForm2">Confirm and Submit Your Post</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end modal -->  
                            </form>       
                        </div>              
                    </div>
                    @endif
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                    @foreach($arrCom->posts() as $eachPostUser)
                      <?php $user = $eachPostUser->getUserByPost; ?>
                        <div class="row tmline">
                            <div class="postedUser">
                                <p>
                                    <img src="{{ $user->profile_pic }}" class="img-circle img-responsive">
                                    <a href="{{ route('profile_page',$user->username) }}">{{ $user->name }}</a><br /> 
                                    <span>{{date("M",strtotime(date($eachPostUser->created_at)))}}</span> 
                                    {{date("j",strtotime(date($eachPostUser->created_at)))}}
                                </p>
                                <p class="status">{!! $eachPostUser->content !!}</p>
                            </div>
                            <ul class="activity">
                                <li><a href="#"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                            </ul>
                            
                            @if(!empty($eachPostUser->userPostImages($user->id)) && count($eachPostUser->userPostImages($user->id))>0)

                            <div class="container-post-image">
                                @foreach($eachPostUser->userPostImages($user->id) as $eachPostImage)
                                    <?php $image_name = $eachPostImage->image; ?>
                                    <div class="item item_count_{{ count($eachPostUser->userPostImages($user->id)) }}">
                                        <img src="{{ $image_name }}" class="img-responsive PostimgHover">
                                    </div>
                                @endforeach
                            </div>
                            @endif
                            @if(!empty($eachPostUser->userPostVideos($user->id)) && count($eachPostUser->userPostVideos($user->id))>0)  
                                @foreach($eachPostUser->userPostVideos($user->id) as $eachPostVideo)
                                <?php $video_name = $eachPostVideo->video; ?>
                                    @if(!empty($video_name))
                                        <video src="{{$video_name}}" height="240"></video>
                                    @endif
                                @endforeach
                            @endif 
                            @if(Auth::check() && !$loggedInUser1->isAdmin())
                            <div class="col-sm-12">
                                <div class="panel panel-white post panel-shadow">
                                    <div class="post-description">
                                        <div class="stats">
                                        <?php $bulb = "bulb"; ?>
                                          @if($eachPostUser->isLikedBy($loggedInUser1->id))
                                            <?php $bulb = "bulb-on"; ?>
                                            <a href="javascript:unlike({{$eachPostUser->id}},{{$loggedInUser1->id}})" class="btn btn-default stat-item activeLike {{$bulb}}" id="postLike_{{$eachPostUser->id}}">
                                          @else
                                            <a href="javascript:like({{$eachPostUser->id}},{{$loggedInUser1->id}})" class="btn btn-default stat-item {{$bulb}}" id="postLike_{{$eachPostUser->id}}">
                                          @endif
                                                {{-- <i class="fa fa-lightbulb-o"></i><span class="count">{{$eachPostUser->likeCount()}}</span> --}}
                                                <span class="count">{{$eachPostUser->likeCount()}}</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-footer">
                                        <div class="input-group">
                                            <input class="form-control add_comment add_comment_{{ $eachPostUser->id }}" data-post_id="{{ $eachPostUser->id }}" placeholder="Add a comment" type="text">
                                            <span class="input-group-addon">
                                                <a href="javascript:void(0)" class="add_comment_submit add_comment_submit_{{ $eachPostUser->id }}" data-post_id="{{ $eachPostUser->id }}"><i class="fa fa-edit"></i></a>  
                                            </span>
                                        </div>
                                        <ul class="comments-list comments_list_{{ $eachPostUser->id }}">
                                        @foreach($eachPostUser->comments_desc() as $eachComment)
                                            <li class="comment">
                                                <a class="pull-left" href="#">
                                                    <img class="avatar" src="{{ $eachComment->user->profile_pic }}" alt="avatar">
                                                </a>
                                                <div class="comment-body">
                                                    <div class="comment-heading">
                                                        <h4 class="user">{{ $eachComment->user->name }}</h4>
                                                        <h5 class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($eachComment->created_at))->diffForHumans() }}</h5>
                                                    </div>
                                                    <p>{!! $eachComment->content !!}</p>
                                                </div>
                                            </li>
                                        @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    @endforeach
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="rightBar">
                        <h5>Members</h5>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
                            <a href="{{ route('profile_page',$arrCom->creator()->username) }}">
                            <img src="{{ $arrCom->creator()->profile_pic}}" class="img-responsive img-circle imgBoder" alt="{{ $arrCom->creator()->name }}" title="{{ $arrCom->creator()->name }}"></a>
                        </div>
                        <?php $allMembers=array(); ?>
                        @foreach($arrCom->members as $eachMembers)
                        <?php $allMembers[] = $eachMembers->user_id; ?>
                        @if($eachMembers->is_accepted == "yes")
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
                            <a href="{{ route('profile_page',$eachMembers->user->username) }}">
                            <img src="{{ $eachMembers->user->profile_pic}}" class="img-responsive img-circle imgBoder" alt="{{ $eachMembers->user->name }}" title="{{ $eachMembers->user->name }}"></a>
                        </div>
                        @endif
                        @endforeach
                      <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div> 
        </div>
    </div>
</div>
<div id="friend_list_modal" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h3 id="myModalLabel3">Invite your friends to join {{ $arrCom->name }}</h3>
          </div>
          <div class="modal-body">
            <div class="similar_interest">
            <?php $allInvitedMembers = array();
                $alreadyInvitedMembers = array();
                $unInvitedFriendsCount = 0;
            ?> 
            @if(Auth::check())
            @foreach($arrCom->invitedMembersBy($loggedInUser1->id) as $eachInvitedMembers)
                <?php $allInvitedMembers[] = $eachInvitedMembers->invited_to; ?>
            @endforeach
              @foreach($loggedInUser1->getFriends() as $eachFriend)
              <?php 
                // $eachUser = app('App\Models\User')->where('id','=',$eachFriend->recipient_id)->first(); 
                $eachUser = $eachFriend; 

            ?>
              @if(!in_array($eachUser->id, $allMembers) && !in_array($eachUser->id, $allInvitedMembers) && !in_array($eachUser->id, $alreadyInvitedMembers) && $arrCom->creator()->id!=$eachUser->id)
              <?php 
                  $alreadyInvitedMembers[] = $eachUser->id;
                  $userDetails = $eachUser;
                  $username = $userDetails->username;
                  $unInvitedFriendsCount++;
              ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5 invite_{{$loggedInUser1->id}}_{{$community_id}}_{{$userDetails->id}}">
                  <a class="notinvited" href="javascript:inviteToCommunity({{$loggedInUser1->id}},{{$community_id}},{{$userDetails->id}})"><img src="{{ $userDetails->profile_pic}}" class="img-responsive img-thumbnail imgBoder" title="{{ $userDetails->name }}" alt="{{ $userDetails->name }}"></a>
                  <a class="hide invited" href="javascript:void(0)"><img src="{{ $userDetails->profile_pic}}" class="img-responsive img-thumbnail imgBoder" title="{{ $userDetails->name }}" alt="{{ $userDetails->name }}">Invited</a>
              </div>
              @endif
              @endforeach
            @endif
              @if($unInvitedFriendsCount<1)
                <p class="no_friends center">All of your friends are either member or already invited.</p>
              @endif
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="modal-footer">
              <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
          </div>
      </div>
  </div>
</div>
@stop

@section('extra_js')
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/mediaelement-and-player.min.js') }}"></script>
@if(Auth::check())
<!-- Submit Post comment Start -->
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/post.js') }}"></script>
<script type="text/javascript">
function addCommentPost(post_id, comment) {
    postCommentAdd(post_id,comment,{{ Auth::user()->id }},"{{ csrf_token() }}","{{ route('add_post_comment') }}");
}
</script>
@endif
<script type="text/javascript">
$(document).ready(function() {
   $('video,audio').mediaelementplayer({
      defaultVideoWidth: 480,
      defaultVideoHeight: 240,
      enableAutosize: true,
      videoHeight: -1,
    });
});
var flag = true;
function like(post_id, user_id) {
  if(flag){
    flag = false;
    $.ajax({
        url: "{{ route('user_post_like') }}", 
        type: "POST",
        data: {
            'post_id':post_id,
            'user_id':user_id,
            '_token':"{{ csrf_token() }}"
        },
        success: function(result){
            $('#postLike_'+post_id).find('.count').html(result.like_count);
            if(result.success){
              $('#postLike_'+post_id).addClass('activeLike');
              $('#postLike_'+post_id).switchClass('bulb','bulb-on');
              $('#postLike_'+post_id).attr('href',"javascript:unlike("+post_id+","+user_id+")");
            }else{
              $('#postLike_'+post_id).removeClass('activeLike');
              $('#postLike_'+post_id).switchClass('bulb-on','bulb');
              $('#postLike_'+post_id).attr('href',"javascript:like("+post_id+","+user_id+")");
            }
            flag = true;
            //location.reload();
        }
    });
  }
}
function unlike(post_id, user_id) {
  if(flag){
    flag = false;
    $.ajax({
        url: "{{ route('user_post_unlike') }}", 
        type: "POST",
        data: {
            'post_id':post_id,
            'user_id':user_id,
            '_token':"{{ csrf_token() }}"
        },
        success: function(result){
            $('#postLike_'+post_id).find('.count').html(result.like_count);
            if(result.success){
              $('#postLike_'+post_id).removeClass('activeLike');
              $('#postLike_'+post_id).switchClass('bulb-on','bulb');
              $('#postLike_'+post_id).attr('href',"javascript:like("+post_id+","+user_id+")");
            }else{
              $('#postLike_'+post_id).addClass('activeLike');
              $('#postLike_'+post_id).switchClass('bulb','bulb-on');
              $('#postLike_'+post_id).attr('href',"javascript:unlike("+post_id+","+user_id+")");
            }
            flag = true;
        }
    });
  }
}
function openInviteModal(){
    $('#friend_list_modal').modal('show')
}
var inviteflag = true;
function inviteToCommunity(invitedBy,communityId,inviteTo) {
    //console.log(invitedBy+communityId+inviteTo);
    if(inviteflag){
    inviteflag = false;
    $.ajax({
        url: "{{ route('invite-community-member') }}", 
        type: "POST",
        data: {
            'invitedBy':invitedBy,
            'communityId':communityId,
            'inviteTo':inviteTo,
            '_token':"{{ csrf_token() }}"
        },
        success: function(result){
            if (result.success) {
                $('.invite_'+invitedBy+'_'+communityId+'_'+inviteTo).find('.notinvited').addClass('hide');
                $('.invite_'+invitedBy+'_'+communityId+'_'+inviteTo).find('.invited').removeClass('hide');
            }
            inviteflag = true;
            //location.reload();
        }
    });
  }
}
</script>
@yield('extra_js1')
@yield('extra_js2')
@stop