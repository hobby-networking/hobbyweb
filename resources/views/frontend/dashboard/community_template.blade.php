<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> 
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
	@include('frontend.dashboard.html.head')
</head> 
<body>
	<!-- nav start -->
	@include('frontend.dashboard.html.header')
	<div id="wrapper" class="toggled-2">
		<!-- Sidebar Start -->
		@include('frontend.dashboard.html.nav')
		<!-- Sidebar End -->
		<!-- Page Content -->
		<div id="page-content-wrapper" class="">
            @yield('content')
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
	@include('frontend.dashboard.html.footer')
	@yield('chat_script')
</body>
</html>
<!-- 
*************************************************
Author: Sudipta Dhara (sudipta@globalsoftwaretechnologies.com)
*************************************************
-->