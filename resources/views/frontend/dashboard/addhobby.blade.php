@extends('frontend.dashboard.template')
@section('extra_css')
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/jquery.tag-editor.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
    .upload_hobby_image_file, #image_preview{
        float: left !important;
    }
    #hobby_image_file {
    padding: 5px;
    margin-top: 10px;
    margin-left: 15%;
	display:none;
}
#message{
position:absolute;
top:120px;
left:815px;
}
#success
{
color:green;
}
#invalid
{
color:red;
}
#line
{
margin-top: 274px;
}
#error
{
color:red;
}
#error_message
{
color:blue;
}
#loading
{
display:none;
position:absolute;
top:50px;
left:850px;
font-size:25px;
}
</style>
@stop
@section('content')
<div class="container-fluid xyz main_div_click">
    <div class="row">
    
        <div class="col-lg-8 mdlBlock click_reverse addhby-bld">
        	<div class="col-lg-12 addhbby">
			  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="leftArrow">
                <a style='text-decoration: none;' href="{{ route('choosehobby_page') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i>Return</a>
                </div>
               </div>
               
			   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            	<h1>Add your Hobby</h1>
               </div>
            
			   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div style="cursor: pointer;" class="rightArrow" id="add_new_hobby_btn">
                Next<i class="fa fa-arrow-right" aria-hidden="true"></i>
                </div>
               </div>
            <div class="clearfix"></div>
            </div>
            <div class="col-lg-12">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p>People have myriad hobbies and you seem to be one of them. Why not create a hobby that others can join? At Hobbynetworking our goal is to provide a platform for anyone and everyone to invite, indulge, interact with people who share similar hobbies, interests and passion.</p>
                </div>
              </div>
            </div>
           
            
            <form id="uploadimage" action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-lg-12 txt">
                <h2>Hobby Name (give your special interest a name)</h2>
                    <div id="custom-search-input">
                        <div class="input-group has-success has-feedback col-md-12">
                            <input title="Enter Hobby Name" onkeyup="return checkAvailability(this.value)" name="hobby_name" type="text" class="form-control" id="inputSuccess2" aria-describedby="inputSuccess2Status">
                            <span style="display: none;" id="success_mark" class="glyphicon glyphicon-ok form-control-feedback" ></span>
                            <span style="display: none;color:#FF9494" id="unsuccess_mark" class="glyphicon has-error glyphicon-remove form-control-feedback" ></span>             
                        </div>
                    </div>
                    <p style="display: none;" class="error hobby-name">Please Fill This</p>
                    <p style="display: none;" class="error hobby-name-msg">Please Enter a Unique hobby name Or choose hobby from "CHOOSE HOBBY PAGE"</p>
                </div>
                <div class="col-lg-12 txt">
                <h2>Hobby Category (select if this hobby falls under any category or select “Other”)</h2>
                    <div id="custom-search-input">
                        <div class="input-group col-md-12">
                            <input title="Enter Hobby Category Name" type="text" class="form-control input-lg" name="hobby_cat" id="auto" />
                        </div>
                    </div>
                    <p style="display: none;" class="error hobby-cat">Please Fill This</p>
                </div>
                <div class="col-lg-12 txt">
                <h2>Related Topics (please help us link other hobbies to this)</h2>
                    <div id="custom-search-input">
                        <div class="input-group col-md-12">
                            <input title="Enter Related Hobby Name" type="text" class="form-control input-lg" name="related_topics" id="related_topics_input" />
                        </div>
                    </div>
                    <p style="display: none;" class="error related-top">Please Fill This</p>
                </div>
                <div class="col-lg-12 txt">
                <h2>Upload a related photograph</h2>
                    <div class="row">
                        <div class="input-group col-lg-4 col-md-4 col-sm-12 col-xs-12 upload_hobby_image_file">
                            <input type="file" name="file" id="hobby_image_file" required />
                            <img title="Click here upload" style="cursor: pointer;" class="select-image" src="{{ Cdn::asset('frontend/dashboard/assets/img/upload.jpg') }}" />
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="image_preview">
                            <img id="previewing" src="{{ Cdn::asset('frontend/dashboard/assets/img/noimage.png') }}" />
                        </div>
                        <h4 id='loading'>loading..</h4>
                        <div id="message"></div>
                    </div>
                    <p style="display: none;" class="error hobby-pic">Please Fill This</p>
                </div>
            </form>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div style="cursor: pointer;" class="newHobbySubmit" id="add_new_hobby_btn1">
                Submit
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
<?php
    $hobbyCategories = array();
    foreach ($hobbycat as $eachHobbyCat){
        array_push($hobbyCategories, $eachHobbyCat->name);
    }
    $hobbyAlias = array();
    foreach ($hobby_alias as $eachHobby_alias) {
        if (!in_array($eachHobby_alias->alias_name, $hobbyAlias)) {
            array_push($hobbyAlias, $eachHobby_alias->alias_name);
        }
    }
?>
@section('extra_js')
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.caret.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.tag-editor.js') }}"></script>
<script type="text/javascript">
var chk_status_hobby_name = false;
$(document).ready(function() {
      
    var chk_status_hobby_name = false;  
    var HobbyAliases = <?php echo json_encode($hobbyAlias) ?>;                  
    $('#related_topics_input').tagEditor({
        autocomplete: {
            delay: 0, // show suggestions immediately
            position: { collision: 'flip' }, // automatic menu position up/down
            source: HobbyAliases,
        },
        delimiter: ',', /* space and comma */
        placeholder: 'Enter Related Topics (Press Enter and add multiple)...',
        forceLowercase: false
        
    });
    
    var source = <?php echo json_encode($hobbyCategories) ?>;
    
    $("#auto").autocomplete({
        source: function (request, response) {
            response($.ui.autocomplete.filter(source, request.term));
        },
        change: function (event, ui) {
            
        }
    });
 
    $("#hobby_image_file").change(function() {
        $("#message").empty(); 
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        //var width = $(that).width();
        //alert(height);
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
            $('#previewing').attr('src','noimage.png');
            $("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        }else{
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
        }
    });
});
$(".select-image").click(function(){
    $("#hobby_image_file").click();
});
function imageIsLoaded(e) {
    $("#hobby_image_file").css("color","green");
    //$('#image_preview').css("display", "block");
    $('#previewing').attr('src', e.target.result);
    //$('#previewing').attr('width', '250px');
    //$('#previewing').attr('height', '230px');
}
function checkAvailability(that){
    var url_chk = "{{route('hobby-availability')}}";
    if(that!=null){
        $.ajax({
                url: url_chk,
                type: "GET",
                data: "name="+that,
                success: function(data) 
                {
                    if(data=="matched"){
                        chk_status_hobby_name = false;
                        $("#success_mark").hide();
                        $("#unsuccess_mark").show();
                        
                    }else if(data=="ok"){
                        
                        chk_status_hobby_name = true;
                        $("#unsuccess_mark").hide();
                        $("#success_mark").show();
                    }
                }
        });
    }
}
$("#add_new_hobby_btn").on("click", function () {
    addNewHobby();
});
$("#add_new_hobby_btn1").on("click", function () {
    addNewHobby();
});
function addNewHobby() {
    var url = "{{ route('addhobby_ajax') }}";
    var formData = new FormData($('#uploadimage')[0]);
    allErrorDivHide();
    var chk_param = allErrorDivShow();
    console.log(chk_param);
    if(chk_param){
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            dataType:'json',
            contentType: false,
            cache: false,
            processData:false,
            success: function(data) 
            {
                if(data.success){
                    window.location.href = "{{ route('choosehobby_page') }}";
                }else{
                    alert(data.msg);
                }
            }
        });
    }
}
    function allErrorDivShow(){
        var flag= true;
        
            if($("#inputSuccess2").val()==''){
                $(".hobby-name").show(); 
                flag=false;
            }
            if($("#auto").val()==''){
                $(".hobby-cat").show();
                flag=false;
            }
            // if($("#related_topics_input").val()==''){
            //     $(".related-top").show();
            //     flag=false;
            // }
            // if($("#hobby_image_file").val()==''){
            //     $(".hobby-pic").show();
            //     flag=false;
            // }
            if($("#inputSuccess2").val()!='' && chk_status_hobby_name!=true){
                $(".hobby-name-msg").show();console.log(chk_status_hobby_name);
                flag=false;
            }
        return flag;
        
    }
    function allErrorDivHide(){
        $(".hobby-name").hide();
        $(".hobby-cat").hide();
        $(".related-top").hide();
        $(".hobby-pic").hide();
        $(".hobby-name-msg").hide();
    }
$(".main_div_click").click(function(e) {
      //console.log($(e.target).parents(".click_reverse"));
    if ( !($(e.target).is('.click_reverse') || $(e.target).parents(".click_reverse").length == 1 )) { 
      window.location.href = "{{ route('choosehobby_page') }}";
    } 
      
});
</script>
<script type="text/javascript">
    $(document).ready(function(e){
        
        //var text = document.html('NOTIFY WITH LINK < a href="http://google.com">EXAMPLE< /a >');
       var n = noty({
        text: 'NOTIFY WITH LINK EXAMPLE',
        type: 'success',
        dismissQueue: true,
        layout: 'bottomCenter',
        closeWith: ['click'],
        theme: 'relax',
        maxVisible: 10,
        animation: {
        open: {height: 'toggle'},
        close: {height: 'toggle'},
        easing: 'swing',
        speed: 1500 // opening & closing animation speed
    }

    });
        $(".noty_bar").css("background-color","#dc4622");
        $(".noty_text").css("color","#FFFFFF");
        $('.noty_text').html('You can check out our Explainer Videos for this section. <a href="javascript:void(0)" rel="Nn1A1HNVDhw" class="youtube" style="color: #000000; text-decoration: underline;">Add a Hobby</a>');
   
        setTimeout(function(){ $('#noty_bottomCenter_layout_container').find('li').fadeOut() }, 5000);
    });
</script>
<script type="text/javascript">
  $(function () {
    $(".youtube").YouTubeModal({autoplay:0});
  });
</script>
@stop