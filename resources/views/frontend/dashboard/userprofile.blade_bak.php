<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Hobby Networking</title>
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300,900' rel='stylesheet' type='text/css'>
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/animate.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/main.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ Cdn::asset('frontend/dashboard/assets/css/font-awesome.min.css') }}">
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery-1.9.1.min.js') }}"></script>
</head>

<body>
<nav class="navbar navbar-default no-margin"> 
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li class="active" >
        <button class="navbar-toggle collapse in" data-toggle="collapse" id="menu-toggle-2"> <i class="fa fa-bars" aria-hidden="true"></i> </button>
      </li>
    </ul>
  </div>
  <!-- bs-example-navbar-collapse-1 -->
  <div class="navbar-header fixed-brand">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle"> <i class="fa fa-bars" aria-hidden="true"></i> </button>
    <a class="navbar-brand" href="#"> hobbynetworking</a> </div>
  <!-- navbar-header--> 
  
</nav>
<div id="wrapper" class="userCover-portfolio toggled-2"> 
  <!-- Sidebar -->
  <div id="sidebar-wrapper">
    <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
      <li class="active"> <a href="#"> Link1</a>
        <ul class="nav-pills nav-stacked" style="list-style-type:none;">
            <li><a href="#">link1</a></li>
            <li><a href="#">link2</a></li>
        </ul>
      </li>
        <li> 
         <a href="#"> Link2</a>
          <ul class="nav-pills nav-stacked" style="list-style-type:none;">
              <li><a href="#">link1</a></li>
              <li><a href="#">link2</a></li>
          </ul>
        </li>
          <li> <a href="#">Link3</a> </li>
          <li> <a href="#">Link4</a> </li>
          <li> <a href="#">Link5</a> </li>
          <li> <a href="#">Link6</a> </li>
          <li> <a href="#">Link7</a> </li>
    </ul>
  </div>
  <!-- /#sidebar-wrapper --> 
  <!-- Page Content -->
  
  <div class="headerTop">
    <div class="bg1 parallax">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="headerTopInner"> <img src="img/pro2.jpg" alt="" class="headerTopInnerImg">
              <h1>AHMED RADWAN</h1>
                <ul class="headerTopInnerOption">
                    <li><a href="#">Engineer</a></li>
                    <li><a href="#">Customer Success</a></li>
                    <li><a href="#">Photography enthusiast</a></li>
                    <li><a href="#">World Traveler Wanna-be</a></li>
                </ul>
                <ul class="socialIcon">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
 </div>
  
    <div class="aboutPart">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
          <h2>ABOUT ME</h2>
          <p>I moved to Canada in 2011 and have been hopping around the country ever since. I've studied engineering in Montreal, Worked in Toronto and currently (indefinitely) residing in Vancouver and working for an awesome company called Unbounce. I transitioned into the startup world a couple of years ago, re-started my career as a customer success coach, then a customer success analyst and currently I'm a data analyst for the product team. I'm coding my way (starting with this website) into becoming a web developer to create my own product one day, in the mean time I'm learning all I possibly can about how to build a company and how to be a versatile web developer. </p>
        </div>
      </div>
    </div>
   </div>

  <div class="servicesSection">
    <div class="wow fadeInUp" data-wow-duration="600ms" data-wow-delay="1200ms">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="servicesBox">
              <div class="boxImg"><i class="fa fa-book boxImgIcon" aria-hidden="true"></i></div>
                <div class="boxText">
                  <h2>EDUCATION</h2>
                  <p>I'm an explorer at heart. I picked up photography sometime along my search for passions in this life and now I'm continously looking for the most exotic landscapes to experience. So far I've learned that iceland is another planet on earth, waterfalls are calming, glacial lakes are nature's perfect form, the best food is in the smallest cities, travelling alone is not so bad, vacation days are never enough.</p>
                </div>
            </div>
          </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <div class="servicesBox">
            <div class="boxImg"><i class="fa fa-globe boxImgIcon" aria-hidden="true"></i></div>
              <div class="boxText">
                <h2>WORK</h2>
                <p>I'm an explorer at heart. I picked up photography sometime along my search for passions in this life and now I'm continously looking for the most exotic landscapes to experience. So far I've learned that iceland is another planet on earth, waterfalls are calming, glacial lakes are nature's perfect form, the best food is in the smallest cities, travelling alone is not so bad, vacation days are never enough.</p>
              </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <div class="servicesBox">
              <div class="boxImg">
                <i class="fa fa-random boxImgIcon" aria-hidden="true"></i>
              </div>
              <div class="boxText">
                <h2>STUFF</h2>
                <p>I'm an explorer at heart. I picked up photography sometime along my search for passions in this life and now I'm continously looking for the most exotic landscapes to experience. So far I've learned that iceland is another planet on earth, waterfalls are calming, glacial lakes are nature's perfect form, the best food is in the smallest cities, travelling alone is not so bad, vacation days are never enough.</p>
              </div>
          </div>
       </div>
      </div>
    </div>
  </div>
</div>
  
  <!-- /#page-content-wrapper -->
  <div class="clearfix"></div>
  </div>

<div class="footer">
  <ul class="footList">
    <li><a href="#">Home</a></li>
    <li><a href="#">AboutUs</a></li>
    <li><a href="#">Group</a></li>
    <li><a href="#">Search Friends</a></li>
    <li><a href="#">FAQ</a></li>
    <li><a href="#">Privacy Policy</a></li>
    <li><a href="#">Terms of Use</a></li>
    <li><a href="#">Contact Us</a></li>
  </ul>
  <p>&copy; <?php echo date("Y"); ?> <a href="#">Hobby Networking</a> all rights reserved</p>
  <div class="clearfix"></div>
</div>

<!--parallax-->
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.nicescroll.js') }}"></script> 
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/parallax.js') }}"></script> 
<!---->
<!--animation-->
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/wow.min.js') }}" type="text/javascript"></script> 
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/custom.js') }}" type="text/javascript"></script>
<!---->
<!-- /#wrapper --> 
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/bootstrap.min.js') }}"></script> 
<script>
$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
     $("#menu-toggle-2").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled-2");
        $('#menu ul').hide();
    });
 
     function initMenu() {
      $('#menu ul').hide();
      $('#menu ul').children('.current').parent().show();
      //$('#menu ul:first').show();
      $('#menu li a').click(
        function() {
          var checkElement = $(this).next();
          if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            return false;
            }
          if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#menu ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
            return false;
            }
          }
        );
      }
    $(document).ready(function() {initMenu();});
</script>
</body>
</html>
