@extends('frontend.dashboard.userprofile_template')
<?php
$loggedinUser = Auth::user();
?>
@section('title', $user->name." @ ")
@section('extra_css')
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/custom_style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/custom_animate.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/flexslider.css') }}" rel="stylesheet" type="text/css">
<link href="{{ Cdn::asset('frontend/dashboard/assets/css/style1.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ Cdn::asset('frontend/plugins/croppic/croppic.css') }}"/>
<link rel="stylesheet" href="{{ Cdn::asset('frontend/plugins/croppic/croppic_main.css') }}"/>
<style type="text/css">
* { margin:auto; }
#page-content-wrapper{ overflow-x: visible;}
.tab-content {
  margin-top: 8px;
}
</style>
@stop

@section('content')
@if(!empty($user->cover_pic))
<style type="text/css">
  .parallax.bg1{
      background-image: url({!! $user->cover_pic !!});
  }
</style>
@endif
    <div class="row bg1 parallax">
    	<div class="container-fluid xyz col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<div class="headerTopInner"> 
                @if(Auth::check() && $user->id == $loggedinUser->id)
                <h1><span class="editable" data-url="{{ route('update.profile.name') }}" data-attribute="['_token','{{ csrf_token() }}']">{{ $user->name }}</span></h1>
                <p class="single_line_desc"><span class="editable" data-url="{{ route('update.profile.status') }}">{{ $user->userSettings->user_status}}</span></p>
                @else
                <h1>{{ $user->name }}</h1>
                <p class="single_line_desc">{{ $user->userSettings->user_status}}</p>
                @endif
                
                {{-- <ul class="socialIcon">
                   <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li> 
                </ul> --}}
                 
            </div>
        </div>
        @if(Auth::check() && ($user->id != $loggedinUser->id))
        <div class="friend_rqust_btn right">
          @if($loggedinUser->isFriendWith($user))
            <a class="mui-btn mui-btn--flat mui-btn--primary" href="{{ route('unfriend_user',$user->id) }}">Unfriend</a>
          @else
            @if($loggedinUser->hasFriendRequestFrom($user))
              <a class="mui-btn mui-btn--flat mui-btn--primary" href="{{ route('accept_friend_request',$user->id) }}">Accept Request</a>
            @else
              @if(!$loggedinUser->hasFriendRequestTo($user))
                <a class="mui-btn mui-btn--flat mui-btn--primary" href="{{ route('friend_user',$user->id) }}">Send friend Request</a>
              @else
                <a class="mui-btn mui-btn--flat mui-btn--primary">Friend Rrquest Send</a>
              @endif
            @endif
          @endif
          @if($loggedinUser->hasBlocked($user))
            <a class="mui-btn mui-btn--flat mui-btn--primary" href="{{ route('unblock_user',$user->id) }}">Unblock</a>
          @else
            <a class="mui-btn mui-btn--flat mui-btn--primary" href="{{ route('block_user',$user->id) }}">Block</a>
              <a href="javascript:startChat({{$loggedinUser->id}},{{$user->id}})" class="mui-btn mui-btn--flat mui-btn--primary">Chat</a>
          @endif
        </div>
        @endif 
    </div>
    <!-- User Activity Section -->
    <div class="container new_profile">
      <div class="row">
      
      <section class="passion about">
        @if(Auth::check() && ($user->id == $loggedinUser->id))
          {{-- <div id="headerTopInnerImg" class="headerTopInnerImg"> --}}
          <img src="{{ $user->profile_pic }}" alt="{{ $user->name }}" class="headerTopInnerImg">
          {{-- </div> --}}
        @else
          <img src="{{ $user->profile_pic }}" alt="{{ $user->name }}" class="headerTopInnerImg">
        @endif
        @if(!empty($user->userSettings->user_about_me))
          <h2>About @if(Auth::check() && $user->id == $loggedinUser->id) Me @else {{ $user->name }} @endif</h2>
          @if(Auth::check() && $user->id == $loggedinUser->id)
            <h3><span class="editable" data-type="textarea" data-url="{{ route('update.profile.about_me') }}">{{ $user->userSettings->user_about_me}}</span></h3>
          @else
            <h3>{{ $user->userSettings->user_about_me}}</h3>
          @endif
        @endif
        <div class="col-md-12 col-xs-12 tab-fnctn" id="menu-center">
          <ul class="nav">
            <li>
            <a href="#hobby-1" >Hobby</a></li>
            <li>
            <a href="#commu-1">Community</a></li>
            <li>
            <a href="#frnd-1"> Friends</a></li>
            <li>
            <a href="#glry-1">Gallery</a></li>
            <li>
            <a href="#post-1">Latest Post</a></li>
          </ul>
        </div>
                  <!-- Tab panes -->
      </section>
      
      <section class="hobby-prfl" id="hobby-1"> 
        <h2>HOBBY</h2>
        <div role="tabpanel" class="tab-pane active" id="hobby-1">
        @foreach($user->hobbies as $eachHobby)
        <div class="col-md-4 col-sm-6 col-xs-12 tab-glry">    
          <a href="{{route('manage_hobby_by_name',$eachHobby->hobby->slug())}}">        
            <div class="thumbnail">
                <div class="caption">
                    <p>{{ $eachHobby->hobby->name }}</p>
                </div>
                <img src="{{ $eachHobby->hobby->profile_pic }}" alt="{{ $eachHobby->hobby->name }}">
            </div>
          </a>
        </div>
        @endforeach
        </div>
        <div class="clearfix"></div>
      </section>
      <section class="community-prfl" id="commu-1">  
        <h2>COMMUNITY</h2>
        <div class="community-btm">
          @if(count($user->communityList())>0)
          @foreach($user->communityList() as $eachCommunity)
          <div class="col-md-2 col-sm-3 col-xs-4 community-itm">
            <a href="{{ route('community_page',$eachCommunity->slug()) }}">
            <img src="{{ $eachCommunity->profile_pic }}" class="img-circle img-border">
              <p>{{ $eachCommunity->name }} </p>
            </a>
          </div>
          @endforeach
        @else
          <div class="center">{{ $user->name }} didnot join any community yet!</div>
        @endif
        </div>
        <div class="clearfix"></div>
      </section>
      <section class="frnd" id="frnd-1">
        <h2>Friends</h2>
        <div class="col-md-12">
          @if(count($user->getFriends())>0)
            @foreach($user->getFriends() as $eachFriend)
            <a href="{{ route('profile_page',$eachFriend->username) }}">
            <div class="frnds-itm">
              <img src="{{ $eachFriend->profile_pic }}" class="img-circle frnd-border">
                <p>{{ $eachFriend->name }}</p>
            </div>
            </a>
            @endforeach
          @else
            <div class="center">{{ $user->name }} don't have any friend yet!</div>
          @endif
        </div>
        <div class="clearfix"></div>
      </section>
      <section class="home-galary" id="glry-1">
        <h2>Gallery</h2>
        @if(count($user->gallery)>0)
          @foreach($user->gallery->take(16) as $eachGallery)
          <div class="col-md-3 col-sm-6 col-xs-12 glry-itm">        
            <div class="thumbnail">
              <a href="{{ $eachGallery->image }}" data-lightbox="profileImageGallery">
                  <img src="{{ $eachGallery->image }}">
              </a>
                
            </div>
          </div>
          @endforeach
        @else
          <div class="center">{{ $user->name }} have not uploaded any photo yet!</div>
        @endif
        <div class="clearfix"></div>
      </section>
      <section class="post-section" id="post-1">
        <h2>Latest Post</h2>
        <div id="show_ajax_post">
            @foreach($userPost as $eachPostUser)
              @if($eachPostUser->isAllowerdToView())
                  @include('frontend.posts.each_post', array("eachPostUser" => $eachPostUser))
              @endif  
            @endforeach
        </div>
        <div class="clearfix"></div>
      </section>
      </div>
    </div>
    <!--End of User Activity Section -->
@stop

@section('extra_js')
 
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/toggler.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.flexslider.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jinplace-1.2.1.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.scrollify.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.scrollify.js') }}"></script>
<script src="https://use.fontawesome.com/9c95426aae.js"></script>
@if(Auth::check())
<!-- Submit Post comment and like Start -->
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/post.js') }}"></script>
<script type="text/javascript">
function addCommentPost(post_id, comment) {
    postCommentAdd(post_id,comment,{{ Auth::user()->id }},"{{ csrf_token() }}","{{ route('add_post_comment') }}");
}
function like(post_id, user_id) {
  $.ajax({
      url: "{{ route('user_post_like') }}", 
      type: "POST",
      data: {
          'post_id':post_id,
          'user_id':user_id,
          '_token':"{{ csrf_token() }}"
      },
      success: function(result){
          $('#postLike_'+post_id).find('.count').html(result.like_count);
          if(result.success){
            $('#postLike_'+post_id).addClass('activeLike');
            $('#postLike_'+post_id).attr('href',"javascript:unlike("+post_id+","+user_id+")");
          }else{
            $('#postLike_'+post_id).removeClass('activeLike');
            $('#postLike_'+post_id).attr('href',"javascript:like("+post_id+","+user_id+")");
          }
          //location.reload();
      }
  });
}
function unlike(post_id, user_id) {
  $.ajax({
      url: "{{ route('user_post_unlike') }}", 
      type: "POST",
      data: {
          'post_id':post_id,
          'user_id':user_id,
          '_token':"{{ csrf_token() }}"
      },
      success: function(result){
          $('#postLike_'+post_id).find('.count').html(result.like_count);
          if(result.success){
            $('#postLike_'+post_id).removeClass('activeLike');
            $('#postLike_'+post_id).attr('href',"javascript:like("+post_id+","+user_id+")");
          }else{
            $('#postLike_'+post_id).addClass('activeLike');
            $('#postLike_'+post_id).attr('href',"javascript:unlike("+post_id+","+user_id+")");
          }
      }
  });
}
</script>
<!-- Submit Post comment and like End -->
{{-- <script src="{{ Cdn::asset('frontend/plugins/croppic/croppic.min.js') }}"></script>
<script>
    var eyeCandy = $('.headerTopInnerImg');
    var croppedOptions = {
        modal: true,
        processInline:true,
        uploadUrl: 'cropupload',
        cropUrl: 'crop',
        // loadPicture: "{{ $user->profile_pic }}",
        cropData:{
            'width' : eyeCandy.width(),
            'height': eyeCandy.height(),
            "user_id":{{ Auth::user()->id }}
        },
        uploadData:{
            "user_id":{{ Auth::user()->id }}
        },
        onAfterImgUpload: function(){ 
          console.log('onAfterImgUpload');
          // $('.cropImgWrapper').width("auto");
          // $('.cropImgWrapper').height("auto");
          // $('.cropImgWrapper').css("left","35%");
        },
        imgEyecandy:true,
        imgEyecandyOpacity:0
    };
    var cropperBox = new Croppic('headerTopInnerImg', croppedOptions);
</script> --}}
@endif
<script type="text/javascript">
$(document).ready(function() {
   $('.editable').jinplace();
      
});
</script>

  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var $window = $(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      $(function() {
        //SyntaxHighlighter.all();
      });

      $window.load(function() {
        $('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 210,
          itemMargin: 5,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            $('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      $window.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>

  <script type="text/javascript">
      jQuery(function () {
        //jQuery('#myTab a:last').tab('show');
        var no=1;
        var currentPage = 1;
        $(window).scroll(function () {
            if(no==1) {
              if ($('#latest_post').hasClass('active') && $(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
                no=2;
                $.ajax({
                    type: "GET",
                    url: "{{ route('welcome_page') }}/?page="+currentPage+"&user_id={{ $user->id }}",
                    cache: true,
                    success: function(data){
                      $('#show_ajax_post').append(data.html);
                      currentPage++;
                      no=1;
                    }
                });
              }
            }
        });
      });
  </script>
  <script type="text/javascript">
      $( document ).ready(function() {
          $("[rel='tooltip']").tooltip();    
       
          $('.thumbnail').hover(
              function(){
                  $(this).find('.caption').slideDown(250); //.fadeIn(250)
              },
              function(){
                  $(this).find('.caption').slideUp(250); //.fadeOut(205)
              }
          ); 
      });
    </script>
  <script>
    $(document).ready(function () {
        //$(document).on("scroll", onScroll);
        //smoothscroll
        $('a[href^="#"]').on('click', function (e) {
            e.preventDefault();
            $(document).off("scroll");
            
            $('a').each(function () {
                $(this).removeClass('active');
            })
            $(this).addClass('active');
          
            var target = this.hash,
                menu = target;
            $target = $(target);
            
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top-125
            }, 500, 'swing', function () {
                window.location.hash = target;
            });
        });
    });

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    //scrollPos += 125
    // console.warn('called')
    $('#menu-center a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        //console.log(refElement)
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('#menu-center ul li a').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
    });
}
  </script>

    <script type="text/javascript">
    var menuCenterTop = $('#menu-center').offset().top;
    $(window).scroll(function(){
      var sticky = $('.tab-fnctn'),
        scroll = $(window).scrollTop();
      if (scroll >= menuCenterTop) sticky.addClass('fixed');
      else sticky.removeClass('fixed');
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function(e){
        
        //var text = document.html('NOTIFY WITH LINK < a href="http://google.com">EXAMPLE< /a >');
       var n = noty({
        text: 'NOTIFY WITH LINK EXAMPLE',
        type: 'success',
        dismissQueue: true,
        layout: 'bottomCenter',
        closeWith: ['click'],
        theme: 'relax',
        maxVisible: 10,
        animation: {
        open: {height: 'toggle'},
        close: {height: 'toggle'},
        easing: 'swing',
        speed: 1500 // opening & closing animation speed
    }

    });
      $(".noty_bar").css("background-color","#dc4622");
      $(".noty_text").css("color","#FFFFFF");
      $('.noty_text').html('You can check out our Explainer Videos for this section. <a href="javascript:void(0)" rel="oiHVA1WBViY" class="youtube" style="color: #000000; text-decoration: underline;">Profile Creation</a>');
      setTimeout(function(){ $('#noty_bottomCenter_layout_container').find('li').fadeOut() }, 5000);
    });
</script>
<script type="text/javascript">
  $(function () {
    $(".youtube").YouTubeModal({autoplay:0, width:640, height:480});
  });
</script>
@stop