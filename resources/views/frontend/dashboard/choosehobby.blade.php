@extends('frontend.dashboard.template')
@section('extra_css')
<style type="text/css">
.img-responsive{
  height: 200px;
}
</style>
@stop
@section('content')
<div class="container-fluid xyz main_div_click choosehobby_page">
    <div class="row2">
        <div class="col-lg-10 mdlBlock click_reverse">
            <h1>Choose your Hobbies</h1>
            <div class="clearfix" style="margin-top: 20px;"></div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 find-add-hbb">
                  <a style="text-decoration: none; font-size: 14px; border-radius: 4px;background: #ef542e; display: inline-block; color: #fff; font-weight: normal;font-family: 'Lato', sans-serif; text-align: center; padding: 7px 9px;" href="{{ route('addhobby_page') }}">Couldn't find your hobby? Add it here!
                  
                  </a>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div id="custom-search-input">
                    <div class="input-group" style="width: 100%;">
                        <input type="text" id ="hobby_search" name="hobby_search" class="form-control input-lg" placeholder="Search Your Hobby Here..." />
                        <span class="input-group-btn">
                            <button class="btn btn-info btn-lg" type="button" id="search_hobby">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-right">
                <p class="next-hob"><a id="next-add-hobby" href="#basic" data-toggle="modal" onclick="addHobby();">next<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a></p>
            </div>
            </div>
            <div class="clearfix" style="margin-top: 20px;"></div>
            <div class="row" id="hobbies_grid">
                @foreach($hobbies as $key=>$eachHobby)
                
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pro_div" onclick="selectHobby(this)">
                        <div @if(!in_array($eachHobby->id,$user_hobbies))
                          style="display:none;" @endif class="chbox"> 
                          <img src="{{ Cdn::asset('frontend/dashboard/assets/img/tick.png') }}">
                        </div>
                        <div class="pro_block">
                          @if(in_array($eachHobby->id,$user_hobbies))
                          <input value="{{$eachHobby->id}}" checked="checked" type="checkbox" id="{{$eachHobby->id}}" name="hobby_{{$key}}" class="hide" onclick="loadHobbies()">
                          @else
                          <input value="" type="checkbox" id="{{$eachHobby->id}}" name="hobby_{{$key}}" class="hide" onclick="loadHobbies()">
                          @endif
                            <img src="{{ $eachHobby->profile_pic }}" class="select_img img-responsive select_hobby_image" width="178px" height="178px">
                            <h5>{{ $eachHobby->name }}</h5>
                            <p> {{ $eachHobby->category->name }}</p> 
                        </div>
                    </div>
                @endforeach    
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
  <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content newcss-modal-content">
              <div class="modal-body" > Hobby Successfully Submitted... You will be redirect shortly...  </div>
          </div>
          <!-- /.modal-content -->
      </div>
  <!-- /.modal-dialog -->
  </div>
@stop
@section('extra_js')
<script>
var canCallAjax = true;
// $("#menu-toggle").click(function(e) {
//     e.preventDefault();
//     $("#wrapper").toggleClass("toggled");
// });
//  $("#menu-toggle-2").click(function(e) {
//     e.preventDefault();
//     $("#wrapper").toggleClass("toggled-2");
//     $('#menu ul').hide();
// });
 function selectHobbyCat (that) {
   selectHobby(that);
 }
 function selectHobby (that) {
   if($(that).find('input[type=checkbox]').prop("checked") == false){
        //$(that).find('.pro_block').css( "background-color", "rgba(0, 0, 0, 0.18)" );
        $(that).find('input[type=checkbox]').prop( "checked", true );
        $(that).find('.chbox').show();
        var hobbyId = $(that).find('input[type=checkbox]').attr('id');
        addRemoveHobby(hobbyId, that);
    }else{
        //$(that).find('.pro_block').css( "background-color", "white" );
        $(that).find('input[type=checkbox]').prop( "checked", false );
        $(that).find('.chbox').hide();
        //$(that).find('.pro_block').find('input[type=checkbox]').value('');
        var hobbyId = $(that).find('input[type=checkbox]').attr('id');
        addRemoveHobby(hobbyId, that);
    }
 }
function addRemoveHobby(hobbyId, that) {
  var uri = '{{ route('add_remove_hobby')}}';
    var data ={
      'hobby_id': hobbyId,
      '_token': "{{ csrf_token() }}",
    }
    $.ajax({
      type:'POST',
      url:uri,
      data:data,
      success: function(data){
        $(that).find('input[type=checkbox]').prop( "checked", data.is_selected );
        if(data.is_selected){
          $(that).find('.chbox').show();
        }else{
          $(that).find('.chbox').hide();
        }
      }
    });
}
function initMenu() {
  $('#menu ul').hide();
  $('#menu ul').children('.current').parent().show();
  $('#menu li a').click(function() {
      var checkElement = $(this).next();
      if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
        return false;
      }
      if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
        $('#menu ul:visible').slideUp('normal');
        checkElement.slideDown('normal');
        return false;
      }
  });
}
$(document).ready(function() {
  initMenu();
  var checked_value = '';
  //$('.chbox').hide();
  $('body').on('click','pro_div',function(e){ //console.log(1);
      //selectHobby($(this));
  });
  /*$('#search_hobby').click(function(){
      
      reloadHobbies();
  });*/
  $("#hobby_search").keyup(function(){
      reloadHobbies();
  });
   
  $('#hobbies_grid').scroll(function(e){
    if($('#hobbies_grid').scrollTop() + $('#hobbies_grid').height() > $('#hobbies_grid').height() - 10) {
      loadHobbiesAjax();
    }
  });
});
var page_no = 1;
function loadHobbiesAjax(){
  if(canCallAjax){
    var uri = '{{ route('limitscroll')}}';
    var data ={
      'page_id': page_no,
      '_token': "{{ csrf_token() }}",
    }
    $.ajax({
      type:'POST',
      url:uri,
      data:data,
      success: function(data){
        if(data.success){
          $('#hobbies_grid').append(data.html);
          canCallAjax = true;
          page_no++;
        }
      }
    });
    canCallAjax = false;
  }      
}
    function reloadHobbies() {
      var uri = '{{ route('searchobby')}}';
      var data = {
        'hobby_search': $('#hobby_search').val(),
        '_token': "{{ csrf_token() }}",
      }
      $.ajax({
        type:'POST',
        url: uri,
        data: data,
        success: function(data){
          if(data.success){
            $('#hobbies_grid').html(data.html);
            //$('.chbox').hide();
          }
        }
      });
    }
    function addHobby(){
        var hobby_ids = [];
        //$('#next-add-hobby').attr("href","javascript:;");
        $('.pro_block').find('input[type=checkbox]').each(function(){
             if($(this).prop("checked")== true ){
                
                hobby_ids.push($(this).attr('id'));
             }
        });
        //console.log(hobby_ids.length);
        var token_val = '<?php echo csrf_token(); ?>';
        var uri = '{{ route("choosehobby_ajax") }}';
        if(hobby_ids.length>0){
            $.ajax({
               type: 'POST',
               url: uri,
               data: "hobby_ids="+hobby_ids+"&_token="+token_val,
               error: function() {
                  alert("OH NO!! Some Error has ocured!!");
               },
               
               success: function(data) {
                  
                    if(data = "success"){
                        //alert("Hobbies Submited Successfully");
                        //$('#next-add-hobby').attr("href","#basic");
                        //$('#next-add-hobby').attr("data-toggle","modal");
                        $('.modal-body').html("Hobby Successfully Submitted... You will be redirect shortly...");
                        setTimeout(function(){
                          @if($nextInvite)
                           window.location.href = "{{ route('invitation',true) }}";
                          @else
                           window.location.href = "{{ route('invitation',true) }}";
                           // window.location.href = "{{ route('welcome_page') }}";
                          @endif
                        }, 1000);
                    }
               }
            });
        }else{
          $('.modal-body').html("You need to select at least one Hobby or create one ! You can always change later");
          //window.location.href = "{{ route('welcome_page') }}"; 
        }
        
       
    }
   
    $(".main_div_click").click(function(e) {
      //console.log($(e.target).parents(".click_reverse"));
      if ( !($(e.target).is('.click_reverse') || $(e.target).parents(".click_reverse").length == 1 )) { 
          
          window.location.href = "{{ URL::previous() }}";
      } 
      
    });
</script>
<script type="text/javascript">
    $(document).ready(function(e){
        
        //var text = document.html('NOTIFY WITH LINK < a href="http://google.com">EXAMPLE< /a >');
       var n = noty({
        text: 'NOTIFY WITH LINK EXAMPLE',
        type: 'success',
        dismissQueue: true,
        layout: 'bottomCenter',
        closeWith: ['click'],
        theme: 'relax',
        maxVisible: 10,
        animation: {
        open: {height: 'toggle'},
        close: {height: 'toggle'},
        easing: 'swing',
        speed: 1500 // opening & closing animation speed
    }

    });
        $(".noty_bar").css("background-color","#dc4622");
        $(".noty_text").css("color","#FFFFFF");
        $('.noty_text').html('You can check out our Explainer Videos for this section. <a href="javascript:void(0)" rel="SZl7AI73xVE" class="youtube" style="color: #000000; text-decoration: underline;">Choose a Hobby</a>');
    
        setTimeout(function(){ $('#noty_bottomCenter_layout_container').find('li').fadeOut() }, 5000);
    });
</script>
<script type="text/javascript">
  $(function () {
    $(".youtube").YouTubeModal({autoplay:0});
  });
</script>
@stop