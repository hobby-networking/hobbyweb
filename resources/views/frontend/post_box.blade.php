<style type="text/css">
.thumbnail {
    float: left;
    width: 50px;
    height: 50px;
    position: relative;
    padding: 0px;
    margin-right: 2px;
    margin-left: 2px;
  }
.post_file{float:left;}
</style>
  <?php 
      $encrypter = app('Illuminate\Encryption\Encrypter');
      $encrypted_token = $encrypter->encrypt(csrf_token()); 
  ?>
  <input type="hidden" id="token_main_form" value="<?php echo $encrypted_token; ?>" />    
  <input type="hidden"  name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" class="post_files_string"  name="files" value="" />
  <input type="hidden" name="post_type" value="{{ $type }}" />
  @if($type == 'community') 
    <input type="hidden" name="community_id" value="{{ $community_id }}" />
  @endif
  <div class="_4d6h _1kvc">
    <div class="clearfix">
      <div>
          <label for="file" class="file_label"><i class="fa fa-picture-o" aria-hidden="true"></i><i class="_4-h8"></i><span>Add Photos/Videos</span></label>
          <input class="file_upload_post" type="file" name="allFiles[]" style="display: none" multiple>
      </div>      
    </div>
  </div>
  <div>
    <div class="clearfix _1hx">
      <div class="_4bl9">
        <textarea id="post_content" name="post_content" title="What's on your mind?" class="uiTextareaAutogrow _3en1 post_content" placeholder="What's on your mind?"></textarea>
        <div class="result"></div>
      </div>
    </div><div class="urlive-container"></div>
  </div>
@if($type=="user" && $selected_hoby != "my_feed")
  <input type="hidden" name="hobby_id[]" value="<?php echo $hobby->id; ?>">
@endif
  <div class="_ei_">
    
    <div class="clearfix">
      <div class="_3-8i rfloat _ohf">
		
        <div class="post_type">
          <select name="post_mode" class="no_border_select">
    
            <option value="public">Public</option>
    
            <option value="private">Private</option>
    
          </select>
    
        </div>
        
        <button id="submitButton" value="1" type="submit">Post</button>
      </div>
    </div>
  </div>
@section('extra_js1')
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.form.js') }}"></script>
<script type="text/javascript">
var aaa;
var output;
var div;
var final_files = [];
var final_files_after_remove = [];
    var fileIndex =0;
$(document).ready(function(e){
  localStorage.clear();
  $(".post_form_2").validate({
      rules: {
          post_content: "required",
          hobby_id: "required",
      },
      messages: {
          post_content: "Please enter your Post Content",
          hobby_id: "Please select atleast one hobby",
      },
      submitHandler: function (form) {
        @if($type=="user" && ($selected_hoby == "my_feed" || !$isMyHobby))
          $("#myModal2").modal('show');
          $('#SubForm2').click(function () {
            if ($("select[name='hobby_id[]'] option:selected").length != 0) {form.submit();}
         });
        @else
          form.submit();
        @endif
      }
  }); 
  $(".post_form").validate({
      rules: {
          post_content: "required",
      },
      messages: {
          post_content: "Please enter your Post Content",
      },
      submitHandler: function (form) {
      @if($type=="user" && ($selected_hoby == "my_feed" || !$isMyHobby))  
        $("#myModal").modal('show');
        $('#SubForm').click(function () {
          form.submit();
        });
      @else
        form.submit();
      @endif
      }
  });  
  $(".file_label").click(function(e11) {
      $(this).next('.file_upload_post').click();
  });
  $('.file_upload_post').on('change',function(e1){ 
    processImage(e1);
  }); 
});
function removeIt(argument) {
  var divId = "post_img_"+argument;
  $('#'+divId).remove();
  localStorage.removeItem("final_files"+argument);
  setFileFieldValue();
}
function processImage(event){
    var e1 = event;
    var files = event.target.files;
    if($(event.target).parents('.post_form').length == 0){
      output = $(e1.target).parents('.post_form_2').find('.result');
    }else{
      output = $(e1.target).parents('.post_form').find('.result');
    }
    for(var i = 0; i< files.length; i++)
    {
        var file = files[i];
        var uploadingDiv = "<img class='thumbnail' src='{{ Cdn::asset('frontend/loading.gif') }}' title='Uploading'/>";                
        if(!file.type.match('image') || !file.type.match('video')){
            if(file.type.match('image')){
                div = document.createElement("div");
                div.setAttribute("id", "post_img_"+fileIndex);
                div.setAttribute("class", "post_file");
                div.innerHTML = uploadingDiv;
                $(output).append(div);
                ajaxPostFileUpload(file,fileIndex,output,div,'image');                      
                fileIndex++;                  
            }else{
              if(file.type == "video/mp4"){
                div = document.createElement("div");
                div.setAttribute("id", "post_img_"+fileIndex);
                div.setAttribute("class", "post_files");
                div.innerHTML = uploadingDiv;
                $(output).append(div);
                //ajaxPostFileUpload(file,fileIndex,output,div,'video');
                upload(file, fileIndex, div)
                fileIndex++;    
              }else{
                alert("Only mp4 format is supported");
              }                
            }
        }
        else{
          alert("Invalide file type");
        }
    } 
}
var fffff;
var vidThumbLink = "http://a5.mzstatic.com/au/r30/Purple/v4/d3/82/b0/d382b0b1-6b45-0494-fa3f-ee0766045cdf/icon175x175.png";
function ajaxPostFileUpload(file_data,fileIndex,output,div,type){
  //console.log(file_data);
  var url = "{{ route('postuser_ajax') }}";
  var $_token_main = $('#token_main_form').val();
  var formData = new FormData();
  var success = false;      
  formData.append('file', file_data);
  //console.log(formData);
  fffff = formData;
    
    $.ajax({
        url:url,
        headers: { 'X-XSRF-TOKEN' : $_token_main },
        data:formData,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        //dataType: "json",
        async: true,
        success: function(data){
          //console.log(data);
          if(data.success){
            //window.location.reload();
            success = true;
            removeIt(fileIndex);
            if(type =='image'){
              var src = data.filePath;
            }
            else{
              var src = vidThumbLink;
            }
            div.innerHTML = "<img class='thumbnail' src='" + src + "'" +
                    "title='" + data.file_name + "'/> <a href='javascript:removeIt("+fileIndex+")' class='remove_pict'>X</a>";
            $(output).append(div);
            var uploadedFile =[];
                uploadedFile['name'] = data.file_name;
                uploadedFile['type'] = type;
            localStorage.setItem("final_files"+fileIndex, type+','+data.file_name); 
            setFileFieldValue();            
          } else {
            success = false;
          }
        }
    });     
}
function upload(file, fileIndex, div){
  
  // take the file from the input
  var url = "{{ route('postuser_ajax_video') }}";
  url = url+"/"+file.name;
  type = "video";
  //console.info(url);
  var reader = new FileReader();
  reader.readAsBinaryString(file); // alternatively you can use readAsDataURL
  reader.onloadend  = function(evt) {
      // create XHR instance
      xhr = new XMLHttpRequest();
      // send the file through POST
      xhr.open("POST", url, true);
      // make sure we have the sendAsBinary method on all browsers
      XMLHttpRequest.prototype.mySendAsBinary = function(text){
        var data = new ArrayBuffer(text.length);
        var ui8a = new Uint8Array(data, 0);
        for (var i = 0; i < text.length; i++) ui8a[i] = (text.charCodeAt(i) & 0xff);
          if(typeof window.Blob == "function") {
            var blob = new Blob([data], {type: file.type});
          }else{
            var bb = new (window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder)();
            bb.append(data);
            var blob = bb.getBlob();
          }
          var fileOfBlob = new File([blob], file.name);
          var formData = new FormData();
          formData.append("blob",fileOfBlob,file.name);
          //formData.append("file_name",file.name);
          console.log(formData);
        this.send(fileOfBlob);
      }
      
      // let's track upload progress
      var eventSource = xhr.upload || xhr;
      eventSource.addEventListener("progress", function(e) {
        // get percentage of how much of the current file has been sent
        var position = e.position || e.loaded;
        var total = e.totalSize || e.total;
        var percentage = Math.round((position/total)*100);
        // here you should write your own code how you wish to proces this
      });
      
      // state change observer - we need to know when and if the file was successfully uploaded
      xhr.onreadystatechange = function() {
        if(xhr.readyState == 4) {
          if(xhr.status == 200) {
            data = JSON.parse(xhr.responseText);
            console.log(data.success);
            if(data.success){
              //window.location.reload();
              success = true;
              removeIt(fileIndex);
              var src = vidThumbLink;
              div.innerHTML = "<img class='thumbnail' src='" + src + "'" +
                      "title='" + data.file_name + "'/> <a href='javascript:removeIt("+fileIndex+")' class='remove_pict'>X</a>";
              $(output).append(div);
              var uploadedFile =[];
                  uploadedFile['name'] = data.file_name;
                  uploadedFile['type'] = "video";
              localStorage.setItem("final_files"+fileIndex, type+','+data.file_name); 
              setFileFieldValue();            
            } else {
              success = false;
            }
          }else{
            // process error
          }
        }
      };
      // start sending
      xhr.mySendAsBinary(evt.target.result);
  };
}
function setFileFieldValue(){
  var stringNew='';
  for(var i = 0; i<fileIndex; i++){
     var string=localStorage.getItem("final_files"+i)+'|';
     if(string){
          stringNew = stringNew.concat(string);
     }
     //string = string.concat(localStorage.getItem("final_files"+i));
  }
  stringNew = stringNew.substring(0, stringNew.length - 1).trim();
  $(".post_files_string").val(stringNew);
  console.log(stringNew);
}
</script>
@stop