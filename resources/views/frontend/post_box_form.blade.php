<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="_4d6h _1kvc">
  <div class="clearfix">
    <div>
        <label for="file" class="file_label" style="cursor: pointer; width: 28%;color: #69c458;"><i class="fa fa-picture-o" aria-hidden="true"></i><i class="_4-h8"></i><span>Add Photos/Videos</span></label>
        <input class="file_upload_post" type="file" name="allFiles[]" style="display: none" multiple>
    </div>      
  </div>
  <div style="float: right;margin: -18px 4px;">
    <select name="post_mode" class="no_border_select">
      <option value="public">Public</option>
      <option value="private">Private</option>
    </select>
  </div>
</div>
<div>
  <div class="clearfix _1hx">
    <div class="_4bl9">
      <textarea id="post_content" name="post_content" title="What's on your mind?" class="uiTextareaAutogrow _3en1" placeholder="What's on your mind?"></textarea>
      <div class="result"></div>
    </div>
  </div>
</div>
<div class="_ei_">
  <div class="clearfix">
    <div class="_3-8i rfloat _ohf">
      <button id="submit" value="1" type="submit">Post</button>
    </div>
  </div>
</div>