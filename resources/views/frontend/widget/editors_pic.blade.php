@if($hobby!="all")
<?php
$editorsPic = $hobby->category->editorsPic;
?>
@else
<?php
$editorsPic = App\Models\HobbyCatEditorsPic::where('is_in_my_feed','=','yes')->where('is_active','=','yes')->get();
?>
@endif
<link rel="stylesheet" href="{{ Cdn::asset('frontend/dashboard/assets/css/carouselTicker.css') }}">

<div id="carouselTicker" class="carouselTicker">
    <ul class="carouselTicker__list">
		@foreach($editorsPic as $eachEditorsPic)
			<li class="carouselTicker__item">
				<a href="{{$eachEditorsPic->link}}" target="_blank">
            	{{-- <div class="title">{!! $eachEditorsPic->title !!}</div> --}}
            	<div class="content"><img src="{{ $eachEditorsPic->image }}" width="100px"></div>
            	<div class="clearfix"></div>
            	</a>
	        </li>
		@endforeach
    </ul>
</div>

@section('editors_pic_js')
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.carouselTicker.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/carouselstart.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
		
	});
</script>
@stop
