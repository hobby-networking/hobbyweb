<?php
$searchQuery = "";
if ($hobby == 'all') {
	foreach (Auth::user()->hobbies as $eachHobby) {
		$searchQuery .= ",".$eachHobby->hobby->name;
	}
	$searchQuery = ltrim($searchQuery, ',');
}else{
	$searchQuery = $hobby->name;
}
$entities = array();
try{
	$ip = \Request::ip();
	$ip=$_SERVER['REMOTE_ADDR'];
	$clientDetails = json_decode(file_get_contents("http://ipinfo.io/$ip/json"));
	$url = "https://ajax.googleapis.com/ajax/services/feed/find";
	$response = Ixudra\Curl\Facades\Curl::to($url)
	        ->withData( array( 'v' => '1.0', 'q'=>$searchQuery,'userip'=>$clientDetails->ip ) )
	        ->withOption('SSL_VERIFYHOST', 0)
	        ->withOption('SSL_VERIFYPEER', 0)
	        ->asJson()
	        ->get();
	$entities = $response->responseData->entries;
}catch(Exception $e) {
}
?>
<link rel="stylesheet" href="{{ Cdn::asset('frontend/dashboard/assets/css/carouselTicker.css') }}">
<div id="carouselTicker2" class="carouselTicker row">
    <ul class="carouselTicker__list">
		@foreach($entities as $eachEntity)
			<li class="carouselTicker__item">
	            <a href="{{ $eachEntity->link }}" target="_blank">
	            	<div class="title">{!! $eachEntity->title !!}</div>
	            	<div class="content">{!! $eachEntity->contentSnippet !!}</div>
	            	<div class="clearfix"></div>
	            </a>
	        </li>
		@endforeach
    </ul>
</div>
@section('editors_pic_js')
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/jquery.carouselTicker.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/dashboard/assets/js/carouselstart.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
		
	});
</script>
@stop