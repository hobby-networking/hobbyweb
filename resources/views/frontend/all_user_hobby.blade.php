@extends('frontend.dashboard.dashboard_template')
@section('extra_css')
<style type="text/css">
.all_user_hobby .nav-tabs { border-bottom: 2px solid #DDD; text-align: center;}
.all_user_hobby .nav-tabs>li {display: inline-block;float: none;margin: 0 auto;}
.all_user_hobby .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
.all_user_hobby .nav-tabs > li > a { border: none; color: #666; }
.all_user_hobby .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
.all_user_hobby .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
.all_user_hobby .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.all_user_hobby .tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
.all_user_hobby .tab-pane { padding: 15px 0; }
.all_user_hobby .tab-content{padding:20px}
.all_user_hobby .each_hobby_member_img { width: 31px; border: 1px solid; border-radius: 50%; float: left;}
.all_user_hobby .each_hobby { position: relative; width: 150px; height: 200px; overflow: hidden;}
.all_user_hobby .each_hobby .hobby_name { position: absolute; top: 0px;}
.all_user_hobby .each_hobby .hobby_members_count {
    bottom: 30px;
    position: absolute;
}
.all_user_hobby .each_hobby .hobby_members {
    position: absolute;
    bottom: 0px;
}
</style>
@stop

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
            <div class="all_user_hobby">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#my_hobby" aria-controls="my_hobby" role="tab" data-toggle="tab">My Hobby</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="my_hobby">
                    	<div class="row">
							<div class="col-md-12">
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<a href="{{ route('choosehobby_page') }}">
									<div class="each_hobby">
										<p class="create_community">Add your hobby</p>
									</div>
									</a>
								</div>

								@foreach($loggedInUser->hobbies as $eachHobby)

								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<a href="{{ route('manage_hobby_by_name',$eachHobby->hobby->hobby_slug) }}">
									<div class="each_hobby">
										<img src="{{ $eachHobby->hobby->profile_pic }}" class="each_comminity_img" />
										<p class="hobby_name">{{ (strlen($eachHobby->hobby->name)>10)?substr($eachHobby->hobby->name,0,6)."...":$eachHobby->hobby->name }}</p>
										<p class="hobby_members_count">{{ count($eachHobby->getConnectedUserByHobby()) }} Friends with similar hobby</p>
										<div class="hobby_members">
											@foreach(array_slice($eachHobby->getConnectedUserByHobby(),0,5) as $eachFriend)
											<?php $userDetails = app('App\Models\User')->where('id','=',$eachFriend)->first(); ?>
												<a href="{{ route('profile_page',$userDetails->username) }}">
													<img src="{{ $userDetails->profile_pic }}" class="each_hobby_member_img" alt="{{ $userDetails->name }}" title="{{ $userDetails->name }}" />
												</a>
											@endforeach
										</div>
									</div>
									</a>
								</div>
								@endforeach
							</div>
						</div>
                    </div>

                </div>
			</div>
        </div>
	</div>
</div>
@stop

@section('extra_js')

@stop