<?php $displayLimit = 5; ?>
<!-- People With Similar Interest Start -->
<div class="rightBar">
	 <h5>people  with similar hobbies around you
    @if($selected_hoby != "my_feed")
      <br>{{ $hobby->name }}
    @endif
    </h5>
    <div class="col-md-12">
      <form action="{{ route('search_slug',array('friends')) }}" class="search-form">
          <div class="form-group has-feedback">
      		<input type="text" class="form-control" name="slug" id="search" placeholder="Search People...">
        		<span class="glyphicon glyphicon-search form-control-feedback"></span>
      	</div>
      </form>
    </div>
    @if($selected_hoby == "my_feed")
    <?php $x=0; ?>
      @if(!empty($loggedinUser->getPeopleWithSimilarHobby()))
          @foreach($loggedinUser->getPeopleWithSimilarHobby() as $eachUser)
          @if($x<$displayLimit)
          <?php $userDetails = app('App\Models\User')->where('id','=',$eachUser)->first(); 
              $username = $userDetails->username;
          ?>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
              <a href="{{ route('profile_page',$username) }}"><img src="{{ $userDetails->profile_pic}}" class="img-responsive img-thumbnail imgBoder" alt="{{ $userDetails->name }}" title="{{ $userDetails->name }}"></a>
          </div>
          <?php $x++; ?>
          @endif
          @endforeach

          @if(count($loggedinUser->getPeopleWithSimilarHobby()) >= $displayLimit )
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
              <a href="javascript:openSimilarInterest({{$loggedinUser->id}})"><img src="{{ Cdn::asset('frontend/dashboard/assets/images/viewmore.png') }}" class="img-responsive img-thumbnail imgBoder"></a>
            </div>
          @endif
          <div id="similar_interest_{{$loggedinUser->id}}" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h3 id="myModalLabel3">People With Similar Hobbies</h3>
                      </div>
                      <div class="modal-body">
                        <div class="similar_interest">
                          @foreach($loggedinUser->getPeopleWithSimilarHobby() as $eachUser)
                          <?php $userDetails = app('App\Models\User')->where('id','=',$eachUser)->first(); 
                              $username = $userDetails->username;
                          ?>
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
                              <a href="{{ route('profile_page',$username) }}"><img src="{{ $userDetails->profile_pic}}" class="img-responsive img-thumbnail imgBoder" title="{{ $userDetails->name }}" alt="{{ $userDetails->name }}"></a>
                          </div>
                          @endforeach
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-footer">
                          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                      </div>
                  </div>
              </div>
          </div>
      @else
        <p style="text-align: center;">Not Matched with your hobbies...</p>
      @endif
    @else
      @if(!empty($listUser))
      <?php $i=0; ?>
        @foreach($listUser as $eachUser)
          @if($i<$displayLimit)
            <?php $userDetails = app('App\Models\User')->where('id','=',$eachUser)->first(); 
                $username = $userDetails->username;
            ?>
        	   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
            	<a href="{{ route('profile_page',$username) }}"><img src="{{ $userDetails->profile_pic}}" class="img-responsive img-thumbnail imgBoder" title="{{ $userDetails->name }}"></a>
            </div>
          <?php $i++; ?>
          @endif
        @endforeach
		    @if(count($listUser) >= $displayLimit )
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
        	<a href="javascript:openSimilarInterest({{$hobby->id}})"><img src="{{ Cdn::asset('frontend/dashboard/assets/images/viewmore.png') }}" class="img-responsive img-thumbnail imgBoder"></a>
        </div>
        @endif
        <div id="similar_interest_{{$hobby->id}}" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h3 id="myModalLabel3">People With Similar Interest</h3>
                    </div>
                    <div class="modal-body">
                      <div class="similar_interest">
                        @foreach($listUser as $eachUser)
                            <?php $userDetails = app('App\Models\User')->where('id','=',$eachUser)->first(); 
                              $username = $userDetails->username;
                            ?>
                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
                              <a href="{{ route('profile_page',$username) }}"><img src="{{ $userDetails->profile_pic}}" class="img-responsive img-thumbnail imgBoder" title="{{ $userDetails->name }}" alt="{{ $userDetails->name }}"></a>
                            </div>
                        @endforeach
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
        </div>
      @else
        <p style="text-align: center;">Not Matched with your interest...</p>
      @endif
    @endif
  <div class="clearfix"></div>
</div>
<!-- People With Similar Interest End -->
<!-- Friends Section Start -->
@if(Auth::check())
<div class="rightBar">
    <h5>Friends</h5>
    @if($selected_hoby == "my_feed")
        <?php $listFriends = Auth::user()->getFriends(); ?>
        @if(!empty($listFriends))
          <?php $x=0; ?>
            @foreach($listFriends as $eachFriend)
              @if($x<$displayLimit)
                <?php 
                  //$userDetails = $userDetails = app('App\Models\User')->where('id','=',$eachFriend->recipient_id)->first(); 
                  //dd($eachFriend);
                  $userDetails = $eachFriend;
                  $username = $userDetails->username;
                ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
                    <img src="{{ $userDetails->profile_pic}}" class="img-responsive img-thumbnail imgBoder friend_image" title="{{ $userDetails->name }}">
                    <ul class="friends_options no_list_style" style="display:none">
                      <li><a class="btn-floating orange" href="javascript:startChat('{{Auth::user()->id}}','{{$userDetails->id}}')" title="Chat with {{ $userDetails->name }}"><i class="material-icons">chat</i></a></li>
                      <li><a class="btn-floating blue" href="{{ route('profile_page',$username) }}" title="Go to {{ $userDetails->name }}'s profile"><i class="material-icons">perm_identity</i></a></li>
                    </ul>
                </div>
                <?php $x++; ?>
              @endif
            @endforeach
            @if(count($listFriends) >= $displayLimit )
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
              <a href="javascript:openFriendsModal({{$loggedinUser->id}})"><img src="{{ Cdn::asset('frontend/dashboard/assets/images/viewmore.png') }}" class="img-responsive img-thumbnail imgBoder"></a>
            </div>
            <div id="friends_modal_{{$loggedinUser->id}}" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h3 id="myModalLabel3">Friends</h3>
                        </div>
                        <div class="modal-body">
                          <div class="similar_interest">
                            @foreach($listFriends as $eachFriend)
                                <?php 
                                  $userDetails = $eachFriend; 
                                  $username = $userDetails->username;
                                ?>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
                                    <img src="{{ $userDetails->profile_pic}}" class="img-responsive img-thumbnail imgBoder friend_image" title="{{ $userDetails->name }}">
                                    <ul class="friends_options no_list_style" style="display:none">
                                      <li><a class="btn-floating orange" href="javascript:startChat('{{Auth::user()->id}}','{{$userDetails->id}}')"><i class="material-icons">chat</i></a></li>
                                      <li><a class="btn-floating blue" href="{{ route('profile_page',$username) }}"><i class="material-icons">perm_identity</i></a></li>
                                    </ul>
                                </div>
                            @endforeach
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @else
        <p style="text-align: center;">Not Matched with your interest...</p>
        @endif
      <div class="clearfix"></div>
    @else
      <?php $listFriends = Auth::user()->hobbyWisefriends($hobby); ?>
      @if(!empty($listFriends))
      <?php $x=0; ?>
          @foreach($listFriends as $eachFriend)
            @if($x<$displayLimit)
              <?php 
                //dd($eachFriend);
                $userDetails = $eachFriend; 
                  $username = $userDetails->username;
              ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
                  <img src="{{ $userDetails->profile_pic}}" class="img-responsive img-thumbnail imgBoder friend_image" title="{{ $userDetails->name }}">
                  <ul class="friends_options no_list_style" style="display:none">
                    <li><a class="btn-floating orange" href="javascript:startChat('{{Auth::user()->id}}','{{$userDetails->id}}')" title="Chat with {{ $userDetails->name }}"><i class="material-icons">chat</i></a></li>
                    <li><a class="btn-floating blue" href="{{ route('profile_page',$username) }}" title="Go to {{ $userDetails->name }}'s profile"><i class="material-icons">perm_identity</i></a></li>
                  </ul>
              </div>
              <?php $x++; ?>
            @endif
          @endforeach
		      @if(count($listFriends) >= $displayLimit )
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
          	<a href="javascript:openFriendsModal({{$hobby->id}})"><img src="{{ Cdn::asset('frontend/dashboard/assets/images/viewmore.png') }}" class="img-responsive img-thumbnail imgBoder"></a>
          </div>
          @endif
          <div id="friends_modal_{{$hobby->id}}" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h3 id="myModalLabel3">Friends</h3>
                      </div>
                      <div class="modal-body">
                        <div class="similar_interest">
                          @foreach($listFriends as $eachFriend)
                              <?php 
                                $userDetails = $eachFriend; 
                                $username = $userDetails->username;
                              ?>
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
                                  <img src="{{ $userDetails->profile_pic}}" class="img-responsive img-thumbnail imgBoder friend_image" title="{{ $userDetails->name }}">
                                  <ul class="friends_options no_list_style" style="display:none">
                                    <li><a class="btn-floating orange" href="javascript:startChat('{{Auth::user()->id}}','{{$userDetails->id}}')"><i class="material-icons">chat</i></a></li>
                                    <li><a class="btn-floating blue" href="{{ route('profile_page',$username) }}"><i class="material-icons">perm_identity</i></a></li>
                                  </ul>
                              </div>
                          @endforeach
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-footer">
                          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                      </div>
                  </div>
              </div>
          </div>
      @else
      <p style="text-align: center;">Not Matched with your interest...</p>
      @endif
    @endif
</div>
@endif
<!-- Friends Section End -->
<!-- Community Start -->
<div class="rightBar">
	<h5>My communities</h5>
  @php $displayLimitcommu = 5; @endphp
    @if(Auth::check())
        <?php $user = $loggedinUser; ?>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 communitiesBlock">
          <a href="{{route('community_create')}}"><img src="{{ URL::asset('frontend/dashboard/assets/images/createcommunities.png') }}" class="img-responsive img-thumbnail imgBoder" title="Create Community"></a>
        </div>
        @if($selected_hoby == "my_feed")
          @if(count($user->communityList())>0)
            <?php $x=0; ?>
              @foreach($user->communityList() as $eachCommunity)
                @if($x<$displayLimitcommu)
                <?php $comSlug = str_slug($eachCommunity->name,'_'); ?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 communitiesBlock">
                    <a href="{{ route('community_page',$comSlug) }}"><img src="{{ $eachCommunity->profile_pic }}" class="img-responsive img-thumbnail imgBoder" title="{{ $eachCommunity->name }}"></a>
                </div>
                <?php $x++; ?>
                @endif
              @endforeach
              @if(count($user->communityList($eachHobby->hobby)) >= $displayLimitcommu )
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
                <a href="javascript:openYourCommunityModal({{$loggedinUser->id}})"><img src="{{ Cdn::asset('frontend/dashboard/assets/images/viewmore.png') }}" class="img-responsive img-thumbnail imgBoder"></a>
              </div>
              @endif
          @else
          <p style="text-align: center;">No Community Found...</p>
          @endif
        <div id="your_community_modal_{{$loggedinUser->id}}" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h3 id="myModalLabel3">My Community</h3>
                      <h4>{{ $eachHobby->hobby->name }}</h4>
                  </div>
                  <div class="modal-body">
                    <div class="your_community">
                      @foreach($user->communityList($eachHobby->hobby) as $eachCommunity)
                      <?php
                          $comSlug = str_slug($eachCommunity->name,'_');
                      ?>
                          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 communitiesBlock">
                              <a href="{{ route('community_page',$comSlug) }}"><img src="{{ $eachCommunity->profile_pic }}" class="img-responsive img-thumbnail imgBoder" title="{{ $eachCommunity->name }}"></a>
                          </div>
                      @endforeach
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="modal-footer">
                      <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
              </div>
          </div>
        </div>
        @else
            @if(count($user->communityList($hobby))>0)
              <?php $x=0; ?>
              @foreach($user->communityList($hobby) as $eachCommunity)
                @if($x<$displayLimitcommu)
                  <?php $comSlug = str_slug($eachCommunity->name,'_'); ?>
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 communitiesBlock">
                      <a href="{{ route('community_page',$comSlug) }}"><img src="{{ $eachCommunity->profile_pic }}" class="img-responsive img-thumbnail imgBoder" title="{{ $eachCommunity->name }}"></a>
                  </div>
                  <?php $x++; ?>
                @endif
              @endforeach
              @if(count($user->communityList($hobby)) >= $displayLimitcommu )
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
              	<a href="javascript:openYourCommunityModal({{$hobby->id}})"><img src="{{ Cdn::asset('frontend/dashboard/assets/images/viewmore.png') }}" class="img-responsive img-thumbnail imgBoder"></a>
              </div>
              @endif
              <div id="your_community_modal_{{$hobby->id}}" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                              <h3 id="myModalLabel3">My Community</h3>
                          </div>
                          <div class="modal-body">
                            <div class="your_community">
                              @foreach($user->communityList($hobby) as $eachCommunity)
                              <?php
                                  $comSlug = str_slug($eachCommunity->name,'_');
                              ?>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 communitiesBlock">
                                      <a href="{{ route('community_page',$comSlug) }}"><img src="{{ $eachCommunity->profile_pic }}" class="img-responsive img-thumbnail imgBoder" title="{{ $eachCommunity->name }}"></a> 
                                  </div>
                              @endforeach
                            </div>
                            <div class="clearfix"></div>
                          </div>
                          <div class="modal-footer">
                              <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                          </div>
                      </div>
                  </div>
              </div>
            @else
            <p style="text-align: center;">No Community Found...</p>
            @endif
        @endif
    @endif
  <div class="clearfix"></div>
</div>
<!-- Community End -->

@if(Auth::check())
<!-- Suggested Community Start -->
<div class="rightBar">
  <h5>Suggested communities</h5>
    <div class="col-md-12">
      <form action="{{ route('search_slug',array('community')) }}" class="search-form">
          <div class="form-group has-feedback">
          <input type="text" class="form-control" name="slug" id="search" placeholder="Search Community...">
            <span class="glyphicon glyphicon-search form-control-feedback"></span>
        </div>
      </form>
    </div>
        <?php $user = $loggedinUser; ?>
        @if($selected_hoby == "my_feed")
        @if(count($loggedinUser->suggestedCommunities())>0)
        <?php $x=0; ?>
            @foreach($loggedinUser->suggestedCommunities() as $eachCommunity)
              @if($x<$displayLimit)
              <?php $comSlug = str_slug($eachCommunity->name,'_'); ?>
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 communitiesBlock">
                <a href="{{ route('community_page',$comSlug) }}"><img src="{{ $eachCommunity->profile_pic }}" class="img-responsive img-thumbnail imgBoder" title="{{ $eachCommunity->name }}"></a>
              </div>
              <?php $x++; ?>
              @endif
            @endforeach
            @if(count($loggedinUser->suggestedCommunities()) >= $displayLimit )
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
              <a href="javascript:openSuggestedCommunityModal({{$loggedinUser->id}})"><img src="{{ Cdn::asset('frontend/dashboard/assets/images/viewmore.png') }}" class="img-responsive img-thumbnail imgBoder"></a>
            </div>  
            @endif
        @else
        <p style="text-align: center;">No Community Found...</p>
        @endif
        <div id="suggested_community_modal_{{$loggedinUser->id}}" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h3 id="myModalLabel3">Suggested Community</h3>
                      <h4>{{ $eachHobby->hobby->name }}</h4>
                  </div>
                  <div class="modal-body">
                    <div class="your_community">
                      @foreach($eachHobby->hobby->suggestedCommunities() as $eachCommunity)
                      <?php
                          $comSlug = str_slug($eachCommunity->name,'_');
                      ?>
                          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 communitiesBlock">
                              <a href="{{ route('community_page',$comSlug) }}"><img src="{{ $eachCommunity->profile_pic }}" class="img-responsive img-thumbnail imgBoder"></a>
                              <!--<p><a href="{{ route('community_page',$comSlug) }}">{{ $eachCommunity->name }}</a></p>-->
                          </div>
                      @endforeach
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="modal-footer">
                      <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
              </div>
          </div>
        </div>
        @else
            @if(count($hobby->suggestedCommunities())>0)
                @foreach($hobby->suggestedCommunities() as $eachCommunity)
                  <?php $comSlug = str_slug($eachCommunity->name,'_'); ?>
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 communitiesBlock">
                    <a href="{{ route('community_page',$comSlug) }}"><img src="{{ $eachCommunity->profile_pic }}" class="img-responsive img-thumbnail imgBoder" title="{{ $eachCommunity->name }}"></a>
                  </div>
                @endforeach
                @if(count($hobby->suggestedCommunities()) >= $displayLimit )
				        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mrgnBttm5">
                	<a href="javascript:openSuggestedCommunityModal({{$hobby->id}})"><img src="{{ Cdn::asset('frontend/dashboard/assets/images/viewmore.png') }}" class="img-responsive img-thumbnail imgBoder"></a>
                </div>
                @endif
                <div id="suggested_community_modal_{{$hobby->id}}" class="modal  fade post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h3 id="myModalLabel3">Suggested Community</h3>
                            </div>
                            <div class="modal-body">
                              <div class="your_community">
                                @foreach($hobby->suggestedCommunities() as $eachCommunity)
                                <?php
                                    $comSlug = str_slug($eachCommunity->name,'_');
                                ?>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 communitiesBlock">
                                        <a href="{{ route('community_page',$comSlug) }}"><img src="{{ $eachCommunity->profile_pic }}" class="img-responsive img-thumbnail imgBoder" title="{{ $eachCommunity->name }}"></a>
                                    </div>
                                @endforeach
                              </div>
                              <div class="clearfix"></div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @else
            <p style="text-align: center;">No Community Found...</p>
            @endif
        @endif
  <div class="clearfix"></div>
</div>
<!-- Suggested Community End -->
@endif
@section('extra_js2')
<script type="text/javascript">
  $(document).ready(function(event) {
    $('.friend_image').mouseover(function(){
      $(this).find('.friends_options').removeClass('hide');
    });
    // Accordion from https://codepen.io/shehab-eltawel/pen/dYwOgX //
    $('.accordion_container.in').prev('.panel-heading').addClass('active');
    $('.accordion, #bs-collapse')
    .on('show.bs.collapse', function(a) {
      $(a.target).prev('.panel-heading').addClass('active');
    })
    .on('hide.bs.collapse', function(a) {
      $(a.target).prev('.panel-heading').removeClass('active');
    });
  });
  function openSimilarInterest(hobby_id) {
    $('#similar_interest_'+hobby_id).modal('show')
  }
  function openFriendsModal(hobby_id) {
    $('#friends_modal_'+hobby_id).modal('show')
  }
  function openYourCommunityModal(hobby_id) {
    $('#your_community_modal_'+hobby_id).modal('show')
  }
  function openSuggestedCommunityModal(hobby_id) {
    $('#suggested_community_modal_'+hobby_id).modal('show')
  }
</script>
@stop