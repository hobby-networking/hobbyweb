@extends('backend.admin.template')

@section('extra_css')
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
@stop

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Manage Hobby Alias</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin_dashboard') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('manage_hobbies') }}">Hobby List</a>
            </li>
            <li class="active">
                List
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        
    </div>
</div>
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#Sr no</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                         <?php $i=1;?>
                        @foreach($hobbyAliases as $eachHobbyAlias)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{ $eachHobbyAlias->alias_name }} 
                            @if($eachHobbyAlias->is_active == "yes")
                                <small class="small btn-info">Active</small>
                            @else
                                <small class="small btn-danger">Deactive</small>
                            @endif
                            </td>
                            <td>
                                <a class="btn btn-info btn-rounded" href="{{ route('edit_hobbies_alias',$eachHobbyAlias->id) }}">Edit</a>&nbsp;<a class="btn btn-danger btn-rounded" href="{{ route('delete_hobbies_alias',$eachHobbyAlias->id) }}">Delete</a> 
                            </td>
                        </tr>
                        <?php $i++;?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        @if(!$edit)
                            Add Hobby Alias
                        @else
                            Edit Hobby Alias
                        @endif
                    </h5>
                </div>
                <div class="ibox-content">
                    @if(!$edit)
                        {!! Form::open(['method'=>'post','action'=>'AdminController@postAddHobbyAlias', 'id'=>'form-add_hobbyAlias','class'=>'form-horizontal','onsubmit'=>"", 'files'=> false]) !!}
                    @else
                        {!! Form::model($hobbyAlias, ['method'=>'post','action'=>'AdminController@postEditHobbyAlias', 'id'=>'form-edit_hobbyAlias','class'=>'form-horizontal','onsubmit'=>"", 'files'=> false]) !!}
                        {!! Form::hidden('hobby_alias_id',$hobbyAlias->id) !!}
                    @endif
                    {!! Form::hidden('hobby_id',$hobby->id) !!}
                    <div class="form-group">{!! Form::label('alias_name','Alias Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::text('alias_name',null,['id'=>'validation-name','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('is_active','Active',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10"><span class="label label-warning">No</span>
                            
                        @if($edit && $hobbyAlias->is_active == "yes")
                            {!! Form::checkbox('is_active',false,true,['id'=>'is_active','class'=>'js-switch'])!!}
                        @else
                            {!! Form::checkbox('is_active',true,false,['id'=>'is_active','class'=>'js-switch'])!!}
                        @endif
                            <span class="label label-primary">Yes</span>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-offset-4">
                            <?php 
                            if($edit){
                                $btnTxt = "Update";
                            }else{
                                $btnTxt = "Add";
                            }
                            ?>
                            {!! Form::button($btnTxt. ' Hobby Alias', array(
                                    'type' => 'submit',
                                    'class'=> 'btn btn-primary',
                                    'onclick'=>'',
                                    'id'=>'add-planButton'
                            )) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footerscript')
<!-- Data picker -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/switchery/switchery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, { color: '#7cc242' });
    });
</script>

@stop