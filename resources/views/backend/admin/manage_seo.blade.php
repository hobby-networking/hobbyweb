@extends('backend.admin.template')

@section('extra_css')
<link href="{{ URL::asset('backend/admin/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
@stop

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Manage SEO</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin_dashboard') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('manage_seo') }}">SEO</a>
            </li>
            <li class="active">
                List
            </li>
        </ol>
    </div>
    
</div>
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form class="form-horizontal " id="user_form" method="post" action="{{ action('AdminController@postAddSeo') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{ Form::hidden('userid', Auth::user()->id, array('id' => 'user_id')) }}
                    <div class="form-group">{!! Form::label('name','Select Page',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">{{ Form::select('page_name', [' '=>'Select Page','home'=>'Home','community'=>'Community','friends'=>'Friends','hobby'=>'Hobby','about_us'=>'About Us','privacy_policy'=>'Privacy Policy','faq'=>'Faq','terms_of_use'=>'Terms Of Use','contact_us'=>'Contact Us','explainer-videos'=>'Explainer Videos','hn-videos'=>'HN Videos'],null,['class'=>'form-control','id'=>'page_name']) }}</div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('keywords','Meta Keywords',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">{!! Form::text('meta_keywords',null,['class'=>'form-control','id'=>'meta_keywords'])!!}</div>
                    </div>

                     <div class="form-group">{!! Form::label('title','Meta Title',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">{!! Form::text('meta_title',null,['class'=>'form-control','id'=>'meta_title'])!!}</div>
                    </div>
                     
                    <div class="form-group">{!! Form::label('meta_desc','Meta Description',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::textarea('desc',null,['id'=>'validation-desc','class'=>'form-control','id'=>'desc'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    {!! Form::button('Save', array(
                            'type' => 'submit',
                            'class'=> 'btn btn-primary',
                            'onclick'=>'',
                            'id'=>'add-planButton'
                    )) !!}
                
                    </form>                 

                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footerscript')
<!-- Data picker -->
<script src="{{ URL::asset('backend/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ URL::asset('backend/admin/assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        $('#page_name').change(function(){
            var page_name = $(this).val();
            $_token = "{{ csrf_token() }}";
        
        $.ajax({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
        url: "{{ route('seo_details') }}",
        type: 'GET',
        cache: false,
        data: { 'page_name': page_name, '_token': $_token}, //see the $_token
        //datatype: 'html',
        beforeSend: function() {
            //something before send
        },
        success: function(data) {
            console.log(data);
            
                $('#meta_keywords').val(data.meta_keywords);
                $('#meta_title').val(data.meta_title);
                $('#desc').val(data.meta_desc);
                //$('#preview').html(data);
              //user_jobs div defined on page
              
           
        },
        error: function(xhr,textStatus,thrownError) {
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });
        });
    });
</script>

@stop