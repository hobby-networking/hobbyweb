@extends('backend.admin.template')

@section('extra_css')
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
@stop

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Manage Hobby For Categories {{ $hobbyCat->name }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin_dashboard') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('manage_hobbies') }}">Hobby Categories</a>
            </li>
            <li class="active">
                List
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        
    </div>
</div>
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#Sr no</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                         <?php $i=1;?>
                        @foreach($hobbies as $eachHobby)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{ $eachHobby->name }} 
                            @if($eachHobby->is_active == "yes")
                                <small class="small btn-info">Active</small>
                            @else
                                <small class="small btn-danger">Deactive</small>
                            @endif
                            </td>
                            <td>{{ $eachHobby->desc }}</td>
                            <td>
                                <a class="btn btn-info btn-rounded" href="{{ route('edit_list_hobbies',$eachHobby->id) }}">Edit</a>&nbsp;<a class="btn btn-danger btn-rounded" href="{{ route('delete_list_hobbies',$eachHobby->id) }}">Delete</a>&nbsp;<a class="btn btn-default btn-rounded" href="{{ route('list_hobbies_alias',$eachHobby->id) }}">Manage Alias</a>&nbsp;<a class="btn btn-default btn-rounded" href="{{route('manage_hobby_by_name',$eachHobby->slug())}}">Posts</a> 
                            </td>
                        </tr>
                        <?php $i++;?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        @if(!$edit)
                            Add Hobby
                        @else
                            Edit Hobby
                        @endif
                    </h5>
                </div>
                <div class="ibox-content">
                    @if(!$edit)
                        {!! Form::open(['method'=>'post','action'=>'AdminController@postAddHobby', 'id'=>'form-add_hobbyCat_message','class'=>'form-horizontal','onsubmit'=>"", 'files'=> true]) !!}
                    @else
                        {!! Form::model($hobby, ['method'=>'post','action'=>'AdminController@postEditHobby', 'id'=>'form-add_hobby_message','class'=>'form-horizontal','onsubmit'=>"", 'files'=> true]) !!}
                        {!! Form::hidden('hobby_id',$hobby->id) !!}
                    @endif
                    {!! Form::hidden('cat_id',$hobbyCat->id) !!}
                    <div class="form-group">{!! Form::label('name','Hobby Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::text('name',null,['id'=>'validation-name','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">{!! Form::label('profile_pic','Hobby Picture',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-4">
                            {{Form::file('profile_pic',['class'=>'hobby_image'])}}
                        </div>
                        @if($edit)
                        <div class="col-sm-2">
                            <img id="previewing" src="{{ $hobby->profile_pic }}" width="200px" />
                        </div>
                        @else
                         <div class="col-sm-2">
                            <img id="previewing" src="" width="200px" />
                        </div>
                        @endif
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('desc','Description',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::textarea('desc',null,['id'=>'validation-desc','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('is_active','Active',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10"><span class="label label-warning">No</span>
                            
                        @if($edit && $hobby->is_active == "yes")
                            {!! Form::checkbox('is_active',false,true,['id'=>'is_active','class'=>'js-switch'])!!}
                        @else
                            {!! Form::checkbox('is_active',true,false,['id'=>'is_active','class'=>'js-switch'])!!}
                        @endif
                            <span class="label label-primary">Yes</span>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-offset-4">
                            {!! Form::button('Cancel', array(
                                    'type' => 'button',
                                    'class'=> 'btn btn-white',
                                    'onclick'=>'document.location.href="'.route('manage_hobbies').'"'
                            )) !!}
                            <?php 
                            if($edit){
                                $btnTxt = "Update";
                            }else{
                                $btnTxt = "Add";
                            }
                            ?>
                            {!! Form::button($btnTxt. ' Hobby Category', array(
                                    'type' => 'submit',
                                    'class'=> 'btn btn-primary',
                                    'onclick'=>'',
                                    'id'=>'add-planButton'
                            )) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footerscript')
<!-- Data picker -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/switchery/switchery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, { color: '#7cc242' });
        $(".hobby_image").change(function(){
            readURL(this,'previewing');
        });
    });
    function readURL(input,ondiv) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#'+ondiv).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

@stop