@extends('backend.admin.template')

@section('extra_css')
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<style type="text/css">
    .modal-body {
    position: relative;
    overflow-y: auto;
    /*max-height: 500px;*/
    padding: 15px;
    }
    .modal-dialog{
        width: 50%;
    }

</style>
@stop

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Manage Users</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin_dashboard') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('manage_users') }}">Users</a>
            </li>
            <li class="active">
                List
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <ol class="breadcrumb">
            <li>
               
            </li>
            
        </ol>
       
    </div>
</div>
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                 <div class="sendemail-groupuser">
                {!! Form::open(['method'=>'post', 'url'=>route("active_users_emailsend"), 'id'=>'form-add_hobbyCat_message','class'=>'form-horizontal','onsubmit'=>"", 'files'=> false]) !!}
                <div class="form-group">{!! Form::label('emailid','Email Id(comma separator i.e. test@gmail.com,demo@gmail.com)',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-10">{!! Form::text('memailid',null,['id'=>'validation-email','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>
                <div class="form-group">{!! Form::label('subject','Subject',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-10">{!! Form::text('emailsubject',null,['id'=>'validation-subject','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                        <textarea rows="5" cols="5" name="desc" class="form-control" id="validation-desc"></textarea>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    
                </div>
                <div class="activeuser">
                  <h2>Active Users </h2>
                   <div class="alluser"><input type="checkbox" id="select_all"/><lable>All User</lable></div>
                    @foreach($users as $userdata)
                     <input type="checkbox" name="sendmailuser[]" class="multicheckbox" value="{{ $userdata->email }}"><lable>{{ ucwords(strtolower($userdata->name)) }}</lable>
                    @endforeach
                  </div>
                   <div class="hr-line-dashed"></div>
                  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-offset-4">
                    {!! Form::button("Send Mail". ' User', array(
                                    'type' => 'submit',
                                    'class'=> 'btn btn-primary',
                                    'onclick'=>'',
                                    'id'=>'add-planButton'
                            )) !!}
                       {!! Form::button('Preview Email', array(
                                    'type' => 'button',
                                    'class'=> 'btn btn-info',
                                    'id'=>'preview_email',
                            )) !!}     
                      </div>
                    </div>
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="your-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Email Preview</h4>
            </div>
            <div class="modal-body">
            <div id="preview"></div>
            </div>
            <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
@stop

@section('footerscript')
<!-- Data picker -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        
    });

    ///multi checked checkbox js start
  var select_all = document.getElementById("select_all"); //select all checkbox
var checkboxes = document.getElementsByClassName("multicheckbox"); //checkbox items

//select all checkboxes
select_all.addEventListener("change", function(e){
    for (i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = select_all.checked;
    }
});


for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].addEventListener('change', function(e){ //".checkbox" change
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){
            select_all.checked = false;
        }
        //check "select all" if all checkbox items are checked
        if(document.querySelectorAll('.checkbox:checked').length == checkboxes.length){
            select_all.checked = true;
        }
    });
}

$('#preview_email').click(function(){
        var user_id = $('#user_id').val();
        var subject = $('#validation-subject').val();
        var validation_desc = tinymce.get('validation-desc').getContent();
        //var datastring = $("#form-add_hobbyCat_message").serialize();
        //alert(validation_desc);
        $_token = "{{ csrf_token() }}";
        
        $.ajax({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
        url: "{{ route('preview_email') }}",
        type: 'GET',
        cache: false,
        data: { 'userid': user_id, '_token': $_token,'subject': subject,'validation_desc': validation_desc }, //see the $_token
        datatype: 'html',
        beforeSend: function() {
            //something before send
        },
        success: function(data) {
            //console.log('success');
            //console.log(JSON.stringify(data));
            
                $('#your-modal').modal('toggle');
                $('#preview').html(data);
              //user_jobs div defined on page
              
           
        },
        error: function(xhr,textStatus,thrownError) {
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });
    });
</script>

@stop