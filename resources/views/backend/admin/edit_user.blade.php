@extends('backend.admin.template')

@section('extra_css')
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
<style type="text/css">
    .modal-body {
    position: relative;
    overflow-y: auto;
    /*max-height: 500px;*/
    padding: 15px;
    }
    .modal-dialog{
        width: 50%;
    }
</style>
@stop

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            @if(!$sendmail)
                @if(!$edit)
                    Add User
                @else
                    Edit User
                @endif
            @else
                Send Mail
            @endif
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin_dashboard') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('manage_users') }}">Manage User</a>
            </li>
            <li class="active">
                @if(!$sendmail)
                    @if(!$edit)
                        Add User
                    @else
                        Edit User
                    @endif
                @else
                    Send Mail
                @endif
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        @if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
    </div>
</div>
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>

                        @if(!$sendmail)
                            @if(!$edit)
                                Add User
                            @else
                                Edit User
                            @endif
                        @else
                            Send Mail
                        @endif

                    </h5>
                </div>
                <div class="ibox-content">
                    @if(!$sendmail)
                        @if(!$edit)
                            {!! Form::open(['method'=>'post', 'url'=>route("add_user_post"), 'id'=>'form-add_hobbyCat_message','class'=>'form-horizontal','onsubmit'=>"", 'files'=> true]) !!}
                        @else
                            {!! Form::model($user, ['method'=>'post','url'=>route("edit_user_post",$user->id), 'id'=>'form-edit_user','class'=>'form-horizontal','onsubmit'=>"", 'files'=> true]) !!}
                            
                        @endif
                    @else
                        {!! Form::model($user,['method'=>'post', 'url'=>route("send_mail_users_post",$user->id), 'id'=>'form-add_hobbyCat_message','class'=>'form-horizontal','onsubmit'=>"", 'files'=> true]) !!}
                    @endif

                    @if(!$sendmail)
                    <div class="form-group">{!! Form::label('name','Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::text('name',null,['id'=>'validation-name','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('email','Email',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::text('email',null,['id'=>'validation-email','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('phone','Phone No',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::text('phone',null,['id'=>'validation-phone','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">{!! Form::label('profile_pic','Profile Picture',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-4">
                            {{Form::file('profile_pic',['class'=>'user_image'])}}
                        </div>
                        @if($edit)
                        <div class="col-sm-2">
                            <img id="previewing" src="{{ $user->profile_pic }}" width="200px" />
                        </div>
                        @else
                         <div class="col-sm-2">
                            <img id="previewing" src="" width="200px" />
                        </div>
                        @endif
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('is_active','Active',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10"><span class="label label-warning">No</span>
                            
                        @if($edit && $user->is_active == "yes")
                            {!! Form::checkbox('is_active',false,true,['id'=>'is_active','class'=>'js-switch'])!!}
                        @else
                            {!! Form::checkbox('is_active',true,false,['id'=>'is_active','class'=>'js-switch'])!!}
                        @endif
                            <span class="label label-primary">Yes</span>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    @else
                    <input type="hidden" id="user_id" value="{{$user->id}}">
                    <div class="form-group">{!! Form::label('email','Email',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::text('email',null,['id'=>'validation-email','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">{!! Form::label('subject','Subject',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-10">{!! Form::text('emailsubject',null,['id'=>'validation-subject','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                        <textarea rows="5" cols="5" name="desc" class="form-control" id="validation-desc"></textarea>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    @endif
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-offset-4">
                            {!! Form::button('Cancel', array(
                                    'type' => 'button',
                                    'class'=> 'btn btn-white',
                                    'onclick'=>'document.location.href="'.route('manage_users').'"'
                            )) !!}
                            <?php 
                            if(!$sendmail){
                                if($edit){
                                    $btnTxt = "Update";
                                }else{
                                    $btnTxt = "Add";
                                }
                            }else{
                                $btnTxt = "Send Mail";
                                ?>
                                 {!! Form::button('Preview Email', array(
                                    'type' => 'button',
                                    'class'=> 'btn btn-info',
                                    'id'=>'preview_email',
                            )) !!}
                           <?php }
                            ?>
                            {!! Form::button($btnTxt. ' User', array(
                                    'type' => 'submit',
                                    'class'=> 'btn btn-primary',
                                    'onclick'=>'',
                                    'id'=>'add-planButton'
                            )) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="your-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Email Preview</h4>
            </div>
            <div class="modal-body">
            <div id="preview"></div>
            </div>
            <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
@stop

@section('footerscript')
<!-- Data picker -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/switchery/switchery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, { color: '#7cc242' });
        $(".user_image").change(function(){
            readURL(this,'previewing');
        });
    });
    function readURL(input,ondiv) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#'+ondiv).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#preview_email').click(function(){
        var user_id = $('#user_id').val();
        var subject = $('#validation-subject').val();
        var validation_desc = tinymce.get('validation-desc').getContent();
        //var datastring = $("#form-add_hobbyCat_message").serialize();
        //alert(validation_desc);
        $_token = "{{ csrf_token() }}";
        
        $.ajax({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
        url: "{{ route('preview_email') }}",
        type: 'GET',
        cache: false,
        data: { 'userid': user_id, '_token': $_token,'subject': subject,'validation_desc': validation_desc }, //see the $_token
        datatype: 'html',
        beforeSend: function() {
            //something before send
        },
        success: function(data) {
            //console.log('success');
            //console.log(JSON.stringify(data));
            
                $('#your-modal').modal('toggle');
                $('#preview').html(data);
              //user_jobs div defined on page
              
           
        },
        error: function(xhr,textStatus,thrownError) {
            alert(xhr + "\n" + textStatus + "\n" + thrownError);
        }
    });
    });    

</script>

@stop