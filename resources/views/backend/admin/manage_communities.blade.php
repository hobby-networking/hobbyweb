@extends('backend.admin.template')

@section('extra_css')
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
<link href="{{ Cdn::asset('backend/admin/assets/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Manage Comunities</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin_dashboard') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('manage_communities') }}">Comunities</a>
            </li>
            <li class="active">
                List
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        
    </div>
</div>
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th>#Sr no</th>
                            <th>Name</th>
                            <th>Picture</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                         <?php $i=1;?>
                        @foreach($communities as $eachCommunity)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{ $eachCommunity->name }}</td>
                            <td><img src="{{ $eachCommunity->profile_pic }}" width="100px" height="100px"></td>
                            <td>
                                @if($eachCommunity->status == "yes")
                                    <small class="small btn-info">Active</small>
                                @else
                                    <small class="small btn-danger">Deactive</small>
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-info btn-rounded" href="{{ route('edit_community',$eachCommunity->id) }}">Edit</a>&nbsp;<a class="btn btn-danger btn-rounded" href="{{ route('delete_community',$eachCommunity->id) }}">Delete</a>
                            </td>
                        </tr>
                        <?php $i++;?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if($edit)
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        @if($edit)
                            Edit Community
                        @endif
                    </h5>
                </div>
                <div class="ibox-content">
                    @if($edit)
                        {!! Form::model($community, ['method'=>'post','action'=>'AdminController@postEditCommunity', 'id'=>'form-add_hobbyCat_message','class'=>'form-horizontal','onsubmit'=>"", 'files'=> true]) !!}
                        {!! Form::hidden('community_id',$community->id) !!}
                    @endif
                    <div class="form-group">{!! Form::label('name','Category Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::text('name',null,['id'=>'validation-name','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('profile_pic','Profile Picture',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-4">
                            {{Form::file('profile_pic',['class'=>'community_image'])}}
                        </div>
                        <div class="col-sm-2">
                            <img id="previewing" src="{{ $community->profile_pic }}" width="200px" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('cover_pic','Cover Picture',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-4">
                            {{Form::file('cover_pic',['class'=>'community_cover_image'])}}
                        </div>
                        <div class="col-sm-2">
                            <img id="previewing1" src="{{ $community->cover_pic }}" width="200px" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('status','Active',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10"><span class="label label-warning">No</span>
                            
                        @if($edit && $community->status == "yes")
                            {!! Form::checkbox('status',false,true,['id'=>'status','class'=>'js-switch'])!!}
                        @else
                            {!! Form::checkbox('status',true,false,['id'=>'status','class'=>'js-switch'])!!}
                        @endif
                            <span class="label label-primary">Yes</span>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-offset-4">
                            {!! Form::button('Cancel', array(
                                    'type' => 'button',
                                    'class'=> 'btn btn-white',
                                    'onclick'=>'document.location.href="'.route('manage_hobbies').'"'
                            )) !!}
                            {!! Form::button(' Update Community', array(
                                    'type' => 'submit',
                                    'class'=> 'btn btn-primary',
                                    'onclick'=>'',
                                    'id'=>'add-planButton'
                            )) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@stop

@section('footerscript')
<!-- Data picker -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/switchery/switchery.js') }}"></script>
<script src="{{ Cdn::asset('backend/admin/assets/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ Cdn::asset('backend/admin/assets/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, { color: '#7cc242' });
        $(".community_image").change(function(){
            readURL(this,'previewing');
        });
        $(".community_cover_image").change(function(){
            readURL(this,'previewing1');
        });
        $(".datatable").DataTable();
    });
    function readURL(input,ondiv) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#'+ondiv).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

@stop