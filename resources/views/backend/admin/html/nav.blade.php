<?php $user = Auth::user(); ?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                    <img alt="image" class="img-circle" src="{{ $user->profile_pic }}" width="48px" height="48px" />
                     </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ $user->name }}</strong> <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="#">Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    <img alt="image" class="img-circle" src="#" width="28px" height="28px" />
                </div>
            </li>
            
            <li>
                <a href="{{ route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li>
                <a href="{{ route('manage_users') }}"><i class="fa fa-users"></i> <span class="nav-label">Manage Users</span></a>
            </li>
             <li>
                <a href="{{ route('manage_hobbies') }}"><i class="fa fa-tasks"></i> <span class="nav-label">Manage Hobbies</span></a>
            </li>
            <li>
                <a href="{{ route('manage_communities') }}"><i class="fa fa-tasks"></i> <span class="nav-label">Manage Communities</span></a>
            </li>
            <li>
                <a href="{{ route('manage_editorspic_hobbycat_list') }}"><i class="fa fa-tasks"></i> <span class="nav-label">Manage Editors Pic</span></a>
            </li>
            <li class="active">
                <a href="#"><i class="fa fa-gears"></i> <span class="nav-label">Settings</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="#">Settings</a></li>
                    <li><a href="{{ route('active_users') }}">Send Mail</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('manage_seo') }}"><i class="fa fa-tasks"></i> <span class="nav-label">Manage SEO</span></a>
            </li>
        </ul>

    </div>
</nav>