<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Hobby | Admin - Dashboard</title>

<meta name="author" content="Global Software Solution" />
<link rel="apple-touch-icon" sizes="57x57" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}">
<link rel="icon" type="image/png" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" sizes="32x32">
<link rel="icon" type="image/png" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" sizes="192x192">
<link rel="icon" type="image/png" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" sizes="96x96">
<link rel="icon" type="image/png" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" sizes="16x16">

<link href="{{ Cdn::asset('backend/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ Cdn::asset('backend/admin/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Toastr style -->
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

<!-- Gritter -->
<link href="{{ Cdn::asset('backend/admin/assets/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">

<link href="{{ Cdn::asset('backend/admin/assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ Cdn::asset('backend/admin/assets/css/style.css') }}" rel="stylesheet">

@yield('extra_css')