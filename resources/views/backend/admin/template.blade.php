<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> 
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
	@include('backend.admin.html.head')
</head> 
<body>
	<div id="wrapper">
		@include('backend.admin.html.nav')
		<div id="page-wrapper" class="gray-bg">
			@include('backend.admin.html.header')
			<!-- Breadcum -->
				@yield('breadcrumb')
			<!-- End Breadcum -->
			<!-- Main content -->
				@yield('content')
			<!-- End main content -->

			<!-- Footer content -->
				<div class="footer">
		            <div class="pull-right">
		                Developed by Hobbynetworking.
		            </div>
		            <div>
		                Hobbynetworking &copy; 2014-2015
		            </div>
		        </div>
			<!-- End Footer content -->
		</div>
	</div>
	@include('backend.admin.html.footer')
	@yield('footerscript')
	<script type="text/javascript">
		function closeHint() {
			$('.hintMessage').slideUp(100);
		}
	</script>
</body>
</html>

<!-- 
*************************************************
Author: Sudipta Dhara (sudiptai26.889@gmail.com)
**********Some Important Notes*******************

1)redirect to route with error message example:
	return Redirect::route('route_name')->withErrors(['error message']);

2)redirect to route with success message example:
	$success_msg[] = "success message";
	Session::flash('success_msg', $success_msg);
	return Redirect::route('profile');

**********End of Some Important Notes************
-->