@extends('backend.admin.template')

@section('extra_css')
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
@stop

@section('content')
<div class="row  border-bottom white-bg dashboard-header">
{{-- Google Analytics Code Start --}}
	<div class="twelve-columns google_analytics" id="google_analytics">
		<iframe src="//hobbynetworking.com/monitor.html" width="100%" height="1500px"></iframe>	
	</div>
{{-- Google Analytics Code End --}}
</div>
@stop

@section('footerscript')
<!-- Data picker -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        
    });
</script>

@stop