@extends('backend.admin.template')

@section('extra_css')
<link href="{{ URL::asset('backend/admin/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<link href="{{ URL::asset('backend/admin/assets/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
@stop

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Show Hobby By </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin_dashboard') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('manage_users') }}">Users</a>
            </li>
            <li class="active">
                List
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        
    </div>
</div>
@stop

@section('content')
<div class="container-fluid">
<form  method="post" name="" action="{{route('userhobby_update')}}">
    <div class="row2">
        <div class="col-lg-10 mdlBlock click_reverse">
            <!-- <div class="col-lg-12">
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input type="text" id ="hobby_search" name="hobby_search" class="form-control input-lg" placeholder="Search Your Hobby Here..." />
                        <span class="input-group-btn">
                            <button class="btn btn-info btn-lg" type="button" id="search_hobby">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div> -->
            
            <div class="col-lg-12">
                <div class="row">
                <input type="hidden" value="{{$user_id}}" name="user_id">
                @foreach($hobbies as $key=>$eachHobby)
                 
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div>
                            <input type="checkbox" name="hobby_ids[]" value="{{$eachHobby->id}}" @if(in_array($eachHobby->id,$user_hobbies)) checked @endif>
                            <img src="{{ $eachHobby->profile_pic }}" class="" width="178px" height="178px">
                            <h5>{{ $eachHobby->name }}</h5>
                            <p> {{ $eachHobby->category->name }}</p> 
                        </div>
                    </div>
                @endforeach    
                </div>
            </div>
            
            
            <div class="clearfix"></div>
            <div class="col-sm-offset-2 col-sm-offset-4">
            <button type="button" class="btn btn-white">Cancel</button>
            <button type="submit" class="btn btn-primary" onclick="" id="add-planButton">Update User</button>
            </div>
        </div>

    </div>

    </form>
</div>
@stop

@section('footerscript')
<!-- Data picker -->
<script src="{{ URL::asset('backend/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ URL::asset('backend/admin/assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ URL::asset('backend/admin/assets/js/plugins/switchery/switchery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, { color: '#7cc242' });
        
    });
   
</script>

@stop