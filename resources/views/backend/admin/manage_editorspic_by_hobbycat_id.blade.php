@extends('backend.admin.template')

@section('extra_css')
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
@stop

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Manage Editor's Pic For {{ $hobbyCat->name }} hobby</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin_dashboard') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('manage_editorspic_hobbycat_list') }}">Editors Pic</a>
            </li>
            <li class="active">
                List
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        
    </div>
</div>
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#Sr no</th>
                            <th>Title</th>
                            <th>Picture</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                         <?php $i=1;?>
                        @foreach($hobbyCat->editorsPic as $eachEditorsPic)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{ $eachEditorsPic->title }}</td>
                            <td><img src="{{ $eachEditorsPic->image }}" width="100px" height="100px"></td>
                            <td>
                                @if($eachEditorsPic->is_active == "yes")
                                    <small class="small btn-info">Active</small>
                                @else
                                    <small class="small btn-danger">Deactive</small>
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-info btn-rounded" href="{{ route('manage_editorspic_by_hobbycat_edit_page',[$hobbyCat->id,$eachEditorsPic->id]) }}">Edit</a>&nbsp;{{-- <a class="btn btn-danger btn-rounded" href="{{ route('delete_community',$eachEditorsPic->id) }}">Delete</a> --}}
                            </td>
                        </tr>
                        <?php $i++;?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        @if(!$edit)
                            Add Hobby Editor's Pic
                        @else
                            Edit Hobby Editor's Pic
                        @endif
                    </h5>
                </div>
                <div class="ibox-content">
                    @if(!$edit)
                        {!! Form::open(['method'=>'post','action'=>'AdminController@postEditorsPicByHobbyCatIdAdd', 'id'=>'form-add_editorsPic','class'=>'form-horizontal','onsubmit'=>"", 'files'=> true]) !!}
                    @else
                        {!! Form::model($editorsPic, ['method'=>'post','action'=>'AdminController@postEditorsPicByHobbyCatIdEdit', 'id'=>'form-edit_editorsPic','class'=>'form-horizontal','onsubmit'=>"", 'files'=> false]) !!}
                        {!! Form::hidden('editors_pic_id',$editorsPic->id) !!}
                    @endif
                    {!! Form::hidden('hobby_cat_id',$hobbyCat->id) !!}
                    <div class="form-group">{!! Form::label('title','Title',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::text('title',null,['id'=>'validation-name','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('link','Link',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">{!! Form::text('link',null,['id'=>'validation-link','class'=>'form-control'])!!}</div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('image','Image',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {{Form::file('image',['class'=>'editors_pic_image'])}}
                            @if($edit)
                            <img src="{{ $editorsPic->image }}" width="200px">
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">{!! Form::label('is_active','Active',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10"><span class="label label-warning">No</span>
                            
                        @if($edit && $editorsPic->is_active == "yes")
                            {!! Form::checkbox('is_active',false,true,['id'=>'is_active','class'=>'js-switch'])!!}
                        @else
                            {!! Form::checkbox('is_active',true,false,['id'=>'is_active','class'=>'js-switch'])!!}
                        @endif
                            <span class="label label-primary">Yes</span>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">{!! Form::label('is_in_my_feed','Show in My Feed',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10"><span class="label label-warning">No</span>
                        @if($edit && $editorsPic->is_in_my_feed == "yes")
                            {!! Form::checkbox('is_in_my_feed',false,true,['id'=>'is_in_my_feed','class'=>'js-switch1'])!!}
                        @else
                            {!! Form::checkbox('is_in_my_feed',true,false,['id'=>'is_in_my_feed','class'=>'js-switch1'])!!}
                        @endif
                            <span class="label label-primary">Yes</span>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-offset-4">
                            <?php 
                            if($edit){
                                $btnTxt = "Update";
                            }else{
                                $btnTxt = "Add";
                            }
                            ?>
                            {!! Form::button($btnTxt. ' Hobby EditorsPic', array(
                                    'type' => 'submit',
                                    'class'=> 'btn btn-primary',
                                    'onclick'=>'',
                                    'id'=>'add-planButton'
                            )) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footerscript')
<!-- Data picker -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/switchery/switchery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, { color: '#7cc242' });
        var elem1 = document.querySelector('.js-switch1');
        var switchery1 = new Switchery(elem1, { color: '#7cc242' });
        $(".community_image").change(function(){
            readURL(this,'previewing');
        });
        $(".community_cover_image").change(function(){
            readURL(this,'previewing1');
        });
    });
    function readURL(input,ondiv) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#'+ondiv).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

@stop