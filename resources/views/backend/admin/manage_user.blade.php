@extends('backend.admin.template')

@section('extra_css')
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
<link href="{{ Cdn::asset('backend/admin/assets/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
<link href="{{ Cdn::asset('backend/admin/assets/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Manage Users</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin_dashboard') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('manage_users') }}">Users</a>
            </li>
            <li class="active">
                List
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <h2><a style="color: #fff;" class="btn btn-success btn-rounded" href="{{ route('add_user') }}">Add New User</a> </h2>
        <ol class="breadcrumb">
            <li>
               
            </li>
            
        </ol>
       
    </div>
</div>
@stop

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th>#Sr no</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Picture</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                         <?php $i=1;?>
                        @foreach($users as $eachUser)
                        @if($eachUser->type == "user")
                        <tr>
                            <td>{{$i}}</td>
                            <td><a href="{{ route('show_hobbies',$eachUser->id) }}" target="_blank">{{ $eachUser->name }}</a></td>
                            <td>{{ $eachUser->email }}</td>
                            <td>{{ $eachUser->phone }}</td>
                            <td><img src="{{ $eachUser->profile_pic }}" width="100px" height="100px"></td>

                            <td>
                                <a class="btn btn-info btn-rounded" href="{{ route('edit_users',$eachUser->id) }}">Edit</a>&nbsp;<a class="btn btn-danger btn-rounded" href="{{ route('delete_users',$eachUser->id) }}">Delete</a> &nbsp;<a class="btn btn-warning btn-rounded" href="{{ route('send_mail_users',$eachUser->id) }}">Send Mail</a>
                            </td>
                        </tr>
                        <?php $i++;?>
                        @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footerscript')
<!-- Data picker -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- Jquery Validate -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
<!-- Switchery -->
<script src="{{ Cdn::asset('backend/admin/assets/js/plugins/switchery/switchery.js') }}"></script>
<script src="{{ Cdn::asset('backend/admin/assets/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ Cdn::asset('backend/admin/assets/js/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(e){
        $(".datatable").DataTable();
    });
</script>

@stop