<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Hobby Networking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
<link rel="shortcut icon" href="{{ Cdn::asset('frontend/welcome/assets/images/favicon.ico') }}" type="image/x-icon"/>
<!--font Css-->
<link href="{{ Cdn::asset('frontend/welcome/assets/css/font-awesome.css') }}" rel="stylesheet" type="text/css">

<!--Image Overlay Css -->
<link rel="stylesheet" href="{{ Cdn::asset('frontend/welcome/assets/css/shortcodes.css') }}" />

<!--animation effect Css -->
<link href="{{ Cdn::asset('frontend/welcome/assets/css/uikit.docs.min.css') }}" rel="stylesheet">

<!--bootstrap Css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{ Cdn::asset('frontend/welcome/assets/css/bootstrap.css') }}">

<!--mother Css -->
<link href="{{ Cdn::asset('frontend/welcome/assets/css/hobby-networking.css') }}" rel="stylesheet" type="text/css">

<!--responsive css -->
<link href="{{ Cdn::asset('frontend/welcome/assets/css/responsive.css') }}" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="{{ Cdn::asset('frontend/welcome/assets/css/reset.css') }}"> <!-- CSS reset -->
<link rel="stylesheet" href="{{ Cdn::asset('frontend/welcome/assets/css/style.css') }}"> <!-- Gem style -->
<script src="{{ Cdn::asset('frontend/welcome/assets/js/modernizr.js') }}"></script> <!-- Modernizr -->
{!! Analytics::render() !!}
</head>

<body>
<div id="page">
<!--Banner Section-->
<section class="banner">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="{{ Cdn::asset('frontend/welcome/assets/images/hobby-networking-banner1.jpg') }}" alt="Chania" >
      </div>
      <div class="item">
        <img src="{{ Cdn::asset('frontend/welcome/assets/images/hobby-networking-banner1.jpg') }}" alt="Chania" >
      </div>    
      <div class="item">
        <img src="{{ Cdn::asset('frontend/welcome/assets/images/hobby-networking-banner1.jpg') }}" alt="Chania" >
      </div>
      <div class="item">
        <img src="{{ Cdn::asset('frontend/welcome/assets/images/hobby-networking-banner1.jpg') }}" alt="Chania" >
      </div>
     </div>
    <div class="banner_contt_box_sec">
        <div class="container">
            <div class="logo_sec">
                <a href="{{ route('welcome_page') }}"><img src="{{ Cdn::asset('frontend/welcome/assets/images/hobby-networking-logo.png') }}" alt="Hobby Networking Logo" title="Hobby Networking Logo"></a></div>
            <div class="banner_contt clearfix">
                <div class="hobby_global">
                <h1>A Single Platform for Multiple Hobbies</h1>
                    <ul>
                        <li>Live healthy</li>
                        <li>Be happy</li>
                        <li>Feel Whole</li>
                    </ul>
                </div>
                <div class="hobby_global clearfix">
                    <div class="field_area clearfix">
                        {!!Form::open(['method'=>'post','url'=>'/auth/login', 'id'=>'form-login','class'=>'contact_page','role'=>"form"])!!}
                            <div class="contact_sec clearfix">
                                <div class="contact_field_sec contact_page">
                                    {!! Form::text('email',Input::old('email'),['name'=>'email', 'id'=>'email','class'=>'form-control', 'required'=>'required', 'placeholder'=>'Email','autocomplete'=>'off']) !!}
                                    <!--<span>Please full fill the form</span>-->
                                </div>
                                <div class="contact_field_sec contact_page">
                                    {!! Form::password('password',['name'=>'password', 'id'=>'pass','class'=>'form-control', 'placeholder'=>'Password', 'required'=>'required', 'autocomplete'=>'off']) !!}
                                    <!--<span>Please full fill the form</span>-->
                                </div>
                                <div class="contact_field_sec contact_page log_sec log_sec1 clearfix">
                                    {!!Form::button('Login', array(
                                        'type' => 'submit',
                                        'class'=> 'sign_up',
                                        'onclick'=>'return true',
                                        'id'=>'login'
                                    ));!!}
                                </div>
                            </div>
                        {!!Form::close()!!}
                    </div>
                    <p class="error">
                    @if (Session::has('success_msg'))
                        @foreach (Session::get('success_msg') as $success)
                            {{ $success }}
                        @endforeach
                    @endif

                    @if($errors->any())
                        @foreach ($errors->all() as $error) 
                            {{$error}}
                        @endforeach
                    @endif
                    </p>
                    <h3 class="para">Are you not a member yet ? Join today. Its Free !</h3>
                    <div class="log_sec clearfix main-nav">
                        <a class="cd-signup sign_up" href="javascript:void(0)">Sign up</a>
                    </div>
                    <div class="forgot clearfix">
                        <a href="javascript:void(0)" class="cd-form-forgot_password">Forgot your password?</a>
                    </div>
                </div>    
             </div>
        </div>
    </div>
</div>
<div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
    <div class="cd-user-modal-container"> <!-- this is the container wrapper -->
        <!-- <ul class="cd-switcher">
            <li><a href="javascript:void(0)">Sign in</a></li>
            <li><a href="javascript:void(0)">New account</a></li>
        </ul> -->

        <div id="cd-login"> <!-- log in form -->
            {!!Form::open(['method'=>'post','url'=>'/auth/login', 'id'=>'form-login','class'=>'cd-form','role'=>"form"])!!}
                <p class="fieldset">
                    <label class="image-replace cd-email" for="signin-email">E-mail</label>
                    {!! Form::text('email',Input::old('email'),['name'=>'email', 'id'=>'signin-email','class'=>'full-width has-padding has-border', 'required'=>'required', 'placeholder'=>'E-mail','autocomplete'=>'off']) !!}
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-password" for="signin-password">Password</label>
                    {!! Form::password('password',['name'=>'password', 'id'=>'signin-password','class'=>'full-width has-padding has-border', 'placeholder'=>'Password', 'required'=>'required', 'autocomplete'=>'off']) !!}
                    <a href="javascript:void(0)" class="hide-password">Hide</a>
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <input type="checkbox" id="remember-me" checked>
                    <label for="remember-me">Remember me</label>
                </p>

                <p class="fieldset">
                    <input class="full-width" type="submit" value="Login">
                </p>
            {!!Form::close()!!}
            
            <p class="cd-form-bottom-message"><a href="javascript:void(0)" class="cd-form-forgot_password">Forgot your password?</a></p>
            <!-- <a href="javascript:void(0)" class="cd-close-form">Close</a> -->
        </div> <!-- cd-login -->

        <div id="cd-signup"> <!-- sign up form -->
            {!!Form::open(['method'=>'post','url'=>'/auth/signup', 'id'=>'form-signup','class'=>'cd-form','role'=>"form"])!!}
                <p class="fieldset">
                    <label class="image-replace cd-username" for="signup-username">Username</label>
                    {!! Form::text('name',null,['name'=>'name', 'id'=>'signup-username','class'=>'full-width has-padding has-border', 'required'=>'required', 'placeholder'=>'Name','autocomplete'=>'off']) !!}
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-email" for="signup-email">E-mail</label>
                    {!! Form::text('email',null,['name'=>'email', 'id'=>'signup-email','class'=>'full-width has-padding has-border', 'required'=>'required', 'placeholder'=>'E-mail','autocomplete'=>'off']) !!}
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-mobile" for="signup-phone">Phone No.</label>
                    {!! Form::text('phone',null,['name'=>'phone', 'id'=>'signup-phone','class'=>'full-width has-padding has-border', 'required'=>'required', 'placeholder'=>'Phone No','autocomplete'=>'off']) !!}
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-password" for="signup-password">Password</label>
                    {!! Form::password('password',['name'=>'password', 'id'=>'signup-password','class'=>'full-width has-padding has-border', 'placeholder'=>'Password', 'required'=>'required', 'autocomplete'=>'off']) !!}
                    <a href="javascript:void(0)" class="hide-password">Hide</a>
                    <span class="cd-error-message">Error message here!</span>
                </p>
                 
                 {!! app('captcha')->display(); !!}

                

                <p class="fieldset">
                    <input type="checkbox" id="accept-terms" name="accept_term">
                    <label for="accept-terms">I agree to the <a href="javascript:void(0)">Terms</a></label>
                </p>

                <p class="fieldset">
                    <input class="full-width has-padding" type="submit" value="Create account">
                </p>
            {!!Form::close()!!}

            <!-- <a href="javascript:void(0)" class="cd-close-form">Close</a> -->
        </div> <!-- cd-signup -->

        <div id="cd-reset-password"> <!-- reset password form -->
            <p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

            <form class="cd-form">
                <p class="fieldset">
                    <label class="image-replace cd-email" for="reset-email">E-mail</label>
                    <input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <input class="full-width has-padding" type="submit" value="Reset password">
                </p>
            </form>

            <p class="cd-form-bottom-message"><a href="javascript:void(0)">Back to log-in</a></p>
        </div> <!-- cd-reset-password -->
        <a href="javascript:void(0)" class="cd-close-form">Close</a>
    </div> <!-- cd-user-modal-container -->
</div> <!-- cd-user-modal -->
</section>

<section id="mid_body_section">
<!--Icon Section-->  
<section id="icon_sec" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div class="container">
            <div class="each_icon_sec wow fadeInRight clearfix" data-wow-duration="600ms" data-wow-delay="400ms">
                <div class="each_icon_sec_top">
                    <div class="icon_circle">
                    <img src="{{ Cdn::asset('frontend/welcome/assets/images/connect-icon.png') }}" alt="Connect Icon" title="Connect Icon"></div>
                    </div>
                <div class="hobby_global">
                    <h3>Connect</h3>
                    <p>Connect with the World people who pursue the same interests and hobbies as yours.</p>
                    </div>    
                </div>
            <div class="each_icon_sec clearfix">
                <div class="each_icon_sec_top">
                    <div class="icon_circle">
                    <img src="{{ Cdn::asset('frontend/welcome/assets/images/share-icon.png') }}" alt="Share Icon" title="Share Icon"></div>
                    </div>
                <div class="hobby_global">
                    <h3>Share</h3>
                    <p>Share your hobbies in different Hobby Groups &nbsp; Pages. Create your own Hobby Group &nbsp; Pages.</p>
                    </div>    
                </div>
            <div class="each_icon_sec clearfix">
                <div class="each_icon_sec_top">
                    <div class="icon_circle">
                    <img src="{{ Cdn::asset('frontend/welcome/assets/images/learn-icon.png') }}" alt="Learn Icon" title="Learn Icon"></div>
                    </div>
                <div class="hobby_global">
                    <h3>Learn</h3>
                    <p>Learn more about your hobbies from others and help others to learn from your experience.</p>
                    </div>    
                </div>
            <div class="each_icon_sec clearfix">
                <div class="each_icon_sec_top">
                    <div class="icon_circle">
                    <img src="{{ Cdn::asset('frontend/welcome/assets/images/invite-icon.png') }}" alt="Invite Icon" title="Invite Icon"></div>
                    </div>
                <div class="hobby_global">
                    <h3>Invite</h3>
                    <p>Invite your friends to be a part of this global platform on Hobbies and help them do something creative.</p>
                    </div>    
                </div>
            <div class="each_icon_sec clearfix">
                <div class="each_icon_sec_top">
                    <div class="icon_circle">
                    <img src="{{ Cdn::asset('frontend/welcome/assets/images/be-famous-icon.png') }}" alt="Be Famous Icon" title="Be Famous Icon"></div>
                    </div>
                <div class="hobby_global">
                    <h3>Be Famous</h3>
                    <p>Connect with the World people who pursue the same interests and hobbies as yours.</p>
                    </div>    
                </div>                
            </div>
        </section>
<!--what is hobby Section-->         
<section id="hobby_networking_sec">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <article class="hobby_networking_contt_area" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
                        <h1>What is Hobby Networking</h1>
                        <h3>A platform created by the Hobbyists, for the Hobbyists. What ever is your Hobby, this platform gives every individual on Earth an opportunity</h3>
                        <p>A platform created by the Hobbyists, for the Hobbyists. What ever is your Hobby, this platform gives every individual on Earth an opportunity to showcase what you have always loved to do, connect with other individuals from all corners of the world driven by the same passion, help you learn from others.</p>
                        <p>It is a medium for expressing the Hobby Networking members wide-ranging and eclectic interests, tastes and travels.</p>
                        <p>We look forward to create a happy world where all of us can pursue what we have always craved for.</p>
                    </article>
                    </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <article class="hobby_networking_img_area" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
                        <img src="{{ Cdn::asset('frontend/welcome/assets/images/what-is-hobby-image.jpg') }}" alt="what is hobby image" title="what is hobby image"></article>
                    </div>    
                </div>
            </div>
        </section>
<!--parallax Section-->         
<section class="bg1 parallax">
        <article class="parallax_contt_area">
            <div class="container">
                <div class="parallax_contt_sec">
                    <h1>Listen to your heart and dwell in exploring new Avenues of Happiness</h1>
                    <div class="bttn_sec">
                        <a href="#" >Click to Join</a></div>
                </div>
             </div>
        </article>
    </section>
<!--explore hobbies Section-->     
<section id="explore_hobbies_sec" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div class="container">
            <div class="hobby_global">
                <h1>Explore your hobbies</h1>
             </div>
            <div class="explore_hobbies_bttn_sec">
                @foreach($welcomePageHobbies as $eachHobby)
                <a href="{{route('manage_hobby_by_name',$eachHobby->slug())}}">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ $eachHobby->profile_pic }}" alt="{{ $eachHobby->name }}" title="{{ $eachHobby->name }}" width="64px" height="37px">
                       </div>
                    <h3>{{ $eachHobby->name }}</h3>
                    </div>
                    </a>
                @endforeach
                {{-- <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/dancing-icon.png') }}" alt="Dancing Icon" title="Dancing Icon">
                       </div>
                    <h3>Dancing</h3>
                    </div>
                    </a>
                <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/photography-icon.png') }}" alt="Photography Icon" title="Photography Icon">
                       </div>
                    <h3>Photography</h3>
                    </div>
                    </a>
                <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/gardening-icon.png') }}" alt="Gardening Icon" title="Gardening Icon">
                       </div>
                    <h3>Gardening</h3>
                    </div>
                    </a>
                <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/tracking-icon.png') }}" alt="Tracking Icon" title="Tracking Icon">
                       </div>
                    <h3>Gardening</h3>
                    </div>
                    </a>
                <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/writing-icon.png') }}" alt="Writing Icon" title="Writing Icon">
                       </div>
                    <h3>Writing</h3>
                    </div>
                    </a>
                <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/football-icon.png') }}" alt="Football Icon" title="Football Icon">
                       </div>
                    <h3>Football</h3>
                    </div>
                    </a>
                <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/golfing-icon.png') }}" alt="Golfing Icon" title="Golfing Icon">
                       </div>
                    <h3>Golfing</h3>
                    </div>
                    </a>
                <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/singing-icon.png') }}" alt="Singing Icon" title="Singing Icon">
                       </div>
                    <h3>Singing</h3>
                    </div>
                    </a>
                <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/driving-icon.png') }}" alt="Driving Icon" title="Driving Icon">
                       </div>
                    <h3>Singing</h3>
                    </div>
                    </a>
                <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/traveling-icon.png') }}" alt="Traveling Icon" title="Traveling Icon">
                       </div>
                    <h3>Traveling</h3>
                    </div>
                    </a>
                <a href="javascript:void(0);">
                    <div class="explore_hobbies_box clearfix">
                        <div class="explore_hobbies_icon_box">
                            <img src="{{ Cdn::asset('frontend/welcome/assets/images/book-worm-icon.png') }}" alt="Book Worm Icon" title="Book Worm Icon">
                       </div>
                    <h3>Book Worm</h3>
                    </div>
                    </a>    --}}                                         
                    </div> 
            </div>
        </section>
<!--work-through Section-->         
<section id="work_through_sec">
        <div class="container">
            <div class="hobby_global">
                <h1>Hobby Networking Walk Through - How It works</h1>
                </div>
            <div class="video_sec">
                <?php /*<iframe src="https://player.vimeo.com/video/131680520?autoplay=1&color=ffffff&title=0&byline=0&portrait=0" width="724" height="420" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> 
                <object width="WIDTH" height="HEIGHT">
                     <param name="allowfullscreen" value="true" />
                     <param name="allowscriptaccess" value="always" />
                     <param name="movie" value="https://vimeo.com/moogaloop.swf?clip_id=131680520&amp;server=vimeo.com&amp;color=00adef&amp;fullscreen=1&amp;autoplay=1" />
                     <embed src="https://vimeo.com/moogaloop.swf?clip_id=131680520&amp;server=vimeo.com&amp;color=00adef&amp;fullscreen=1&amp;autoplay=1"
                     type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="724" height="420"></embed>
                </object>*/ ?>
                    <!-- Vimeo Player -->
                    <iframe id="myvideo" src="//player.vimeo.com/video/131680520?background=1" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    <br>
                    <span id="play-pause"></span>
                    <span id="mute-unmute"></span>
            </div>
            <div class="video_bttn">
                <a href="#">Watch more videos of hobby networking</a></div>    
            </div>
        </section>

<!--h-mag Section-->  
<?php /*       
<section id="h_mag" data-uk-scrollspy="{cls:'uk-animation-slide-bottom', repeat: true}">
    <div class="container">
            <div class="hobby_global">
                <h1 class="wow fadeInUp" data-wow-duration="600ms" data-wow-delay="1000ms">H-MAG</h1>
                <h3>The story behind H- Mag</h3>
             </div>
            <div class="hobby_global">
                <p>The word "Magazine" is derived from Arabic word "makhazin", or "storehouse." It generally means a collection of somehow-related stuff all bundled together in one package.</p>
                <p>H- Mag, Hobby Networking's online magazine, was born to display its Members interesting, unusual and occasionally eccentric Hobbies or Interests. We assure you they are full of wondrous things, from artefacts to unique creations, adventures to simple wishes. You are treated to a fascinating, mind-expanding, unique set of wonders.</p>
                <p>Isn’t that what a magazine should do?</p>
                </div>
            <div class="hobby_global">
                <div class="project-wrap">
                    <div class="item project-item cloud_app">
                        <div class="item-wrap">
                            <div class="project-thumb">
                                 <img src="{{ Cdn::asset('frontend/welcome/assets/images/h-mag-image1.png') }}" alt="H-mag Image" title="H-mag Image">
                                 <div class="opacity"></div>
                                 <div class="detail">
                                    <div class="detail_table">
                                        <div class="detail_cell">
                                             <a href="#">
                <div class="download_icon_circle">
                    <i class="fa fa-download download_icon"></i>
                    </div>
            </a>
                                             <p class="link_title">Vis possit aliquam</p>
                                        </div>
                                    </div>
                                 </div>
                                  </div>
                       </div>
                    </div>
                    <div class="item project-item cloud_app">
                        <div class="item-wrap">
                            <div class="project-thumb">
                                 <img src="{{ Cdn::asset('frontend/welcome/assets/images/h-mag-image2.png') }}" alt="H-mag Image" title="H-mag Image">
                                 <div class="opacity"></div>
                                 <div class="detail">
                                    <div class="detail_table">
                                        <div class="detail_cell">
                                             <a href="#">
                <div class="download_icon_circle">
                    <i class="fa fa-download download_icon"></i>
                    </div>
            </a>
                                             <p class="link_title">Vis possit aliquam</p>
                                        </div>
                                    </div>
                                 </div>
                                  </div>
                       </div>
                    </div>
                    <div class="item project-item cloud_app">
                        <div class="item-wrap">
                            <div class="project-thumb">
                                 <img src="{{ Cdn::asset('frontend/welcome/assets/images/h-mag-image3.png') }}" alt="H-mag Image" title="H-mag Image">
                                 <div class="opacity"></div>
                                 <div class="detail">
                                    <div class="detail_table">
                                        <div class="detail_cell">
                                             <a href="#">
                <div class="download_icon_circle">
                    <i class="fa fa-download download_icon"></i>
                    </div>
            </a>
                                             <p class="link_title">Vis possit aliquam</p>
                                        </div>
                                    </div>
                                 </div>
                                  </div>
                       </div>
                    </div>
                    <div class="item project-item cloud_app">
                        <div class="item-wrap">
                            <div class="project-thumb">
                                 <img src="{{ Cdn::asset('frontend/welcome/assets/images/h-mag-image4.png') }}" alt="H-mag Image" title="H-mag Image">
                                 <div class="opacity"></div>
                                 <div class="detail">
                                    <div class="detail_table">
                                        <div class="detail_cell">
                                             <a href="#">
                <div class="download_icon_circle">
                    <i class="fa fa-download download_icon"></i>
                    </div>
            </a>
                                             <p class="link_title">Vis possit aliquam</p>
                                        </div>
                                    </div>
                                 </div>
                                  </div>
                       </div>
                    </div>
                </div>
           </div>     
        </div>
</section>
*/ ?>

<!--footer Section-->  
<footer>
        <div class="container">
            <div class="footer_top">
                <a href="#"><img src="{{ Cdn::asset('frontend/welcome/assets/images/footer-hobby-networking-logo.png') }}" alt="Hobby Networking Logo" title="Hobby Networking Logo"></a></div>
            <div class="footer_bttn">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Group</a></li>
                    <li><a href="{{route('search_slug','friends')}}">Search Friends</a></li>
                    <li><a href="{{route('search_slug','hobby')}}">Search Hobbies</a></li>
                    <li><a href="{{route('search_slug','community')}}">Search Community</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms of Use</a></li>
                    <li><a href="#">Contact Us</a></li>
                    </ul>
                <p>© 2015 Hobby Networking All rights reserved</p>    
                </div>    
            </div>
        </footer>
</div>

<!--bootstrap Js-->
<script type="text/javascript" src="{{ Cdn::asset('frontend/welcome/assets/js/jquery-1.11.3.min.js') }}"></script>

<script src="{{ Cdn::asset('frontend/welcome/assets/js/bootstrap.js') }}"></script>

<!--animation Js-->
<script type="text/javascript" src="{{ Cdn::asset('frontend/welcome/assets/js/uikit.min.js') }}"></script>
<script src="{{ Cdn::asset('frontend/welcome/assets/js/main.js') }}"></script>
<script src="{{ Cdn::asset('frontend/welcome/assets/js/jquery.vimeo.api.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
          $('#mute-unmute').hide();
          var video = $("#myvideo");
          video.vimeo("play");
          video.vimeo("setVolume", 0);

            //toggle play/pause
          $('#play-pause').click(function() {
            $(this).toggleClass('play');
            if ($(this).hasClass('play')) {
              //pause video
              video.vimeo("pause");
              $(this).css('background', 'url("{{ Cdn::asset('frontend/welcome/assets/img/play.png') }}") no-repeat');
            } else {
              //unpause video
              video.vimeo("play");
              $(this).css('background', 'url("{{ Cdn::asset('frontend/welcome/assets/img/pause.png') }}") no-repeat');
            }
          });
            
          //toggle mute/unmute
          $('#mute-unmute').click(function() {
            $(this).toggleClass('mute');
            if ($(this).hasClass('mute')) {
              //unmute video
                video.vimeo("setVolume", 1);
                $(this).css('background', 'url("{{ Cdn::asset('frontend/welcome/assets/img/volume.png') }}") no-repeat');
            } else {
              //mute video
                video.vimeo("setVolume", 0);
                $(this).css('background', 'url("{{ Cdn::asset('frontend/welcome/assets/img/mute.png') }}") no-repeat');
            }
          });

        video.on('mouseenter', function() { 
            console.log("Enter");
            if ($('#mute-unmute').hasClass('mute')) {
                //console.log("Mute");
            }else{
                //console.log("Unmute"); 
            }
        });

        video.on('mouseleave', function() { 
            console.log("Leave");
        });

        $(".video_sec").on('mouseenter',function(){
            $('#mute-unmute').show();
        });

        $(".video_sec").on('mouseleave',function(){
            $('#mute-unmute').hide();
        });
    });
</script>
</body>
</html>