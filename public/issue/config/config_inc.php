<?php
$g_hostname               = 'localhost';
$g_db_type                = 'mysqli';
$g_database_name          = 'mantis_issue';
$g_db_username            = 'mantis_issue';
$g_db_password            = 'mantis_issue';

$g_default_timezone       = 'UTC';

$g_crypto_master_salt     = 'eF4FXMsP22PdcvKTPzDqHk52qLRBVVx94jQjvycxHT4=';
$g_antispam_max_event_count = 0;
