function postCommentAdd(post_id, comment, user_id,token,url) {
	if (comment!="") {
        $.ajax({
            url: url, 
            type: "POST",
            data: {
                'post_id':post_id,
                'user_id':user_id,
                'comment':comment,
                '_token':token
            },
            success: function(result){
                //console.log(result.su);
                if(result.success){
                    var commentBoxClass = "add_comment_"+post_id;
                    var commentBox = $('.'+commentBoxClass);
                    commentBox.val('');
                    //console.log(result.comments);
                    $('.comments_list_'+post_id).html(result.comments);
                }else{
                    location.reload();
                }
            }
        });
    }
}
$(document).ready(function(e){
	/*
    * Add comment Box Post Start
    */
    $('.add_comment_submit').click(function(event){
        var commentBoxClass = "add_comment_"+$(this).data('post_id');
        var commentBox = $('.'+commentBoxClass);
        addCommentPost($(this).data('post_id'),commentBox.val());
    });
    $(".add_comment").keyup(function(event){
        if(event.keyCode == 13){
            addCommentPost($(this).data('post_id'), $(this).val());
        }
    });
    /*
    * Add comment Box Post End
    */
});