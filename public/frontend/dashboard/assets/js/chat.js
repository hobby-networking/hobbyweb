$(document).on('click', '.panel-heading span.icon_minim', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).on('focus', '.panel-footer input.chat_input', function (e) {
    var $this = $(this);
    if ($('#minim_chat_window').hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideDown();
        $('#minim_chat_window').removeClass('panel-collapsed');
        $('#minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).on('click', '#new_chat', function (e) {
    var messages = '<div class="row msg_container base_sent">'+
                        '<div class="col-md-10 col-xs-10">'+
                            '<div class="messages msg_sent">'+
                                '<p>that mongodb thing looks good, huh?'+
                                'tiny master db, and huge document store</p>'+
                                '<time datetime="2009-11-13T20:00">Timothy • 51 min</time>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-2 col-xs-2 avatar">'+
                            '<img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">'+
                        '</div>'+
                    '</div>'+
                    '<div class="row msg_container base_receive">'+
                        '<div class="col-md-2 col-xs-2 avatar">'+
                           '<img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">'+
                        '</div>'+
                        '<div class="col-md-10 col-xs-10">'+
                            '<div class="messages msg_receive">'+
                                '<p>that mongodb thing looks good, huh?'+
                                'tiny master db, and huge document store</p>'+
                                '<time datetime="2009-11-13T20:00">Timothy • 51 min</time>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
    openChatBox("Chat",messages,0);
});
function openChatBox(title,messages,receiver) {
    if($('.chat-window[data-receiver="'+receiver+'"]').length<1){
        var lastChild = $( ".chat-window:last-child" );
        var lastId = $( ".chat-window:last-child" ).attr('id').replace('chat_window_','');
        var size = lastChild.css("right");
        size_total = parseInt(size) + 285;
        if(lastId == 0){
            size_total = 100;
        }
        if(parseInt(lastId)+1 <= 4) {
            var clone = $( ".chat-window:last-child" ).clone().appendTo( ".chat_container" );
            clone.css("right", size_total);
            clone.attr('id','chat_window_'+(parseInt(lastId)+1));
            clone.attr('data-receiver',receiver);
            clone.find('.chat_title').html(title);
            clone.find('.chat_input').attr('data-receiver_id',receiver);
            clone.find('.msg_container_base').html(messages);
            clone.removeClass('hide');
            var $t = $(".chat-window[data-receiver='"+receiver+"']").find('.msg_container_base');
            $t.animate({"scrollTop": $t[0].scrollHeight}, "slow");
        }
    }else{
        var chatbox = $('.chat-window[data-receiver="'+receiver+'"]');
        chatbox.find('.chat_title').html(title);
        chatbox.find('.msg_container_base').html(messages);
        var $t = $(".chat-window[data-receiver='"+receiver+"']").find('.msg_container_base');
        $t.animate({"scrollTop": $t[0].scrollHeight}, "slow");
    }
}
$(document).on('click', '.icon_close', function (e) {
    //$(this).parent().parent().parent().parent().remove();
    var receiverIdd = $(this).closest('.chat-window').attr('data-receiver');
    console.log('receiverId',receiverIdd);
    $(this).closest('.chat-window').remove();
    var data = JSON.parse($.cookie("openchat"));
    data.items = data.items.filter(function(item) { 
       return item.receiverId != receiverIdd;  
    });
    //console.log(data);
    $.removeCookie('openchat');
    $.cookie("openchat",JSON.stringify(data));
});
