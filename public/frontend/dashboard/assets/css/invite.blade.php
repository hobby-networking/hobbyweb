@extends('frontend.dashboard.template')

@section('extra_css')
<style type="text/css">
#hobbies_grid {
    max-height: 550px;
    overflow-y: scroll;
}
.btn{

    width: 50%;
    margin-top: 17px;
    margin-left: 25%;
    padding: 12px 116px;
}
#hobbies_grid{
    overflow-y:hidden;
}
</style>
@stop

@section('content')
<div class="container-fluid xyz">
                <div class="row2">
                    <div class="col-lg-6 mdlBlock invite-page">
                        <h1>Invite Your Friends</h1>
                        
                        <div class="col-lg-12">
                          <div class="row">
                          
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 text-left">
                            @if($withbtn)
                                <p><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i><a href="{{ route('choosehobby_page') }}" onclick="">Back</a></p>
                            @else
                                <p><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i><a href="{{ route('choosehobby_page') }}" onclick="">Select Your Hobby</a></p>
                            @endif
                            </div>
                          
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right">
                                <p><a href="{{ route('welcome_page') }}" onclick="">next<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a></p>
                            </div>
                          </div>
                        </div>
                        

                        <div class="col-lg-12">
                            <div class="row invite_hobbies_grid" id="hobbies_grid">
                                  <div id="fb-root"></div>
                                  <button onclick="InviteF()" class="btn btn-large btn-fb"><i class="fa fa-facebook" aria-hidden="true"></i>INVITE VIA FACEBOOK</button>
                                  <div class="clearfix"></div>
                                  <button onclick="gPlus()" class="btn btn-large btn-fb gle-btn"><i class="fa fa-google" aria-hidden="true"></i>INVITE VIA GOOGLE</button>
                                  <div class="clearfix"></div>
                                  <button onclick="linkedIn()" class="btn btn-large btn-fb linkdin-btn"><i class="fa fa-linkedin" aria-hidden="true"></i>INVITE VIA LINKEDIN</button>
                                  <div class="clearfix"></div>
                                  <button onclick="twitter()" class="btn btn-large btn-fb twtr-btn"><i class="fa fa-twitter" aria-hidden="true"></i> INVITE VIA TIWTEER</button>
                                  <div class="clearfix"></div>
                                  <button onclick="mail()" class="btn btn-large btn-fb hidden">INVITE VIA EMAIL</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
@stop

@section('extra_js')
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>
var url = 'http://dev.hobbynetworking.com';
var description= 'Welcome to Hobby Networking. Click here to signup';
var title= 'Welcome to hobbynetworking';
FB.init({
  appId:'515326071876163',
  cookie:true,
  status:true,
  xfbml:true,
  redirect_uri: url, 
});

function InviteF()
{
  FB.ui({
    method: 'feed',
    link: url,
    caption: title,
    picture: 'https://scontent.fmaa1-1.fna.fbcdn.net/t31.0-8/12471394_547065292117403_5793609828223003075_o.jpg',
    mobile_iframe: 'true',
    display: 'popup',
    description: description,
  }, function(response){
    console.log(response);
  });
}
function gPlus(){
    window.open(
        'https://plus.google.com/share?url='+url,
        'popupwindow',
        'scrollbars=yes,width=800,height=400'
    ).focus();
    return false;
}
function linkedIn(){
    window.open(
        'https://www.linkedin.com/shareArticle?mini=true&url='+url+'&title='+title+'&summary='+description+'&source=hobbynetworking',
        'popupwindow',
        'scrollbars=yes,width=800,height=400'
    ).focus();
    return false;
}
function twitter(){
    window.open(
        'http://twitter.com/share?text='+description+'&url='+url+'&hashtags=hobbynetworking',
        'popupwindow',
        'scrollbars=yes,width=800,height=400'
    ).focus();
    return false;
}
function mail(){
    parser=new DOMParser();
    txt = '<a href='+url+'>'+description+'</a>';
    htmlDoc=parser.parseFromString(txt, "text/html");
    window.open(
        'mailto:?subject='+title+'&body='+txt,
        'popupwindow',
        'scrollbars=yes,width=800,height=400'
    ).focus();
    return false;
}
</script>
<script type='text/javascript'>
if (top.location!= self.location)
{
top.location = self.location
}
</script>
@stop